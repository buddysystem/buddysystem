# BuddySystem v5

This repository is the open source [Symfony](https://symfony.com/) application that made the [buddysystem.eu](https://buddysystem.eu) website. 

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

## Requirement

- [Docker](https://www.docker.com/)
    - docker network create proxy

- [NodeJS](https://nodejs.org/en/download/)
    - Yarn: npm install -g yarn

## Installation

Clone the project from [gitlab](https://gitlab.com/buddysystem/buddysystem) :

```shell
git clone https://gitlab.com/buddysystem/buddysystem.git
```

Create a .env.local file (modify it if necessary)
```shell
cd buddysystem
cp .env .env.local
Set APP_ENV=dev
```

Create a docker-compose.override.yml file (modify it if necessary)
```shell
cp docker-compose.override.yml.dist docker-compose.override.yml

```

Let's make do the rest

```shell
make start
```

Add buddysystem.local to your hosts
```shell
127.0.0.1 buddysystem.local
```

The BuddySystem is now accessible at [https://buddysystem.local](https://buddysystem.local)


## Usage

#### Fixtures 

The project came with fixtures, you can load them with :

```
make db-load
```

#### Login

You can now login with the following credentials :  

* admin@buddysystem.eu / admin  
* buddycoordinator@buddysystem.eu / buddycoordinator
* ri@buddysystem.eu / ri
* international@buddysystem.eu / international
* local@buddysystem.eu / local  

#### Make

You can use have a complete list of make commands by typing:

```
make help
```

### Dependencies

Docker is based on the version 3 of docker-compose, and is build with the following containers: 

* apache => debian:stretch
* mysql => mariadb:10.3.18
* php => php:7.4-fpm-buster
* phpmyadmin => phpmyadmin/phpmyadmin
* maildev => maildev/maildev

all of these container, except php, are using the latest releases of the image they are based on.
