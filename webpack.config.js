const Encore = require('@symfony/webpack-encore');
const DotenvFlow = require('dotenv-flow-webpack');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableIntegrityHashes(Encore.isProduction())
    .enableSingleRuntimeChunk()
    .splitEntryChunks()
    .enableSassLoader()
    .enableReactPreset()
    .enableTypeScriptLoader()
    .configureCssLoader((options) => {
        delete options.localIdentName;
    })
    .addPlugin(new DotenvFlow({ system_vars: true, silent: true }))
    .autoProvideVariables({
        'bazinga-translator': 'Translator'
    })
    .addEntry('main', './assets/js/main.js')
    .addEntry('landing_page', './assets/js/landing_page.js')
    .addEntry('homepage', './assets/js/homepage.js')
    .addEntry('project', './assets/js/project.js')
    .addEntry('partners', './assets/js/partners.js')
    .addEntry('institutions', './assets/js/institutions.js')
    .addEntry('profile', './assets/js/profile.js')
    .addEntry('dashboard', './assets/js/dashboard.js')
    .addEntry('dashboard_insight', './assets/js/dashboard_insight.js')
    .addEntry('matching', './assets/js/matching.js')
    .addEntry('matching_manager', './assets/js/matching_manager.js')
    .addEntry('matching_summary', './assets/js/matching_summary.js')
    .addEntry('register', './assets/js/register.js')
    .addEntry('hobby', './assets/js/hobby.js')
    .addEntry('buddy', './assets/js/buddy.js')
    .addEntry('motivation', './assets/js/motivation.js')
    .addEntry('institute', './assets/js/institute.js')
    .addEntry('institute_association_link', './assets/js/institute_association_link.js')
    .addEntry('institute_delegate_rights', './assets/js/institute_delegate_rights.js')
    .addEntry('user', './assets/js/user.js')
    .addEntry('association', './assets/js/association.js')
    .addEntry('city', './assets/js/city.js')
    .addEntry('study', './assets/js/study.js')
    .addEntry('stats', './assets/js/stats.js')
    .addEntry('user_approval', './assets/js/user_approval.js')
    .addEntry('consent', './assets/js/consent.js')
    .addEntry('institute_requested', './assets/js/institute_requested.js')
    .addEntry('toolbox', './assets/js/toolbox.js')
    .addEntry('extract', './assets/js/extract.js')
    .addEntry('messaging', './assets/js/messaging/index.tsx');
module.exports = Encore.getWebpackConfig();
