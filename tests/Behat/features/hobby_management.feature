Feature: Hobby
  As an authenticated user
  I should have a page to manage hobbies

  Scenario: As a non-manager, I can not access to the list of hobbies
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/hobbies"
    Then the response status code should be 403

  Scenario Outline: As a non-admin, I can not edit a hobby
    Given I am logged as "<email>" with "<password>"
    When I go to "en/admin/hobbies/1/edit"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a non-admin, I can not create a new hobby
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/hobbies/new"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a manager, I can access to the list of hobbies
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "Hobbies"
    Then I should be on "/en/admin/hobbies"
    And the response status code should be 200
    And the ".white-box" element should contain "Hobbies List"
    And the ".white-box" element should not contain "Create a new Hobby"
    And the "#hobbies" element should not contain "Edit"
    And the "#hobbies" element should not contain "Delete"
    Examples:
    | email                           | password         |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario: As an admin, I can access to the list of hobbies
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    And I follow "Hobbies"
    Then I should be on "/en/admin/hobbies"
    And the response status code should be 200
    And the ".white-box" element should contain "Hobbies List"
    And the ".white-box" element should contain "Create a new Hobby"
    And the "#hobbies" element should contain "Edit"
    And the "#hobbies" element should contain "Delete"

  Scenario: As an admin, I can edit a hobby
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/hobbies/1/edit"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit hobby"

  Scenario: As a admin, I can create a new hobby
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/hobbies/new"
    Then the response status code should be 200
    And the ".white-box" element should contain "Create hobby"
