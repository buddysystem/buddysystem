Feature: Profile
  As an authenticated user
  I should have access to a profile page

  Scenario Outline: As a user, I should have access to a profile page
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "My profile"
    Then I should be on "/en/profile/edit"
    And the response status code should be 200
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | local@buddysystem.eu            | local            |
    | international@buddysystem.eu    | international    |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a user with edit other permission, I should not see the export profile button
  when I'm editing a user profile
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/users/3/edit"
    And the response status code should be 200
    And I should not see "Export profile"
    Examples:
      | email                           | password         |
      | admin@buddysystem.eu            | admin            |
      | buddycoordinator@buddysystem.eu | buddycoordinator |
      | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a user, I should see the export profile button when I'm editing my profile
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/profile/edit"
    And the response status code should be 200
    And I should see "Export profile"
    Examples:
      | email                           | password         |
      | admin@buddysystem.eu            | admin            |
      | buddycoordinator@buddysystem.eu | buddycoordinator |
      | local@buddysystem.eu            | local            |
      | international@buddysystem.eu    | international    |
      | ri@buddysystem.eu               | ri               |

  @javascript
  Scenario: As a user who has not consent to the privacy policy, I should be able to delete my profile on the consent page
    When I am logged as "localWithoutConsent@buddysystem.eu" with "secret"
    Then I should be on "/en/profile/consent"
    And I should see "I agree that the data filled in this form will be used by the BuddySystem to find a person with a profile as similar as possible"
    And I should see an "input.btn-success" element
    And I should see an "#user_delete" element
    And I press "user_delete"
    And I wait until I see "Delete"

  @javascript
  Scenario: As a user who has not consent to the privacy policy, I should be redirected to the consent page
    When I am logged as "localWithoutConsent@buddysystem.eu" with "secret"
    Then I should be on "/en/profile/consent"
    And I should see "I agree that the data filled in this form will be used by the BuddySystem to find a person with a profile as similar as possible"
    And I should see an "input.btn-success" element
    And I should see an "#user_delete" element
    And I press "I Agree"
    And I should be on "/en/profile/consent_charter"

  @javascript
  Scenario: As a user who has not consent to the charter, I should be redirected to the consent charter page
    When I am logged as "localWithoutConsent@buddysystem.eu" with "secret"
    Then I should be on "/en/profile/consent_charter"
    And I should see "charter of commitment to the proper use of the buddy system platform"
    And I should see "1. Preamble"
    And I press "Next"
    Then I should be on "/en/profile/consent_charter/2"
    And I should see "2. Acceptance of the user charter"
    And I press "Next"
    Then I should be on "/en/profile/consent_charter/3"
    And I should see "3. User responsibility"
    And I press "Next"
    Then I should be on "/en/profile/consent_charter/4"
    And I should see "4. Responsibility of the platform"
    And I should see an "input.btn-success" element
    And I should see an "#user_delete" element
    And I press "I Agree"
    And I should be on "/en/profile/edit"
