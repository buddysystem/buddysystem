Feature: Login
  As a visitor,
  I should be able to login

    @javascript
    Scenario: The login popup is well loaded
      Given I am on "/en/"
      When I press "Log in"
      Then I wait for "#login_form" element
      And I should see an "#email" element
      And I should see an "#password" element

    Scenario: As a visitor, I can login using valid identifiers
      Given I am on "/en/login"
      When I fill in "email" with "admin@buddysystem.eu"
      And I fill in "password" with "admin"
      And I press "_submit"
      Then I should be on "/en/dashboard"
      And the response status code should be 200

    Scenario: As a visitor, I can not login using invalid identifiers
      Given I am on "/en/login"
      When I fill in "email" with "admin@buddysystem.eu"
      And I fill in "password" with "bad_password"
      And I press "_submit"
      Then I should be on "/en/login_fail"
      And the response status code should be 200
