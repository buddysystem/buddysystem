Feature: Matching
  As an authenticated user
  I should have a page to match users

  Scenario Outline: As a non-manager, I can not access to the matching page
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/matching"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | international@buddysystem.eu    | international |

  Scenario Outline: As a manager, I can access to the matching page
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "Matching"
    Then I should be on "/en/admin/matching"
    And the response status code should be 200
    And the ".white-box" element should contain "International students"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | ri@buddysystem.eu               | ri               |
