Feature: Home
  As a visitor,
  I should have a menu to navigate through the website

  Scenario: As a visitor, I can access the homepage
    When I go to "/en/"
    Then the "#navbarSupportedContent" element should contain "Home"
    And the "#navbarSupportedContent" element should contain "Institutions"
    And the "#navbarSupportedContent" element should contain "Project"
    And the "#navbarSupportedContent" element should contain "Our partners"
    And the "#navbarSupportedContent" element should contain "Log in"
    And the "#navbarSupportedContent" element should contain "Sign up"
    And the "#navbarSupportedContent" element should contain "ENGLISH"

  Scenario: As a visitor, I can go to the homepage
    Given I am on "/en/"
    When I follow "Home"
    Then I should be on "/en/"
    And the response status code should be 200

  Scenario: As a visitor, I can go to the project page
    Given I am on "/en/"
    When I follow "Institutions"
    Then I should be on "/en/institutions"
    And the response status code should be 200

  Scenario: As a visitor, I can go to the project page
    Given I am on "/en/"
    When I follow "Project"
    Then I should be on "/en/the-project"
    And the response status code should be 200

  Scenario: As a visitor, I can go to the partners page
    Given I am on "/en/"
    When I follow "Our partners"
    Then I should be on "/en/our-partners"
    And the response status code should be 200

  Scenario: As a visitor, I can go to the privacy policy page
    Given I am on "/en/"
    When I follow "Privacy Policy"
    Then I should be on "/en/privacy"
    And the response status code should be 200

  Scenario: As a visitor, I can go to the terms and conditions page
    Given I am on "/en/"
    When I follow "Terms and conditions"
    Then I should be on "/en/terms-of-use"
    And the response status code should be 200

  @javascript
  Scenario: As a visitor, I must see the cookie policy banner
    Given I am on "/en"
    And I wait 1 second
    Then the ".cc-message" element should be visible
    And I should see "This website uses cookies to ensure you get the best experience."
    And I should see "Got it!"
    When I press "Got it!"
    And I wait 1 second
    Then the ".cc-message" element should not be visible
