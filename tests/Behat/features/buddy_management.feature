Feature: Buddy
  As an authenticated user
  I should have a page to manage buddies

  Scenario: As a non-manager, I can not access to the list of buddies
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/buddies"
    Then the response status code should be 403

  Scenario: As a non-manager, I can not show a buddy
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/buddies/1/show"
    Then the response status code should be 403

  Scenario Outline: As a manager, I can access to the list of buddies
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "Buddies"
    Then I should be on "/en/admin/buddies"
    And the response status code should be 200
    And the ".white-box" element should contain "Buddies list"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a manager, I can show a buddy
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/buddies/2/show"
    Then the response status code should be 200
    And the ".white-box .text-right" element should contain "INTERNATIONAL STUDENT"
    And the ".white-box .text-left" element should contain "LOCAL STUDENT"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |
