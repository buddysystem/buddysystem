Feature: Register
  As a visitor,
  I should be able to register

  @javascript
  Scenario: As a visitor, I can sign-up using valid identifiers
    Given I am on "/en/register"
    And I fill in "registration_email" with "test@test.com"
    And I fill in "registration_plainPassword_first" with "testtest"
    And I fill in "registration_plainPassword_second" with "testtest"
    And I check the "registration_isLocal_0" label of button
    And I fill in "registration_firstName" with "test"
    And I fill in "registration_lastName" with "test"
    And I select "Oct" from "registration_dob_month"
    And I select "8" from "registration_dob_day"
    And I select "1990" from "registration_dob_year"
    And I select "Male" from "registration_sex"
    And I fill in select2 input "registration[languages][]" with "French" and select "French"
    And I select "France" from "registration_country"
    And I wait until I see 'Select a city of'
    And I select "Le Havre" from "registration_city"
    And I wait until I see 'Select an institute of'
    And I select "Université Le Havre" from "registration_institute"
    And I wait until I see 'Site de Caucriauville'
    And I select "Site de Caucriauville" from "registration_institute"
    And I wait until I see 'Institut Universitaire de Technologie'
    And I select "Institut Universitaire de Technologie" from "registration_institute"
    And I wait until I see 'semester' in the '#registration_semester' element
    And I select "Alumni" from "registration_levelOfStudy"
    And I scroll "#footer" into view
    And I wait 1 second
    And I check the "registration_privacyAccepted" label of button
    And I check the "registration_termsOfUse" label of button
    And I should see "Your email is not matching with the domain(s) approved by your institute" in the ".domain_warning" element
    And I press "_submit"
    And I wait 1 second
    Then I should be on "/en/"
    And I should see "An email has been sent. It contains an activation link you must click to activate your account." in the ".flashbag" element

  @javascript
  Scenario: As a visitor, I can not sign-up using already used email
    Given I am on "/en/register"
    When I fill in "registration_email" with "admin@buddysystem.eu"
    And I fill in "registration_plainPassword_first" with "adminadmin"
    And I fill in "registration_plainPassword_second" with "adminadmin"
    And I check the "registration_isLocal_0" label of button
    And I fill in "registration_firstName" with "admin"
    And I fill in "registration_lastName" with "admin"
    And I select "Oct" from "registration_dob_month"
    And I select "8" from "registration_dob_day"
    And I select "1990" from "registration_dob_year"
    And I select "Male" from "registration_sex"
    And I fill in select2 input "registration[languages][]" with "English" and select "English"
    And I select "France" from "registration_country"
    And I wait until I see 'Select a city of'
    And I select "Le Havre" from "registration_city"
    And I wait until I see 'Select an institute of'
    And I select "Université Le Havre" from "registration_institute"
    And I wait until I see 'Site de Caucriauville'
    And I select "Site de Caucriauville" from "registration_institute"
    And I wait until I see 'Institut Universitaire de Technologie'
    And I select "Institut Universitaire de Technologie" from "registration_institute"
    And I wait until I see 'semester' in the '#registration_semester' element
    And I select "Alumni" from "registration_levelOfStudy"
    And I scroll "#footer" into view
    And I wait 1 second
    And I check the "registration_privacyAccepted" label of button
    And I check the "registration_termsOfUse" label of button
    And I press "_submit"
    Then I should be on "/en/register"
    And I should see "There are some mistakes in your registration" in the ".flashbag" element

  @javascript
  Scenario: As a visitor, I can not sign-up using a disposable email
    Given I am on "/en/register"
    When I fill in "registration_email" with "test@yopmail.com"
    And I fill in "registration_plainPassword_first" with "adminadmin"
    And I fill in "registration_plainPassword_second" with "adminadmin"
    And I check the "registration_isLocal_0" label of button
    And I fill in "registration_firstName" with "admin"
    And I fill in "registration_lastName" with "admin"
    And I select "Oct" from "registration_dob_month"
    And I select "8" from "registration_dob_day"
    And I select "1990" from "registration_dob_year"
    And I select "Male" from "registration_sex"
    And I fill in select2 input "registration[languages][]" with "English" and select "English"
    And I select "France" from "registration_country"
    And I wait until I see 'Select a city of'
    And I select "Le Havre" from "registration_city"
    And I wait until I see 'Select an institute of'
    And I select "Université Le Havre" from "registration_institute"
    And I wait until I see 'Site de Caucriauville'
    And I select "Site de Caucriauville" from "registration_institute"
    And I wait until I see 'Institut Universitaire de Technologie'
    And I select "Institut Universitaire de Technologie" from "registration_institute"
    And I wait until I see 'semester' in the '#registration_semester' element
    And I select "Alumni" from "registration_levelOfStudy"
    And I scroll "#footer" into view
    And I wait 1 second
    And I check the "registration_privacyAccepted" label of button
    And I check the "registration_termsOfUse" label of button
    And I press "_submit"
    Then I should be on "/en/register"
    And I should see "There are some mistakes in your registration" in the ".flashbag" element

  @javascript
  Scenario: As a BC, I can sign-up using valid identifiers
    Given I am on "/en/register/association-manager"
    And I fill in "registration_email" with "bc@test.com"
    And I fill in "registration_plainPassword_first" with "testtest"
    And I fill in "registration_plainPassword_second" with "testtest"
    And I fill in "registration_firstName" with "bc"
    And I fill in "registration_lastName" with "test"
    And I fill in select2 input "registration[languages][]" with "French" and select "French"
    And I select "France" from "registration_country"
    And I wait until I see 'Select a city of'
    And I select "Le Havre" from "registration_city"
    And I wait until I see 'Select an association of'
    And I select "Association ESN Le Havre" from "registration_association"
    And I scroll "#footer" into view
    And I wait 1 second
    And I check the "registration_privacyAccepted" label of button
    And I check the "registration_termsOfUse" label of button
    And I press "_submit"
    And I wait 1 second
    Then I should be on "/en/"
    And I should see "An email has been sent. It contains an activation link you must click to activate your account." in the ".flashbag" element

  @javascript
  Scenario: As a RI, I can sign-up using valid identifiers
    Given I am on "/en/register/institute-manager"
    And I fill in "registration_email" with "ri@test.com"
    And I fill in "registration_plainPassword_first" with "testtest"
    And I fill in "registration_plainPassword_second" with "testtest"
    And I fill in "registration_firstName" with "ri"
    And I fill in "registration_lastName" with "test"
    And I fill in select2 input "registration[languages][]" with "French" and select "French"
    And I select "France" from "registration_country"
    And I wait until I see 'Select a city of'
    And I select "Le Havre" from "registration_city"
    And I wait until I see 'Select an institute of'
    And I select "Université Le Havre" from "registration_institute"
    And I scroll "#footer" into view
    And I wait 1 second
    And I check the "registration_privacyAccepted" label of button
    And I check the "registration_termsOfUse" label of button
    And I press "_submit"
    And I wait 1 second
    Then I should be on "/en/"
    And I should see "An email has been sent. It contains an activation link you must click to activate your account." in the ".flashbag" element
