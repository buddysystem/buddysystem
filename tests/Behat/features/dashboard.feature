Feature: Dashboard
  As an authenticated user
  I should have access to a dashboard page

  Scenario: As a non-manager, I should have access to a dashboard page
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And the ".col-sm-9 .white-box" element should contain "MY BUDDIES"

  Scenario Outline: As a manager, I should have access to the insights page
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And the ".col-sm-9 .white-box" element should contain "INSIGHTS"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |
