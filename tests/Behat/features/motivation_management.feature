Feature: Motivation
  As an authenticated user
  I should have a page to manage motivations

  Scenario: As a non-manager, I can not access to the list of motivations
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/motivations"
    Then the response status code should be 403

  Scenario Outline: As a non-admin, I can not edit a motivation
    Given I am logged as "<email>" with "<password>"
    When I go to "en/admin/motivations/1/edit"
    Then the response status code should be 403
    Examples:
      | email                           | password         |
      | local@buddysystem.eu            | local            |
      | buddycoordinator@buddysystem.eu | buddycoordinator |
      | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a non-admin, I can not create a new motivation
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/motivations/new"
    Then the response status code should be 403
    Examples:
      | email                           | password         |
      | local@buddysystem.eu            | local            |
      | buddycoordinator@buddysystem.eu | buddycoordinator |
      | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a manager, I can access to the list of motivations
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "Motivations"
    Then I should be on "/en/admin/motivations"
    And the response status code should be 200
    And the ".white-box" element should contain "Motivations List"
    And the ".white-box" element should not contain "Create a new Motivation"
    And the "#motivations" element should not contain "Edit"
    And the "#motivations" element should not contain "Delete"
    Examples:
      | email                           | password         |
      | buddycoordinator@buddysystem.eu | buddycoordinator |
      | ri@buddysystem.eu               | ri               |

  Scenario: As an admin, I can access to the list of motivations
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    And I follow "Motivations"
    Then I should be on "/en/admin/motivations"
    And the response status code should be 200
    And the ".white-box" element should contain "Motivations List"
    And the ".white-box" element should contain "Create a new Motivation"
    And the "#motivations" element should contain "Edit"
    And the "#motivations" element should contain "Delete"

  Scenario: As an admin, I can edit a motivation
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/motivations/1/edit"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit motivation"

  Scenario: As a admin, I can create a new motivation
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/motivations/new"
    Then the response status code should be 200
    And the ".white-box" element should contain "Create motivation"
