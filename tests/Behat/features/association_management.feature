Feature: Association
  As an authenticated user
  I should have a page to manage associations

  Scenario Outline: As a non-admin, I can not access to the list of associations
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/associations"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a non-bc, I can not edit an association
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/associations/1/edit"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | ri@buddysystem.eu               | ri               |


  Scenario Outline: As a non-admin, I can not create a new association
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/associations/new"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario: As a BC, I can edit my association
    Given I am logged as 'buddycoordinator@buddysystem.eu' with 'buddycoordinator'
    When I go to "/en/dashboard"
    And I follow "Association"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit my association"

  Scenario: As an admin, I can access to the list of associations
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    And I follow "Association"
    Then I should be on "/en/admin/associations"
    And the response status code should be 200
    And the ".white-box" element should contain "Association list"
    And the ".white-box" element should contain "Create a new Association"

  Scenario: As an admin, I can create a new association
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/associations/new"
    Then the response status code should be 200
    And the ".white-box" element should contain "Create Association"
