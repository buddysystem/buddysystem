Feature: Study
  As an authenticated user
  I should have a page to manage studies

  Scenario Outline: As a non-admin, I can not access to the list of studies
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/studies"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a non-admin, I can not edit a study
    Given I am logged as "<email>" with "<password>"
    When I go to "en/admin/studies/1/edit"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a non-admin, I can not create a new study
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/studies/new"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario: As an admin, I can access to the list of studies
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    And I follow "Studies"
    Then I should be on "/en/admin/studies"
    And the response status code should be 200
    And the ".white-box" element should contain "Studies List"
    And the ".white-box" element should contain "Create a new Study"
    And the "#studies" element should contain "Edit"
    And the "#studies" element should contain "Delete"

  Scenario: As an admin, I can edit a study
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/studies/1/edit"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit study"

  Scenario: As a admin, I can create a new study
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/studies/new"
    Then the response status code should be 200
    And the ".white-box" element should contain "Create study"
