Feature: Navigation
  As a authenticated user,
  I should have a menu to navigate through the website

  Scenario: As a user, I should have a menu
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And I should see an "nav" element
    And the "nav" element should contain "Dashboard"
    And the "nav" element should contain "My profile"
    And the "nav" element should contain "Messages"
    And the "nav" element should contain "Toolbox"
    And the "nav" element should not contain "Management"
    And the "nav" element should not contain "Matching"
    And the "nav" element should not contain "User"
    And the "nav" element should not contain "Buddies"
    And the "nav" element should not contain "Cities"
    And the "nav" element should not contain "Association"
    And the "nav" element should not contain "Institutes"
    And the "nav" element should not contain "Studies"
    And the "nav" element should not contain "Hobbies"
    And the "nav" element should not contain "Motivation"
    And the "nav" element should contain "Logout"
    And the "nav" element should contain "ENGLISH"

  Scenario: As a BC, I should have a menu
    Given I am logged as 'buddycoordinator@buddysystem.eu' with 'buddycoordinator'
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And I should see an "nav" element
    And the "nav" element should contain "Dashboard"
    And the "nav" element should contain "My profile"
    And the "nav" element should contain "Messages"
    And the "nav" element should contain "Management"
    And the "nav" element should contain "Toolbox"
    And the "nav" element should not contain "Matching"
    And the "nav" element should contain "User"
    And the "nav" element should contain "Buddies"
    And the "nav" element should not contain "Cities"
    And the "nav" element should contain "Association"
    And the "nav" element should contain "Institutes"
    And the "nav" element should not contain "Studies"
    And the "nav" element should contain "Hobbies"
    And the "nav" element should contain "Motivation"
    And the "nav" element should contain "Logout"
    And the "nav" element should contain "ENGLISH"

  Scenario: As a RI, I should have a menu
    Given I am logged as 'ri@buddysystem.eu' with 'ri'
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And I should see an "nav" element
    And the "nav" element should contain "Dashboard"
    And the "nav" element should contain "My profile"
    And the "nav" element should contain "Messages"
    And the "nav" element should contain "Management"
    And the "nav" element should contain "Toolbox"
    And the "nav" element should contain "Matching"
    And the "nav" element should contain "User"
    And the "nav" element should contain "Buddies"
    And the "nav" element should not contain "Cities"
    And the "nav" element should not contain "Association"
    And the "nav" element should contain "Institutes"
    And the "nav" element should not contain "Studies"
    And the "nav" element should contain "Hobbies"
    And the "nav" element should contain "Motivation"
    And the "nav" element should contain "Logout"
    And the "nav" element should contain "ENGLISH"

  Scenario: As an admin, I should have a menu
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    Then the response status code should be 200
    And I should see an "nav" element
    And the "nav" element should contain "Dashboard"
    And the "nav" element should contain "My profile"
    And the "nav" element should contain "Messages"
    And the "nav" element should contain "Management"
    And the "nav" element should contain "Toolbox"
    And the "nav" element should contain "Matching"
    And the "nav" element should contain "User"
    And the "nav" element should contain "Buddies"
    And the "nav" element should contain "Cities"
    And the "nav" element should contain "Association"
    And the "nav" element should contain "Institutes"
    And the "nav" element should contain "Studies"
    And the "nav" element should contain "Hobbies"
    And the "nav" element should contain "Motivation"
    And the "nav" element should contain "Logout"
    And the "nav" element should contain "ENGLISH"

  Scenario: As a user, I can logout
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/"
    And I follow "Logout"
    Then I should be on "/en/"
    And the response status code should be 200
