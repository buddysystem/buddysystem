Feature: Institute
  As an authenticated user
  I should have a page to manage institutes

  Scenario: As a non-manager, I can not access to the list of institutes
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/institutes"
    Then the response status code should be 403

  Scenario: As a non-manager, I can not edit a institute
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/institutes/11/edit"
    Then the response status code should be 403

  Scenario: As a non-manager, I can not create a new institute
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/institutes/new"
    Then the response status code should be 403

  Scenario Outline: As a non-bc, I can not manage institutes linked to an association
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/institutes/link_association"
    Then the response status code should be 403
    Examples:
    | email                           | password         |
    | local@buddysystem.eu            | local            |
    | ri@buddysystem.eu               | ri               |

  Scenario: As a BC, I can access to the list of institutes
    Given I am logged as 'buddycoordinator@buddysystem.eu' with 'buddycoordinator'
    When I go to "/en/dashboard"
    And I follow "Institutes"
    Then I should be on "/en/admin/institutes"
    And the response status code should be 200
    And the ".white-box" element should contain "Institutes list"
    And the ".white-box" element should not contain "Create a new Institute"
    And the ".white-box" element should contain "Manage institutes linked"

  Scenario: As a RI, I can access to the list of institutes
    Given I am logged as 'ri@buddysystem.eu' with 'ri'
    When I go to "/en/dashboard"
    And I follow "Institutes"
    Then I should be on "/en/admin/institutes"
    And the response status code should be 200
    And the ".white-box" element should contain "Institutes list"
    And the ".white-box" element should contain "Create a new Institute"
    And the ".white-box" element should not contain "Manage institutes linked"

  Scenario: As an admin, I can access to the list of institutes
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/dashboard"
    And I follow "Institutes"
    Then I should be on "/en/admin/institutes"
    And the response status code should be 200
    And the ".white-box" element should contain "Institutes list"
    And the ".white-box" element should contain "Create a new Institute"
    And the ".white-box" element should contain "Manage institutes linked"

  Scenario Outline: As a RI, I can edit my institute
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/institutes/1/edit"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit institute"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | ri@buddysystem.eu               | ri               |

  Scenario: As a BC, I can not edit an institute if there is a RI
    Given I am logged as 'buddycoordinator@buddysystem.eu' with 'buddycoordinator'
    When I go to "/en/admin/institutes/1/edit"
    Then the response status code should be 403

  Scenario: As an admin, I can edit an institute
    Given I am logged as 'admin@buddysystem.eu' with 'admin'
    When I go to "/en/admin/institutes/3/edit"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit institute"

  Scenario Outline: As a manager, I can create a new institute
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/institutes/new"
    Then the response status code should be 200
    And the ".white-box" element should contain "Create institute"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |

  Scenario Outline: As a bc, I can manage institutes linked to an association
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/institutes/link_association"
    Then the response status code should be 200
    And the ".white-box" element should contain "Linkable institutes"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |

  Scenario: As a RI, I can not manager association linked to my institute
    Given I am logged as 'ri@buddysystem.eu' with 'ri'
    When I go to "/en/admin/institutes/link_association"
    Then the response status code should be 403

  Scenario Outline: As a RI, I can edit my institute preferences
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/admin/institutes/1/edit_preferences"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit institute preferences"
    And the "#accordion-preferences" element should contain "Matching Preference"
    And the "#accordion-preferences" element should contain "Registration Preference"
    And the "#accordion-preferences" element should contain "Bonus"
    Examples:
      | email                           | password         |
      | admin@buddysystem.eu            | admin            |
      | ri@buddysystem.eu               | ri               |

  Scenario: As a RI, I can edit parameters of sub institute preferences
    Given I am logged as 'ri@buddysystem.eu' with 'ri'
    When I go to "/en/admin/institutes/2/edit_preferences"
    Then the response status code should be 200
    And the ".white-box" element should contain "Edit institute preferences"
    And the "#accordion-preferences" element should contain "Matching Preference"
    And the "#accordion-preferences" element should contain "Registration Preference"
    And the "#accordion-preferences" element should contain "Bonus"
