Feature: User
  As an authenticated user
  I should have a page to manage users

  Scenario: As a non-manager, I can not access to the list of users
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/users"
    Then the response status code should be 403

  Scenario: As a non-manager, I can not edit a user
    Given I am logged as 'local@buddysystem.eu' with 'local'
    When I go to "/en/admin/users/1/edit"
    Then the response status code should be 403

  Scenario Outline: As a manager, I can access to the list of users
    Given I am logged as "<email>" with "<password>"
    When I go to "/en/dashboard"
    And I follow "Users"
    Then I should be on "/en/admin/users"
    And the response status code should be 200
    And the ".white-box" element should contain "Users list"
    Examples:
    | email                           | password         |
    | admin@buddysystem.eu            | admin            |
    | buddycoordinator@buddysystem.eu | buddycoordinator |
    | ri@buddysystem.eu               | ri               |
