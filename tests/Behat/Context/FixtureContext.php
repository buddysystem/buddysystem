<?php

declare(strict_types=1);

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Doctrine\Bundle\FixturesBundle\Loader\SymfonyFixturesLoader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManagerInterface;

class FixtureContext implements Context
{
    private $em;
    private $loader;
    private $executor;

    public function __construct(SymfonyFixturesLoader $loader, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->loader = $loader;
        $purger = new ORMPurger($em);
        $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $this->executor = new ORMExecutor($em, $purger);
    }

    /**
     * @BeforeScenario @loadFixtures
     *
     * @throws \Exception
     */
    public function loadDataFixtures(): void
    {
        try {
            $this->em->getConnection()->executeStatement('SET foreign_key_checks = 0');
            $this->executor->purge();
            $this->em->getConnection()->executeStatement('SET foreign_key_checks = 1');

            $fixtures = $this->loader->getFixtures();
            if (!$fixtures) {
                throw new \InvalidArgumentException(sprintf('Could not find any fixtures to load in: %s.', "\n\n- ".implode("\n- ", $fixtures)));
            }

            $this->executor->execute($fixtures, true);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
