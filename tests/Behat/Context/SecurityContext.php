<?php

declare(strict_types=1);

namespace App\Tests\Behat\Context;

use App\Entity\User;
use App\Repository\UserRepository;
use Behat\Behat\Context\Context;
use Behat\Mink\Driver\PantherDriver;
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityContext extends RawMinkContext implements Context
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @When I am logged as :email with :pass
     *
     * @throws \Exception
     */
    public function iAmLoggedAs(string $email, string $pass): void
    {
        if (!$user = $this->getUserRepository()->findOneBy(['email' => $email])) {
            throw new \Exception(sprintf('User %s not found.', $email));
        }

        $this->logAs($user, $pass);
    }

    protected function getUserRepository(): UserRepository
    {
        return $this->em->getRepository(User::class);
    }

    private function logAs(User $user, string $pass): void
    {
        $session = $this->getSession();
        $driver = $session->getDriver();

        if ($driver instanceof PantherDriver) {
            $page = $this->getSession()->getPage();

            $this->visitPath('/en/login');

            $page->findField('email')->setValue($user->getEmail());
            $page->findField('password')->setValue($pass);
            $page->findButton('_submit')->press();

            return;
        }

        $client = $driver->getClient();
        $session = $client->getContainer()->get('session');
        $cookieJar = $client->getCookieJar();

        $cookieJar->set(new Cookie(session_name(), null));
        $session->set(
            '_security_main',
            serialize(new UsernamePasswordToken($user, null, 'main', $user->getRoles()))
        );
        $session->save();
        $cookieJar->set(new Cookie($session->getName(), $session->getId()));
    }
}
