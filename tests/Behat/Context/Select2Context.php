<?php

declare(strict_types=1);

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Mink\Element\DocumentElement;
use Behat\Mink\Element\NodeElement;
use Behat\MinkExtension\Context\RawMinkContext;

class Select2Context extends RawMinkContext implements Context
{
    public const DEFAULT_TIMEOUT = 60;

    /** @var int */
    private $timeout;

    public function __construct($timeout = self::DEFAULT_TIMEOUT)
    {
        $this->timeout = $timeout;
    }

    /**
     * Fills in Select2 field with specified
     *
     * @When /^(?:|I )fill in select2 "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
     * @When /^(?:|I )fill in select2 "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)"$/
     *
     * @throws \Exception
     */
    public function iFillInSelect2Field(string $field, string $value): void
    {
        $page = $this->getSession()->getPage();

        $this->openField($page, $field);
        $this->selectValue($page, $field, $value, $this->timeout);
    }

    /**
     * Fills in Select2 field with specified and wait for results
     *
     * @When /^(?:|I )fill in select2 "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)" and wait (?P<time>(?:[^"]|\\")*) seconds until results are loaded$/
     * @When /^(?:|I )fill in select2 "(?P<value>(?:[^"]|\\")*)" for "(?P<field>(?:[^"]|\\")*)" and wait (?P<time>(?:[^"]|\\")*) seconds until results are loaded$/
     *
     * @throws \Exception
     */
    public function iFillInSelect2FieldWaitUntilResultsAreLoaded(string $field, string $value, int $time): void
    {
        $page = $this->getSession()->getPage();

        $this->openField($page, $field);
        $this->selectValue($page, $field, $value, $time);
    }

    /**
     * Fill Select2 input field
     *
     * @When /^(?:|I )fill in select2 input "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)"$/
     *
     * @throws \Exception
     */
    public function iFillInSelect2InputWith(string $field, string $value): void
    {
        $page = $this->getSession()->getPage();

        $this->openField($page, $field);
        $this->fillSearchField($page, $field, $value);
    }

    /**
     * Fill Select2 input field and select a value
     *
     * @When /^(?:|I )fill in select2 input "(?P<field>(?:[^"]|\\")*)" with "(?P<value>(?:[^"]|\\")*)" and select "(?P<entry>(?:[^"]|\\")*)"$/
     *
     * @throws \Exception
     */
    public function iFillInSelect2InputWithAndSelect(string $field, string $value, string $entry): void
    {
        $page = $this->getSession()->getPage();

        $this->openField($page, $field);
        $this->fillSearchField($page, $field, $value);
        $this->selectValue($page, $field, $entry, $this->timeout);
    }

    /**
     * Fill Select2 input field
     *
     * @Then /^(?:|I )should see (?P<num>\d+) choice(?:|s) in select2 "(?P<field>(?:[^"]|\\")*)"$/
     *
     * @throws \Exception
     */
    public function iShouldSeeSelectChoices(string $field, int $num): void
    {
        $selector = sprintf('#select2-%s-results li', $field);

        $this->assertSession()->elementsCount('css', $selector, \intval($num));
    }

    /**
     * Open Select2 choice list
     *
     * @throws \Exception
     */
    private function openField(DocumentElement $page, string $field): void
    {
        // force select2 to be closed
        $page->find('css', 'body')->press();

        $fieldName = sprintf('select[name="%s"] + .select2-container', $field);

        $inputField = $page->find('css', $fieldName);
        if (!$inputField) {
            throw new \Exception(sprintf('No field "%s" found.', $field));
        }

        $choice = $inputField->find('css', '.select2-selection');
        if (!$choice) {
            throw new \Exception(sprintf('No select2 choice found for "%s".', $field));
        }
        $choice->press();
    }

    /**
     * Fill Select2 search field
     *
     * @throws \Exception
     */
    private function fillSearchField(DocumentElement $page, string $field, string $value): void
    {
        $select2Input = $page->find('css', '.select2-search__field');
        if (!$select2Input) {
            throw new \Exception(sprintf('No input found for "%s".', $field));
        }
        $select2Input->setValue($value);

        $this->waitForLoadingResults($this->timeout);
    }

    /**
     * Select value in choice list
     *
     * @throws \Exception
     */
    private function selectValue(DocumentElement $page, string $field, string $value, int $time): void
    {
        $this->waitForLoadingResults($time);

        $chosenResults = $page->findAll('css', '.select2-results li');
        /** @var NodeElement $result */
        foreach ($chosenResults as $result) {
            if ($result->getText() == $value) {
                $result->click();

                return;
            }
        }

        throw new \Exception(sprintf('Value "%s" not found for "%s".', $value, $field));
    }

    /**
     * Wait the end of fetching Select2 results
     */
    private function waitForLoadingResults(int $time): void
    {
        for ($i = 0; $i < $time; ++$i) {
            if (!$this->getSession()->getPage()->find('css', '.select2-results__option.loading-results')) {
                return;
            }

            sleep(1);
        }
    }
}
