<?php

declare(strict_types=1);

namespace App\Tests\Behat\Context;

use Behat\Behat\Context\Context;
use Behat\Behat\Hook\Scope\AfterStepScope;
use Behat\Mink\Driver\PantherDriver;
use Behat\MinkExtension\Context\MinkContext;

class FeatureContext extends MinkContext implements Context
{
    /**
     * @When /^I check the "([^"]*)" label of button$/
     *
     * @throws \Exception
     */
    public function iCheckTheLabelOfButton(string $id): void
    {
        $session = $this->getSession();
        $label = $this->getSession()->getPage()->find('xpath', $session->getSelectorsHandler()->selectorToXpath('css', "label[for='".$id."']"));
        if (null === $label) {
            throw new \Exception('Cannot find label for button .');
        }

        $label->click();
    }

    /**
     * @When I scroll :selector into view
     *
     * @param string $selector Allowed selectors: #id, .className, //xpath
     *
     * @throws \Exception
     */
    public function scrollIntoView(string $selector): void
    {
        $locator = substr($selector, 0, 1);
        switch ($locator) {
            case '$': // Query selector
                $selector = substr($selector, 1);
                $function = <<<JS
                    (function(){
                      var elem = document.querySelector("{$selector}");
                      elem.scrollIntoView(false);
                    })()
JS;
                break;
            case '/': // XPath selector
                $function = <<<JS
                    (function(){
                      var elem = document.evaluate("{$selector}", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
                      elem.scrollIntoView(false);
                    })()
JS;
                break;
            case '#': // ID selector
                $selector = substr($selector, 1);
                $function = <<<JS
                    (function(){
                      var elem = document.getElementById("{$selector}");
                      elem.scrollIntoView(false);
                    })()
JS;
                break;
            case '.': // Class selector
                $selector = substr($selector, 1);
                $function = <<<JS
                    (function(){
                      let elem = document.getElementsByClassName("{$selector}");
                      elem[0].scrollIntoView(false);
                    })()
JS;
                break;
            default:
                throw new \Exception(__METHOD__.' Couldn\'t find selector: '.$selector.' - Allowed selectors: #id, .className, //xpath');
                break;
        }
        try {
            $this->getSession()->executeScript($function);
        } catch (\Exception $e) {
            throw new \Exception(__METHOD__.' failed');
        }
    }

    /**
     * Grab the JavaScript errors from the session. Only works in companion
     * with a global window variable `errors` that contains the JavaScript
     * and/or XHR errors.
     *
     * @AfterStep
     */
    public function takeJSErrorsAfterFailedStep(AfterStepScope $event): void
    {
        $code = $event->getTestResult()->getResultCode();
        $driver = $this->getSession()->getDriver();

        if ($driver instanceof PantherDriver && 99 === $code) {
            try {
                $json = $this->getSession()->evaluateScript('return JSON.stringify(window.errors);');
            } catch (\Exception $e) {
                return;
            }

            $errors = json_decode($json);
            $messages = [];

            if (\JSON_ERROR_NONE == json_last_error()) {
                foreach ($errors as $error) {
                    if ('javascript' == $error->type) {
                        $messages[] = "- {$error->message} ({$error->location})";
                    } elseif ('xhr' == $error->type) {
                        $messages[] = "- {$error->message} ({$error->method} {$error->url}): {$error->statusCode} {$error->response}";
                    }
                }

                printf("JavaScript errors:\n\n".implode("\n", $messages));
            }
        }
    }
}
