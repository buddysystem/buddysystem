<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Entity;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class UserTest extends WebTestCase
{
    private $em;
    private $matchingManagerHelper;

    protected function setUp(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $this->em = $container->get('doctrine')->getManager();
        $this->matchingManagerHelper = $container->get('app.matching_manager_helper');
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    public function testUserManager(): void
    {
        $user = new User();
        $fac = new Institute();

        $this->assertEquals(null, $this->matchingManagerHelper->getMatchingManagerOfUser($user));

        $user->setInstitute($fac);
        $this->assertEquals($fac, $this->matchingManagerHelper->getMatchingManagerOfUser($user));

        $association = new Association();
        $associationInstitute = new AssociationInstitute();
        $associationInstitute->setAssociation($association);
        $associationInstitute->setInstitute($fac);
        $associationInstitute->setCanMatch(true);
        $association->addAssociationInstitute($associationInstitute);
        $fac->addAssociationInstitute($associationInstitute);

        $this->assertEquals([$association], $this->matchingManagerHelper->getAllMatchingManagerOfUser($user));

        $association->setActivated(false);
        $this->assertEquals([$fac], $this->matchingManagerHelper->getAllMatchingManagerOfUser($user));
    }

    public function testArchiveUser(): void
    {
        $buddies = $this->em->getRepository(Buddy::class)->findBy(['status' => BuddyStatusEnum::STATUS_CONFIRMED]);

        /** @var Buddy $buddy */
        foreach ($buddies as $buddy) {
            $mentor = $buddy->getMentor();
            $mentee = $buddy->getMentee();

            $mentor->setArchived(true);
            $mentee->setArchived(true);
            $this->em->flush();

            $this->assertEquals(0, $mentor->getNbBuddies());
            $this->assertEquals(0, $mentee->getNbBuddies());
            $this->assertEquals(0, $mentor->getNbBuddiesArchived());
            $this->assertEquals(0, $mentee->getNbBuddiesArchived());
            $this->assertTrue($buddy->isArchived());
        }
    }
}
