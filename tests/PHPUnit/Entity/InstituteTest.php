<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Entity;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class InstituteTest extends WebTestCase
{
    public function testInstituteLevels(): void
    {
        $fac = new Institute();
        $fac->setLevel();
        $this->assertSame(0, $fac->getLevel());

        $campus = new Institute();
        $campus->addChild($fac);
        $campus->setLevel();
        $this->assertSame(1, $campus->getLevel());
        $this->assertSame($campus->getLevel() + 1, $fac->getLevel());

        $univ = new Institute();
        $univ->addChild($campus);
        $univ->setLevel();

        $this->assertSame(1, $univ->getLevel());
        $this->assertSame($univ->getLevel() + 1, $campus->getLevel());
        $this->assertSame($campus->getLevel() + 1, $fac->getLevel());

        $campus2 = new Institute();
        $univ->addChild($campus2);
        $univ->setLevel();

        $this->assertSame(1, $univ->getLevel());
        $this->assertSame($univ->getLevel() + 1, $campus2->getLevel());
    }

    public function testInstituteManager(): void
    {
        self::bootKernel();
        $matchingManagerHelper = static::getContainer()->get('app.matching_manager_helper');

        $fac = new Institute();
        $this->assertEquals([$fac], $matchingManagerHelper->getMatchingManagersOfInstitute($fac));

        $association = new Association();
        $associationInstitute = new AssociationInstitute();
        $associationInstitute->setAssociation($association);
        $associationInstitute->setInstitute($fac);
        $association->addAssociationInstitute($associationInstitute);
        $associationInstitute->setCanMatch(true);
        $fac->addAssociationInstitute($associationInstitute);

        $this->assertEquals([$association], $matchingManagerHelper->getMatchingManagersOfInstitute($fac));

        $association->setActivated(false);
        $this->assertEquals([$association], $matchingManagerHelper->getMatchingManagersOfInstitute($fac));

        $this->assertEquals([$fac], $matchingManagerHelper->getMatchingManagersOfInstitute($fac, true));

        $campus = new Institute();
        $campus->addChild($fac);

        $this->assertEquals([$fac], $matchingManagerHelper->getMatchingManagersOfInstitute($fac, true));
    }
}
