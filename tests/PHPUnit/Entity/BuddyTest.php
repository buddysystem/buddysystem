<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Entity;

use App\Entity\Buddy;
use App\Enum\BuddyStatusEnum;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class BuddyTest extends WebTestCase
{
    private $em;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->em = static::getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    public function testRemove(): void
    {
        $buddies = $this->em->getRepository(Buddy::class)->findBy(['status' => BuddyStatusEnum::STATUS_PENDING]);

        /** @var Buddy $buddy */
        foreach ($buddies as $buddy) {
            $mentor = $buddy->getMentor();
            $mentee = $buddy->getMentee();

            $nbBuddiesMentor = $mentor->getNbBuddies();
            $nbBuddiesMentee = $mentee->getNbBuddies();

            $this->em->remove($buddy);
            $this->em->flush();

            $this->assertEquals($nbBuddiesMentor - 1, $mentor->getNbBuddies());
            $this->assertEquals($nbBuddiesMentee - 1, $mentee->getNbBuddies());
        }
    }
}
