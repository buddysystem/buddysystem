<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Services;

use App\Entity\Institute;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class DuplicateInstituteHelperTest extends WebTestCase
{
    protected $duplicateInstituteHelper;

    public function setUp(): void
    {
        self::bootKernel();
        $this->duplicateInstituteHelper = static::getContainer()->get('app.duplicate_institute_helper');
    }

    public function testManageDuplicatesSingleParent(): void
    {
        $fac = new Institute();
        $campus = new Institute();
        $this->duplicateInstituteHelper->manageDuplicates($fac, [$campus]);

        $this->assertEquals($campus, $fac->getParent());
        $this->assertEquals(1, $campus->getLevel());
        $this->assertEquals(2, $campus->getDepth());
        $this->assertEquals($campus->getLevel() + 1, $fac->getLevel());
        $this->assertEquals($campus->getDepth() - 1, $fac->getDepth());
        $this->assertContains($campus, $fac->getAncestors());

        $univ = new Institute();
        $this->duplicateInstituteHelper->manageDuplicates($campus, [$univ]);

        $this->assertEquals($univ, $campus->getParent());
        $this->assertEquals(1, $univ->getLevel());
        $this->assertEquals(3, $univ->getDepth());
        $this->assertEquals($univ->getLevel() + 1, $campus->getLevel());
        $this->assertEquals($univ->getDepth() - 1, $campus->getDepth());
        $this->assertContains($univ, $campus->getAncestors());
        $this->assertEquals($campus->getLevel() + 1, $fac->getLevel());
        $this->assertEquals($campus->getDepth() - 1, $fac->getDepth());
    }

    public function testManageDuplicatesMultipleParent(): void
    {
        $fac = new Institute();
        $fac->setName('fac');
        $campusA = new Institute();
        $campusA->setName('campus A');
        $campusB = new Institute();
        $campusA->setName('campus B');
        $this->duplicateInstituteHelper->manageDuplicates($fac, [$campusA, $campusB]);

        $this->assertEquals($campusA, $fac->getParent());
        $this->assertEquals(1, $campusA->getLevel());
        $this->assertEquals(2, $campusA->getDepth());
        $this->assertEquals(1, $campusB->getLevel());
        $this->assertEquals(2, $campusB->getDepth());
        $this->assertEquals($campusA->getLevel() + 1, $fac->getLevel());
        $this->assertEquals($campusA->getDepth() - 1, $fac->getDepth());
        $this->assertContains($campusA, $fac->getAncestors());
        $this->assertNotContains($campusB, $fac->getAncestors());

        $this->assertEquals(1, $fac->getCopies()->count());
        $copyFac = $fac->getCopies()->first();
        $this->assertEquals($fac->getName(), $copyFac->getName());
        $this->assertEquals($campusB->getLevel() + 1, $copyFac->getLevel());
        $this->assertEquals(1, $copyFac->getDepth());
        $this->assertContains($campusB, $copyFac->getAncestors());
        $this->assertNotContains($campusA, $copyFac->getAncestors());
    }
}
