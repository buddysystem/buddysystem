<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Services;

use App\Entity\Institute;
use App\Entity\Semester;
use App\Enum\SemesterPeriodEnum;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class SemesterHelperTest extends WebTestCase
{
    private $semesterHelper;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->semesterHelper = static::getContainer()->get('app.semester_helper');
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->semesterHelper = null;
    }

    public function provideSemesterExpected(): \Generator
    {
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-02'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, '2018-12-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-02-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, '2018-12-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, '2019-01-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_ALL_YEAR, '2019-11-01', true];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, '2019-06-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, '2019-05-30'];
        yield [new \DateTime('2010-07-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-10'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, '2019-06-06'];
    }

    /**
     * @dataProvider provideSemesterExpected
     */
    public function testGetCurrentSemester(
        \DateTime $firstSemesterStart,
        \DateTime $firstSemesterEnd,
        \DateTime $secondSemesterStart,
        \DateTime $secondSemesterEnd,
        string $result,
        string $time = 'now',
        bool $isFullYearOnly = false
    ): void {
        $institute = new Institute();
        $institute->setFirstSemesterStart($firstSemesterStart);
        $institute->setFirstSemesterEnd($firstSemesterEnd);
        $institute->setSecondSemesterStart($secondSemesterStart);
        $institute->setSecondSemesterEnd($secondSemesterEnd);
        $institute->setFullYearOnly($isFullYearOnly);

        $this->assertEquals($result, $this->semesterHelper->getSemester($institute, $time)->getPeriod());
    }

    public function provideInfoEndReached(): \Generator
    {
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, true, '2018-12-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-02-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, false, '2018-12-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, true, '2019-12-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_FIRST_SEMESTER, false, '2019-11-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-01'), SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, true, '2019-06-01'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, false, '2019-05-30'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-01'), SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, true, '2019-06-06'];
    }

    /**
     * @dataProvider provideInfoEndReached
     */
    public function testIsEndOfSemesterReached(
        \DateTime $firstSemesterStart,
        \DateTime $firstSemesterEnd,
        \DateTime $secondSemesterStart,
        \DateTime $secondSemesterEnd,
        string $period,
        bool $result,
        string $time = 'now'
    ): void {
        $institute = new Institute();
        $institute->setFirstSemesterStart($firstSemesterStart);
        $institute->setFirstSemesterEnd($firstSemesterEnd);
        $institute->setSecondSemesterStart($secondSemesterStart);
        $institute->setSecondSemesterEnd($secondSemesterEnd);
        $institute->setSemester($this->getSemester($period, $time));

        $this->assertEquals($result, $this->semesterHelper->isEndOfSemesterReached($institute, $time));
    }

    public function provideSwitchDate(): \Generator
    {
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), new \DateTime('2018-12-03'), '2018-10-22'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-07'), new \DateTime('2010-01-07'), new \DateTime('2010-07-01'), new \DateTime('2018-12-10'), '2018-10-22'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-01'), new \DateTime('2019-07-01'), '2019-05-22'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-07-05'), new \DateTime('2019-07-08'), '2019-05-22'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-01'), new \DateTime('2019-06-03'), '2019-05-06'];
        yield [new \DateTime('2010-09-01'), new \DateTime('2010-01-01'), new \DateTime('2010-01-01'), new \DateTime('2010-06-01'), new \DateTime('2019-12-02'), '2019-06-01'];
    }

    /**
     * @dataProvider provideSwitchDate
     */
    public function testDateNextSwitch(
        \DateTime $firstSemesterStart,
        \DateTime $firstSemesterEnd,
        \DateTime $secondSemesterStart,
        \DateTime $secondSemesterEnd,
        \DateTime $result,
        string $time = 'now'
    ): void {
        $institute = new Institute();
        $institute->setFirstSemesterStart($firstSemesterStart);
        $institute->setFirstSemesterEnd($firstSemesterEnd);
        $institute->setSecondSemesterStart($secondSemesterStart);
        $institute->setSecondSemesterEnd($secondSemesterEnd);
        $institute->setSemester($this->semesterHelper->getSemester($institute, $time));

        $this->assertEquals($result, $this->semesterHelper->getDateNextSwitch($institute, $time));
    }

    private function getSemester(string $period, string $time = 'now'): Semester
    {
        $semester = new Semester();
        $semester->setPeriod($period);

        $date = new \DateTime($time);
        $year = (int) substr($date->format('Y'), 0, 4);
        if (SemesterPeriodEnum::PERIOD_FIRST_SEMESTER === $period) {
            $semester->setYear($year.' - '.($year + 1));
        } else {
            $semester->setYear(($year - 1).' - '.$year);
        }

        return $semester;
    }
}
