<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Helper;

use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

trait TestHelperTrait
{
    public function getManager(): EntityManager
    {
        return $this->client->getContainer()->get('doctrine.orm.entity_manager');
    }

    public function getRepository($class): EntityRepository
    {
        return $this->getManager()->getRepository($class);
    }

    private function getUserRepository(): EntityRepository
    {
        return $this->getRepository(User::class);
    }
}
