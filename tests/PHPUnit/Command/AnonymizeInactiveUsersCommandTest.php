<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Command;

use App\Command\AnonymizeInactiveUsersCommand;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\MailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 * @coversNothing
 */
class AnonymizeInactiveUsersCommandTest extends KernelTestCase
{
    private $userRepo;
    private $mailManager;
    private $commandTester;

    protected function setUp(): void
    {
        $em = $this->getMockBuilder(EntityManagerInterface::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->userRepo = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $this->mailManager = $this->getMockBuilder(MailManager::class)
            ->disableOriginalConstructor()
            ->getMock()
        ;

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $application->add(new AnonymizeInactiveUsersCommand(
            $em,
            $this->userRepo,
            $this->mailManager
        ));
        $command = $application->find('cron:user:anonymize');
        $this->commandTester = new CommandTester($command);
    }

    /**
     * @dataProvider provideUsers
     */
    public function testAnonymizeCommand(string $firstName, string $lastName, string $email): void
    {
        $user = new User();
        $user->setFirstName($firstName);
        $user->setLastname($lastName);
        $user->setEmail($email);

        $this->userRepo
            ->expects($this->once())
            ->method('getInactiveNonAnonymizedUsers')
            ->willReturn([$user])
        ;

//        $this->mailManager
//            ->expects($this->once())
//            ->method('sendAnonymisationEmail')
//            ->with($user)
//        ;

        $this->commandTester->execute([]);

        $this->assertEquals('[OK] Finish', trim($this->commandTester->getDisplay()));
    }

    public function provideUsers(): \Generator
    {
        yield ['foo', 'bar', 'foo@bar.com'];
    }
}
