<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Controller;

use App\DataFixtures\SecurityUserFixtures;
use App\Tests\PHPUnit\Helper\TestHelperTrait;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

trait ControllerTestTrait
{
    use TestHelperTrait;

    protected $loggedUser;

    public function authenticateBuddyCoordinator(): void
    {
        $this->authenticateUser(SecurityUserFixtures::BUDDY_COORDINATOR_USERNAME.'@buddysystem.eu');
    }

    public function authenticateAdmin(): void
    {
        $this->authenticateUser(SecurityUserFixtures::ADMIN_USERNAME.'@buddysystem.eu');
    }

    public function authenticateInternationalStudent(): void
    {
        $this->authenticateUser(SecurityUserFixtures::INTERNATIONAL_STUDENT_USERNAME.'@buddysystem.eu');
    }

    public function authenticateLocalStudent(): void
    {
        $this->authenticateUser(SecurityUserFixtures::LOCAL_STUDENT_USERNAME.'@buddysystem.eu');
    }

    public function authenticateUser(string $email): void
    {
        if (!$user = $this->getUserRepository()->findByEmail($email)) {
            throw new \Exception(sprintf('User with email %s not found.', $email));
        }

        $this->authenticate($user, 'main');
    }

    private function authenticate(UserInterface $user, string $firewallName): void
    {
        $session = $this->client->getContainer()->get('session');

        $token = new UsernamePasswordToken($user, $user->getPassword(), $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);

        $this->loggedUser = $user;
    }
}
