<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
class DefaultControllerTest extends WebTestCase
{
    public function testHomepage(): void
    {
        $client = static::createClient();
        $client->request('GET', '/en/');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }

    public function testFooterElements(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/');

        $this->assertCount(3, $footerElements = $crawler->filter('#footer .links div'));

        $this->assertCount(4, $aboutElements = $footerElements->eq(0)->filter('a'));
        $this->assertEquals('/en/', $aboutElements->eq(0)->attr('href'));
        $this->assertEquals('/en/institutions', $aboutElements->eq(1)->attr('href'));
        $this->assertEquals('/en/the-project', $aboutElements->eq(2)->attr('href'));
        $this->assertEquals('/en/our-partners', $aboutElements->eq(3)->attr('href'));

        $this->assertCount(4, $legalInformationElements = $footerElements->eq(1)->filter('a'));
        $this->assertEquals('/en/privacy', $legalInformationElements->eq(0)->attr('href'));
        $this->assertEquals('/en/terms-of-use', $legalInformationElements->eq(1)->attr('href'));
        $this->assertEquals('/en/cookie-policy', $legalInformationElements->eq(2)->attr('href'));
        $this->assertEquals('/en/charter', $legalInformationElements->eq(3)->attr('href'));

        $this->assertCount(2, $help = $footerElements->eq(2)->filter('a'));
        $this->assertEquals('https://docs.buddysystem.local', $help->eq(0)->attr('href'));
        $this->assertEquals('/en/contact-us', $help->eq(1)->attr('href'));
    }

    public function testFooterPages(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/en/');
        $footerLinks = $crawler->filter('#footer .links div a');

        foreach ($footerLinks as $footerLink) {
            $href = $footerLink->getAttribute('href');
            if (false === strpos($href, 'mailto')) {
                $client->request('GET', $href);

                $code = '_blank' === $footerLink->getAttribute('target')
                    ? 302
                    : 200
                ;

                $this->assertEquals($code, $client->getResponse()->getStatusCode());
            }
        }
    }
}
