<?php

declare(strict_types=1);

namespace App\Tests\PHPUnit\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @internal
 * @coversNothing
 */
class ProfileControllerTest extends WebTestCase
{
    use ControllerTestTrait;

    private const PROFILE_URL = '/en/profile/edit';
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testProfileSecurity(): void
    {
        $this->client->request(Request::METHOD_GET, self::PROFILE_URL);

        $this->assertEquals(Response::HTTP_FOUND, $this->client->getResponse()->getStatusCode());

        $this->client->followRedirect();

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertStringContainsString('/login', $this->client->getRequest()->getUri());
    }

    public function testProfileSecurityWithLoggedUser(): void
    {
        $this->authenticateLocalStudent();

        $this->client->request(Request::METHOD_GET, self::PROFILE_URL);

        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
}
