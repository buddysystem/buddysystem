context('Messaging', () => {
    Cypress.Cookies.defaults({ preserve: 'PHPSESSID' });

    it('should allow to select thread as RI', () => {
        cy.login('ri@buddysystem.eu', 'ri');
        cy.visit('/messaging');
        cy.get('[data-cy^="inbox-item-"]').as('inboxItems');

        cy.get('@inboxItems').should('have.length', 2);
        cy.get('[data-cy="thread-header"]').should('not.exist');

        cy.get('@inboxItems').eq(0).click();
        cy.get('[data-cy="thread-header"]').should('contain', 'Students Channel');

        cy.get('@inboxItems').eq(1).click();
        cy.get('[data-cy="thread-header"]').should('contain', 'Institute Managers Channel');
    });

    it('should allow to select thread as BC', () => {
        cy.login('buddycoordinator@buddysystem.eu', 'buddycoordinator');
        cy.visit('/messaging');
        cy.get('[data-cy^="inbox-item-"]').as('inboxItems');

        cy.get('@inboxItems').should('have.length', 2);
        cy.get('[data-cy="thread-header"]').should('not.exist');

        cy.get('@inboxItems').eq(0).click();
        cy.get('[data-cy="thread-header"]').should('contain', 'Students Channel');

        cy.get('@inboxItems').eq(1).click();
        cy.get('[data-cy="thread-header"]').should('contain', 'BCs Channel');
    });

    it('should select thread from location hash', () => {
        cy.login('ri@buddysystem.eu', 'ri');

        cy.visit('/messaging#matching_managers');
        cy.get('[data-cy="thread-header"]').should('contain', 'Students Channel');
    });
});
