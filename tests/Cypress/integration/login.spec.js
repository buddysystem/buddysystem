context('Login', () => {
    it('should redirect to dashboard', () => {
        cy.login('admin@buddysystem.eu', 'admin');

        cy.location('pathname').should('eq', '/en/dashboard');
    });
});
