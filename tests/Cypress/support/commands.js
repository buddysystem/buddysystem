Cypress.Commands.add('login', (username, password) => {
    cy.visit('/en/login');

    cy.get('#login_form').find('#email').type(username);
    cy.get('#login_form').find('#password').type(password);

    cy.get('#login_form').find('#_submit').click();
});
