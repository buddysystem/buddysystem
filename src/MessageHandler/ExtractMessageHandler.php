<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Extract\ExtractFactory;
use App\Message\ExtractMessage;
use App\Repository\ExtractRepository;
use App\Repository\UserRepository;
use App\Services\FileWriterHelper;
use App\Services\MailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ExtractMessageHandler implements MessageHandlerInterface
{
    private $em;
    private $extractFactory;
    private $bus;
    private $mailManager;
    private $fileWriterHelper;
    private $userRepo;
    private $extractRepo;

    public function __construct(
        EntityManagerInterface $em,
        ExtractFactory $extractFactory,
        MessageBusInterface $bus,
        MailManager $mailManager,
        FileWriterHelper $fileWriterHelper,
        UserRepository $userRepo,
        ExtractRepository $extractRepo
    ) {
        $this->em = $em;
        $this->extractFactory = $extractFactory;
        $this->bus = $bus;
        $this->mailManager = $mailManager;
        $this->fileWriterHelper = $fileWriterHelper;
        $this->userRepo = $userRepo;
        $this->extractRepo = $extractRepo;

        $filesystem = new Filesystem();

        if (!$filesystem->exists($this->fileWriterHelper->getExtractDir())) {
            $filesystem->mkdir($this->fileWriterHelper->getExtractDir(), 0755);
        }
    }

    public function __invoke(ExtractMessage $extractMessage): void
    {
        $extractModel = $extractMessage->getExtractModel();
        $extract = $this->extractFactory->create($extractModel);

        if ($stream = $this->fileWriterHelper->getStream($this->fileWriterHelper->getExtractDir().$extract->getFilename())) {
            $rows = $extract->getRows();

            foreach ($rows as $row) {
                fputcsv($stream, $row, ';');
            }

            $this->fileWriterHelper->closeStream($stream);

            if (\count($rows) >= $extractModel->getMaxResults()) {
                $extractModel->setFirstResult($extractModel->getFirstResult() + $extractModel->getMaxResults());
                $this->bus->dispatch(new ExtractMessage($extractModel));
            } else {
                $user = $this->userRepo->findOneBy(['email' => $extract->getUser()->getEmail()]);
                $extractEntity = $this->extractRepo->findOneBy(
                    [
                        'filename' => $extract->getFilename(),
                        'user' => $user,
                        'generated' => false,
                    ]
                );

                if ($extractEntity) {
                    $extractEntity->setGenerated(true);
                    $this->em->flush();
                    $this->mailManager->sendExtractLink($extractEntity);
                }
            }
        } else {
            $this->bus->dispatch($extractMessage);
        }
    }
}
