<?php

declare(strict_types=1);

namespace App\DataTable;

interface DataTableResponseInterface
{
    public function configure($entity, array $options): void;

    public function getArrayResponse(): array;
}
