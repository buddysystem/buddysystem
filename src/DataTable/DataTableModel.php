<?php

declare(strict_types=1);

namespace App\DataTable;

use App\Model\DataModelInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class DataTableModel implements DataModelInterface
{
    private $parameters;
    private $sort;
    private $search;
    private $firstResult;
    private $maxResults;
    private $draw;

    public function __construct(RequestStack $requestStack)
    {
        if ($request = $requestStack->getCurrentRequest()) {
            $this->parameters = $request->query->all();
            $this->setSort();
            $this->setSearch();
            $this->setFirstResult();
            $this->setMaxResults();
            $this->setDraw();
        }
    }

    public function getSort(): array
    {
        return $this->sort;
    }

    public function setSort(array $sort = []): void
    {
        if (empty($sort) && \array_key_exists('order', $this->parameters)
            && \array_key_exists('columns', $this->parameters)) {
            $columnToSort = $this->parameters['order'][0]['column'];
            $this->sort = ($this->parameters['columns'][$columnToSort]['orderable'])
                ? [
                    'field' => $this->parameters['columns'][$columnToSort]['data'],
                    'dir' => $this->parameters['order'][0]['dir'],
                ]
                : []
            ;
        } else {
            $this->sort = $sort;
        }
    }

    public function getSearch(): array
    {
        return $this->search;
    }

    public function setSearch(array $search = []): void
    {
        if (empty($search) && \array_key_exists('columns', $this->parameters)) {
            $this->search = [];
            foreach ($this->parameters['columns'] as $column) {
                $columnName = $column['data'];

                $searchValue = $column['search']['value'];
                if ('' !== $searchValue) {
                    $this->search[$columnName] = (false === strpos($searchValue, ','))
                        ? $searchValue
                        : explode(',', $searchValue)
                    ;
                }
            }
        } else {
            $this->search = $search;
        }
    }

    public function getFirstResult(): int
    {
        return (int) $this->firstResult;
    }

    public function setFirstResult(int $firstResult = 0): void
    {
        $this->firstResult = (0 === $firstResult && \array_key_exists('start', $this->parameters))
            ? $this->parameters['start']
            : $this->firstResult = $firstResult
        ;
    }

    public function getMaxResults(): int
    {
        return (int) $this->maxResults;
    }

    public function setMaxResults(int $maxResults = 0): void
    {
        $this->maxResults = (0 === $maxResults && \array_key_exists('length', $this->parameters))
            ? $this->parameters['length']
            : $this->maxResults = $maxResults
        ;
    }

    public function getDraw(): int
    {
        return (int) $this->draw;
    }

    public function setDraw(int $draw = 0): void
    {
        $this->draw = (0 === $draw && \array_key_exists('draw', $this->parameters))
            ? $this->parameters['draw']
            : $this->draw = $draw
        ;
    }
}
