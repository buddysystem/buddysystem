<?php

declare(strict_types=1);

namespace App\DataTable;

use App\Services\GrantedService;
use App\Services\LocaleManager;
use App\Services\MatchingHelper;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class DataTableResponseFactory
{
    private $em;
    private $translator;
    private $security;
    private $router;
    private $templating;
    private $uploaderHelper;
    private $localeManager;
    private $matchingHelper;
    private $formFactory;
    private $matchingManagerHelper;
    private $grantedService;

    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        Security $security,
        UrlGeneratorInterface $router,
        Environment $templating,
        UploaderHelper $uploaderHelper,
        LocaleManager $localeManager,
        MatchingHelper $matchingHelper,
        FormFactoryInterface $formFactory,
        MatchingManagerHelper $matchingManagerHelper,
        GrantedService $grantedService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->security = $security;
        $this->router = $router;
        $this->templating = $templating;
        $this->uploaderHelper = $uploaderHelper;
        $this->localeManager = $localeManager;
        $this->matchingHelper = $matchingHelper;
        $this->formFactory = $formFactory;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->grantedService = $grantedService;
    }

    public function create(string $type): DataTableResponseInterface
    {
        return new $type(
            $this->em,
            $this->translator,
            $this->security,
            $this->router,
            $this->templating,
            $this->uploaderHelper,
            $this->localeManager,
            $this->matchingHelper,
            $this->formFactory,
            $this->matchingManagerHelper,
            $this->grantedService
        );
    }

    public function createDataTableResponse(string $type, $entity, array $options = []): array
    {
        if (class_exists($type)) {
            $interfaces = class_implements($type);

            if (isset($interfaces[DataTableResponseInterface::class])) {
                $response = $this->create($type);
                $response->configure($entity, $options);

                return $response->getArrayResponse();
            }
        }

        return [];
    }

    public function getResponse(string $type, array $entities, array $options = []): array
    {
        $response = [];

        foreach ($entities as $entity) {
            array_push($response, $this->createDataTableResponse($type, $entity, $options));
        }

        return $response;
    }
}
