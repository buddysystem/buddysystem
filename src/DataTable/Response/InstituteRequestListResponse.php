<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\InstituteRequest;

class InstituteRequestListResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof InstituteRequest) {
            $this->values['name'] = $entity->getName();
            $this->values['country'] = $entity->getCountry()->getName();
            $this->values['city'] = $entity->getCity() ? $entity->getCity()->getName() : $entity->getCityOther();
            $this->values['createdAt'] = $entity->getCreatedAt()->format('d/m/Y');
        }
    }
}
