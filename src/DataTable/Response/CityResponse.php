<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\City;
use App\Entity\User;
use Symfony\Component\Intl\Countries;

class CityResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof City) {
            $city = $entity;
            $userRepo = $this->em->getRepository(User::class);

            $this->values['name'] = $city->getName();
            $this->values['country'] = Countries::getName($city->getCountry()->getCode());
            $this->values['DT_RowAttr'] = ['data-id' => $city->getId()];

            $paramsActions = ['urlEdit' => $this->router->generate('city_edit', ['id' => $city->getId()])];
            if (empty($userRepo->findByCity($city))) {
                $paramsActions['deleteClass'] = 'city_delete';
            }

            $this->values['actions'] = $this->templating->render('Default/_list_actions.html.twig', $paramsActions);
        }
    }
}
