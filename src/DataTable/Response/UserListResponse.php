<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\Association;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\SemesterPeriodEnum;
use App\Enum\UserGenderEnum;
use App\Enum\UserLevelStudyEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserTypeMobilityEnum;
use App\Enum\UserVoterEnum;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Languages;

class UserListResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof User) {
            $user = $entity;
            $semester = $user->getSemester();

            $expanded = \array_key_exists('expanded', $options) && $options['expanded'];

            $isManager = $this->security->isGranted(UserVoterEnum::IS_MANAGER, $user);
            $roles = ucfirst(strtolower(str_replace('_', ' ', str_replace('ROLE_', '', $this->grantedService->getRole($user)))));

            $this->values['institute'] = '';
            if ($institute = $user->getInstitute()) {
                $this->values['institute'] = implode('<br>', array_map(function ($parent) {
                    return $parent->getName();
                }, $institute->getParents()));

                if ($institute->getParent()) {
                    $this->values['institute'] .= '<br>';
                }

                $this->values['institute'] .= $institute->getName();
            }

            if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
                if ($isManager) {
                    $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($user);

                    $this->values['association'] = $matchingManager instanceof Association
                        ? $matchingManager->getName()
                        : '';
                } else {
                    $this->values['association'] = implode('<br>', array_map(function ($m) {
                        return $m->getName();
                    }, $this->matchingManagerHelper->getAllMatchingManagerOfUser(
                        $user,
                        false,
                        false,
                        MatchingManagerInterface::TYPE_ASSOCIATION
                    )));
                }
            }

            $this->values['studies'] = !$expanded && $user->getStudies()->count() > 3
                ? $this->templating->render(
                    'Default/_list_accordion_cell.html.twig',
                    [
                        'id' => '_'.$user->getId().'-studies',
                        'objects' => $user->getStudies(),
                    ]
                )
                : implode('<br>', array_map(function ($study) {
                    return $study->getName();
                }, $user->getStudies()->getValues()));

            $readableLanguages = [];
            foreach ($user->getLanguages() as $language) {
                $readableLanguages[] = ucfirst(Languages::getName($language));
            }

            $this->values['languages'] = !$expanded && \count($readableLanguages) > 3
                ? $this->templating->render(
                    'Default/_list_accordion_cell.html.twig',
                    [
                        'id' => '_'.$user->getId().'-languages',
                        'objects' => $readableLanguages,
                    ]
                )
                : implode('<br>', array_map(function ($language) {
                    return $language;
                }, $readableLanguages));

            $readableLanguagesWanted = [];
            foreach ($user->getLanguagesWanted() as $language) {
                $readableLanguagesWanted[] = ucfirst(Languages::getName($language));
            }

            $this->values['languagesWanted'] = !$expanded && \count($readableLanguagesWanted) > 3
                ? $this->templating->render(
                    'Default/_list_accordion_cell.html.twig',
                    [
                        'id' => '_'.$user->getId().'-languages_wanted',
                        'objects' => $readableLanguagesWanted,
                    ]
                )
                : implode('<br>', array_map(function ($language) {
                    return $language;
                }, $readableLanguagesWanted));

            $this->values['firstname'] = $user->getFirstname();
            $this->values['lastname'] = $user->getLastname();
            $this->values['email'] = $user->getEmail();
            $this->values['semester'] = $semester ? $this->translator->trans(SemesterPeriodEnum::getReadableValue($semester->getPeriod())) : '';
            $this->values['year'] = $semester ? $semester->getYear() : '';
            $this->values['nationality'] = ($user->getNationality() && Countries::exists($user->getNationality())) ? Countries::getName($user->getNationality()) : '';
            $this->values['city'] = $user->getCity() ? $user->getCity()->getName() : '';
            $this->values['age'] = $user->getAge() ? $user->getAge() : '';
            $this->values['sex'] = !$isManager && $user->getSex() ? $this->translator->trans(UserGenderEnum::getReadableValue($user->getSex())) : '';
            $this->values['sexWanted'] = $isManager ? '' : $this->translator->trans($user->getSexWanted() ? 'yes' : 'no');
            $this->values['motivation'] = $user->getMotivationName();
            $this->values['local'] = $isManager ? '' : $this->translator->trans($user->isLocal() ? 'yes' : 'no');
            $this->values['arrival'] = $user->getArrival() ? $user->getArrival()->format('d/m/Y') : '';
            $this->values['createdAt'] = $user->getCreatedAt()->format('d/m/Y');
            $this->values['nbBuddies'] = $isManager ? '' : $user->getNbBuddies();
            $this->values['nbBuddiesWanted'] = $isManager ? '' : $user->getNbBuddiesWanted();
            $this->values['hasMaxBuddies'] = $isManager ? '' : $this->translator->trans($user->getNbBuddies() === $user->getNbBuddiesWanted() ? 'yes' : 'no');
            $this->values['nbReport'] = $user->getNbReport();
            $this->values['nbWarned'] = $user->getNbWarned();
            $this->values['nbBuddiesRefused'] = $user->getNbBuddiesRefused();
            $this->values['nbRefusedByBuddies'] = $user->getNbRefusedByBuddies();
            $this->values['nbBuddiesRemoved'] = $user->getNbBuddiesRemoved();
            $this->values['score'] = $user->getScore();
            $this->values['archived'] = $this->translator->trans($user->isArchived() ? 'yes' : 'no');
            $this->values['bonusOption'] = $this->translator->trans($user->isBonusOption() ? 'yes' : 'no');
            $this->values['roles'] = $roles;
            $this->values['typeOfMobility'] = $user->getTypeOfMobility()
                ? $this->translator->trans(UserTypeMobilityEnum::getReadableValue($user->getTypeOfMobility()))
                : '';

            if ($levelOfStudy = $user->getLevelOfStudy()) {
                $this->values['levelOfStudy'] = UserLevelStudyEnum::LEVEL_OTHER === $levelOfStudy
                    ? $user->getLevelOfStudyOther() ?? $this->translator->trans('user.level_of_study.other')
                    : $this->translator->trans(UserLevelStudyEnum::getReadableValue($levelOfStudy));
            } else {
                $this->values['levelOfStudy'] = '';
            }

            if (\array_key_exists('showActions', $options) && $options['showActions']) {
                $paramsAction = [];

                if ($this->security->isGranted(UserVoterEnum::SHOW, $user)) {
                    $paramsAction['urlEdit'] = $this->router->generate('user_edit', ['id' => $user->getId()]);
                    $paramsAction['reportClass'] = ($user->getNbReport() > 0) ? 'user_report' : null;
                    $paramsAction['deleteClass'] = 'user_delete';

                    if ($user->isApproved()) {
                        $paramsAction['urlMessage'] = $this->router->generate('messaging_homepage', ['_fragment' => 'new-thread', 'recipient' => $user->getId()]);
                    } else {
                        $paramsAction['approveClass'] = 'user_approve';
                    }

                    /*$paramsAction['urlMatch'] = (!$user->isLocal() && $this->security->isGranted(UserVoterEnum::MATCH, $user))
                        ? $this->router->generate('matching_step2', ['id' => $user->getId()])
                        : null
                    ;*/
                }

                $this->values['actions'] = $this->templating->render('Default/_list_actions.html.twig', $paramsAction);
            }

            $this->values['DT_RowAttr'] = ['data-id' => $user->getId()];
        }
    }
}
