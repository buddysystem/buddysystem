<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\Entity\Institute;
use App\Enum\AssociationInstituteVoterEnum;
use App\Form\AssociationInstituteRightType;

class InstituteRightResponse extends MatchingManagerListResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Institute) {
            parent::configure($entity, $options);
            $institute = $entity;

            $this->values['parents'] = implode('<br>', array_map(function ($parent) {
                return $parent->getName();
            }, $institute->getParents()));

            $this->values['association'] = '';
            $this->values['rights'] = '';

            foreach ($institute->getAssociationInstitutes() as $associationInstitute) {
                $association = $associationInstitute->getAssociation();

                if (!empty($this->values['association'])) {
                    $this->values['association'] .= '<br>';
                }
                $this->values['association'] .= '<div>'.$association->getName().'</div>';

                $isSelectorDisabled = !$this->security->isGranted(AssociationInstituteVoterEnum::CHANGE_RIGHTS, $associationInstitute);

                $form = $this->formFactory->create(AssociationInstituteRightType::class, $associationInstitute, ['disabled' => $isSelectorDisabled]);

                $this->values['rights'] .= $this->templating->render(
                    'Form/_association_institute_rights.html.twig',
                    [
                        'id' => $association->getId(),
                        'form' => $form->createView(),
                        'disabled' => $isSelectorDisabled,
                    ]
                );
            }
        }
    }
}
