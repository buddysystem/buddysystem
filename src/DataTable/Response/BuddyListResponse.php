<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\Buddy;
use App\Enum\BuddyRefuseEnum;
use App\Enum\BuddyStatusEnum;
use App\Enum\BuddyVoterEnum;
use App\Enum\SemesterPeriodEnum;

class BuddyListResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Buddy) {
            $buddy = $entity;
            $mentor = $buddy->getMentor();
            $mentee = $buddy->getMentee();
            $semester = $buddy->getSemester();

            $this->values['menteeInstitute'] = '';
            if ($institute = $mentee->getInstitute()) {
                $this->values['menteeInstitute'] = implode('<br>', array_map(function ($parent) {
                    return $parent->getName();
                }, $institute->getParents()));

                if ($institute->getParent()) {
                    $this->values['menteeInstitute'] .= '<br>';
                }

                $this->values['menteeInstitute'] .= $institute->getName();
            }

            $this->values['mentorInstitute'] = '';
            if ($institute = $mentor->getInstitute()) {
                $this->values['mentorInstitute'] = implode('<br>', array_map(function ($parent) {
                    return $parent->getName();
                }, $institute->getParents()));

                if ($institute->getParent()) {
                    $this->values['mentorInstitute'] .= '<br>';
                }

                $this->values['mentorInstitute'] .= $institute->getName();
            }

            $this->values['association'] = $buddy->getAssociation() ? $buddy->getAssociation()->getName() : '';
            $this->values['institute'] = $buddy->getInstitute()->getName();
            $this->values['status'] = $this->translator->trans(BuddyStatusEnum::getReadableValue($buddy->getStatus()));
            $this->values['createdAt'] = $buddy->getCreatedAt()->format('d/m/Y');
            $this->values['mentor'] = $mentor->getFullName();
            $this->values['mentorEmail'] = $mentor->getEmail();
            $this->values['mentee'] = $mentee->getFullName();
            $this->values['menteeEmail'] = $mentee->getEmail();
            $this->values['semester'] = $this->translator->trans(SemesterPeriodEnum::getReadableValue($semester->getPeriod()));
            $this->values['year'] = $semester->getYear();
            $this->values['languagePairing'] = $this->translator->trans($buddy->isLanguagePairing() ? 'yes' : 'no');
            $this->values['lastMessageAt'] = $buddy->getLastMessageAt() ? $buddy->getLastMessageAt()->format('d/m/Y') : '';
            $this->values['warnedAt'] = $buddy->getWarnedAt() ? $buddy->getWarnedAt()->format('d/m/Y') : '';
            $this->values['responseAt'] = $buddy->getResponseAt() ? $buddy->getResponseAt()->format('d/m/Y') : '';
            $this->values['archived'] = $this->translator->trans($buddy->isArchived() ? 'yes' : 'no');
            $this->values['reasonRefusal'] = !empty($buddy->getReasonRefusal()) ? BuddyRefuseEnum::REASON_REFUSAL_OTHER === $buddy->getReasonRefusal() && $buddy->getReasonRefusalOther() ? $buddy->getReasonRefusalOther() : $this->translator->trans(BuddyRefuseEnum::getReadableValue($buddy->getReasonRefusal())) : '';
            $this->values['actions'] = $this->security->getUser()
                ? $this->templating->render('Default/_list_actions.html.twig', [
                    'urlShow' => $this->router->generate('buddy_show', ['id' => $buddy->getId()]),
                    'reportClass' => !$buddy->getReports()->isEmpty() ? 'buddy_reports' : null,
                    'deleteClass' => $this->security->isGranted(BuddyVoterEnum::DELETE, $buddy) && !$buddy->isArchived() ? 'buddy_delete' : null,
                ])
                : []
            ;
            $this->values['DT_RowAttr'] = ['data-id' => $buddy->getId()];
        }
    }
}
