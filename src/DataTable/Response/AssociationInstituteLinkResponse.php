<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\Entity\AssociationInstitute;
use App\Entity\Institute;

class AssociationInstituteLinkResponse extends MatchingManagerListResponse
{
    public function configure($entity, array $options): void
    {
        parent::configure($entity, $options);

        if ($entity instanceof Institute
            && \array_key_exists('manager', $options) && null !== $options['manager']
        ) {
            $institute = $entity;

            $this->values['parents'] = implode('<br>', array_map(function ($parent) {
                return $parent->getName();
            }, $institute->getParents()));

            $associationInstituteRepo = $this->em->getRepository(AssociationInstitute::class);
            $associationInstitute = $associationInstituteRepo->findOneBy(
                [
                    'institute' => $institute,
                    'association' => $options['manager'],
                ]
            );

            $this->values['isAvailable'] = $this->templating->render(
                'Form/_toggle_button.html.twig',
                [
                    'object' => $institute,
                    'isChecked' => null !== $associationInstitute,
                ]
            );
        }
    }
}
