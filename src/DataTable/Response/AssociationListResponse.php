<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\Entity\Association;

class AssociationListResponse extends MatchingManagerListResponse
{
    public function configure($entity, array $options): void
    {
        parent::configure($entity, $options);

        if ($entity instanceof Association) {
            $association = $entity;

            $this->values['city'] = implode(
                '<br>',
                array_map(
                function ($city) {
                    return $city->getName();
                },
                $association->getCities()->getValues()
            )
            );
            $this->values['facebook'] = $association->getFacebook();
            $this->values['twitter'] = $association->getTwitter();
            $this->values['instagram'] = $association->getInstagram();
            $this->values['actions'] = $this->templating->render('Default/_list_actions.html.twig', [
                'urlEdit' => $this->router->generate('association_edit', ['id' => $association->getId()]),
            ]);
        }
    }
}
