<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\Entity\AssociationInstitute;
use App\Entity\Country;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\InstituteVoterEnum;

class InstituteListResponse extends MatchingManagerListResponse
{
    public function configure($entity, array $options): void
    {
        parent::configure($entity, $options);

        if ($entity instanceof Institute) {
            $institute = $entity;
            $instituteRepo = $this->em->getRepository(Institute::class);
            $countryRepo = $this->em->getRepository(Country::class);

            $this->values['level'] = $institute->getLevel();

            $this->values['matchingPreferences'] = $institute->isLowerLevelManagement()
                ? $this->matchingManagerHelper->getLowerLevelCriteriasManager($institute)->getName()
                : $this->translator->trans('institute.criteria.itself')
            ;

            $this->values['treeview'] = '';
            foreach ($institute->getParents() as $parent) {
                if ('' !== $this->values['treeview']) {
                    $this->values['treeview'] .= '<br>';
                }

                $this->values['treeview'] .= $this->translator->trans('institute.level').' '.$parent->getLevel().' : '.$parent->getName();
            }

            for ($level = $institute->getLevel(); $level <= Institute::MAX_LEVEL; ++$level) {
                $children = $instituteRepo->getByLevelAndParentId($level, $institute->getId());

                if (!empty($children)) {
                    if ('' !== $this->values['treeview']) {
                        $this->values['treeview'] .= '<br>';
                    }

                    $this->values['treeview'] .= $this->translator->trans('institute.level').' '.$level.' : ';
                    $this->values['treeview'] .= $this->templating->render(
                        'Default/_list_accordion_cell.html.twig',
                        [
                            'id' => '_'.$institute->getId().'-'.$level,
                            'objects' => $children,
                        ]
                    );
                }
            }

            $cities = $institute->getCitiesWithParents();
            $this->values['country'] = implode(
                '<br>',
                array_map(
                function ($country) {
                    return $country->getName();
                },
                $countryRepo->getByCities($this->localeManager->getCurrentLocale(), $cities)
            )
            );

            $this->values['city'] = \count($cities) > 3
                ? $this->templating->render(
                    'Default/_list_accordion_cell.html.twig',
                    [
                        'id' => '_'.$institute->getId().'-cities',
                        'objects' => $cities,
                    ]
                )
                : implode('<br>', array_map(function ($city) {
                    return $city->getName();
                }, $cities))
            ;

            $this->values['studies'] = $institute->getStudies()->count() > 3
                ? $this->templating->render(
                    'Default/_list_accordion_cell.html.twig',
                    [
                        'id' => '_'.$institute->getId().'-studies',
                        'objects' => $institute->getStudies(),
                    ]
                )
                : implode('<br>', array_map(function ($study) {
                    return $study->getName();
                }, $institute->getStudies()->getValues()))
            ;

            if (\array_key_exists('matchingManager', $options) && $options['matchingManager']) {
                $matchingManager = $options['matchingManager'];

                $this->values['rights'] = $this->getCanMatch($institute, $matchingManager);
            } else {
                $associations = $this->matchingManagerHelper->getMatchingManagersOfInstitute(
                    $institute,
                    false,
                    false,
                    MatchingManagerInterface::TYPE_ASSOCIATION
                );
                $this->values['association'] = implode('<br>', array_map(function ($asso) {
                    return $asso->getName();
                }, $associations))
                ;
            }

            $actions = [];

            if ($this->security->getUser()) {
                if ($this->security->isGranted(InstituteVoterEnum::EDIT, $institute)) {
                    $actions['urlEdit'] = $this->router->generate(
                        'institute_edit',
                        ['id' => $institute->getDuplicate() ? $institute->getDuplicate()->getId() : $institute->getId()]
                    );

                    if (!$institute->hasChildren()) {
                        $actions['urlToggleStudies'] = $this->router->generate('institute_study_link', ['id' => $institute->getId()]);
                    }
                }

                if ($this->security->isGranted(InstituteVoterEnum::EDIT_PREFERENCES, $institute)) {
                    $actions['urlInstitutePreference'] = $this->router->generate('institute_edit-preferences', ['id' => $institute->getId()]);
                }

                if ($this->security->isGranted(InstituteVoterEnum::DELETE, $institute)) {
                    $actions['deleteClass'] = 'institute_delete';
                }
            }

            $this->values['actions'] = $this->templating->render('Default/_list_actions.html.twig', $actions);
        }
    }

    private function getCanMatch(Institute $institute, MatchingManagerInterface $matchingManager): string
    {
        if ($matchingManager instanceof Institute) {
            $value = 'institute.rights.all';
        } else {
            $associationInstitute = $this->em->getRepository(AssociationInstitute::class)->findOneBy(
                [
                    'institute' => $institute,
                    'association' => $matchingManager,
                ]
            );

            $value = '';
            if ($associationInstitute) {
                if ($right = $associationInstitute->getUpperRight()) {
                    $value = AssociationInstitutePermEnum::getReadableValue($right);
                }
            }
        }

        return $this->translator->trans($value);
    }
}
