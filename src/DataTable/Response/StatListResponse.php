<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\Stat;
use App\Enum\SemesterPeriodEnum;
use App\Enum\StatTypeEnum;

class StatListResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof Stat) {
            $stat = $entity;
            $semester = $stat->getSemester();

            $this->values['semester'] = $this->translator->trans(SemesterPeriodEnum::getReadableValue($semester->getPeriod()));
            $this->values['year'] = $semester->getYear();
            $this->values['nbUsers'] = $stat->getNbUsers();
            $this->values['nbNewUsers'] = $stat->getNbNewUsers();
            $this->values['nbMentees'] = $stat->getNbMentees();
            $this->values['nbNewMentees'] = $stat->getNbNewMentees();
            $this->values['nbMenteesNotMatched'] = $stat->getNbMenteesNotMatched();
            $this->values['nbMentors'] = $stat->getNbMentors();
            $this->values['nbNewMentors'] = $stat->getNbNewMentors();
            $this->values['nbMentorsNotMatched'] = $stat->getNbMentorsNotMatched();
            $this->values['nbMatchConfirmed'] = $stat->getNbMatchConfirmed();
            $this->values['nbMatchRefused'] = $stat->getNbMatchRefused();
            $this->values['nbMatchWaiting'] = $stat->getNbMatchWaiting();
            $this->values['createdAt'] = $stat->getCreatedAt()->format('d/m/Y');
            $this->values['type'] = $this->translator->trans(StatTypeEnum::getReadableValue($stat->getType()));

            $this->values['institute'] = '';
            if ($institute = $stat->getInstitute()) {
                $this->values['institute'] = implode('<br>', array_map(function ($parent) {
                    return $parent->getName();
                }, $institute->getParents()));

                if ($institute->getParent()) {
                    $this->values['institute'] .= '<br>';
                }

                $this->values['institute'] .= $institute->getName();
            }
        }
    }
}
