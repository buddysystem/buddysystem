<?php

declare(strict_types=1);

namespace App\DataTable\Response;

use App\DataTable\AbstractDataTableResponse;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\UserRoleEnum;

abstract class MatchingManagerListResponse extends AbstractDataTableResponse
{
    public function configure($entity, array $options): void
    {
        if ($entity instanceof MatchingManagerInterface) {
            $manager = $entity;
            $userRepo = $this->em->getRepository(User::class);

            $this->values['name'] = $manager->getName();
            $this->values['website'] = $manager->getWebsite();
            $this->values['activated'] = $this->translator->trans($manager->isActivated() ? 'yes' : 'no');
            $this->values['logo'] = '<img src="'.$this->uploaderHelper->asset($manager, 'logoFile').'" width="75" >';
            $this->values['nbUsers'] = $userRepo->getNbUsers(
                [],
                $manager,
                UserRoleEnum::$managerAndAdmin
            );
            $this->values['DT_RowAttr'] = ['data-id' => $manager->getId()];
        }
    }
}
