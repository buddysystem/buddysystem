<?php

declare(strict_types=1);

namespace App\DataTable;

use App\Services\GrantedService;
use App\Services\LocaleManager;
use App\Services\MatchingHelper;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

abstract class AbstractDataTableResponse implements DataTableResponseInterface
{
    protected $em;
    protected $translator;
    protected $security;
    protected $router;
    protected $templating;
    protected $uploaderHelper;
    protected $values;
    protected $localeManager;
    protected $matchingHelper;
    protected $formFactory;
    protected $matchingManagerHelper;
    protected $grantedService;

    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        Security $security,
        UrlGeneratorInterface $router,
        Environment $templating,
        UploaderHelper $uploaderHelper,
        LocaleManager $localeManager,
        MatchingHelper $matchingHelper,
        FormFactoryInterface $formFactory,
        MatchingManagerHelper $matchingManagerHelper,
        GrantedService $grantedService
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->security = $security;
        $this->router = $router;
        $this->templating = $templating;
        $this->uploaderHelper = $uploaderHelper;
        $this->localeManager = $localeManager;
        $this->values = [];
        $this->matchingHelper = $matchingHelper;
        $this->formFactory = $formFactory;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->grantedService = $grantedService;
    }

    abstract public function configure($entity, array $options): void;

    public function getArrayResponse(): array
    {
        return $this->values;
    }
}
