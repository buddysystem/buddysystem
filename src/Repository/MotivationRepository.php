<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Motivation;
use App\Enum\SemesterPeriodEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class MotivationRepository extends ServiceEntityRepository
{
    use SemesterFilterTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Motivation::class);
    }

    public function getIdsByMatchingManager(MatchingManagerInterface $matchingManager): array
    {
        $qb = $this->createQueryBuilder('m', 'm.id')
            ->where('mm.id = :matching_manager')
            ->setParameter('matching_manager', $matchingManager->getId())
        ;

        if ($matchingManager instanceof Institute) {
            $qb->innerJoin('m.institutes', 'mm');
        } else {
            $qb->innerJoin('m.associations', 'mm');
        }

        return array_keys($qb->getQuery()->getArrayResult());
    }

    public function getStats(MatchingManagerInterface $matchingManager = null, array $othersPeriods = []): array
    {
        $qb = $this->createQueryBuilder('m')
            ->select('m.nameMentee, m.nameMentor, count(distinct u) as nbUsers')
            ->innerJoin('m.users', 'u')
            ->innerJoin('u.semester', 'semester')
            ->innerJoin('u.institute', 'i')
            ->leftJoin('i.ancestors', 'ancestor')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->where('u.enabled = :enabled')
            ->andWhere('u.approved = :approved')
            ->setParameter('enabled', true)
            ->setParameter('approved', true)
            ->groupBy('m.id')
            ->orderBy('nbUsers', 'DESC')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);
        $qb = $this->addSameSemesterFilter($qb, $othersPeriods);

        return $qb->getQuery()->getResult();
    }

    public function getAllSelected(MatchingManagerInterface $matchingManager, bool $isLocal): array
    {
        $qb = $this->createQueryBuilder('m')
            ->innerJoin('m.users', 'u')
            ->innerJoin('u.semester', 'semester')
            ->innerJoin('u.institute', 'i')
            ->leftJoin('i.ancestors', 'ancestor')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->andWhere('u.local = :isLocal')
            ->setParameter('isLocal', $isLocal)
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);
        $qb = $this->addSameSemesterFilter($qb, [SemesterPeriodEnum::PERIOD_ALL_YEAR]);

        return $qb->getQuery()->getResult();
    }

    private function addMatchingManagerFilter(
        QueryBuilder $qb,
        MatchingManagerInterface $matchingManager = null
    ): QueryBuilder {
        if ($matchingManager) {
            if ($matchingManager instanceof Association) {
                $where = 'a.id = :matching_manager';
            } else {
                $where = 'i.id = :matching_manager or ancestor.id = :matching_manager';
            }

            $qb
                ->andWhere($where)
                ->setParameter('matching_manager', $matchingManager->getId())
            ;
        }

        return $qb;
    }
}
