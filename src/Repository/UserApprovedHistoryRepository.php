<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\UserApprovedHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserApprovedHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserApprovedHistory::class);
    }
}
