<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class MessageRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Message::class);
        $this->security = $security;
    }

    public function getByThreads(array $threads, int $offset = 0, int $limit = 10): array
    {
        $qb = $this->createQueryBuilder('m')
            ->innerJoin('m.thread', 't')
            ->where('t.id in (:threads)')
            ->setParameter('threads', $threads)
            ->orderBy('m.createdAt', 'desc')
            ->setFirstResult($offset * $limit)
            ->setMaxResults($limit)
        ;

        return array_reverse($qb->getQuery()->getResult());
    }
}
