<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Semester;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class SemesterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Semester::class);
    }

    public function getDistinctYears(MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s.year')
            ->distinct()
            ->orderBy('s.year')
        ;

        if ($matchingManager) {
            $subQuery = $this->createQueryBuilder('semester')
                ->select('semester.id')
                ->innerJoin('semester.users', 'users')
                ->innerJoin('users.institute', 'institute')
                ->leftJoin('institute.ancestors', 'ancestor')
                ->leftJoin('institute.associationInstitutes', 'ai')
                ->leftJoin('ai.association', 'a')
                ->where('institute.id = :matching_manager or ancestor.id = :matching_manager or a.id = :matching_manager')
                ->getQuery()->getDQL();

            $qb
                ->where($qb->expr()->in('s.id', $subQuery))
                ->setParameter('matching_manager', $matchingManager->getId())
            ;
        }

        return array_map(function ($a) {
            return array_pop($a);
        }, $qb->getQuery()->getArrayResult());
    }

    public function getNbAvailableChoices(Institute $institute): int
    {
        return (int) $this->createQueryBuilder('s')
            ->select('count(s.id)')
            ->where('s.id > :idSemester')
            ->setParameter('idSemester', $institute->getSemester()->getId())
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
