<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Thread;
use App\Entity\ThreadUserManager;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class ThreadUserManagerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThreadUserManager::class);
    }

    public function getByUser(User $user): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.users', 'u')
            ->andWhere('u.id = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()->getResult()
        ;
    }

    public function getBySubjectAndMatchingManagers(string $subject, array $matchingManagers): array
    {
        $qb = $this->getBySubjectAndMatchingManagersQueryBuilder($subject, $matchingManagers)
            ->andWhere('u.archived = :archived')
            ->setParameter('archived', false)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getBySubjectMatchingManagersAndUser(string $subject, array $matchingManagers, User $user): ?Thread
    {
        $qb = $this->getBySubjectAndMatchingManagersQueryBuilder($subject, $matchingManagers)
            ->andWhere('u.id = :user')
            ->setParameter('user', $user->getId())
        ;

        return $qb->getQuery()->getOneOrNullResult();
    }

    private function getBySubjectAndMatchingManagersQueryBuilder(string $subject, array $matchingManagers): QueryBuilder
    {
        $qb = $this->createQueryBuilder('t')
            ->innerJoin('t.users', 'u')
            ->where('t.subject = :subject')
            ->setParameter('subject', $subject)
        ;

        foreach ($matchingManagers as $k => $matchingManager) {
            if ($matchingManager instanceof Association) {
                $qb->innerJoin('t.associations', "mm_{$k}");
            } else {
                $qb->innerJoin('t.institutes', "mm_{$k}");
            }

            $qb
                ->andWhere("mm_{$k} = :matching_manager_{$k}")
                ->setParameter("matching_manager_{$k}", $matchingManager)
            ;
        }

        return $qb;
    }
}
