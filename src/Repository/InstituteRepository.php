<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\City;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class InstituteRepository extends ServiceEntityRepository
{
    use SemesterFilterTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Institute::class);
    }

    public function findAll(string $orderBy = 'gen_order'): array
    {
        $qb = $this->createQueryBuilder('i');

        if ('gen_order' === $orderBy) {
            $qb
                ->leftJoin('i.root', 'root')
                ->leftJoin('i.parent', 'parent')
                ->addSelect("CONCAT(IF(root.id is null,'',CONCAT('/',root.id)), IF(parent.id is null or parent.id = root.id,'',CONCAT('/',parent.id)),'/',i.id) AS HIDDEN gen_order")
            ;
        }

        return $qb->orderBy($orderBy)->getQuery()->getResult();
    }

    public function getAllDuplicates(Institute $institute, bool $includeOriginal = true): array
    {
        if ($duplicate = $institute->getDuplicate()) {
            $institute = $duplicate;
        }

        $where = 'd.id = :institute';

        if ($includeOriginal) {
            $where .= ' or i.id = :institute';
        }

        return $this->createQueryBuilder('i')
            ->leftJoin('i.duplicate', 'd')
            ->where($where)
            ->setParameter('institute', $institute->getId())
            ->getQuery()
            ->getResult()
        ;
    }

    public function getMaxLevelManaged(MatchingManagerInterface $matchingManager): int
    {
        $qb = $this->getFilteredInstitutes([], $matchingManager)
            ->select('max(i.level)')
        ;

        $maxLevel = (int) $qb->getQuery()->getSingleScalarResult();

        if ($maxLevel < 1) {
            $maxLevel = 1;
        }

        return $maxLevel;
    }

    public function getByAssociationOrderByChildren(Association $association): array
    {
        return $this->createQueryBuilder('i')
            ->addSelect("CONCAT(IF(root.id is null,'',CONCAT('/',root.id)), IF(parent.id is null or parent.id = root.id,'',CONCAT('/',parent.id)),'/',i.id) AS HIDDEN gen_order")
            ->innerJoin('i.associationInstitutes', 'ai')
            ->innerJoin('ai.association', 'a')
            ->leftJoin('i.root', 'root')
            ->leftJoin('i.parent', 'parent')
            ->orderBy('gen_order')
            ->where('a.id = :association')
            ->setParameter('association', $association->getId())
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByMatchingManager(
        MatchingManagerInterface $matchingManager = null,
        bool $excludeMaxLevel = false,
        bool $canMatchOnly = false
    ): array {
        $qb = $this->sortByLevelAndName($this->getFilteredInstitutes([], $matchingManager, $canMatchOnly))
            ->andWhere('i.duplicate is null')
        ;

        if ($excludeMaxLevel) {
            $qb
                ->andWhere('i.level < :maxLevel')
                ->setParameter('maxLevel', Institute::MAX_LEVEL)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getListInfo(DataModelInterface $dataModel, MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->getFilteredInstitutes($dataModel->getSearch(), $matchingManager);

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('i.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort) && property_exists(Institute::class, $sort['field'])) {
            switch ($sort['field']) {
                default:
                    $selector = 'i.'.$sort['field'];
                    break;
            }
            $qb->orderBy($selector, $sort['dir']);
        } else {
            $qb = $this->sortByLevelAndName($qb);
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbInstitutes(array $search = [], MatchingManagerInterface $matchingManager = null): int
    {
        return (int) $this->getFilteredInstitutes($search, $matchingManager)
            ->select('count(distinct i.id)')
            ->getQuery()->getSingleScalarResult();
    }

    public function hasMatchableInstitute(MatchingManagerInterface $matchingManager): bool
    {
        $qb = $this->createQueryBuilder('i')
            ->select('count(distinct i.id)')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager, true);

        return (int) $qb->getQuery()->getSingleScalarResult() > 0;
    }

    public function getStats(MatchingManagerInterface $matchingManager = null, array $othersPeriods = []): array
    {
        $qb = $this->createQueryBuilder('i')
            ->select('i.name, count(distinct u) as nbUsers')
            ->leftJoin('i.users', 'u')
            ->leftJoin('u.semester', 'semester')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->where('u.enabled = :enabled')
            ->andWhere('u.approved = :approved')
            ->andWhere('i.duplicate is null or i.duplicate = i.id')
            ->setParameter('enabled', true)
            ->setParameter('approved', true)
            ->groupBy('i.id')
            ->orderBy('nbUsers', 'DESC')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);
        $qb = $this->addSameSemesterFilter($qb, $othersPeriods);

        $institutes = $qb->getQuery()->getResult();
        $result = ['labels' => [], 'series' => []];

        foreach ($institutes as $institute) {
            array_push($result['labels'], $institute['name']);
            array_push($result['series'], $institute['nbUsers']);
        }

        return $result;
    }

    public function getPotentialParentsQueryBuilder(
        Institute $institute,
        MatchingManagerInterface $matchingManager = null
    ): array {
        $qb = $this->sortByLevelAndName($this->createQueryBuilder('i'));

        $qb
            ->distinct()
            ->innerJoin('i.cities', 'city')
            ->leftJoin('i.ancestors', 'ancestor')
            ->leftJoin('ancestor.cities', 'cParent')
            ->where('(city.id in (:cities) and ancestor.id is null) or (city.id in (:cities) and cParent.id in (:cities))')
            ->setParameter('cities', $institute->getCities())
        ;

        if ($matchingManager) {
            $qb
                ->leftJoin('i.associationInstitutes', 'ai')
                ->leftJoin('ai.association', 'a')
                ->setParameter('matching_manager', $matchingManager->getId())
            ;

            if ($matchingManager instanceof Association) {
                $qb->andWhere('a.id = :matching_manager');
            } else {
                $qb->andWhere('i.id = :matching_manager or ancestor.id = :matching_manager');
            }
        }

        if ($this->_em->contains($institute)) {
            $qb
                ->andWhere('i.id <> :institute and (i.duplicate <> :institute or i.duplicate is null)')
                ->setParameter('institute', $institute->getId())
            ;
        }

        $levelsAllowed = [];
        if ($institute->getDepth() < Institute::MAX_LEVEL) {
            $levelsAllowed[] = 0;

            switch ($level = $institute->getLevel()) {
                case 0:
                    while ($level < Institute::MAX_LEVEL - 1) {
                        $levelsAllowed[] = ++$level;
                    }
                    break;
                case 1:
                    $levelsAllowed[] = 1;
                    break;
                default:
                    $levelsAllowed[] = $level - 1;
                    break;
            }
        }

        if (!empty($levelsAllowed)) {
            $qb
                ->andWhere('i.level in (:levelsAllowed)')
                ->setParameter('levelsAllowed', $levelsAllowed)
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getByLevelAndParentId(int $level, int $parentId): array
    {
        $institutes = $this->createQueryBuilder('i1')
            ->select('CASE WHEN duplicate.id is not null THEN duplicate.id ELSE i1.id END')
            ->leftJoin('i1.ancestors', 'ancestor')
            ->leftJoin('i1.duplicate', 'duplicate')
            ->where('i1.level = :level')
            ->andWhere('ancestor.id = :parentId')
        ;

        $qb = $this->createQueryBuilder('i')
            ->distinct()
            ->orderBy('i.name')
        ;

        return $qb->where($qb->expr()->in('i.id', $institutes->getQuery()->getDQL()))
            ->setParameter('level', $level)
            ->setParameter('parentId', $parentId)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getAllParents(Institute $institute): array
    {
        if ($duplicate = $institute->getDuplicate()) {
            $institute = $duplicate;
        }

        return $this->createQueryBuilder('i')
            ->distinct()
            ->innerJoin('i.children', 'child')
            ->where('child.id = :institute or child.duplicate = :institute')
            ->setParameter('institute', $institute->getId())
            ->getQuery()->getResult();
    }

    public function getByCity(
        City $city,
        Institute $parent = null,
        bool $includeDeactivated = false,
        MatchingManagerInterface $matchingManager = null,
        string $mode = null
    ): array {
        $qb = $this->createQueryBuilder('i')
            ->innerJoin('i.cities', 'city')
            ->leftJoin('i.parent', 'parent')
            ->leftJoin('i.children', 'child')
            ->leftJoin('child.cities', 'cityChild')
            ->where('city.id = :city')
            ->andWhere('child.id is null or cityChild.id = :city')
            ->setParameter('city', $city->getId())
            ->orderBy('i.name', 'ASC')
        ;

        if ($parent) {
            $where = 'parent.id = :parent';

            if (UserProfileTypeEnum::PROFILE_INSTITUTE === $mode) {
                $where .= ' or i.id = :parent';
                $qb
                    ->addSelect('IF(i.id = :parent, 1, 0) AS HIDDEN mainSort')
                    ->orderBy('mainSort', 'Desc')
                ;
            }

            $qb
                ->andWhere($where)
                ->setParameter('parent', $parent->getId())
            ;
        } else {
            $qb->andWhere('parent is null');
        }

        if (!$includeDeactivated) {
            $qb->andWhere('i.activated = :activated')->setParameter('activated', true);
        }

        if ($matchingManager) {
            if ($matchingManager instanceof Association) {
                $qb
                    ->leftJoin('i.associationInstitutes', 'ai')
                    ->leftJoin('ai.association', 'a')
                    ->andWhere('a.id = :matching_manager')
                ;
            } else {
                $qb
                    ->leftJoin('i.ancestors', 'ancestor')
                    ->andWhere('i.id = :matching_manager or ancestor.id = :matching_manager')
                ;
            }

            $qb->setParameter('matching_manager', $matchingManager->getId());
        }

        return $qb->getQuery()->getResult();
    }

    public function getWithManager(): array
    {
        return $this->createQueryBuilder('i')
            ->distinct()
            ->innerJoin('i.users', 'u')
            ->where('u.roles like :role_ri')
            ->setParameter('role_ri', '%'.UserRoleEnum::ROLE_INSTITUTE_MANAGER.'%')
            ->addOrderBy('i.name')
            ->getQuery()->getResult();
    }

    public function hasManagers(Institute $institute): bool
    {
        if ($this->_em->contains($institute)) {
            return 0 < (int) $this->createQueryBuilder('i')
                ->distinct()
                ->select('count(i.id)')
                ->innerJoin('i.users', 'u')
                ->where('u.roles like :role_ri')
                ->andWhere('i.id = :institute')
                ->setParameter('role_ri', '%'.UserRoleEnum::ROLE_INSTITUTE_MANAGER.'%')
                ->setParameter('institute', $institute->getId())
                ->addOrderBy('i.name')
                ->getQuery()->getSingleScalarResult()
            ;
        }

        return false;
    }

    private function getFilteredInstitutes(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        bool $canMatchOnly = false
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('i')
            ->distinct()
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->leftJoin('i.ancestors', 'ancestor')
            ->innerJoin('i.cities', 'city')
            ->leftJoin('i.users', 'u')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager, $canMatchOnly);

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'country':
                    $qb->innerJoin('city.country', 'country');

                    $selector = 'country.id';
                    break;
                case 'city':
                    $qb
                        ->leftJoin('i.parent', 'parent')
                        ->leftJoin('parent.cities', 'cityParent')
                    ;

                    if (\is_array($value)) {
                        $qb->andWhere('city.id in (:city) and (parent.id is null or cityParent.id in (:city))');
                    } else {
                        $qb->andWhere('city.id = :city and (parent.id is null or cityParent.id = :city)');
                    }

                    $qb->setParameter('city', $value);

                    $selector = null;
                    break;
                case 'studies':
                    $qb->leftJoin('i.studies', 'study');
                    $selector = 'study.id';
                    break;
                case 'association':
                    if (\is_array($value)) {
                        $qb->andWhere('a.id in (:matching_manager)');
                    } else {
                        $qb->andWhere('a.id = :matching_manager');
                    }

                    $qb->setParameter('matching_manager', $value);

                    $selector = null;
                    break;
                case 'treeview':
                    if (!\is_array($value)) {
                        $value = [$value];
                    }

                    $allDuplicates = [];
                    foreach ($value as $id) {
                        $allDuplicates = array_merge(
                            $allDuplicates,
                            $this->createQueryBuilder('i')
                                ->leftJoin('i.duplicate', 'd')
                                ->where('d.id = :institute or i.id = :institute')
                                ->setParameter('institute', $id)
                                ->getQuery()
                                ->getResult()
                        );
                    }

                    $qb
                        ->leftJoin('i.children', 'child')
                        ->leftJoin('i.copies', 'copy')
                        ->leftJoin('copy.children', 'copyChild')
                        ->andWhere('ancestor.id in (:levels) or copyChild.id in (:levels) or child.id in (:levels)')
                        ->setParameter('levels', $allDuplicates)
                    ;

                    $selector = null;
                    break;
                case 'parents':
                    $selector = 'ancestor.id';
                    break;
                default:
                    $selector = 'i.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }

    private function addMatchingManagerFilter(
        QueryBuilder $qb,
        MatchingManagerInterface $matchingManager = null,
        bool $canMatch = false
    ): QueryBuilder {
        if ($matchingManager) {
            if ($matchingManager instanceof Association) {
                $whereClause = 'a.id = :matching_manager';

                if ($canMatch) {
                    $qb
                        ->andWhere('JSON_CONTAINS(ai.rights, :can_match) = 1')
                        ->setParameter('can_match', '"'.AssociationInstitutePermEnum::PERM_MATCH.'"')
                    ;
                }
            } else {
                // distinct search & filters
                $qb->leftJoin('i.ancestors', 'ancestor2');

                $whereClause = 'i.id = :matching_manager or ancestor2.id = :matching_manager';
            }

            $qb
                ->andWhere($whereClause)
                ->setParameter('matching_manager', $matchingManager->getId())
            ;
        }

        return $qb;
    }

    private function sortByLevelAndName(QueryBuilder $qb): QueryBuilder
    {
        $levelOrder = [];
        for ($i = 1; $i <= Institute::MAX_LEVEL; ++$i) {
            $levelOrder[] = $i;
        }
        $levelOrder[] = 0;

        return $qb
            ->orderBy("FIELD(i.level, '".implode("', '", $levelOrder)."')")
            ->addOrderBy('i.name')
        ;
    }
}
