<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Country;
use App\Entity\MatchingManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Intl\Countries;

class CountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function getAllSorted(string $locale): array
    {
        return $this->sortByCountry(
            $this->createQueryBuilder('c')->getQuery()->getResult(),
            $locale
        );
    }

    public function getWithMatchingManager(
        string $locale,
        string $matchingManagerType,
        bool $includeDeactivated = false,
        MatchingManagerInterface $matchingManager = null
    ): array {
        $qb = $this->createQueryBuilder('co')
            ->distinct()
            ->innerJoin('co.cities', 'ci')
        ;

        if (MatchingManagerInterface::TYPE_INSTITUTE === $matchingManagerType) {
            $qb->innerJoin('ci.institutes', 'mm');
        } else {
            $qb->innerJoin('ci.associations', 'mm');
        }

        if (!$includeDeactivated) {
            $qb->andWhere('mm.activated = :activated')->setParameter('activated', true);
        }

        if ($matchingManager) {
            $qb
                ->andWhere('mm.id = :matchingManager')
                ->setParameter('matchingManager', $matchingManager->getId())
            ;
        }

        return $this->sortByCountry($qb->getQuery()->getResult(), $locale);
    }

    public function getByCities(string $locale, array $cities): array
    {
        $countries = $this->createQueryBuilder('country')
            ->innerJoin('country.cities', 'city')
            ->where('city.id in (:cities)')
            ->setParameter('cities', $cities)
            ->getQuery()
            ->getResult()
        ;

        return $this->sortByCountry($countries, $locale);
    }

    private function sortByCountry(array $countries, string $locale): array
    {
        foreach ($countries as &$country) {
            if (Countries::exists($country->getCode())) {
                $country->setName(ucfirst(Countries::getName($country->getCode(), $locale)));
            }
        }

        setlocale(\LC_ALL, $locale.'_'.strtoupper($locale).'.utf8');
        uasort($countries, function ($a, $b) {
            return strcoll($a->getName(), $b->getName());
        });

        return $countries;
    }
}
