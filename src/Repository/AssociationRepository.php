<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Enum\UserRoleEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class AssociationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Association::class);
    }

    public function findAll(): array
    {
        return $this->findBy([], ['name' => 'ASC']);
    }

    public function getListInfo(DataModelInterface $dataModel): array
    {
        $qb = $this->getFilteredManagers($dataModel->getSearch());

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('a.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort) && property_exists(Association::class, $sort['field'])) {
            $qb->orderBy('a.'.$sort['field'], $sort['dir']);
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbAssociations(array $search = []): int
    {
        $qb = $this->getFilteredManagers($search);
        $qb->select('count(distinct a.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function hasManagers(Association $association): bool
    {
        return 0 < (int) $this->createQueryBuilder('a')
            ->distinct()
            ->select('count(a.id)')
            ->innerJoin('a.users', 'u')
            ->where('u.roles like :role_bc')
            ->andWhere('a.id = :association')
            ->setParameter('role_bc', '%'.UserRoleEnum::ROLE_BUDDYCOORDINATOR.'%')
            ->setParameter('association', $association->getId())
            ->addOrderBy('a.name')
            ->getQuery()->getSingleScalarResult()
        ;
    }

    public function getWithManager(): array
    {
        return $this->createQueryBuilder('a')
            ->distinct()
            ->innerJoin('a.users', 'u')
            ->where('u.roles like :role_bc')
            ->setParameter('role_bc', '%'.UserRoleEnum::ROLE_BUDDYCOORDINATOR.'%')
            ->addOrderBy('a.name')
            ->getQuery()->getResult()
        ;
    }

    private function getFilteredManagers(array $search = []): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a')
            ->innerJoin('a.cities', 'c')
        ;

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'city':
                    $selector = 'c.id';
                    break;
                default:
                    $selector = 'a.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }
}
