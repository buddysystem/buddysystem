<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Study;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class StudyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Study::class);
    }

    public function getIdsByInstitute(Institute $institute): array
    {
        $qb = $this->createQueryBuilder('s', 's.id')
            ->innerJoin('s.institutes', 'i')
            ->where('i.id = :institute')
            ->setParameter('institute', $institute->getId())
        ;

        return array_keys($qb->getQuery()->getArrayResult());
    }

    public function getUsedByInstitutes(MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->createQueryBuilder('s')
            ->distinct()
            ->innerJoin('s.institutes', 'i')
            ->orderBy('s.name')
        ;

        if ($matchingManager) {
            $qb

                ->leftJoin('i.associationInstitutes', 'ai')
                ->leftJoin('ai.association', 'a')
                ->leftJoin('i.ancestors', 'ancestor')
                ->setParameter('matching_manager', $matchingManager->getId())
            ;

            if ($matchingManager instanceof Association) {
                $qb->andWhere('a.id = :matching_manager');
            } else {
                $qb->andWhere('i.id = :matching_manager or ancestor.id = :matching_manager');
            }
        }

        return $qb->getQuery()->getResult();
    }
}
