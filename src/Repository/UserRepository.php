<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\SemesterPeriodEnum;
use App\Enum\UserRoleEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Intl\Countries;

class UserRepository extends ServiceEntityRepository
{
    use SemesterFilterTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findByEmail(string $email, bool $onlyEnabled = false): ?User
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
        ;

        if ($onlyEnabled) {
            $qb
                ->andWhere('u.enabled = :enabled')
                ->setParameter('enabled', true)
            ;
        }

        return $qb
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getListInfo(DataModelInterface $dataModel, MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->getFilteredUsers(
            $dataModel->getSearch(),
            $matchingManager,
        );

        $qb
            ->setFirstResult($dataModel->getFirstResult())
            ->groupBy('u.id')
        ;

        if ($dataModel->getMaxResults()) {
            $qb->setMaxResults($dataModel->getMaxResults());
        }

        $qb = $this->addSorter($qb, $dataModel->getSort());

        return $qb->getQuery()->getResult();
    }

    public function getNbUsers(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        array $rolesToExclude = [],
        bool $canMatchOnly = false,
        bool $semesterFilter = false,
        array $othersPeriods = [SemesterPeriodEnum::PERIOD_ALL_YEAR]
    ): int {
        $qb = $this->getFilteredUsers(
            $search,
            $matchingManager,
            $rolesToExclude,
            $canMatchOnly,
            $semesterFilter,
            $othersPeriods
        );
        $qb->select('count(distinct u.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getListStudentsNotMatched(
        DataModelInterface $dataModel,
        MatchingManagerInterface $matchingManager = null,
        User $menteeToExclude = null
    ): array {
        $qb = $this->getStudentsToMatch($dataModel->getSearch(), $matchingManager, $menteeToExclude);

        if (!$menteeToExclude && $dataModel->getMaxResults()) {
            $qb->setFirstResult($dataModel->getFirstResult());
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('u.id')
            ;
        }

        $qb = $this->addSorter($qb, $dataModel->getSort());

        return $qb->getQuery()->getResult();
    }

    public function getNbUsersNotMatched(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        User $menteeToExclude = null
    ): int {
        $qb = $this->getStudentsToMatch($search, $matchingManager, $menteeToExclude);
        $qb->select('count(distinct u.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getUsersToUnarchive(MatchingManagerInterface $matchingManager, array $period, string $year): array
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->leftJoin('u.semester', 'semester')
            ->andWhere('u.archived = :archived')
            ->andWhere('semester.period in (:period)')
            ->andWhere('semester.year = :year')
            ->setParameter('archived', true)
            ->setParameter('period', $period)
            ->setParameter('year', $year)
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager, true);
        $qb = $this->addRoles($qb, UserRoleEnum::$managerAndAdmin);

        return $qb->getQuery()->getResult();
    }

    public function getUsersToArchive(Institute $institute): array
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->leftJoin('u.semester', 'semester')
            ->andWhere('u.archived = :archived')
            ->setParameter('archived', false)
        ;

        $qb = $this->addMatchingManagerFilter($qb, $institute, true, false);
        $qb = $this->addSameSemesterFilter(
            $qb,
            SemesterPeriodEnum::PERIOD_SECOND_SEMESTER === $institute->getSemester()->getPeriod()
            ? [SemesterPeriodEnum::PERIOD_ALL_YEAR]
            : []
        );

        $qb = $this->addRoles($qb, UserRoleEnum::$managerAndAdmin);

        return $qb->getQuery()->getResult();
    }

    public function getGrantedManager(MatchingManagerInterface $matchingManager = null): array
    {
        $roles = UserRoleEnum::$managers;

        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.institute', 'i')
            ->leftJoin('u.association', 'a')
            ->leftJoin('a.associationInstitutes', 'ai')
            ->where('u.approved = :approved')
            ->andWhere('u.enabled = :enabled')
            ->setParameter('approved', true)
            ->setParameter('enabled', true)
        ;

        $qb = $this->addRoles($qb, $roles, true);
        $qb = $this->addMatchingManagerFilter($qb, $matchingManager, true, false);

        return $qb->getQuery()->getResult();
    }

    public function getUsersSearchAutocomplete(
        string $search,
        int $limit = null,
        int $offset = null,
        User $userToExclude = null,
        MatchingManagerInterface $matchingManager = null
    ): array {
        $qb = $this->createQueryBuilder('u')
            ->distinct()
            ->leftJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->where("u.firstname like :search or u.lastname like :search or u.email like :search or concat(u.firstname, ' ', u.lastname) like :search")
            ->andWhere('u.enabled = :enabled')
            ->andWhere('u.archived = :archived')
            ->setParameter('search', $search.'%')
            ->setParameter('enabled', true)
            ->setParameter('archived', false)
        ;

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($userToExclude instanceof User) {
            $qb
                ->andWhere('u.id <> :user')
                ->setParameter('user', $userToExclude->getId())
            ;
        }

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);

        return $qb->getQuery()->getResult();
    }

    public function getNbWeekUsers(
        string $startDate,
        MatchingManagerInterface $matchingManager = null,
        array $rolesToExclude = [],
        bool $isLocal = null
    ): int {
        $qb = $this->createQueryBuilder('u')
            ->select('count(distinct u.id)')
            ->innerJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->andWhere('u.archived = :archived')
            ->andWhere('u.createdAt >= :start_date')
            ->setParameter('archived', false)
            ->setParameter('start_date', $startDate)
        ;

        if (null !== $isLocal) {
            $qb
                ->andWhere('u.local = :local')
                ->setParameter('local', $isLocal)
            ;
        }

        $qb = $this->addRoles($qb, $rolesToExclude);
        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getNationalitiesByMatchingManager(MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->createQueryBuilder('u', 'u.nationality')
            ->select('u.nationality')
            ->innerJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->where('u.enabled = :enabled')
            ->andWhere('u.nationality is not null')
            ->setParameter('enabled', true)
            ->groupBy('u.nationality')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);
        $nationalityCodes = array_keys($qb->getQuery()->getResult());

        $nationalities = [];
        foreach ($nationalityCodes as $nationality) {
            if (Countries::exists($nationality)) {
                $nationalities[$nationality] = Countries::getName($nationality);
            }
        }

        return $nationalities;
    }

    public function getNationalitiesStats(
        MatchingManagerInterface $matchingManager = null,
        array $othersPeriods = []
    ): array {
        $qb = $this->createQueryBuilder('u')
            ->select('u.nationality as code, count(distinct u) as nbUsers')
            ->innerJoin('u.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->innerJoin('u.semester', 'semester')
            ->where('u.enabled = :enabled')
            ->andWhere('u.approved = :approved')
            ->andWhere('u.nationality is not null')
            ->setParameter('enabled', true)
            ->setParameter('approved', true)
            ->groupBy('u.nationality')
            ->orderBy('nbUsers', 'DESC')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);
        $qb = $this->addSameSemesterFilter($qb, $othersPeriods);

        $nationalities = $qb->getQuery()->getResult();
        $result = ['labels' => [], 'series' => []];

        foreach ($nationalities as $nationality) {
            array_push($result['labels'], $nationality['code']);
            array_push($result['series'], $nationality['nbUsers']);
        }

        return $result;
    }

    /**
     * @param int $interval in months
     * @param int $limit    max users to return
     *
     * @throws \Exception
     */
    public function getInactiveNonAnonymizedUsers(int $interval, int $limit = 10000): array
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval('P'.$interval.'M'));

        return $this->createQueryBuilder('u')
            ->where('(u.lastLogin < :sinceDate) OR (u.lastLogin IS NULL AND u.createdAt < :sinceDate)')
            ->setParameter('sinceDate', $date->format('Y-m-d H:i:s'))
            ->andWhere('u.email NOT LIKE \'%*%\'') // firstname could be null but the user is not anonymized
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByMatchingManager(MatchingManagerInterface $matchingManager, bool $isLocal): array
    {
        return $this->getFilteredUsers(
            [
                'local' => $isLocal,
            ],
            $matchingManager,
            UserRoleEnum::$managerAndAdmin,
            true
        )->getQuery()->getResult();
    }

    private function getFilteredUsers(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        array $rolesToExclude = [],
        bool $canMatch = false,
        bool $semesterFilter = false,
        array $othersPeriods = [SemesterPeriodEnum::PERIOD_ALL_YEAR]
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('u', 'u.id')
            ->distinct()
            ->leftJoin('u.institute', 'i')
            ->leftJoin('i.ancestors', 'ancestor')
            ->leftJoin('i.duplicate', 'duplicate')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->leftJoin('u.association', 'a2')
            ->leftJoin('u.semester', 'semester')
            ->where('u.enabled = :enabled')
            ->setParameter('enabled', true)
        ;

        if (!empty($search)) {
            $qb
                ->leftJoin('u.studies', 's')
                ->leftJoin('u.city', 'c')
                ->leftJoin('u.motivation', 'm')
            ;
        } else {
            $search = [];
        }

        if (!\array_key_exists('approved', $search)) {
            $search['approved'] = 1;
        }

        if ($matchingManager) {
            $qb = $this->addMatchingManagerFilter($qb, $matchingManager, $canMatch);
            $rolesToExclude = array_unique(array_merge($rolesToExclude, UserRoleEnum::$admins));
        }

        if ($semesterFilter) {
            $qb = $this->addSameSemesterFilter($qb, $othersPeriods);
        }

        $qb = $this->addRoles($qb, $rolesToExclude);

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'city':
                    $selector = 'c.id';
                    break;
                case 'studies':
                    $selector = 's.id';
                    break;
                case 'institute':
                    if (\is_array($value)) {
                        $qb->andWhere('i.id in (:institute) or ancestor.id in (:institute) or duplicate.id in (:institute)');
                    } else {
                        $qb->andWhere('i.id = :institute or ancestor.id = :institute or duplicate.id = :institute');
                    }

                    $qb->setParameter('institute', $value);
                    $selector = null;
                    break;
                case 'languages':
                    if (!\is_array($value)) {
                        $value = [$value];
                    }

                    foreach ($value as $k => $code) {
                        $qb
                            ->andWhere('JSON_CONTAINS(u.languages, :code_'.$k.') = 1')
                            ->setParameter('code_'.$k, '"'.$code.'"')
                        ;
                    }

                    $selector = null;
                    break;
                case 'languagesWanted':
                    if (!\is_array($value)) {
                        $value = [$value];
                    }

                    foreach ($value as $k => $code) {
                        $qb
                            ->andWhere('JSON_CONTAINS(u.languagesWanted, :code_'.$k.') = 1')
                            ->setParameter('code_'.$k, '"'.$code.'"')
                        ;
                    }

                    $selector = null;
                    break;
                case 'association':
                    if (\is_array($value)) {
                        $qb->andWhere('a.id in (:association) or a2.id in (:association)');
                    } else {
                        $qb->andWhere('a.id = :association or a2.id = :association');
                    }

                    $qb->setParameter('association', $value);

                    $selector = null;
                    break;
                case 'isProfileCompleted':
                    if ($value) {
                        $operator = ' and ';
                        $value = ' IS NOT NULL ';
                    } else {
                        $operator = ' or ';
                        $value = ' IS NULL ';
                    }
                    $qb->andWhere('u.firstname'.$value.$operator.
                        'u.institute'.$value.$operator.
                        'u.arrival'.$value);
                    $selector = null;
                    break;
                case 'semester':
                    $selector = 'semester.period';
                    break;
                case 'year':
                    $selector = 'semester.year';
                    break;
                case 'motivation':
                    $selector = 'm.id';
                    break;
                case 'hasMaxBuddies':
                    if ($value) {
                        $operator = '=';
                    } else {
                        $operator = '<>';
                    }
                        $qb->andWhere('u.nbBuddies '.$operator.' u.nbBuddiesWanted');

                    $selector = null;
                    break;
                case 'local':
                    $qb
                        ->andWhere('u.local = :local')
                        ->setParameter('local', $value)
                    ;
                    $selector = null;
                    break;
                default:
                    $selector = 'u.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = 'roles' == $column && UserRoleEnum::ROLE_USER == $val
                                ? 'a:0:{}'
                                : '%'.$val.'%'
                            ;
                        }
                        unset($val);
                    } else {
                        $value = 'roles' == $column && UserRoleEnum::ROLE_USER == $value
                            ? 'a:0:{}'
                            : '%'.$value.'%'
                        ;
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }

    private function getStudentsToMatch(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        User $menteeToExclude = null
    ): QueryBuilder {
        $qb = $this->getFilteredUsers(
            array_merge($search, ['archived' => 0]),
            $matchingManager,
            UserRoleEnum::$managerAndAdmin,
            true,
            true
        );

        $qb
            ->distinct()
            ->andWhere('u.firstname IS NOT NULL')
            ->andWhere('u.semester IS NOT NULL')
            ->andWhere('u.institute IS NOT NULL')
            ->andWhere('u.nbBuddies < u.nbBuddiesWanted')
            ->andWhere('i.nbBuddiesRefusedMax <= 0 or u.nbBuddiesRefused < i.nbBuddiesRefusedMax or u.overrideMaxRefusedLimitation = :overrideMaxRefusedLimitation')
            ->setParameter('overrideMaxRefusedLimitation', true)
        ;

        if ($menteeToExclude) {
            $subquery = $this->createQueryBuilder('mentor')
                ->select('mentor.id')
                ->innerJoin('mentor.mentors', 'b')
                ->innerJoin('b.mentee', 'mentee')
                ->where('mentee.id = :menteeId')
                ->getQuery()->getDQL();

            $qb
                ->andWhere($qb->expr()->notIn('u.id', $subquery))
                ->setParameter('menteeId', $menteeToExclude->getId())
            ;
        }

        return $qb;
    }

    private function addRoles(QueryBuilder $qb, array $roles, bool $like = false): QueryBuilder
    {
        if (!empty($roles)) {
            if ($like) {
                $mode = 'LIKE';
                $method = ' or ';
            } else {
                $mode = 'NOT LIKE';
                $method = ' and ';
            }

            $where = '';
            foreach ($roles as $key => $role) {
                if (!empty($where)) {
                    $where .= $method;
                }

                $where .= 'u.roles '.$mode.' :role_'.$key;

                $qb->setParameter('role_'.$key, '%'.$role.'%');
            }

            $qb->andWhere($where);
        }

        return $qb;
    }

    private function addMatchingManagerFilter(
        QueryBuilder $qb,
        MatchingManagerInterface $matchingManager = null,
        bool $canMatch = false,
        bool $includeChildren = true
    ): QueryBuilder {
        if ($matchingManager) {
            if ($matchingManager instanceof Association) {
                $whereClause = 'a.id = :matching_manager';

                if ($canMatch) {
                    $qb
                        ->andWhere('JSON_CONTAINS(ai.rights, :can_match) = 1')
                        ->setParameter('can_match', '"'.AssociationInstitutePermEnum::PERM_MATCH.'"')
                    ;
                } else {
                    $qb->leftJoin('u.association', 'association');
                    $whereClause .= ' or association.id = :matching_manager';
                }
            } else {
                $whereClause = 'i.id = :matching_manager';

                if ($includeChildren) {
                    // distinct search & filters
                    $qb->leftJoin('i.ancestors', 'ancestor2');

                    $whereClause .= ' or ancestor2.id = :matching_manager';
                }
            }

            $qb
                ->andWhere($whereClause)
                ->setParameter('matching_manager', $matchingManager->getId())
                ->innerJoin('u.city', 'city')
                ->andWhere('city.id in (:cities)')
                ->setParameter('cities', array_map(function ($city) {
                    return $city->getId();
                }, $matchingManager->getCities()->getValues()))
            ;
        }

        return $qb;
    }

    private function addSorter(QueryBuilder $qb, array $sort): QueryBuilder
    {
        if (!empty($sort)) {
            switch ($sort['field']) {
                case 'institute':
                    $selector = 'i.name';
                    break;
                case 'semester':
                    $selector = 'semester.period';
                    break;
                case 'year':
                    $selector = 'semester.year';
                    break;
                case 'motivation':
                    $selector = 'm.id';
                    break;
                default:
                    $selector = 'score' !== $sort['field']
                        ? 'u.'.$sort['field']
                        : null
                    ;
                    break;
            }

            if ($selector) {
                $qb->orderBy($selector, $sort['dir']);
            }
        }

        return $qb;
    }
}
