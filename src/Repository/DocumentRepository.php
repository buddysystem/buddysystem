<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Document;
use App\Services\LocaleManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DocumentRepository extends ServiceEntityRepository
{
    private $localeManager;

    public function __construct(ManagerRegistry $registry, LocaleManager $localeManager)
    {
        $this->localeManager = $localeManager;
        parent::__construct($registry, Document::class);
    }

    public function getDocuments(
        array $matchingManagers = [],
        string $recipient = null,
        bool $applyLocaleFilter = true
    ): array {
        $qb = $this->createQueryBuilder('document');

        if ($applyLocaleFilter) {
            $qb
                ->where('document.locale = :locale or document.locale = :defaultLocale')
                ->setParameter('locale', $this->localeManager->getCurrentLocale())
                ->setParameter('defaultLocale', $this->localeManager->getDefaultLocale())
            ;
        }

        if (!empty($matchingManagers)) {
            $where = '';
            foreach ($matchingManagers as $k => $matchingManager) {
                if ($matchingManager instanceof Association) {
                    $qb->leftJoin('document.association', "mm_{$k}");
                } else {
                    $qb->leftJoin('document.institute', "mm_{$k}");
                }

                if (!empty($where)) {
                    $where .= ' or ';
                }

                $where .= "mm_{$k}.id = :matchingManager_{$k}";
                $qb->setParameter("matchingManager_{$k}", $matchingManager);
            }

            $qb->andWhere($where);
        } else {
            $qb->andWhere('document.association is null and document.institute is null');
        }

        if ($recipient) {
            $qb
                ->andWhere('JSON_CONTAINS(document.recipients, :recipient) = 1')
                ->setParameter('recipient', '"'.$recipient.'"')
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
