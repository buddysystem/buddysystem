<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Stat;
use App\Enum\SemesterPeriodEnum;
use App\Enum\StatTypeEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class StatRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stat::class);
    }

    public function getLastWeeksValues(
        MatchingManagerInterface $matchingManager = null,
        bool $isYearStats = false
    ): array {
        $qb = $this->getWeekStatBySemesterBuilder($matchingManager, $isYearStats);

        if ($matchingManager) {
            $qb->select('stat.nbUsers,
                                stat.nbNewUsers,
                                stat.nbMentees,
                                stat.nbNewMentees,
                                stat.nbMenteesNotMatched,
                                stat.nbMentors,
                                stat.nbNewMentors,
                                stat.nbMentorsNotMatched,
                                stat.nbMatchConfirmed,
                                stat.nbMatchRefused,
                                stat.nbMatchWaiting,
                                stat.createdAt')
                ;
        } else {
            $qb
                ->select('sum(stat.nbUsers) as nbUsers,
                                sum(stat.nbNewUsers) as nbNewUsers,
                                sum(stat.nbMentees) as nbMentees,
                                sum(stat.nbNewMentees) as nbNewMentees,
                                sum(stat.nbMenteesNotMatched) as nbMenteesNotMatched,
                                sum(stat.nbMentors) as nbMentors,
                                sum(stat.nbNewMentors) as nbNewMentors,
                                sum(stat.nbMentorsNotMatched) as nbMentorsNotMatched,
                                sum(stat.nbMatchConfirmed) as nbMatchConfirmed,
                                sum(stat.nbMatchRefused) as nbMatchRefused,
                                sum(stat.nbMatchWaiting) as nbMatchWaiting')
                ->addSelect("DATE_FORMAT(stat.createdAt, '%Y-%m-%d') as createdAt")
                ->groupBy('createdAt')
            ;
        }

        return $qb->getQuery()->getResult();
    }

    public function getListInfo(DataModelInterface $dataModel, MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->getFilteredStats($dataModel->getSearch(), $matchingManager);

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('stat.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort)) {
            switch ($sort['field']) {
                case 'association':
                    $selector = 'a.name';
                    break;
                case 'institute':
                    $selector = 'i.name';
                    break;
                case 'semester':
                    $selector = 'semester.period';
                    break;
                case 'year':
                    $selector = 'semester.year';
                    break;
                default:
                    $selector = 'stat.'.$sort['field'];
                    break;
            }

            if ($selector) {
                $qb->orderBy($selector, $sort['dir']);
            }
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbStats(array $search = [], MatchingManagerInterface $matchingManager = null): int
    {
        $qb = $this->getFilteredStats($search, $matchingManager);
        $qb->select('count(distinct stat.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getDistinctYears(MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->createQueryBuilder('stat')
            ->select('s.year')
            ->distinct()
            ->innerJoin('stat.institute', 'i')
            ->innerJoin('stat.semester', 's')
        ;

        if ($matchingManager) {
            if ($matchingManager instanceof Institute) {
                $qb->andWhere('i.id = :matching_manager');
            } else {
                $qb
                    ->innerJoin('i.associationInstitutes', 'ai')
                    ->innerJoin('ai.association', 'a')
                    ->andWhere('a.id = :matching_manager')
                ;
            }

            $qb->setParameter('matching_manager', $matchingManager->getId());
        }

        return array_map(function ($a) {
            return array_pop($a);
        }, $qb->getQuery()->getArrayResult());
    }

    public function getDateOfFirstStat(
        MatchingManagerInterface $matchingManager = null,
        bool $isYearStats = false
    ): ?string {
        $qb = $this->getWeekStatBySemesterBuilder($matchingManager, $isYearStats)
            ->setMaxResults(1)
        ;

        /** @var Stat $stat */
        $stat = $qb->getQuery()->getOneOrNullResult();

        if ($stat) {
            return $stat->getCreatedAt()->format('Y-m-d');
        }

        return null;
    }

    private function getFilteredStats(
        array $search = [],
        MatchingManagerInterface $matchingManager = null
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('stat')
            ->innerJoin('stat.semester', 'semester')
            ->leftJoin('stat.institute', 'i')
            ->leftJoin('i.associationInstitutes', 'ai')
            ->leftJoin('ai.association', 'a')
            ->leftJoin('i.ancestors', 'ancestor')
        ;

        if ($matchingManager) {
            if ($matchingManager instanceof Institute) {
                $qb->andWhere('i.id = :matching_manager');
            } else {
                $qb->andWhere('a.id = :matching_manager');
            }

            $qb->setParameter('matching_manager', $matchingManager->getId());
        }

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'institute':
                    if (\is_array($value)) {
                        $qb->andWhere('i.id in (:institute) or ancestor.id in (:institute)');
                    } else {
                        $qb->andWhere('i.id = :institute or ancestor.id = :institute');
                    }

                    $qb->setParameter('institute', $value);

                    $selector = null;
                    break;
                case 'semester':
                    $selector = 'semester.period';
                    break;
                case 'year':
                    $selector = 'semester.year';
                    break;
                default:
                    $selector = 'stat.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }

    private function getWeekStatBySemesterBuilder(
        MatchingManagerInterface $matchingManager = null,
        bool $isYearStats = false
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('stat')
            ->innerJoin('stat.semester', 'semester')
            ->innerJoin('stat.institute', 'i', 'WITH', 'i.activated = :activated')
            ->innerJoin('i.semester', 'instituteSemester')
            ->where('stat.type = :type')
            ->andWhere('semester.year = instituteSemester.year')
            ->setParameter('type', StatTypeEnum::TYPE_WEEK)
            ->setParameter('activated', true)
            ->orderBy('stat.createdAt', 'ASC')
        ;

        if ($matchingManager) {
            if ($matchingManager instanceof Institute) {
                $qb->andWhere('i.id = :matching_manager');
            } else {
                $qb
                    ->innerJoin('i.associationInstitutes', 'ai')
                    ->innerJoin('ai.association', 'a')
                    ->andWhere('a.id = :matching_manager')
                ;
            }

            $qb->setParameter('matching_manager', $matchingManager->getId());
        }

        $periodWhere = 'semester.period = instituteSemester.period';

        if ($isYearStats) {
            $periodWhere .= ' or semester.period = :period';
            $qb->setParameter('period', SemesterPeriodEnum::PERIOD_FIRST_SEMESTER);
        }

        $qb->andWhere($periodWhere);

        return $qb;
    }
}
