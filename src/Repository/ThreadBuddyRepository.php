<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ThreadBuddy;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ThreadBuddyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThreadBuddy::class);
    }

    public function getByUser(User $user): array
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.users', 'u')
            ->andWhere('u.id = :user')
            ->setParameter('user', $user->getId())
            ->getQuery()->getResult()
        ;
    }
}
