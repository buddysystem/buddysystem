<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\QueryBuilder;

trait SemesterFilterTrait
{
    public function addSameSemesterFilter(QueryBuilder $qb, array $othersPeriods = []): QueryBuilder
    {
        $qb
            ->innerJoin('i.semester', 'instituteSemester')
            ->andWhere('semester.year = instituteSemester.year')
            ->setParameter('fullYearOnly', true)
        ;

        $periodWhere = 'i.fullYearOnly = :fullYearOnly or semester.period = instituteSemester.period';

        foreach ($othersPeriods as $k => $semester) {
            $periodWhere .= ' or semester.period = :semester_'.$k;
            $qb->setParameter('semester_'.$k, $semester);
        }

        $qb->andWhere($periodWhere);

        return $qb;
    }
}
