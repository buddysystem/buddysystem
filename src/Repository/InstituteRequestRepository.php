<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\InstituteRequest;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class InstituteRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InstituteRequest::class);
    }

    public function getListInfo(DataModelInterface $dataModel): array
    {
        $qb = $this->getFilteredRequests($dataModel->getSearch());

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('ir.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort) && property_exists(InstituteRequest::class, $sort['field'])) {
            switch ($sort['field']) {
                default:
                    $selector = 'ir.'.$sort['field'];
                    break;
            }
            $qb->orderBy($selector, $sort['dir']);
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbRequests(array $search = []): int
    {
        return (int) $this->getFilteredRequests($search)
            ->select('count(distinct ir.id)')
            ->getQuery()->getSingleScalarResult()
       ;
    }

    private function getFilteredRequests(array $search): QueryBuilder
    {
        $qb = $this->createQueryBuilder('ir');

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'country':
                    $qb->innerJoin('ir.country', 'country');
                    $selector = 'country.id';
                    break;
                case 'city':
                    $qb->leftJoin('ir.city', 'city');

                    $selector = 'city.id';
                    break;
                default:
                    $selector = 'ir.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }
}
