<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\City;
use App\Entity\Country;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Enum\UserProfileTypeEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    public function getAllSortedByCountryAndName(): array
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.country', 'country')
            ->orderBy('country.name', 'asc')
            ->addOrderBy('c.name', 'asc')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getUsedByManager(string $type, MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->createQueryBuilder('c')
            ->distinct()
            ->innerJoin('c.'.$type.'s', 'mm')
            ->orderBy('c.name')
        ;

        if ($matchingManager) {
            $qb
                ->leftJoin(Institute::class, 'i', 'WITH', 'i.id = mm.id')
                ->leftJoin('i.ancestors', 'ancestor')
                ->leftJoin('i.associationInstitutes', 'ai')
                ->leftJoin('ai.association', 'a')
                ->setParameter('matching_manager', $matchingManager->getId())
            ;

            if ($matchingManager instanceof Association) {
                $qb->andWhere('a.id = :matching_manager');
            } else {
                $qb->andWhere('i.id = :matching_manager or ancestor.id = :matching_manager');
            }
        }

        return $qb->getQuery()->getResult();
    }

    public function getIdsByMatchingManager(MatchingManagerInterface $matchingManager): array
    {
        $qb = $this->createQueryBuilder('c', 'c.id')
            ->select('c.id')
            ->where('mm.id = :matching_manager')
            ->setParameter('matching_manager', $matchingManager->getId())
        ;

        if ($matchingManager instanceof Institute) {
            $qb->innerJoin('c.institutes', 'mm');
        } else {
            $qb->innerJoin('c.associations', 'mm');
        }

        $cities = $qb->getQuery()->getResult();

        return array_keys($cities);
    }

    public function getListInfo(DataModelInterface $dataModel): array
    {
        $qb = $this->getFilteredCities($dataModel->getSearch());

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('c.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort)) {
            $qb->orderBy('c.'.$sort['field'], $sort['dir']);
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbCities(array $search = []): int
    {
        $qb = $this->getFilteredCities($search);
        $qb->select('count(distinct c.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getWithMatchingManager(
        Country $country,
        string $matchingManagerType,
        bool $includeDeactivated = false,
        MatchingManagerInterface $matchingManager = null,
        string $mode = UserProfileTypeEnum::PROFILE_USER
    ): array {
        $qb = $this->createQueryBuilder('c')
            ->distinct()
            ->innerJoin('c.country', 'country')
            ->where('country.id = :country')
            ->setParameter('country', $country->getId())
            ->orderBy('c.name', 'ASC')
        ;

        if (MatchingManagerInterface::TYPE_INSTITUTE === $matchingManagerType) {
            $qb->innerJoin('c.institutes', 'mm');

            if (UserProfileTypeEnum::PROFILE_USER === $mode) {
                $qb
                    ->leftJoin('mm.children', 'child')
                    ->andWhere('child is null or mm.parent is not null')
                ;
            }
        } else {
            $qb->innerJoin('c.associations', 'mm');
        }

        if (!$includeDeactivated) {
            $qb->andWhere('mm.activated = :activated')->setParameter('activated', true);
        }

        if ($matchingManager) {
            $qb
                ->andWhere('mm.id = :matchingManager or mm.parent = :matchingManager')
                ->setParameter('matchingManager', $matchingManager->getId())
            ;
        }

        return $qb->getQuery()->getResult();
    }

    private function getFilteredCities(array $search = []): QueryBuilder
    {
        $qb = $this->createQueryBuilder('c')
            ->innerJoin('c.country', 'country')
        ;

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'country':
                    $selector = 'country.code';
                    break;
                default:
                    $selector = 'c.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }
}
