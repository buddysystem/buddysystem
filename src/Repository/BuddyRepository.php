<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Association;
use App\Entity\Buddy;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Enum\SemesterPeriodEnum;
use App\Model\DataModelInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class BuddyRepository extends ServiceEntityRepository
{
    use SemesterFilterTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Buddy::class);
    }

    public function getListInfo(DataModelInterface $dataModel, MatchingManagerInterface $matchingManager = null): array
    {
        $qb = $this->getFilteredBuddies($dataModel->getSearch(), $matchingManager);

        $qb->setFirstResult($dataModel->getFirstResult());

        if ($dataModel->getMaxResults()) {
            $qb
                ->setMaxResults($dataModel->getMaxResults())
                ->groupBy('b.id')
            ;
        }

        $sort = $dataModel->getSort();
        if (!empty($sort)) {
            $qb->orderBy('b.'.$sort['field'], $sort['dir']);
        }

        return $qb->getQuery()->getResult();
    }

    public function getNbBuddies(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        bool $semesterFilter = false,
        array $othersPeriods = [SemesterPeriodEnum::PERIOD_ALL_YEAR]
    ): int {
        $qb = $this->getFilteredBuddies($search, $matchingManager, $semesterFilter, $othersPeriods);
        $qb->select('count(distinct b.id)');

        return (int) $qb->getQuery()->getSingleScalarResult();
    }

    public function getMyBuddiesWaitingValidation(User $user): array
    {
        $qb = $this->createQueryBuilder('b')
            ->where('b.mentor = :user')
            ->andWhere('b.status = :status')
            ->andWhere('b.archived = :archived')
            ->setParameter('user', $user->getId())
            ->setParameter('archived', false)
            ->setParameter('status', BuddyStatusEnum::STATUS_PENDING)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getMyBuddiesValidated(User $user): array
    {
        $qb = $this->createQueryBuilder('b');

        if ($user->isLocal()) {
            $qb->where('b.mentor = :user');
        } else {
            $qb->where('b.mentee = :user');
        }

        $qb->andWhere('b.status = :status')
            ->andWhere('b.archived = :archived')
            ->setParameter('user', $user->getId())
            ->setParameter('status', BuddyStatusEnum::STATUS_CONFIRMED)
            ->setParameter('archived', false)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getMyBuddiesArchived(User $user): array
    {
        $qb = $this->createQueryBuilder('b');

        if ($user->isLocal()) {
            $qb->where('b.mentor = :user');
        } else {
            $qb->where('b.mentee = :user');
        }

        $qb->andWhere('b.archived = :archived')
            ->setParameter('user', $user->getId())
            ->setParameter('archived', true)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getRealNbBuddiesByUser(User $user): int
    {
        return \count(
            $this->getBuddiesByUser(
                $user,
                $user->isLocal(),
                false,
                [BuddyStatusEnum::STATUS_CONFIRMED, BuddyStatusEnum::STATUS_PENDING]
            )
        );
    }

    public function getBuddiesByUser(
        User $user,
        bool $isLocal,
        bool $includeArchived = false,
        array $status = []
    ): array {
        $qb = $this->createQueryBuilder('b');

        if (!$includeArchived) {
            $qb
                ->andWhere('b.archived = :archived')
                ->setParameter('archived', false)
            ;
        }

        if (!empty($status)) {
            $qb
                ->andWhere('b.status in (:status)')
                ->setParameter('status', $status)
            ;
        }

        if ($isLocal) {
            $qb->andwhere('b.mentor = :mentor')->setParameter('mentor', $user->getId());
        } else {
            $qb->andwhere('b.mentee = :mentee')->setParameter('mentee', $user->getId());
        }

        return $qb->getQuery()->getResult();
    }

    public function getBuddiesToRemove(): array
    {
        $qb = $this->createQueryBuilder('b');

        $qb
            ->innerJoin('b.mentor', 'mentor')
            ->innerJoin('b.mentee', 'mentee')
            ->innerJoin('mentor.institute', 'i')
            ->where('mentor.archived = :archived')
            ->andWhere('mentee.archived = :archived')
            ->andWhere('b.archived = :archived')
            ->andWhere('b.status = :status')
            ->andWhere('b.warnedAt is NOT NULL')
            ->andWhere("CURRENT_DATE() >= DATE_ADD(b.createdAt, i.unmatchDelay, 'DAY')")
            ->setParameter('status', BuddyStatusEnum::STATUS_PENDING)
            ->setParameter('archived', false)
        ;

        return $qb->getQuery()->getResult();
    }

    public function getBuddiesToWarn(): array
    {
        $qb = $this->createQueryBuilder('b');
        $qb
            ->innerJoin('b.mentor', 'mentor')
            ->innerJoin('b.mentee', 'mentee')
            ->innerJoin('mentor.institute', 'i')
            ->where('mentor.archived = :archived')
            ->andWhere('mentee.archived = :archived')
            ->andWhere('b.archived = :archived')
            ->andWhere('b.status = :status')
            ->andWhere('b.warnedAt is NULL')
            ->andWhere("CURRENT_DATE() >= DATE_ADD(b.createdAt, i.revivalDelay, 'DAY')")
            ->setParameter('status', BuddyStatusEnum::STATUS_PENDING)
            ->setParameter('archived', false)
        ;

        return $qb->getQuery()->getResult();
    }

    private function getFilteredBuddies(
        array $search = [],
        MatchingManagerInterface $matchingManager = null,
        bool $semesterFilter = false,
        array $othersPeriods = [SemesterPeriodEnum::PERIOD_ALL_YEAR]
    ): QueryBuilder {
        $qb = $this->createQueryBuilder('b')
            ->select('b, umentor, umentee')
            ->innerJoin('b.mentor', 'umentor')
            ->innerJoin('b.mentee', 'umentee')
            ->innerJoin('umentor.institute', 'iMentor')
            ->innerJoin('umentee.institute', 'iMentee')
            ->leftJoin('iMentor.ancestors', 'ancestorMentor')
            ->leftJoin('iMentee.ancestors', 'ancestorMentee')
            ->leftJoin('iMentor.associationInstitutes', 'aiMentor')
            ->leftJoin('iMentee.associationInstitutes', 'aiMentee')
            ->leftJoin('aiMentor.association', 'aMentor')
            ->leftJoin('aiMentee.association', 'aMentee')
            ->innerJoin('b.semester', 'semester')
            ->leftJoin('b.association', 'a')
            ->leftJoin('b.institute', 'i')
        ;

        $qb = $this->addMatchingManagerFilter($qb, $matchingManager);

        if ($semesterFilter) {
            $qb = $this->addSameSemesterFilter($qb, $othersPeriods);
        }

        foreach ($search as $column => $value) {
            switch ($column) {
                case 'mentor':
                    $selector = 'umentor.firstname like :'.$column.'_value
                                 or umentor.lastname';
                    $value = '%'.$value.'%';
                    break;
                case 'mentorEmail':
                    $selector = 'umentor.email';
                    $value = '%'.$value.'%';
                    break;
                case 'mentee':
                    $selector = 'umentee.firstname like :'.$column.'_value
                                 or umentee.lastname';
                    $value = '%'.$value.'%';
                    break;
                case 'menteeEmail':
                    $selector = 'umentee.email';
                    $value = '%'.$value.'%';
                    break;
                case 'association':
                    $qb->setParameter('association', $value);

                    if (\is_array($value)) {
                        $qb->andWhere('aMentor.id in (:association) or aMentee.id in (:association)');
                    } else {
                        $qb->andWhere('aMentor.id = :association or aMentee.id = :association');
                    }

                    $selector = null;
                    break;
                case 'institute':
                    $qb->setParameter('institute', $value);

                    if (\is_array($value)) {
                        $qb->andWhere(
                            'iMentor.id in (:institute) or ancestorMentor.id in (:institute) or
                                       iMentee.id in (:institute) or ancestorMentee.id in (:institute)'
                        );
                    } else {
                        $qb->andWhere(
                            'iMentor.id = :institute or ancestorMentor.id = :institute or
                                       iMentee.id = :institute or ancestorMentee.id = :institute'
                        );
                    }

                    $selector = null;
                    break;
                case 'mentorInstitute':
                    $qb->setParameter('institute', $value);

                    if (\is_array($value)) {
                        $qb->andWhere('iMentor.id in (:institute) or ancestorMentor.id in (:institute)');
                    } else {
                        $qb->andWhere('iMentor.id = :institute or ancestorMentor.id = :institute');
                    }

                    $selector = null;
                    break;
                case 'menteeInstitute':
                    $qb->setParameter('institute', $value);

                    if (\is_array($value)) {
                        $qb->andWhere('iMentee.id in (:institute) or ancestorMentee.id in (:institute)');
                    } else {
                        $qb->andWhere('iMentee.id = :institute or ancestorMentee.id = :institute');
                    }

                    $selector = null;
                    break;
                case 'semester':
                    $selector = 'semester.period';
                    break;
                case 'year':
                    $selector = 'semester.year';
                    break;
                default:
                    $selector = 'b.'.$column;
                    if (\is_array($value)) {
                        foreach ($value as &$val) {
                            $val = '%'.$val.'%';
                        }
                        unset($val);
                    } else {
                        $value = '%'.$value.'%';
                    }
                    break;
            }

            if (!empty($selector) && !empty($value)) {
                if (\is_array($value)) {
                    $whereClause = '';
                    foreach ($value as $key => $val) {
                        if ('' != $whereClause) {
                            $whereClause .= ' Or ';
                        }
                        $whereClause .= $selector.' like :'.$column.'_'.$key.'_value';
                        $qb->setParameter($column.'_'.$key.'_value', $val);
                    }

                    $qb->andWhere($whereClause);
                } else {
                    $qb
                        ->andWhere($selector.' like :'.$column.'_value')
                        ->setParameter($column.'_value', $value)
                    ;
                }
            }
        }

        return $qb;
    }

    private function addMatchingManagerFilter(
        QueryBuilder $qb,
        MatchingManagerInterface $matchingManager = null
    ): QueryBuilder {
        if ($matchingManager) {
            if ($matchingManager instanceof Association) {
                $where = 'aMentor.id = :matching_manager or aMentee.id = :matching_manager';
            } else {
                $where = 'iMentor.id = :matching_manager or ancestorMentor.id = :matching_manager or
                          iMentee.id = :matching_manager or ancestorMentee.id = :matching_manager'
                ;
            }

            $qb
                ->andWhere($where)
                ->setParameter('matching_manager', $matchingManager->getId())
            ;
        }

        return $qb;
    }
}
