<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AssociationInstituteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AssociationInstitute::class);
    }

    public function getWithRight(Institute $institute, string $right): array
    {
        return $this->createQueryBuilder('associationInstitute')
            ->innerJoin('associationInstitute.institute', 'institute')
            ->where('institute.id = :institute')
            ->andWhere('JSON_CONTAINS(associationInstitute.rights, :right) = 1')
            ->setParameter('institute', $institute->getId())
            ->setParameter('right', '"'.$right.'"')
            ->getQuery()->getResult()
        ;
    }
}
