<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Hobby;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class HobbyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hobby::class);
    }

    public function getIdsByMatchingManager(MatchingManagerInterface $matchingManager): array
    {
        $qb = $this->createQueryBuilder('h', 'h.id')
            ->where('mm.id = :matching_manager')
            ->setParameter('matching_manager', $matchingManager->getId())
        ;

        if ($matchingManager instanceof Institute) {
            $qb->innerJoin('h.institutes', 'mm');
        } else {
            $qb->innerJoin('h.associations', 'mm');
        }

        return array_keys($qb->getQuery()->getArrayResult());
    }
}
