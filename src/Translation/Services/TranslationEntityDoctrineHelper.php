<?php

declare(strict_types=1);

namespace App\Translation\Services;

use App\Services\LocaleManager;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Gedmo\Mapping\Annotation\Translatable as TranslatableAnnotation;
use Gedmo\Translatable\Entity\Translation;
use Gedmo\Translatable\Translatable;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\Inflector\EnglishInflector;
use Translation\Common\Model\Message;

final class TranslationEntityDoctrineHelper
{
    public const REF_CLASS = 0;
    public const REF_ID_VALUE = 1;
    public const REF_PROPERTY = 2;
    public const REF_EXTENSION = 3;

    private $em;
    private $defaultLocale;
    private $propertyAccessor;
    private $translationRepo;
    private $relativePathTemplate;

    public function __construct(EntityManagerInterface $em, LocaleManager $localeManager, bool $hasExtension = false)
    {
        $this->em = $em;
        $this->defaultLocale = $localeManager->getDefaultLocale();
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->translationRepo = $this->em->getRepository(Translation::class);
        $this->relativePathTemplate = $hasExtension
            ? '%'.self::REF_CLASS.'%.%'.self::REF_ID_VALUE.'%.%'.self::REF_PROPERTY.'%.%'.self::REF_EXTENSION.'%'
            : '%'.self::REF_CLASS.'%.%'.self::REF_ID_VALUE.'%.%'.self::REF_PROPERTY.'%'
        ;
    }

    public function getKey(string $class, Translatable $object, string $property, string $extension = ''): string
    {
        return strtr($this->relativePathTemplate, [
            '%'.self::REF_CLASS.'%' => $this->getClassName($class),
            '%'.self::REF_ID_VALUE.'%' => $this->propertyAccessor->getValue($object, $this->getIdProperty($class)),
            '%'.self::REF_PROPERTY.'%' => $this->camelCaseToUnderscore($property),
            '%'.self::REF_EXTENSION.'%' => $extension,
        ]);
    }

    public function translate(string $key, string $locale, string $translation): void
    {
        $infos = explode('.', $key);
        $object = $this->getObjectFromKey($key, $locale);

        if ($object) {
            $this->translationRepo->translate(
                $object,
                $this->underscoreToCamelCase($infos[self::REF_PROPERTY]),
                $locale,
                $translation
            );
        }
    }

    public function getObjectFromKey(string $key, string $locale): ?Translatable
    {
        $class = $this->getClass($this->getInfosFromKey($key, self::REF_CLASS));

        if ($class) {
            /** @var Translatable $object */
            $object = $this->em->getRepository($class)->findOneBy(
                [$this->getIdProperty($class) => $this->getInfosFromKey($key, self::REF_ID_VALUE)]
            );

            if ($object && \in_array(Translatable::class, class_implements($object), true)
                && method_exists($object, 'setTranslatableLocale')
            ) {
                $object->setTranslatableLocale($locale);
                $this->em->refresh($object);

                return $object;
            }
        }

        return null;
    }

    public function getMessageFromKey(string $key, string $domain, string $locale): ?Message
    {
        if ($object = $this->getObjectFromKey($key, $locale)) {
            if ($value = $this->propertyAccessor->getValue($object, $this->getInfosFromKey($key, self::REF_PROPERTY))) {
                return new Message($key, $domain, $locale, $value);
            }
        }

        return null;
    }

    public function getMessagesFromObject(Translatable $object): array
    {
        $class = \get_class($object);
        $domain = $this->getDomainFromClass($class);
        $messages = [];

        foreach ($this->getAllTranslations($object) as $locale => $translations) {
            foreach ($translations as $property => $value) {
                array_push(
                    $messages,
                    new Message(
                        $this->getKey($class, $object, $property),
                        $domain,
                        $locale,
                        $value
                    )
                );
            }
        }

        return $messages;
    }

    public function getAllTranslations(Translatable $object): array
    {
        $translations = $this->translationRepo->findTranslations($object);

        if (!\array_key_exists($this->defaultLocale, $translations)) {
            $translations[$this->defaultLocale] = [];
            foreach ($this->getTranslatableProperties(\get_class($object)) as $property) {
                $translations[$this->defaultLocale][$property] = $this->propertyAccessor->getValue($object, $property);
            }
        }

        return $translations;
    }

    public function getDomainFromClass(string $class): string
    {
        $inflector = new EnglishInflector();

        return $inflector->pluralize($this->getClassName($class))[0];
    }

    public function getClassName(string $class): string
    {
        return strtolower(substr(strrchr($class, '\\') ?: $class, 1));
    }

    public function getTranslatableClasses(): array
    {
        $entities = $this->em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $translatableEntities = [];

        foreach ($entities as $entity) {
            if (\in_array(Translatable::class, class_implements($entity))) {
                array_push($translatableEntities, $entity);
            }
        }

        return $translatableEntities;
    }

    public function getTranslatableDomains(): array
    {
        $classes = $this->getTranslatableClasses();

        foreach ($classes as &$class) {
            $class = $this->getDomainFromClass($class);
        }

        return $classes;
    }

    private function getClass(string $className): ?string
    {
        $className = ucfirst($className);
        $entitiesNamespaces = $this->em->getConfiguration()->getEntityNamespaces();

        foreach ($entitiesNamespaces as $entitiesNamespace) {
            $class = $entitiesNamespace.'\\'.$className;
            if (class_exists($class)) {
                return $class;
            }
        }

        return null;
    }

    private function getIdProperty(string $class): string
    {
        $reader = new AnnotationReader();
        $reflector = new \ReflectionClass($class);

        $properties = $reflector->getProperties();
        foreach ($properties as $property) {
            if ($reader->getPropertyAnnotation($property, Id::class)) {
                return $property->getName();
            }
        }

        return 'id';
    }

    private function getTranslatableProperties(string $class): array
    {
        $translatableProperties = [];

        $reader = new AnnotationReader();
        $reflector = new \ReflectionClass($class);

        $properties = $reflector->getProperties();
        foreach ($properties as $property) {
            if ($reader->getPropertyAnnotation($property, TranslatableAnnotation::class)) {
                $translatableProperties[] = $property->getName();
            }
        }

        return $translatableProperties;
    }

    private function getInfosFromKey(string $key, int $refIndex): ?string
    {
        $explodedKey = explode('.', $key);

        if (\array_key_exists($refIndex, $explodedKey)) {
            return $explodedKey[$refIndex];
        }

        return null;
    }

    private function camelCaseToUnderscore(string $input): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $input)), '_');
    }

    private function underscoreToCamelCase(string $input): string
    {
        return lcfirst(str_replace('_', '', ucwords($input, '_')));
    }
}
