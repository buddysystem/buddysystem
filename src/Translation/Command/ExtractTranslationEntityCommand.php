<?php

declare(strict_types=1);

namespace App\Translation\Command;

use App\Translation\Services\TranslationEntityStorage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Translation\MessageCatalogue;
use Translation\Bundle\Catalogue\CatalogueWriter;
use Translation\Bundle\Service\ConfigurationManager;

class ExtractTranslationEntityCommand extends Command
{
    protected static $defaultName = 'translation:extract:entities';

    private $catalogueWriter;
    private $configurationManager;
    private $translationEntityStorage;

    public function __construct(
        CatalogueWriter $catalogueWriter,
        ConfigurationManager $configurationManager,
        TranslationEntityStorage $translationEntityStorage
    ) {
        $this->catalogueWriter = $catalogueWriter;
        $this->configurationManager = $configurationManager;
        $this->translationEntityStorage = $translationEntityStorage;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Extract translations from translatable entities')
            ->addArgument('configuration', InputArgument::OPTIONAL, 'The configuration to use', 'default')
            ->addArgument('locale', InputArgument::OPTIONAL, 'The locale to use. If omitted, we use all configured locales.')
            ->addOption('domain', null, InputOption::VALUE_REQUIRED, 'The domain you want extract translations from.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $configName = $input->getArgument('configuration');
        $config = $this->configurationManager->getConfiguration($configName);

        if ($inputLocale = $input->getArgument('locale')) {
            $locales = [$inputLocale];
        } else {
            $locales = $config->getLocales();
        }

        $options = [];
        if ($domain = $input->getOption('domain')) {
            $options['domains'] = [$domain];
        }

        $catalogues = [];
        foreach ($locales as $locale) {
            $currentCatalogue = new MessageCatalogue($locale);
            $this->translationEntityStorage->export($currentCatalogue, $options);
            $catalogues[] = $currentCatalogue;
        }

        $this->catalogueWriter->writeCatalogues($config, $catalogues);

        return 0;
    }
}
