<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationInstituteRepository;
use App\Repository\UserRepository;
use App\Services\InstituteLevelManagement;
use App\Services\MailManager;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssociationInstituteListener
{
    use UnitOfWorkTrait;

    private $matchingManagerHelper;
    private $instituteLevelManagement;
    private $translator;
    private $userRepo;
    private $mailManager;
    private $mailRecipients;
    private $mailBodies;
    private $security;

    public function __construct(
        MatchingManagerHelper $matchingManagerHelper,
        InstituteLevelManagement $instituteLevelManagement,
        TranslatorInterface $translator,
        Security $security,
        UserRepository $userRepo,
        MailManager $mailManager
    ) {
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->instituteLevelManagement = $instituteLevelManagement;
        $this->mailManager = $mailManager;
        $this->security = $security;
        $this->mailRecipients = [];
        $this->mailBodies = [];
        $this->translator = $translator;
        $this->userRepo = $userRepo;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof AssociationInstitute) {
            $institute = $entity->getInstitute();
            $association = $entity->getAssociation();

            if (AssociationInstitutePermEnum::PERM_VIEW === $entity->getUpperRight()) {
                $associationInstituteRepo = $args->getEntityManager()->getRepository(AssociationInstitute::class);
                $entity->setRights($this->getParentRights($associationInstituteRepo, $association, $institute));
            }

            if ($entity->canMatch()) {
                $this->instituteLevelManagement->setLowerLevelManagement($institute, true);
            }

            $associationCities = $association->getCities()->toArray();
            /** @var Institute $child */
            foreach ($institute->getChildren() as $child) {
                if (!$child->isAssociationInstituteExists($association)
                    && !empty(array_intersect($child->getCitiesWithParents(), $associationCities))
                ) {
                    $ai = new AssociationInstitute();
                    $ai->setInstitute($child);
                    $ai->setAssociation($association);
                    $ai->setRights($entity->getRights());
                    $child->addAssociationInstitute($ai);
                }
            }
        }
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            if ($entity instanceof AssociationInstitute) {
                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('rights' === $changeColumn) {
                        $association = $entity->getAssociation();
                        $institute = $entity->getInstitute();
                        $rights = $entity->getRights();

                        $this->instituteLevelManagement->setLowerLevelManagement(
                            $institute,
                            \in_array(AssociationInstitutePermEnum::PERM_MATCH, $rights),
                        );

                        $this->applyRightsOnChildren(
                            $em,
                            $association,
                            $institute,
                            $rights
                        );

                        if ($this->security->getToken()) {
                            array_push($this->mailBodies, $this->translator->trans(
                                'institute.rights.changed',
                                [
                                    '%institute%' => '<b>'.$institute->getName().'</b>',
                                    '%manager%' => $entity->canMatch()
                                        ? '<b>'.$association->getName().'</b>'
                                        : '<b>'.$institute->getName().'</b>',
                                ]
                            ));

                            if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
                                $this->addManagersToMailRecipients($association);
                                $this->addManagersToMailRecipients($institute);
                            } elseif ($this->security->isGranted(UserRoleEnum::ROLE_INSTITUTE_MANAGER)) {
                                $this->addManagersToMailRecipients($association);
                            } else {
                                $this->addManagersToMailRecipients($institute);
                            }
                        }
                    }
                }
            }
        }

        if (!empty($this->mailBodies)) {
            $this->mailManager->sendRightsChangedNotification($this->mailBodies, $this->mailRecipients);
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof AssociationInstitute) {
            $institute = $entity->getInstitute();
            $institute->removeAssociationInstitute($entity);

            if ($institute->getAssociationInstitutes()->isEmpty()) {
                $this->instituteLevelManagement->setLowerLevelManagement($institute, false);
            }

            $this->unlinkChildren($args->getEntityManager(), $entity->getAssociation(), $institute);

            if ($entity->canMatch()) {
                array_push($this->mailBodies, $this->translator->trans(
                    'institute.rights.changed',
                    [
                        '%institute%' => '<b>'.$institute->getName().'</b>',
                        '%manager%' => '<b>'.$institute->getName().'</b>',
                    ]
                ));

                $this->addManagersToMailRecipients($institute);
                $this->mailManager->sendRightsChangedNotification($this->mailBodies, $this->mailRecipients);
            }
        }
    }

    private function unlinkChildren(EntityManager $em, Association $association, Institute $institute): void
    {
        /** @var Institute $child */
        foreach ($institute->getChildren() as $child) {
            /** @var AssociationInstitute $associationInstitute */
            foreach ($child->getAssociationInstitutes() as $associationInstitute) {
                if ($associationInstitute->getAssociation() === $association) {
                    $child->removeAssociationInstitute($associationInstitute);
                    $em->remove($associationInstitute);
                }
            }

            $this->unlinkChildren($em, $association, $child);
        }

        $instituteManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute($institute, true);
        $instituteRepo = $em->getRepository(Institute::class);
        if (1 === \count($instituteManagers)
            && reset($instituteManagers) === $institute
            && !$instituteRepo->hasManagers($institute)
        ) {
            $institute->setActivated(false);
        }
    }

    private function applyRightsOnChildren(
        EntityManagerInterface $em,
        Association $association,
        Institute $institute,
        array $rights
    ): void {
        /** @var Institute $child */
        foreach ($institute->getChildren() as $child) {
            /** @var AssociationInstitute $associationInstitute */
            foreach ($child->getAssociationInstitutes() as $associationInstitute) {
                if ($associationInstitute->getAssociation() === $association) {
                    $associationInstitute->setRights($rights);
                } elseif (\in_array(AssociationInstitutePermEnum::PERM_MATCH, $rights)
                    && $associationInstitute->canMatch()
                ) {
                    $associationInstitute->setCanMatch(false);
                    $associationInstitute->setEditPreferences(false);
                }
                $this->addUpdateToUOW($em, $associationInstitute);
                $this->applyRightsOnChildren($em, $association, $child, $rights);
            }
        }
    }

    private function addManagersToMailRecipients(MatchingManagerInterface $matchingManager): void
    {
        $managers = $this->userRepo->getGrantedManager($matchingManager);

        /** @var User $manager */
        foreach ($managers as $manager) {
            $idManager = $manager->getId();

            if (!\array_key_exists($idManager, $this->mailRecipients)) {
                $this->mailRecipients[$idManager] = [
                    'mail' => $manager->getEmail(),
                    'locale' => $manager->getLocaleLanguage(),
                    'mailBodies' => [],
                ];
            }

            array_push($this->mailRecipients[$idManager]['mailBodies'], array_key_last($this->mailBodies));
        }
    }

    private function getParentRights(
        AssociationInstituteRepository $associationInstituteRepo,
        Association $association,
        Institute $institute
    ): array {
        if ($parent = $institute->getParent()) {
            if ($associationInstitute = $associationInstituteRepo->findOneBy(['association' => $association, 'institute' => $parent])) {
                return $associationInstitute->getRights();
            }

            return $this->getParentRights($associationInstituteRepo, $association, $parent);
        }

        return [AssociationInstitutePermEnum::PERM_VIEW];
    }
}
