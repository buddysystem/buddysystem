<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Message;
use App\Services\MessagingProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\SerializerInterface;

class MessageListener
{
    use UnitOfWorkTrait;

    private $publisher;
    private $serializer;
    private $messagingProvider;

    public function __construct(
        PublisherInterface $publisher,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider
    ) {
        $this->publisher = $publisher;
        $this->serializer = $serializer;
        $this->messagingProvider = $messagingProvider;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Message) {
            $thread = $entity->getThread();
            $thread->setLastMessageAt($entity->getCreatedAt());
            $this->addUpdateToUOW($args->getEntityManager(), $thread);

            $this->publisher->__invoke(new Update(
                $this->messagingProvider->getThreadIRI($entity->getThread()),
                $this->serializer->serialize($this->messagingProvider->getMessageDTO($entity), 'json')
            ));
        }
    }
}
