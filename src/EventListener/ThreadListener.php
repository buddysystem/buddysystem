<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Thread;
use App\Entity\ThreadManagerInterface;
use App\Entity\ThreadUserManager;
use App\Entity\User;
use App\Messaging\DTO\RefreshTopics;
use App\Services\MessagingProvider;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Mercure\PublisherInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Serializer\SerializerInterface;

class ThreadListener
{
    private $publisher;
    private $serializer;
    private $messagingProvider;

    public function __construct(
        PublisherInterface $publisher,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider
    ) {
        $this->publisher = $publisher;
        $this->serializer = $serializer;
        $this->messagingProvider = $messagingProvider;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $entity = $args->getEntity();
        $userRepo = $em->getRepository(User::class);

        if ($entity instanceof Thread && !$entity instanceof ThreadManagerInterface) {
            $users = $entity->getUsers()->toArray();

            if ($entity instanceof ThreadUserManager) {
                if ($matchingManagers = $entity->getMatchingManagers()) {
                    foreach ($matchingManagers as $matchingManager) {
                        $users = array_merge(
                            $users,
                            $userRepo->getGrantedManager($matchingManager)
                        );
                    }
                }
            }

            foreach ($users as $user) {
                $this->publisher->__invoke(new Update(
                    $this->messagingProvider->getUserIRI($user),
                    $this->serializer->serialize(
                        new RefreshTopics(),
                        'json'
                    )
                ));
            }
        }
    }
}
