<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Document;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\MimeTypes;
use Vich\UploaderBundle\Event\Event;

class FileUploadListener
{
    public function onVichUploaderPostUpload(Event $event): void
    {
        $object = $event->getObject();
        if ($object instanceof Document) {
            if ('thumbnailFile' === $event->getMapping()->getFilePropertyName()) {
                return;
            }

            if (\in_array($object->getDocumentFile()->getMimeType(), (new MimeTypes())->getMimeTypes('pdf'))) {
                $format = sprintf('%s.png', $object->getDocumentFile()->getRealPath());
                $imagick = new \Imagick();
                $imagick->readImage(sprintf('%s[0]', $object->getDocumentFile()->getRealPath()));
                $imagick->writeImage($format);

                $file = new File($format);
                $thumbnail = new UploadedFile(
                    $file->getPathname(),
                    $file->getFilename(),
                    $file->getMimeType(),
                    null,
                    true
                );

                $object->setThumbnailFile($thumbnail);
                $object->setThumbnailName($file->getFilename());
            } else {
                $object->setThumbnailName($object->getDocumentName());
            }
        }
    }
}
