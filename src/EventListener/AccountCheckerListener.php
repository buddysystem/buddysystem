<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use App\Enum\UserVoterEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class AccountCheckerListener implements EventSubscriberInterface
{
    private $router;
    private $security;

    public function __construct(RouterInterface $router, Security $security)
    {
        $this->router = $router;
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest']],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $user = $this->security->getUser();

        if (!$user instanceof User) {
            return;
        }

        $route = $event->getRequest()->get('_route');
        $routesAllowed = [
            'user_delete',
            'bazinga_jstranslation_js',
            '_wdt',
            '_profiler',
            'php_translation_',
            'logout',
        ];

        foreach ($routesAllowed as $routeAllowed) {
            if (0 === strpos($route, $routeAllowed)) {
                return;
            }
        }

        if (!$user->privacyHasBeenAcceptedFully()) {
            if ('user_consent' !== $route) {
                $event->setResponse(new RedirectResponse($this->router->generate('user_consent')));
            }
        } elseif (!$user->isCharterAccepted()) {
            if ('user_consent-charter' !== $route) {
                $event->setResponse(new RedirectResponse($this->router->generate('user_consent-charter')));
            }
        } elseif (!$user->isApproved()) {
            if ('user_not-approved' !== $route) {
                $event->setResponse(new RedirectResponse($this->router->generate('user_not-approved')));
            }
        } elseif (!$this->security->isGranted(UserVoterEnum::IS_MANAGER, $user) && !$user->isMinimallyCompleted()) {
            if ('profile_edit' !== $route) {
                $event->setResponse(new RedirectResponse($this->router->generate('profile_edit')));
            }
        } elseif (($user->privacyHasBeenAcceptedFully() && 'user_consent' === $route)
                 || ($user->isCharterAccepted() && 'user_consent-charter' === $route)
                 || ($user->isApproved() && 'user_not-approved' === $route)
        ) {
            $event->setResponse(new RedirectResponse($this->router->generate('dashboard')));
        }
    }
}
