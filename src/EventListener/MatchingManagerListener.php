<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Services\InstituteLevelManagement;
use App\Services\MatchingManagerHelper;
use App\Services\MatchingManagerInitiator;
use App\Services\SemesterHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Security\Core\Security;

class MatchingManagerListener
{
    use UnitOfWorkTrait;

    private $security;
    private $matchingManagerInitiator;
    private $matchingManagerHelper;
    private $instituteLevelManagement;
    private $semesterHelper;

    public function __construct(
        Security $security,
        MatchingManagerInitiator $matchingManagerInitiator,
        MatchingManagerHelper $matchingManagerHelper,
        InstituteLevelManagement $instituteLevelManagement,
        SemesterHelper $semesterHelper
    ) {
        $this->security = $security;
        $this->matchingManagerInitiator = $matchingManagerInitiator;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->instituteLevelManagement = $instituteLevelManagement;
        $this->semesterHelper = $semesterHelper;
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $em = $args->getObjectManager();

        if ($entity instanceof MatchingManagerInterface) {
            $this->matchingManagerInitiator->initMatchingManager($entity);
            $em->flush();
        }
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            if ($entity instanceof MatchingManagerInterface) {
                $matchingManager = $entity;
                $changeSets = $uow->getEntityChangeSet($matchingManager);

                /** @var PersistentCollection $cities */
                $cities = $matchingManager->getCities();
                $citiesDeleted = array_diff($cities->getDeleteDiff(), $cities->getInsertDiff());

                if (!empty($citiesDeleted)) {
                    /** @var AssociationInstitute $associationInstitute */
                    foreach ($matchingManager->getAssociationInstitutes() as $associationInstitute) {
                        if (empty(array_intersect(
                            $associationInstitute->getAssociation()->getCities()->toArray(),
                            $associationInstitute->getInstitute()->getCities()->toArray()
                        ))) {
                            $em->remove($associationInstitute);
                        }
                    }
                }

                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('activated' === $changeColumn) {
                        if ($matchingManager instanceof Institute) {
                            $this->setActivatedOnChildren($em, $matchingManager, $matchingManager->isActivated());
                        } elseif ($matchingManager instanceof Association) {
                            foreach ($matchingManager->getAssociationInstitutes() as $associationInstitute) {
                                if ($associationInstitute->canMatch()) {
                                    $this->setActivated(
                                        $em,
                                        $associationInstitute->getInstitute(),
                                        $matchingManager->isActivated(),
                                        $matchingManager
                                    );
                                }
                            }
                        }
                    }

                    if ('fullYearOnly' === $changeColumn && $matchingManager instanceof Institute) {
                        $this->matchingManagerInitiator->setSemester($matchingManager, true);
                    }
                }

                $this->instituteLevelManagement->applyPreferencesOnChildren($matchingManager);
                $institutes = $this->matchingManagerHelper->getUpperLevelsCriteriasManaged($matchingManager);
                foreach ($institutes as $institute) {
                    $this->addUpdateToUOW($em, $institute);
                }
            }
        }
    }

    private function setActivated(
        EntityManagerInterface $em,
        Institute $institute,
        bool $activated,
        Association $association = null
    ): void {
        if ($association) {
            $matchingManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute($institute);

            if (1 === \count($matchingManagers) && reset($matchingManagers) === $association) {
                $institute->setActivated($activated);
                $this->addUpdateToUOW($em, $institute);
            }
        } else {
            $institute->setActivated($activated);
            $this->addUpdateToUOW($em, $institute);
        }
    }

    private function setActivatedOnChildren(
        EntityManagerInterface $em,
        Institute $institute,
        bool $activated,
        Association $association = null
    ): void {
        /** @var Institute $child */
        foreach ($institute->getChildren() as $child) {
            $this->setActivated($em, $child, $activated, $association);
            $this->setActivatedOnChildren($em, $child, $activated, $association);
        }
    }
}
