<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Services\MatchingHelper;
use App\Services\MatchingManagerHelper;
use App\Services\MessagingHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class BuddyListener
{
    use UnitOfWorkTrait;

    private MatchingHelper $matchingHelper;
    private MessagingHelper $messagingHelper;
    private MatchingManagerHelper $matchingManagerHelper;

    public function __construct(
        MatchingHelper $matchingHelper,
        MessagingHelper $messagingHelper,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        $this->matchingHelper = $matchingHelper;
        $this->messagingHelper = $messagingHelper;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Buddy) {
            $em = $args->getEntityManager();

            $mentor = $entity->getMentor();
            $mentor->incrementNbBuddies();
            $this->addUpdateToUOW($em, $mentor);

            $mentee = $entity->getMentee();
            $mentee->incrementNbBuddies();
            $this->addUpdateToUOW($em, $mentee);
        }
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            $changeSets = $uow->getEntityChangeSet($entity);

            // Match accepted / refused
            if ($entity instanceof Buddy) {
                $buddy = $entity;

                if (!$buddy->isArchived() && !$buddy->isDeleted()) {
                    foreach ($changeSets as $changeColumn => $changeSet) {
                        if ('status' === $changeColumn) {
                            $status = $buddy->getStatus();
                            $buddy->setResponseAt(new \DateTime('now'));

                            $mentor = $buddy->getMentor();
                            $mentee = $buddy->getMentee();

                            if (BuddyStatusEnum::STATUS_CONFIRMED === $status) {
                                $mentor->setIsMatched(true);
                                $mentee->setIsMatched(true);
                                $thread = $this->messagingHelper->createBuddyThread($buddy);
                                $this->addUpdateToUOW($em, $thread);
                            } elseif (BuddyStatusEnum::STATUS_REFUSED === $status) {
                                $mentor->decrementNbBuddies();
                                $mentee->decrementNbBuddies();
                                $mentor->incrementNbBuddiesRefused();
                                $mentor->setOverrideMaxRefusedLimitation(false);
                                $mentee->incrementNbRefusedByBuddies();
                            }

                            $em->persist($buddy);
                            $this->addUpdateToUOW($em, $mentor);
                            $this->addUpdateToUOW($em, $mentee);
                        }
                    }
                }
            }

            if ($entity instanceof User) {
                $user = $entity;
                $buddyRepo = $em->getRepository(Buddy::class);

                foreach ($changeSets as $changeColumn => $changeSet) {
                    if (\in_array($changeColumn, ['local', 'sex', 'sexWanted', 'semester', 'institute'])) {
                        $isLocal = $user->isLocal();

                        if ('local' === $changeColumn) {
                            $isLocal = !$isLocal;
                        }

                        $buddies = $buddyRepo->getBuddiesByUser($user, $isLocal);

                        /** @var Buddy $buddy */
                        foreach ($buddies as $buddy) {
                            $mentee = $buddy->getMentee();
                            $mentor = $buddy->getMentor();

                            if (\in_array($changeColumn, ['sex', 'sexWanted'])) {
                                if ($this->matchingHelper->isSexEqual($mentee->getSex(), $mentee->getSexWanted(), $mentor->getSex(), $mentor->getSexWanted())) {
                                    break;
                                }
                            } elseif ('semester' === $changeColumn) {
                                if ($this->matchingHelper->isSemesterEqual($mentee->getSemester(), $mentor->getSemester())) {
                                    break;
                                }
                            } elseif ('institute' === $changeColumn) {
                                /** @var Institute $oldValue */
                                $oldValue = $changeSet[0];
                                $oldManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute($oldValue);
                                $newManagers = $this->matchingManagerHelper->getAllMatchingManagerOfUser($user, false);

                                if (!empty(array_intersect($oldManagers, $newManagers))) {
                                    break;
                                }
                            }

                            $em->remove($buddy);
                        }
                    } elseif ('archived' === $changeColumn) {
                        if ($changeSet[1]) {
                            $buddies = $buddyRepo->getBuddiesByUser($user, $user->isLocal());

                            /** @var Buddy $buddy */
                            foreach ($buddies as $buddy) {
                                if (!$buddy->isArchived()) {
                                    $buddy->setArchived(true);

                                    $this->addUpdateToUOW($em, $buddy);

                                    $mentor = $buddy->getMentor();
                                    $mentee = $buddy->getMentee();

                                    if (BuddyStatusEnum::STATUS_REFUSED !== $buddy->getStatus()) {
                                        if (!$mentee->isArchived()) {
                                            $mentee->decrementNbBuddies();
                                            $mentee->incrementNbBuddiesArchived();
                                            $this->addUpdateToUOW($em, $mentee);
                                        }

                                        if (!$mentor->isArchived()) {
                                            $mentor->incrementNbBuddiesArchived();
                                            $mentor->decrementNbBuddies();
                                            $this->addUpdateToUOW($em, $mentor);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $em = $args->getEntityManager();

        if ($entity instanceof Buddy) {
            if (!$entity->isArchived()) {
                $mentor = $entity->getMentor();
                $mentee = $entity->getMentee();

                if (BuddyStatusEnum::STATUS_REFUSED !== $entity->getStatus()) {
                    $mentor->decrementNbBuddies();
                    $mentee->decrementNbBuddies();
                } else {
                    $mentor->decrementNbBuddiesRefused();
                    $mentee->decrementNbRefusedByBuddies();
                }

                $this->addUpdateToUOW($em, $mentor);
                $this->addUpdateToUOW($em, $mentee);
            }

            $entity->setThread(null);
        }
    }
}
