<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Report;
use App\Services\MailManager;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ReportListener
{
    private $mailManager;
    private $matchingManagerHelper;

    public function __construct(MailManager $mailManager, MatchingManagerHelper $matchingManagerHelper)
    {
        $this->mailManager = $mailManager;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Report) {
            $user = $entity->getUser();
            $user->incrementNbReport();

            $this->mailManager->reportBuddy(
                $entity,
                $entity->getBuddy()->getMatchingManager()
            );
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Report) {
            $entity->getUser()->decrementNbReport();
        }
    }
}
