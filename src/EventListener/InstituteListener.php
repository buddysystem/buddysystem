<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\Stat;
use App\Entity\User;
use App\Enum\UserVoterEnum;
use App\Repository\UserRepository;
use App\Services\InstituteLevelManagement;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Symfony\Component\Security\Core\Security;

class InstituteListener
{
    use UnitOfWorkTrait;

    private $security;
    private $matchingManagerHelper;
    private $instituteLevelManagement;

    public function __construct(
        Security $security,
        MatchingManagerHelper $matchingManagerHelper,
        InstituteLevelManagement $instituteLevelManagement
    ) {
        $this->security = $security;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->instituteLevelManagement = $instituteLevelManagement;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof Institute) {
            $manager = $this->matchingManagerHelper->getMatchingManagerOfManager();

            if ($manager instanceof Association) {
                $associationInstitute = new AssociationInstitute();
                $associationInstitute->setAssociation($manager);
                $associationInstitute->setInstitute($entity);
                $associationInstitute->setCanMatch(true);
                $entity->addAssociationInstitute($associationInstitute);

                $this->instituteLevelManagement->applyPreferencesOnChildren($manager, $entity);
            } else {
                if ($parent = $entity->getParent()) {
                    $this->instituteLevelManagement->applyPreferencesOnChildren($parent, $entity);
                    $entity->setShowBonusOption($parent->isShowBonusOption());
                } else {
                    $entity->setLowerLevelManagement(false);
                }
            }
        }
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            if ($entity instanceof Institute) {
                if (!$entity->getParent() && $entity->getAssociationInstitutes()->isEmpty()) {
                    $entity->setLowerLevelManagement(false);
                }

                $changeSets = $uow->getEntityChangeSet($entity);
                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('showBonusOption' === $changeColumn) {
                        $this->instituteLevelManagement->setShowBonusOptionOnChildren($entity, $entity->isShowBonusOption());
                    }

                    if (!$entity->hasChildren()) {
                        if ('nbMentors' === $changeColumn || 'nbMentees' === $changeColumn) {
                            /** @var UserRepository $userRepo */
                            $userRepo = $em->getRepository(User::class);

                            $local = ('nbMentees' === $changeColumn);

                            $maxBuddies = $changeSet[1];
                            $users = $userRepo->getByMatchingManager($entity, $local);

                            /** @var User $u */
                            foreach ($users as $u) {
                                if ($u->getNbBuddiesWanted() > $maxBuddies) {
                                    $u->setNbBuddiesWanted($maxBuddies);

                                    $em->persist($u);
                                    $this->addUpdateToUOW($em, $u);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $em = $args->getObjectManager();

        if ($entity instanceof Institute) {
            $newInstitute = null;
            if ($parent = $entity->getParent()) {
                $newInstitute = $parent;
            }

            if ($entity->getUsers()->count()) {
                /** @var User $user */
                foreach ($entity->getUsers() as $user) {
                    $user->setInstitute($newInstitute);

                    if ($this->security->isGranted(UserVoterEnum::IS_MANAGER, $user)) {
                        $user->setApproved(false);
                        // todo : notify
                    }
                }
            }

            if ($entity->getBuddies()->count()) {
                /** @var Buddy $buddy */
                foreach ($entity->getBuddies() as $buddy) {
                    $buddy->setInstitute($newInstitute);
                }
            }

            if ($entity->getStats()->count()) {
                /** @var Stat $stat */
                foreach ($entity->getStats() as $stat) {
                    if ($newInstitute) {
                        $stat->setInstitute($newInstitute);
                    } else {
                        $em->remove($stat);
                    }
                }
            }

            $copies = $entity->getCopies();
            if (!$copies->isEmpty()) {
                $entity = $copies->first();
                $entity->setDuplicate(null);
                foreach ($copies as $copy) {
                    $entity->addCopy($copy);
                }
            }

            /** @var Institute $child */
            foreach ($entity->getChildren() as $child) {
                $child->setParent(null);

                $this->instituteLevelManagement->setLevels($child);
            }
        }
    }
}
