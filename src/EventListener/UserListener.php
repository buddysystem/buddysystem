<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\UserRoleEnum;
use App\Services\GrantedService;
use App\Services\InstituteLevelManagement;
use App\Services\MatchingManagerHelper;
use App\Services\MatchingManagerInitiator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;

class UserListener
{
    use UnitOfWorkTrait;

    private $matchingManagerHelper;
    private $matchingManagerInitiator;
    private $instituteLevelManagement;
    private $grantedService;

    public function __construct(
        MatchingManagerHelper $matchingManagerHelper,
        MatchingManagerInitiator $matchingManagerInitiator,
        InstituteLevelManagement $instituteLevelManagement,
        GrantedService $grantedService
    ) {
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->matchingManagerInitiator = $matchingManagerInitiator;
        $this->instituteLevelManagement = $instituteLevelManagement;
        $this->grantedService = $grantedService;
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $user = $args->getObject();

        if (!$user instanceof User) {
            return;
        }

        if ($user->isPrivacyAccepted()) {
            $user->acceptPrivacy();
        }

        if (UserRoleEnum::ROLE_INSTITUTE_MANAGER === $this->grantedService->getRole($user)) {
            $institute = $user->getInstitute();

            $this->instituteLevelManagement->setLowerLevelManagement($institute, false);
        }
    }

    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $entities = $uow->getScheduledEntityUpdates();

        foreach ($entities as $entity) {
            if ($entity instanceof User) {
                $user = $entity;
                $changeSets = $uow->getEntityChangeSet($user);

                foreach ($changeSets as $changeColumn => $changeSet) {
                    if ('nbBuddiesWanted' === $changeColumn) {
                        if ($nbBuddiesWanted = $user->getNbBuddiesWanted()) {
                            $institute = $user->getInstitute();

                            if ($user->isLocal()) {
                                if ($nbBuddiesWanted > $institute->getNbMentees()) {
                                    $nbBuddiesWanted = $institute->getNbMentees();
                                }
                            } elseif ($nbBuddiesWanted > $institute->getNbMentors()) {
                                $nbBuddiesWanted = $institute->getNbMentors();
                            }

                            $user->setNbBuddiesWanted($nbBuddiesWanted);
                        }
                    } elseif ('roles' === $changeColumn) {
                        if ($this->grantedService->isGranted($user, UserRoleEnum::ROLE_ADMIN)) {
                            $user->setAssociation(null);
                            $user->setInstitute(null);
                        }
                    } elseif ('archived' === $changeColumn) {
                        if ($changeSet[1]) {
                            $history = $user->createHistory();
                            $this->addUpdateToUOW($em, $history);
                        } else {
                            $user->setIsAskNextSemester(false);
                        }
                    }
                }
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        $em = $args->getEntityManager();

        if ($entity instanceof User) {
            if ($matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($entity)) {
                $this->cleanMatchingManager($em, $entity, $matchingManager);
            }
        }
    }

    private function cleanMatchingManager(
        EntityManager $em,
        User $user,
        MatchingManagerInterface $matchingManager
    ): void {
        $userRepo = $em->getRepository(User::class);
        $associationInstituteRepo = $em->getRepository(AssociationInstitute::class);
        $matchingManager->removeUser($user);

        $managers = $userRepo->getGrantedManager($matchingManager);

        if (1 === \count($managers) && reset($managers) === $user) {
            if ($matchingManager instanceof Institute) {
                if (empty($associationInstituteRepo->getWithRight($matchingManager, AssociationInstitutePermEnum::PERM_MATCH))
                    && $firstAssoInstitute = $matchingManager->getAssociationInstitutes()->first()
                ) {
                    $firstAssoInstitute->setCanMatch(true);

                    $this->instituteLevelManagement->setLowerLevelManagement($matchingManager, true);
                }

                foreach ($matchingManager->getChildren() as $child) {
                    $this->cleanMatchingManager($em, $user, $child);
                }
            } elseif ($matchingManager instanceof Association) {
                $matchingManager->setActivated(false);

                /** @var AssociationInstitute $assoInstitute */
                foreach ($matchingManager->getAssociationInstitutes() as $assoInstitute) {
                    $assoInstitute->setCanMatch(false);
                }
            }
        }
    }
}
