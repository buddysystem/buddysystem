<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\User;
use App\Services\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class LocaleListener implements EventSubscriberInterface
{
    private $router;
    private $locales;
    private $em;
    private $security;

    public function __construct(
        RouterInterface $router,
        LocaleManager $localeManager,
        Security $security,
        EntityManagerInterface $em
    ) {
        $this->router = $router;
        $this->locales = $localeManager->getLocalesAllowed();
        $this->em = $em;
        $this->security = $security;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 7]],
            KernelEvents::EXCEPTION => ['onKernelException'],
            KernelEvents::RESPONSE => ['onKernelResponse'],
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        if (!$event->getThrowable() instanceof NotFoundHttpException) {
            return;
        }

        if (!$event->isMainRequest()) {
            return;
        }

        $request = $event->getRequest();
        $uriParts = explode('/', $request->getPathInfo());

        if ($this->isLocaleMissing($uriParts)) {
            $locale = $this->getBestLocale($request);
            $response = new RedirectResponse($this->constructRedirectUri($request, $locale, $uriParts));
            $event->setResponse($response);
        }
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();
        if (!$event->isMainRequest() || !$request->hasPreviousSession()) {
            return;
        }

        $locale = $request->attributes->get('_locale');

        if ($locale && \in_array($locale, $this->locales, true)) {
            $request->getSession()->set('_locale', $locale);

            if ($user = $this->getUser()) {
                $user->setLocaleLanguage($event->getRequest()->getLocale());

                $this->em->persist($user);
                $this->em->flush();
            }
        } else {
            $request->setLocale($this->getBestLocale($request));
        }
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $this->setLocaleCookie($event);
    }

    private function isLocaleMissing(array $uriParts): bool
    {
        return !isset($uriParts[1]) || !$uriParts[1] || !\in_array($uriParts[1], $this->locales, true);
    }

    private function constructRedirectUri(Request $request, string $locale, array $uriParts): string
    {
        $pathInfo = $request->getPathInfo();

        if (isset($uriParts[1]) && $uriParts[1]) {
            $urlToTest = preg_replace(
                '/\/'.$uriParts[1].'\//',
                '/'.$locale.'/',
                $pathInfo
            );

            try {
                $params = $this->router->match($urlToTest);

                if (\array_key_exists('_route', $params)) {
                    $route = $params['_route'];
                    unset($params['_route']);

                    return $this->router->generate(
                        $route,
                        $params,
                        UrlGeneratorInterface::ABSOLUTE_URL
                    );
                }
            } catch (\Exception $e) {
            }
        }

        $find = str_replace('/', '\/', $request->getPathInfo());

        return preg_replace(
            '/'.$find.'$/',
            '/'.$locale.'/'.trim($request->getPathInfo(), '/'),
            $request->getUri()
        );
    }

    /**
     * Priority:
     *      - session
     *      - cookie
     *      - userProfile
     *      - browser
     *      - defaultLocale
     */
    private function getBestLocale(Request $request): string
    {
        $session = $request->getSession();

        $possibleLocales = [];

        if ($session->has('_locale')) {
            $possibleLocales[] = $session->get('_locale');
        }

        $possibleLocales[] = $this->getLocaleCookie($request);

        if ($user = $this->getUser()) {
            $possibleLocales[] = $user->getLocaleLanguage();
        }

        if ($browserPreference = $request->getPreferredLanguage()) {
            $possibleLocales[] = substr($browserPreference, 0, 2);
        }

        $possibleLocales[] = $request->getDefaultLocale();

        do {
            $locale = next($possibleLocales);
        } while (!$locale || !\in_array($locale, $this->locales, true));

        return $locale;
    }

    private function getLocaleCookie(Request $request): ?string
    {
        $cookie = $request->cookies;

        $value = null;
        if ($cookie->has('locale')) {
            $value = (string) $cookie->get('locale');
        }

        return $value;
    }

    private function setLocaleCookie(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $locale = $request->getLocale();
        $cookie = $this->getLocaleCookie($request);

        if (!$cookie || $cookie !== $locale) {
            $event->getResponse()->headers->setCookie(
                Cookie::create('locale', $locale, time() + 3600 * 24 * 365)
            );
        }
    }

    private function getUser(): ?User
    {
        $user = $this->security->getUser();

        if ($user instanceof User) {
            return $user;
        }

        return null;
    }
}
