<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThreadBuddyRepository")
 */
class ThreadBuddy extends Thread implements ThreadInterface
{
    /**
     * @var Buddy
     *
     * @ORM\OneToOne(targetEntity="Buddy", inversedBy="thread", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $buddy;

    public function getBuddy(): Buddy
    {
        return $this->buddy;
    }

    public function setBuddy(Buddy $buddy): void
    {
        $this->buddy = $buddy;
    }
}
