<?php

declare(strict_types=1);

namespace App\Entity;

interface MatchingCriteriaInterface
{
    public function isActivated(): bool;

    public function setActivated(bool $activated): void;

    public function getNbMentors(): int;

    public function setNbMentors(int $nbMentors): void;

    public function getNbMentees(): int;

    public function setNbMentees(int $nbMentees): void;

    public function isFullYearOnly(): bool;

    public function setFullYearOnly(bool $fullYearOnly): void;

    public function getFirstSemesterStart(): \DateTime;

    public function setFirstSemesterStart(\DateTime $firstSemesterStart): void;

    public function getFirstSemesterEnd(): ?\DateTime;

    public function setFirstSemesterEnd(?\DateTime $firstSemesterEnd): void;

    public function getSecondSemesterStart(): ?\DateTime;

    public function setSecondSemesterStart(?\DateTime $secondSemesterStart): void;

    public function getSecondSemesterEnd(): \DateTime;

    public function setSecondSemesterEnd(\DateTime $secondSemesterEnd): void;

    public function getMatchingHobbyValue(): int;

    public function setMatchingHobbyValue(int $matchingHobbyValue): void;

    public function getMatchingStudyValue(): int;

    public function setMatchingStudyValue(int $matchingStudyValue): void;

    public function getMatchingCityValue(): int;

    public function setMatchingCityValue(int $matchingCityValue): void;

    public function getMatchingLanguageValue(): int;

    public function setMatchingLanguageValue(int $matchingLanguageValue): void;

    public function getMatchingAgeValue(): int;

    public function setMatchingAgeValue(int $matchingAgeValue): void;

    public function getMatchingMotivationValue(): int;

    public function setMatchingMotivationValue(int $matchingMotivationValue): void;

    public function getMatchingAvailabilityValue(): int;

    public function setMatchingAvailabilityValue(int $matchingAvailabilityValue): void;

    public function getMatchingInstituteValues(): array;

    public function setMatchingInstituteValues(array $matchingInstituteValues): void;

    public function getMatchingLevelOfStudyValue(): int;

    public function setMatchingLevelOfStudyValue(int $matchingLevelOfStudyValue): void;

    public function getRevivalDelay(): int;

    public function setRevivalDelay(int $revivalDelay): void;

    public function getUnmatchDelay(): int;

    public function setUnmatchDelay(int $unmatchDelay): void;

    public function getNbBuddiesRefusedMax(): int;

    public function setNbBuddiesRefusedMax(int $nbBuddiesRefusedMax): void;

    public function isMentorValidationRequired(): bool;

    public function setMentorValidationRequired(bool $mentorValidationRequired): void;
}
