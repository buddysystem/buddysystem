<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\MessagingRecipientTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstituteRepository")
 *
 * @Vich\Uploadable
 */
class Institute implements MatchingManagerInterface, BasicInformationInterface, MatchingCriteriaInterface
{
    use BasicInformationTrait;
    use MatchingCriteriaTrait;
    use TimestampableEntity;
    public const MAX_LEVEL = 4;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    protected $schacId;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadInstitute", mappedBy="institute", cascade={"persist"}))
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $threads;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $showBonusOption = false;

    /**
     * @var Semester
     *
     * @ORM\ManyToOne(targetEntity="Semester")
     */
    protected $semester;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="institute", cascade={"persist"})
     */
    private $users;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="RegistrationEmailDomain", mappedBy="institute", cascade={"persist", "remove"})
     */
    private $registrationEmailDomains;

    /**
     * @var Institute|null
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="children", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Institute", mappedBy="parent", cascade={"persist"})
     * @Orm\OrderBy({"name" : "ASC"})
     */
    private $children;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Institute")
     * @Orm\OrderBy({"level" : "DESC"})
     */
    private $ancestors;

    /**
     * @var Institute|null
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="copies", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $duplicate;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Institute", mappedBy="duplicate", cascade={"persist"})
     */
    private $copies;

    /**
     * @var Institute|null
     *
     * @ORM\ManyToOne(targetEntity="Institute")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $root;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AssociationInstitute", mappedBy="institute", cascade={"persist", "remove"})
     */
    private $associationInstitutes;

    /**
     * @var int
     *
     * @Assert\Range(min="0", max=Institute::MAX_LEVEL)
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $level = 0;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Study", inversedBy="institutes", cascade={"persist"})
     * @Orm\OrderBy({"name" : "ASC"})
     */
    private $studies;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $studiesRequired = true;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="City", inversedBy="institutes", cascade={"persist"})
     */
    private $cities;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Stat", mappedBy="institute", cascade={"persist", "remove"})
     */
    private $stats;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Buddy", mappedBy="institute", cascade={"persist"})
     */
    private $buddies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Hobby", inversedBy="institutes", cascade={"persist"})
     */
    private $hobbies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Motivation", inversedBy="institutes", cascade={"persist"})
     */
    private $motivations;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="institute", cascade={"persist", "remove"})
     */
    private $documents;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $lowerLevelManagement = true;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->studies = new ArrayCollection();
        $this->associationInstitutes = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->hobbies = new ArrayCollection();
        $this->motivations = new ArrayCollection();
        $this->stats = new ArrayCollection();
        $this->buddies = new ArrayCollection();
        $this->copies = new ArrayCollection();
        $this->ancestors = new ArrayCollection();
        $this->threads = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->registrationEmailDomains = new ArrayCollection();

        $this->firstSemesterStart = new \DateTime('2014-09-01');
        $this->firstSemesterEnd = new \DateTime('2014-01-01');
        $this->secondSemesterStart = new \DateTime('2014-01-01');
        $this->secondSemesterEnd = new \DateTime('2014-07-01');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchacId(): ?string
    {
        return $this->schacId;
    }

    public function setSchacId(?string $schacId): void
    {
        $this->schacId = $schacId;
    }

    public function getThreads(): Collection
    {
        return $this->threads;
    }

    public function isShowBonusOption(): bool
    {
        return $this->showBonusOption;
    }

    public function setShowBonusOption(bool $showBonusOption): void
    {
        $this->showBonusOption = $showBonusOption;
    }

    public function getSemester(): ?Semester
    {
        return $this->semester;
    }

    public function setSemester(Semester $semester): void
    {
        $this->semester = $semester;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getRegistrationEmailDomains(): Collection
    {
        return $this->registrationEmailDomains;
    }

    public function getParent(): ?Institute
    {
        return $this->parent;
    }

    public function setParent(?Institute $parent): void
    {
        $this->parent = $parent;

        $root = $parent;
        if ($root) {
            while ($root->getParent()) {
                $root = $root->getParent();
            }
        }

        $this->setRoot($root);
        $this->setAncestors();
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function hasChildren(): bool
    {
        return !$this->children->isEmpty();
    }

    public function getAncestors(): Collection
    {
        return $this->ancestors;
    }

    public function setAncestors(): void
    {
        $this->ancestors = new ArrayCollection();

        foreach ($this->getParents() as $parent) {
            if (!$this->ancestors->contains($parent)) {
                $this->ancestors->add($parent);
            }
        }

        /** @var Institute $child */
        foreach ($this->children as $child) {
            $child->setAncestors();
        }
    }

    public function getDuplicate(): ?Institute
    {
        return $this->duplicate;
    }

    public function setDuplicate(?Institute $duplicate): void
    {
        $this->duplicate = $duplicate;
    }

    public function getCopies(): Collection
    {
        return $this->copies;
    }

    public function getRoot(): ?Institute
    {
        return $this->root;
    }

    public function setRoot(?Institute $root): void
    {
        $this->root = $root;

        /** @var Institute $child */
        foreach ($this->children as $child) {
            $child->setRoot($root ?? $this);
        }
    }

    public function getAssociationInstitutes(): Collection
    {
        return $this->associationInstitutes;
    }

    public function hasAssociationInstitutes(): bool
    {
        return !$this->associationInstitutes->isEmpty();
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function isLevel(int $level): bool
    {
        return $this->level === $level;
    }

    public function setLevel(int $level = null): void
    {
        if (!$level) {
            $level = $this->hasChildren() ? 1 : 0;
        }

        $this->level = $level;

        $level = ++$level;
        foreach ($this->children as $child) {
            $child->setLevel($level);
        }
    }

    public function getStudies(): Collection
    {
        return $this->studies;
    }

    public function isStudiesRequired(): bool
    {
        return $this->studiesRequired;
    }

    public function setStudiesRequired(bool $studiesRequired): void
    {
        $this->studiesRequired = $studiesRequired;
    }

    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function setCities(Collection $cities): void
    {
        $this->cities = $cities;
    }

    public function getStats(): Collection
    {
        return $this->stats;
    }

    public function getBuddies(): Collection
    {
        return $this->buddies;
    }

    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function getMotivations(): Collection
    {
        return $this->motivations;
    }

    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function isLowerLevelManagement(): bool
    {
        return $this->lowerLevelManagement;
    }

    public function setLowerLevelManagement(bool $lowerLevelManagement): void
    {
        $this->lowerLevelManagement = $lowerLevelManagement;
    }

    public function getFullname(): string
    {
        $name = '';

        /** @var Institute $parent */
        foreach ($this->getParents() as $parent) {
            if (!empty($name)) {
                $name .= ' >> ';
            }

            $name .= $parent->getName();
        }

        if (!empty($name)) {
            $name .= ' >> ';
        }

        return $name.$this->name;
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setInstitute($this);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function addChild(Institute $child): void
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
        }
    }

    public function removeChild(Institute $child): void
    {
        if (!$this->children->contains($child)) {
            return;
        }
        $this->children->removeElement($child);
        $child->setParent(null);
    }

    public function getAllChildren(): array
    {
        $children = [];

        /** @var Institute $child */
        foreach ($this->children as $child) {
            $children[] = $child;
            $children = array_merge($children, $child->getAllChildren());
        }

        return $children;
    }

    public function getParents(): array
    {
        $parents = [];

        $parent = $this->parent;
        while ($parent) {
            array_unshift($parents, $parent);
            $parent = $parent->getParent();
        }

        if ($this->root && !\in_array($this->root, $parents)) {
            array_unshift($parents, $this->root);
        }

        return $parents;
    }

    public function isParentOf(Institute $institute): bool
    {
        if ($this->children->contains($institute)) {
            return true;
        }

        /** @var Institute $child */
        foreach ($this->getChildren() as $child) {
            if ($child->isParentOf($institute)) {
                return true;
            }
        }

        return false;
    }

    public function getDepth(): int
    {
        $maxDepth = 1;

        /** @var Institute $child */
        foreach ($this->children as $child) {
            $depth = $child->getDepth() + 1;

            if ($depth > $maxDepth) {
                $maxDepth = $depth;
            }
        }

        return $maxDepth;
    }

    public function addAssociationInstitute(AssociationInstitute $associationInstitute): void
    {
        if (!$this->associationInstitutes->contains($associationInstitute)) {
            $this->associationInstitutes->add($associationInstitute);
            $associationInstitute->setInstitute($this);
        }
    }

    public function removeAssociationInstitute(AssociationInstitute $associationInstitute): void
    {
        $this->associationInstitutes->removeElement($associationInstitute);
    }

    public function isAssociationInstituteExists(Association $association): bool
    {
        /** @var AssociationInstitute $associationInstitute */
        foreach ($this->associationInstitutes as $associationInstitute) {
            if ($associationInstitute->getAssociation() === $association) {
                return true;
            }
        }

        return false;
    }

    public function addStudy(Study $study): void
    {
        if (!$this->studies->contains($study)) {
            $this->studies->add($study);
        }
    }

    public function removeStudy(Study $study): void
    {
        $this->studies->removeElement($study);
    }

    public function hasSocialLink(): bool
    {
        return !empty($this->website);
    }

    public function addCity(City $city): void
    {
        if (!$this->cities->contains($city)) {
            $this->cities->add($city);
            $city->addInstitute($this);
        }
    }

    public function removeCity(City $city): void
    {
        $this->cities->removeElement($city);
    }

    public function getCitiesWithParents(): array
    {
        $cities = $this->getCities()->toArray();

        /** @var Institute $parent */
        foreach ($this->getParents() as $parent) {
            $cities = array_intersect($cities, $parent->getCities()->toArray());
        }

        return $cities;
    }

    public function addBuddy(Buddy $buddy): void
    {
        if (!$this->buddies->contains($buddy)) {
            $this->buddies->add($buddy);
            $buddy->setInstitute($this);
        }
    }

    public function removeBuddy(Buddy $buddy): void
    {
        $this->buddies->removeElement($buddy);
    }

    public function addMotivation(Motivation $motivation): void
    {
        if (!$this->motivations->contains($motivation)) {
            $this->motivations->add($motivation);
        }
    }

    public function removeMotivation(Motivation $motivation): void
    {
        $this->motivations->removeElement($motivation);
    }

    public function addHobby(Hobby $hobby): void
    {
        if (!$this->hobbies->contains($hobby)) {
            $this->hobbies->add($hobby);
        }
    }

    public function removeHobby(Hobby $hobby): void
    {
        $this->hobbies->removeElement($hobby);
    }

    public function addStat(Stat $stat): void
    {
        if (!$this->stats->contains($stat)) {
            $this->stats->add($stat);
            $stat->setInstitute($this);
        }
    }

    public function removeStat(Stat $stat): void
    {
        $this->stats->removeElement($stat);
    }

    public function addDocument(Document $document): void
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setInstitute($this);
        }
    }

    public function removeDocument(Document $document): void
    {
        $this->documents->removeElement($document);
    }

    public function addCopy(Institute $copy): void
    {
        if ($copy !== $this && !$this->copies->contains($copy)) {
            $this->copies->add($copy);

            $copy->setDuplicate($this);
        }
    }

    public function removeCopy(Institute $copy): void
    {
        $this->copies->removeElement($copy);
    }

    public function addThread(ThreadManagerInterface $thread): void
    {
        if ($thread instanceof ThreadInstitute
            && !$this->threads->contains($thread)
        ) {
            $this->threads->add($thread);
            $thread->setInstitute($this);
        }
    }

    public function removeThread(ThreadManagerInterface $thread): void
    {
        $this->threads->removeElement($thread);
    }

    public function canAddMessageInThread(ThreadManagerInterface $thread): bool
    {
        $canAddMessage = $this->getThreads()->contains($thread);

        if (!$canAddMessage) {
            /** @var Institute $child */
            foreach ($this->getAllChildren() as $child) {
                if ($child->getThreads()->contains($thread)) {
                    $canAddMessage = true;
                    break;
                }
            }
        }

        return $canAddMessage
            || (
                MessagingRecipientTypeEnum::ONLY_MANAGERS === $thread->getRecipientType()
                && $this->getRoot() && $this->getRoot()->getThreads()->contains($thread)
            )
        ;
    }

    public function addRegistrationEmailDomain(RegistrationEmailDomain $registrationEmailDomain): void
    {
        if (!$this->registrationEmailDomains->contains($registrationEmailDomain)) {
            $this->registrationEmailDomains->add($registrationEmailDomain);
            $registrationEmailDomain->setInstitute($this);
        }
    }

    public function removeRegistrationEmailDomain(RegistrationEmailDomain $registrationEmailDomain): void
    {
        $this->registrationEmailDomains->removeElement($registrationEmailDomain);
    }
}
