<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 */
class RegistrationEmailDomain
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Institute
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="registrationEmailDomains")
     */
    private $institute;

    /**
     * @var string
     *
     * @ORM\Column(nullable=false, type="RegistrationEmailDomainUserTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\RegistrationEmailDomainUserTypeEnum")
     */
    private $userType;

    /**
     * @var string
     *
     * @ORM\Column(nullable=false)
     */
    private $domain;

    public function getId(): int
    {
        return $this->id;
    }

    public function getInstitute(): Institute
    {
        return $this->institute;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function getUserType(): string
    {
        return $this->userType;
    }

    public function setUserType(string $userType): void
    {
        $this->userType = $userType;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(string $domain): void
    {
        $this->domain = $domain;
    }
}
