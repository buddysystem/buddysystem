<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\BuddyStatusEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BuddyRepository")
 * @Gedmo\SoftDeleteable
 */
class Buddy
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mentors")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $mentor;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mentees")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $mentee;

    /**
     * @var string
     *
     * @ORM\Column(type="BuddyStatusType", options={"default" : "pending"})
     * @DoctrineAssert\Enum(entity="App\Enum\BuddyStatusEnum")
     */
    private $status = BuddyStatusEnum::STATUS_PENDING;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true, type="BuddyRefusedType")
     * @DoctrineAssert\Enum(entity="App\Enum\BuddyRefuseEnum")
     */
    private $reasonRefusal;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $reasonRefusalOther;

    /**
     * @var \DateTime|null
     *
     * @Gedmo\Timestampable(on="change", field="{'status'}")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $responseAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $warnedAt;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Report", mappedBy="buddy", cascade={"persist"})
     */
    private $reports;

    /**
     * @var Institute
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="buddies")
     */
    private $institute;

    /**
     * @var Association|null
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="buddies")
     */
    private $association;

    /**
     * @var ThreadBuddy|null
     *
     * @ORM\OneToOne(targetEntity="App\Entity\ThreadBuddy", mappedBy="buddy", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $thread;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $archived = false;

    /**
     * @var Semester
     *
     * @ORM\ManyToOne(targetEntity="Semester", inversedBy="buddies", cascade={"persist"})
     */
    private $semester;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $languagePairing = false;

    public function __construct(User $mentor, User $mentee)
    {
        $this->mentor = $mentor;
        $this->mentee = $mentee;
        $this->reports = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMentor(): User
    {
        return $this->mentor;
    }

    public function setMentor(User $mentor): void
    {
        $this->mentor = $mentor;
    }

    public function getMentee(): User
    {
        return $this->mentee;
    }

    public function setMentee(User $mentee): void
    {
        $this->mentee = $mentee;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function getReasonRefusal(): ?string
    {
        return $this->reasonRefusal;
    }

    public function setReasonRefusal(?string $reasonRefusal): void
    {
        $this->reasonRefusal = $reasonRefusal;
    }

    public function getReasonRefusalOther(): ?string
    {
        return $this->reasonRefusalOther;
    }

    public function setReasonRefusalOther(?string $reasonRefusalOther): void
    {
        $this->reasonRefusalOther = $reasonRefusalOther;
    }

    public function getResponseAt(): ?\DateTime
    {
        return $this->responseAt;
    }

    public function setResponseAt(?\DateTime $responseAt): void
    {
        $this->responseAt = $responseAt;
    }

    public function getWarnedAt(): ?\DateTime
    {
        return $this->warnedAt;
    }

    public function setWarnedAt(?\DateTime $warnedAt): void
    {
        $this->warnedAt = $warnedAt;
    }

    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function getInstitute(): Institute
    {
        return $this->institute;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;

        $this->setSemester($institute->getSemester());
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(Association $association): void
    {
        $this->association = $association;
    }

    public function getThread(): ?ThreadBuddy
    {
        return $this->thread;
    }

    public function setThread(?ThreadBuddy $thread): void
    {
        $this->thread = $thread;

        if ($thread) {
            $thread->setBuddy($this);
        }
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): void
    {
        $this->archived = $archived;
    }

    public function getSemester(): Semester
    {
        return $this->semester;
    }

    public function setSemester(Semester $semester): void
    {
        $this->semester = $semester;
    }

    public function isLanguagePairing(): bool
    {
        return $this->languagePairing;
    }

    public function setLanguagePairing(bool $languagePairing): void
    {
        $this->languagePairing = $languagePairing;
    }

    public function addReport(Report $report): void
    {
        if (!$this->reports->contains($report)) {
            $this->reports->add($report);
            $report->setBuddy($this);
        }
    }

    public function removeReport(Report $report): void
    {
        $this->reports->removeElement($report);
    }

    public function getMatchingManager(): ?MatchingManagerInterface
    {
        return $this->association ?? $this->institute;
    }

    public function getLastMessageAt(): ?\DateTime
    {
        if ($thread = $this->thread) {
            if ($message = $thread->getMessages()->last()) {
                return $message->getCreatedAt();
            }
        }

        return null;
    }
}
