<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 */
class ThreadAssociation extends Thread implements ThreadManagerInterface
{
    /**
     * @var Association
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="threads")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $association;

    /**
     * @var string
     *
     * @ORM\Column(type="MessagingRecipientTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\MessagingRecipientTypeEnum")
     */
    private $recipientType;

    public function getAssociation(): Association
    {
        return $this->association;
    }

    public function setAssociation(Association $association): void
    {
        $this->association = $association;
    }

    public function getRecipientType(): string
    {
        return $this->recipientType;
    }

    public function setRecipientType(string $type): void
    {
        $this->recipientType = $type;
    }

    public function getMatchingManager(): MatchingManagerInterface
    {
        return $this->association;
    }

    public function getMatchingManagerType(): string
    {
        return MatchingManagerInterface::TYPE_ASSOCIATION;
    }
}
