<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields={"nameMentee", "nameMentor"}, message="name.unique")
 * @ORM\Entity(repositoryClass="App\Repository\MotivationRepository")
 */
class Motivation implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column
     */
    private $nameMentee;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column
     */
    private $nameMentor;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="motivation", cascade={"persist"})
     */
    private $users;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Institute", mappedBy="motivations", cascade={"persist"})
     */
    private $institutes;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Association", mappedBy="motivations", cascade={"persist"})
     */
    private $associations;

    /**
     * @Gedmo\Locale
     *
     * @var string
     */
    private $locale;

    public function __construct()
    {
        $this->institutes = new ArrayCollection();
        $this->associations = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getNameMentee(): ?string
    {
        return $this->nameMentee;
    }

    public function setNameMentee(string $nameMentee): void
    {
        $this->nameMentee = $nameMentee;
    }

    public function getNameMentor(): ?string
    {
        return $this->nameMentor;
    }

    public function setNameMentor(string $nameMentor): void
    {
        $this->nameMentor = $nameMentor;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getInstitutes(): Collection
    {
        return $this->institutes;
    }

    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function getName(bool $isLocal): string
    {
        return $isLocal
            ? $this->nameMentor
            : $this->nameMentee
        ;
    }

    public function setTranslatableLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    public function getTranslatableLocale(): ?string
    {
        return $this->locale;
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setMotivation($this);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function addInstitute(Institute $institute): void
    {
        if (!$this->institutes->contains($institute)) {
            $this->institutes->add($institute);
            $institute->addMotivation($this);
        }
    }

    public function removeInstitute(Institute $institute): void
    {
        $this->institutes->removeElement($institute);
        $institute->removeMotivation($this);
    }

    public function addAssociation(Association $association): void
    {
        if (!$this->associations->contains($association)) {
            $this->associations->add($association);
            $association->addMotivation($this);
        }
    }

    public function removeAssociation(Association $association): void
    {
        $this->associations->removeElement($association);
        $association->removeMotivation($this);
    }
}
