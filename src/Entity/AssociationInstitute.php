<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AssociationInstitutePermEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssociationInstituteRepository")
 * @UniqueEntity(fields={"association", "institute"})
 */
class AssociationInstitute
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var Association
     *
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="associationInstitutes")
     */
    private $association;

    /**
     * @var Institute
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="associationInstitutes", cascade={"persist"})
     */
    private $institute;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $rights;

    public function __construct()
    {
        $this->rights = [AssociationInstitutePermEnum::PERM_VIEW];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAssociation(): Association
    {
        return $this->association;
    }

    public function setAssociation(Association $association): void
    {
        $this->association = $association;
    }

    public function getInstitute(): Institute
    {
        return $this->institute;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function getRights(): array
    {
        return $this->rights;
    }

    public function setRights(array $rights): void
    {
        $this->rights = $rights;
    }

    public function canView(): bool
    {
        return \in_array(AssociationInstitutePermEnum::PERM_VIEW, $this->rights);
    }

    public function setCanView(bool $canView): void
    {
        if ($canView) {
            $this->addRight(AssociationInstitutePermEnum::PERM_VIEW);
        } else {
            $this->removeRight(AssociationInstitutePermEnum::PERM_VIEW);
        }
    }

    public function canMatch(): bool
    {
        return \in_array(AssociationInstitutePermEnum::PERM_MATCH, $this->rights);
    }

    public function setCanMatch(bool $canMatch): void
    {
        if ($canMatch) {
            $this->addRight(AssociationInstitutePermEnum::PERM_MATCH);
        } else {
            $this->removeRight(AssociationInstitutePermEnum::PERM_MATCH);
        }
    }

    public function canEditPreferences(): bool
    {
        return \in_array(AssociationInstitutePermEnum::PERM_PREFERENCES, $this->rights);
    }

    public function setEditPreferences(bool $canEditPreferences): void
    {
        if ($canEditPreferences) {
            $this->addRight(AssociationInstitutePermEnum::PERM_PREFERENCES);
        } else {
            $this->removeRight(AssociationInstitutePermEnum::PERM_PREFERENCES);
        }
    }

    public function getUpperRight(): ?string
    {
        if ($this->canEditPreferences()) {
            return AssociationInstitutePermEnum::PERM_PREFERENCES;
        }

        if ($this->canMatch()) {
            return AssociationInstitutePermEnum::PERM_MATCH;
        }

        if ($this->canView()) {
            return AssociationInstitutePermEnum::PERM_VIEW;
        }

        return null;
    }

    public function addRight(string $right): void
    {
        if (false === array_search($right, $this->rights)) {
            array_push($this->rights, $right);
        }
    }

    public function removeRight(string $right): void
    {
        $key = array_search($right, $this->rights);
        if (false !== $key) {
            unset($this->rights[$key]);
        }
    }
}
