<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;

interface BasicInformationInterface
{
    public function setName(string $name): void;

    public function getName(): ?string;

    public function setDescription(?string $description): void;

    public function getDescription(): ?string;

    public function getWebsite(): ?string;

    public function setWebsite(?string $website): void;

    public function setLogoFile(File $logo): void;

    public function getLogoFile(): ?File;

    public function setLogoName(?string $logoName): void;

    public function getLogoName(): ?string;

    public function addCity(City $city): void;

    public function removeCity(City $city): void;

    public function getCities(): Collection;

    public function increaseRegistration(bool $isIframe = false): void;
}
