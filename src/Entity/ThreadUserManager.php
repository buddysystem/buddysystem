<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThreadUserManagerRepository")
 */
class ThreadUserManager extends Thread implements ThreadInterface
{
    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Association")
     */
    private $associations;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Institute")
     * @Orm\OrderBy({"level" : "DESC"})
     */
    private $institutes;

    public function __construct()
    {
        parent::__construct();
        $this->associations = new ArrayCollection();
        $this->institutes = new ArrayCollection();
    }

    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function getInstitutes(): Collection
    {
        return $this->institutes;
    }

    public function addAssociation(Association $association): void
    {
        if (!$this->associations->contains($association)) {
            $this->associations->add($association);
        }
    }

    public function removeAssociation(Association $association): void
    {
        $this->associations->removeElement($association);
    }

    public function addInstitute(Institute $institute): void
    {
        if (!$this->institutes->contains($institute)) {
            $this->institutes->add($institute);
        }
    }

    public function removeInstitute(Institute $institute): void
    {
        $this->institutes->removeElement($institute);
    }

    public function setUser(User $user): void
    {
        if ($this->getUsers()->isEmpty()) {
            $this->addUser($user);
        }
    }

    public function getUser(): ?User
    {
        return $this->getUsers()->first() ?? null;
    }

    public function getMatchingManagerType(): ?string
    {
        if (!$this->associations->isEmpty()) {
            return MatchingManagerInterface::TYPE_ASSOCIATION;
        }

        if (!$this->institutes->isEmpty()) {
            return MatchingManagerInterface::TYPE_INSTITUTE;
        }

        return null;
    }

    public function addMatchingManager(MatchingManagerInterface $matchingManager): void
    {
        if ($matchingManager instanceof Association) {
            $this->addAssociation($matchingManager);
        } else {
            $this->addInstitute($matchingManager);
        }
    }

    public function getMatchingManagers(): ?Collection
    {
        switch ($this->getMatchingManagerType()) {
            case MatchingManagerInterface::TYPE_ASSOCIATION:
                return $this->getAssociations();
            case MatchingManagerInterface::TYPE_INSTITUTE:
                return $this->getInstitutes();
        }

        return null;
    }
}
