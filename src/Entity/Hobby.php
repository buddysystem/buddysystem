<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields="name", message="name.unique")
 * @ORM\Entity(repositoryClass="App\Repository\HobbyRepository")
 */
class Hobby implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Gedmo\Translatable
     * @ORM\Column
     */
    private $name;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Institute", mappedBy="hobbies", cascade={"persist"})
     */
    private $institutes;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Association", mappedBy="hobbies", cascade={"persist"})
     */
    private $associations;

    /**
     * @Gedmo\Locale
     *
     * @var string
     */
    private $locale;

    public function __construct()
    {
        $this->institutes = new ArrayCollection();
        $this->associations = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getId().'_'.$this->getName();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getInstitutes(): Collection
    {
        return $this->institutes;
    }

    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function addInstitute(Institute $institute): void
    {
        if (!$this->institutes->contains($institute)) {
            $this->institutes->add($institute);
            $institute->addHobby($this);
        }
    }

    public function removeInstitute(Institute $institute): void
    {
        $this->institutes->removeElement($institute);
        $institute->removeHobby($this);
    }

    public function addAssociation(Association $association): void
    {
        if (!$this->associations->contains($association)) {
            $this->associations->add($association);
            $association->addHobby($this);
        }
    }

    public function removeAssociation(Association $association): void
    {
        $this->associations->removeElement($association);
        $association->removeHobby($this);
    }

    public function setTranslatableLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    public function getTranslatableLocale(): ?string
    {
        return $this->locale;
    }
}
