<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserApprovedHistoryRepository")
 */
class UserApprovedHistory
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $userApproved;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $userValidator;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUserApproved(): User
    {
        return $this->userApproved;
    }

    public function setUserApproved(User $userApproved): void
    {
        $this->userApproved = $userApproved;
    }

    public function getUserValidator(): User
    {
        return $this->userValidator;
    }

    public function setUserValidator(User $userValidator): void
    {
        $this->userValidator = $userValidator;
    }
}
