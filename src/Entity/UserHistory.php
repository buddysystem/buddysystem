<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class UserHistory
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbWarned = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbReport = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddiesRemoved = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddiesRefused = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbRefusedByBuddies = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddies = 0;

    /**
     * @var Semester
     *
     * @ORM\ManyToOne(targetEntity="Semester", inversedBy="histories", cascade={"persist"})
     *
     * @ORM\JoinColumn
     */
    private $semester;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="histories", cascade={"persist"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var Institute|null
     * @ORM\ManyToOne(targetEntity="Institute")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $institute;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private $local;

    public function getId(): int
    {
        return $this->id;
    }

    public function getNbWarned(): int
    {
        return $this->nbWarned;
    }

    public function setNbWarned(int $nbWarned): void
    {
        $this->nbWarned = $nbWarned;
    }

    public function getNbReport(): int
    {
        return $this->nbReport;
    }

    public function setNbReport(int $nbReport): void
    {
        $this->nbReport = $nbReport;
    }

    public function getNbBuddiesRemoved(): int
    {
        return $this->nbBuddiesRemoved;
    }

    public function setNbBuddiesRemoved(int $nbBuddiesRemoved): void
    {
        $this->nbBuddiesRemoved = $nbBuddiesRemoved;
    }

    public function getNbBuddiesRefused(): int
    {
        return $this->nbBuddiesRefused;
    }

    public function setNbBuddiesRefused(int $nbBuddiesRefused): void
    {
        $this->nbBuddiesRefused = $nbBuddiesRefused;
    }

    public function getNbRefusedByBuddies(): int
    {
        return $this->nbRefusedByBuddies;
    }

    public function setNbRefusedByBuddies(int $nbRefusedByBuddies): void
    {
        $this->nbRefusedByBuddies = $nbRefusedByBuddies;
    }

    public function getNbBuddies(): int
    {
        return $this->nbBuddies;
    }

    public function setNbBuddies(int $nbBuddies): void
    {
        $this->nbBuddies = $nbBuddies;
    }

    public function getSemester(): Semester
    {
        return $this->semester;
    }

    public function setSemester(Semester $semester): void
    {
        $this->semester = $semester;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getInstitute(): ?Institute
    {
        return $this->institute;
    }

    public function setInstitute(?Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function isLocal(): ?bool
    {
        return $this->local;
    }

    public function setLocal(?bool $local): void
    {
        $this->local = $local;
    }
}
