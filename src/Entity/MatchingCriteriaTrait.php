<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait MatchingCriteriaTrait
{
    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    protected $activated = true;

    /**
     * @Assert\GreaterThan(0)
     *
     * @var int
     * @ORM\Column(type="integer", options={"default" : 2, "unsigned" : true})
     */
    protected $nbMentors = 2;

    /**
     * @Assert\GreaterThan(0)
     *
     * @var int
     * @ORM\Column(type="integer", options={"default" : 2, "unsigned" : true})
     */
    protected $nbMentees = 2;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $fullYearOnly = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $firstSemesterStart;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="date", nullable=true)
     */
    protected $firstSemesterEnd;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="date", nullable=true)
     */
    protected $secondSemesterStart;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $secondSemesterEnd;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 2, "unsigned" : true})
     */
    protected $matchingHobbyValue = 4;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 7, "unsigned" : true})
     */
    protected $matchingStudyValue = 7;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 10, "unsigned" : true})
     */
    protected $matchingCityValue = 10;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[7]"})
     */
    protected $matchingInstituteValues = [7];

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 5, "unsigned" : true})
     */
    protected $matchingLevelOfStudyValue = 5;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 6, "unsigned" : true})
     */
    protected $matchingLanguageValue = 6;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 3, "unsigned" : true})
     */
    protected $matchingAgeValue = 3;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 8, "unsigned" : true})
     */
    protected $matchingMotivationValue = 8;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default" : 3, "unsigned" : true})
     */
    protected $matchingAvailabilityValue = 3;

    /**
     * @Assert\Range(min=1)
     *
     * @var int
     * @ORM\Column(type="integer", options={"default" : 7, "unsigned" : true})
     */
    protected $revivalDelay = 7;

    /**
     * @Assert\Range(min=2)
     *
     * @var int
     * @ORM\Column(type="integer", options={"default" : 14, "unsigned" : true})
     * @Assert\Type(type="integer")
     * @Assert\Expression(expression="this.getRevivalDelay() <= value > 0", message="This value must be upper than the warning days value")
     */
    protected $unmatchDelay = 14;

    /**
     * @Assert\Range(min=0)
     *
     * @var int
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    protected $nbBuddiesRefusedMax = 0;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    protected $mentorValidationRequired = true;

    public function isActivated(): bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated): void
    {
        $this->activated = $activated;
    }

    public function getNbMentors(): int
    {
        return $this->nbMentors;
    }

    public function setNbMentors(int $nbMentors): void
    {
        $this->nbMentors = $nbMentors;
    }

    public function getNbMentees(): int
    {
        return $this->nbMentees;
    }

    public function setNbMentees(int $nbMentees): void
    {
        $this->nbMentees = $nbMentees;
    }

    public function isFullYearOnly(): bool
    {
        return $this->fullYearOnly;
    }

    public function setFullYearOnly(bool $fullYearOnly): void
    {
        $this->fullYearOnly = $fullYearOnly;
    }

    public function getFirstSemesterStart(): \DateTime
    {
        return $this->firstSemesterStart;
    }

    public function setFirstSemesterStart(\DateTime $firstSemesterStart): void
    {
        $this->firstSemesterStart = $firstSemesterStart;
    }

    public function getFirstSemesterEnd(): ?\DateTime
    {
        return $this->firstSemesterEnd;
    }

    public function setFirstSemesterEnd(?\DateTime $firstSemesterEnd): void
    {
        $this->firstSemesterEnd = $firstSemesterEnd;
    }

    public function getSecondSemesterStart(): ?\DateTime
    {
        return $this->secondSemesterStart;
    }

    public function setSecondSemesterStart(?\DateTime $secondSemesterStart): void
    {
        $this->secondSemesterStart = $secondSemesterStart;
    }

    public function getSecondSemesterEnd(): \DateTime
    {
        return $this->secondSemesterEnd;
    }

    public function setSecondSemesterEnd(\DateTime $secondSemesterEnd): void
    {
        $this->secondSemesterEnd = $secondSemesterEnd;
    }

    public function getMatchingHobbyValue(): int
    {
        return $this->matchingHobbyValue;
    }

    public function setMatchingHobbyValue(int $matchingHobbyValue): void
    {
        $this->matchingHobbyValue = $matchingHobbyValue;
    }

    public function getMatchingStudyValue(): int
    {
        return $this->matchingStudyValue;
    }

    public function setMatchingStudyValue(int $matchingStudyValue): void
    {
        $this->matchingStudyValue = $matchingStudyValue;
    }

    public function getMatchingCityValue(): int
    {
        return $this->matchingCityValue;
    }

    public function setMatchingCityValue(int $matchingCityValue): void
    {
        $this->matchingCityValue = $matchingCityValue;
    }

    public function getMatchingInstituteValues(): array
    {
        return $this->matchingInstituteValues;
    }

    public function setMatchingInstituteValues(array $matchingInstituteValues): void
    {
        $this->matchingInstituteValues = $matchingInstituteValues;
    }

    public function getMatchingLevelOfStudyValue(): int
    {
        return $this->matchingLevelOfStudyValue;
    }

    public function setMatchingLevelOfStudyValue(int $matchingLevelOfStudyValue): void
    {
        $this->matchingLevelOfStudyValue = $matchingLevelOfStudyValue;
    }

    public function getMatchingLanguageValue(): int
    {
        return $this->matchingLanguageValue;
    }

    public function setMatchingLanguageValue(int $matchingLanguageValue): void
    {
        $this->matchingLanguageValue = $matchingLanguageValue;
    }

    public function getMatchingAgeValue(): int
    {
        return $this->matchingAgeValue;
    }

    public function setMatchingAgeValue(int $matchingAgeValue): void
    {
        $this->matchingAgeValue = $matchingAgeValue;
    }

    public function getMatchingMotivationValue(): int
    {
        return $this->matchingMotivationValue;
    }

    public function setMatchingMotivationValue(int $matchingMotivationValue): void
    {
        $this->matchingMotivationValue = $matchingMotivationValue;
    }

    public function getMatchingAvailabilityValue(): int
    {
        return $this->matchingAvailabilityValue;
    }

    public function setMatchingAvailabilityValue(int $matchingAvailabilityValue): void
    {
        $this->matchingAvailabilityValue = $matchingAvailabilityValue;
    }

    public function getRevivalDelay(): int
    {
        return $this->revivalDelay;
    }

    public function setRevivalDelay(int $revivalDelay): void
    {
        $this->revivalDelay = $revivalDelay;
    }

    public function getUnmatchDelay(): int
    {
        return $this->unmatchDelay;
    }

    public function setUnmatchDelay(int $unmatchDelay): void
    {
        $this->unmatchDelay = $unmatchDelay;
    }

    public function getNbBuddiesRefusedMax(): int
    {
        return $this->nbBuddiesRefusedMax;
    }

    public function setNbBuddiesRefusedMax(int $nbBuddiesRefusedMax): void
    {
        $this->nbBuddiesRefusedMax = $nbBuddiesRefusedMax;
    }

    public function isMentorValidationRequired(): bool
    {
        return $this->mentorValidationRequired;
    }

    public function setMentorValidationRequired(bool $mentorValidationRequired): void
    {
        $this->mentorValidationRequired = $mentorValidationRequired;
    }

    public function getMatchingInstituteValue(int $level = 0): int
    {
        return $this->matchingInstituteValues[$level] ?? 0;
    }
}
