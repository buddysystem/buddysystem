<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\UserLevelStudyEnum;
use App\Enum\UserRegistrationTypeEnum;
use App\Enum\UserRoleEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use EmailChecker\Constraints as EmailCheckerAssert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Hslavich\OneloginSamlBundle\Security\User\SamlUserInterface;
use libphonenumber\PhoneNumber;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Email;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="user.email.unique")
 * @Vich\Uploadable
 */
class User implements SamlUserInterface, \Serializable, PasswordAuthenticatedUserInterface
{
    use TimestampableEntity;

    public const MIN_AGE = 16;
    public const MAX_AGE = 50;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    protected $bonusOption = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $enabled = false;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLogin;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $languages = [];

    /**
     * @var Motivation|null
     *
     * @ORM\ManyToOne(targetEntity="Motivation", inversedBy="users"))
     */
    private $motivation;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\Email(mode=Email::VALIDATION_MODE_HTML5)
     * @EmailCheckerAssert\NotThrowawayEmail
     */
    private $email;

    /**
     * @var array
     *
     * @ORM\Column(type="array")
     */
    private $roles = [];

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $firstname;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $lastname;

    /**
     * @var PhoneNumber|null
     *
     * @ORM\Column(type="phone_number", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string|null
     * @Assert\NotBlank(groups={"ProfileUser", "RegistrationUser"})
     * @ORM\Column(name="gender", length=1, nullable=true)
     */
    private $sex;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sexWanted;

    /**
     * @var \DateTime|null
     *
     * @Assert\NotBlank(groups={"ProfileUser", "RegistrationUser"})
     * @ORM\Column(name="date_of_birthday", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_of_arrival", type="date", nullable=true)
     */
    private $arrival;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date_of_depart", type="date", nullable=true)
     */
    private $departure;

    /**
     * @var string|null
     *
     * @ORM\Column(length=10, nullable=true)
     */
    private $esncard;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $privacyAccepted;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $privacyAcceptedAt;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $privacyAcceptedWith;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $termsOfUse;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $charterAccepted = false;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $newsletter;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Hobby")
     * @ORM\JoinTable(name="user_hobby")
     */
    private $hobbies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Study")
     * @ORM\JoinTable(name="user_study")
     */
    private $studies;

    /**
     * @var array
     *
     * @ORM\Column(type="json", options={"default" : "[]"})
     */
    private $languagesWanted = [];

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isMatched = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddies = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddiesArchived = 0;

    /**
     * @var City|null
     * @Assert\NotBlank(groups={
     *     "ProfileUser",
     *     "RegistrationUser",
     *     "ProfileRI",
     *     "RegistrationRI",
     *     "ProfileBC",
     *     "RegistrationBC"
     * })
     * @ORM\ManyToOne(targetEntity="City")
     */
    private $city;

    /**
     * @var Country|null
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="Country")
     */
    private $country;

    /**
     * @var File|null
     *
     * @Vich\UploadableField(mapping="profile_picture", fileNameProperty="pictureName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     */
    private $pictureFile;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $pictureName;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbWarned = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbReport = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddiesRemoved = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbBuddiesRefused = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbRefusedByBuddies = 0;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $overrideMaxRefusedLimitation = false;

    /**
     * @var Institute|null
     * @Assert\NotBlank(groups={"ProfileUser", "RegistrationUser", "ProfileRI", "RegistrationRI"})
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="users")
     */
    private $institute;

    /**
     * @var Association|null
     * @Assert\NotBlank(groups={"ProfileBC", "RegistrationBC"})
     * @ORM\ManyToOne(targetEntity="Association", inversedBy="users")
     */
    private $association;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean")
     */
    private $local = true;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @Assert\Range(min=1)
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 1, "unsigned" : true})
     */
    private $nbBuddiesWanted = 1;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true, length=2)
     */
    private $nationality;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $archived = false;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Buddy", mappedBy="mentor", cascade={"persist", "remove"})
     */
    private $mentors;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Buddy", mappedBy="mentee", cascade={"persist", "remove"})
     */
    private $mentees;

    /**
     * @var Semester|null
     *
     * @Assert\NotBlank(groups={"ProfileUser", "RegistrationUser"})
     *
     * @ORM\ManyToOne(targetEntity="Semester", inversedBy="users", cascade={"persist"})
     */
    private $semester;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Report", mappedBy="user", cascade={"persist"}))
     */
    private $reports;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="UserHistory", mappedBy="user", cascade={"persist", "remove"}))
     */
    private $histories;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $isAskNextSemester = false;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $localeLanguage;

    /**
     * @var UserNotification|null
     *
     * @ORM\OneToOne(targetEntity="UserNotification", cascade={"persist", "remove"})
     */
    private $userNotification;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="sender", cascade={"persist", "remove"}))
     */
    private $messages;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadMetadata", mappedBy="participant", cascade={"persist", "remove"}))
     */
    private $threadMetadatas;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadFavorite", mappedBy="user", cascade={"persist", "remove"}))
     */
    private $threadFavorites;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Thread", mappedBy="users", cascade={"persist", "remove"})
     * @ORM\OrderBy({"lastMessageAt" : "DESC"})
     */
    private $threads;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $approved = false;

    /**
     * @var int|null
     */
    private $score;

    /**
     * @var string|null
     *
     * @ORM\Column(type="UserLevelStudyType", nullable=true)
     * @Assert\NotBlank(groups={"ProfileUser", "RegistrationUser"})
     * @DoctrineAssert\Enum(entity="App\Enum\UserLevelStudyEnum")
     */
    private $levelOfStudy;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $levelOfStudyOther;

    /**
     * @var string|null
     *
     * @ORM\Column(type="UserTypeMobilityType", nullable=true)
     * @DoctrineAssert\Enum(entity="App\Enum\UserTypeMobilityEnum")
     */
    private $typeOfMobility;

    /**
     * @var string
     *
     * @ORM\Column(type="UserRegistrationType")
     * @DoctrineAssert\Enum(entity="App\Enum\UserRegistrationTypeEnum")
     */
    private $typeOfRegistration = UserRegistrationTypeEnum::REGISTRATION_CLASSIC;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Extract", mappedBy="user", cascade={"persist", "remove"}))
     */
    private $extracts;

    public function __construct()
    {
        $this->mentors = new ArrayCollection();
        $this->mentees = new ArrayCollection();
        $this->hobbies = new ArrayCollection();
        $this->studies = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->histories = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->threads = new ArrayCollection();
        $this->threadMetadatas = new ArrayCollection();
        $this->threadFavorites = new ArrayCollection();
        $this->extracts = new ArrayCollection();
    }

    public function isBonusOption(): bool
    {
        return $this->bonusOption;
    }

    public function setBonusOption(bool $bonusOption): void
    {
        $this->bonusOption = $bonusOption;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getLastLogin(): ?\DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTime $lastLogin): void
    {
        $this->lastLogin = $lastLogin;
    }

    public function getLanguages(): array
    {
        return $this->languages;
    }

    public function setLanguages(array $languages): void
    {
        $this->languages = $languages;
    }

    public function getMotivation(): ?Motivation
    {
        return $this->motivation;
    }

    public function setMotivation(?Motivation $motivation): void
    {
        $this->motivation = $motivation;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = UserRoleEnum::ROLE_USER;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getPhoneNumber(): ?PhoneNumber
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?PhoneNumber $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): void
    {
        $this->sex = $sex;
    }

    public function getSexWanted(): ?bool
    {
        return $this->sexWanted;
    }

    public function setSexWanted(bool $sexWanted): void
    {
        $this->sexWanted = $sexWanted;
    }

    public function getDob(): ?\DateTime
    {
        return $this->dob;
    }

    public function setDob(?\DateTime $dob): void
    {
        $this->dob = $dob;
    }

    public function getArrival(): ?\DateTime
    {
        return $this->arrival;
    }

    public function setArrival(?\DateTime $arrival): void
    {
        $this->arrival = $arrival;
    }

    public function getDeparture(): ?\DateTime
    {
        return $this->departure;
    }

    public function setDeparture(?\DateTime $departure): void
    {
        $this->departure = $departure;
    }

    public function getEsncard(): ?string
    {
        return $this->esncard;
    }

    public function setEsncard(?string $esncard): void
    {
        $this->esncard = $esncard;
    }

    public function isPrivacyAccepted(): ?bool
    {
        return $this->privacyAccepted;
    }

    public function setPrivacyAccepted(bool $privacyAccepted): void
    {
        $this->privacyAccepted = $privacyAccepted;
    }

    public function getPrivacyAcceptedAt(): ?\DateTime
    {
        return $this->privacyAcceptedAt;
    }

    public function setPrivacyAcceptedAt(?\DateTime $privacyAcceptedAt): void
    {
        $this->privacyAcceptedAt = $privacyAcceptedAt;
    }

    public function getPrivacyAcceptedWith(): ?string
    {
        return $this->privacyAcceptedWith;
    }

    public function setPrivacyAcceptedWith(?string $privacyAcceptedWith): void
    {
        $this->privacyAcceptedWith = $privacyAcceptedWith;
    }

    public function getTermsOfUse(): ?bool
    {
        return $this->termsOfUse;
    }

    public function setTermsOfUse(bool $termsOfUse): void
    {
        $this->termsOfUse = $termsOfUse;
    }

    public function isCharterAccepted(): bool
    {
        return $this->charterAccepted;
    }

    public function setCharterAccepted(bool $charterAccepted): void
    {
        $this->charterAccepted = $charterAccepted;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): void
    {
        $this->newsletter = $newsletter;
    }

    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function getStudies(): Collection
    {
        return $this->studies;
    }

    public function getLanguagesWanted(): array
    {
        return $this->languagesWanted;
    }

    public function setLanguagesWanted(array $languagesWanted): void
    {
        $this->languagesWanted = $languagesWanted;
    }

    public function isMatched(): bool
    {
        return $this->isMatched;
    }

    public function setIsMatched(bool $isMatched): void
    {
        $this->isMatched = $isMatched;
    }

    public function getNbBuddies(): int
    {
        return $this->nbBuddies;
    }

    public function setNbBuddies(int $nbBuddies): void
    {
        $this->nbBuddies = $nbBuddies;
    }

    public function getNbBuddiesArchived(): int
    {
        return $this->nbBuddiesArchived;
    }

    public function setNbBuddiesArchived(int $nbBuddiesArchived): void
    {
        $this->nbBuddiesArchived = $nbBuddiesArchived;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): void
    {
        $this->city = $city;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }

    public function getPictureFile(): ?File
    {
        return $this->pictureFile;
    }

    public function setPictureFile(?File $picture): void
    {
        $this->pictureFile = $picture;

        if (null !== $picture) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function getPictureName(): ?string
    {
        return $this->pictureName;
    }

    public function setPictureName(?string $pictureName): void
    {
        $this->pictureName = $pictureName;
    }

    public function getNbWarned(): int
    {
        return $this->nbWarned;
    }

    public function setNbWarned(int $nbWarned): void
    {
        $this->nbWarned = $nbWarned;
    }

    public function getNbReport(): ?int
    {
        return $this->nbReport;
    }

    public function setNbReport(int $nbReport): void
    {
        $this->nbReport = $nbReport;
    }

    public function getNbBuddiesRemoved(): int
    {
        return $this->nbBuddiesRemoved;
    }

    public function setNbBuddiesRemoved(int $nbBuddiesRemoved): void
    {
        $this->nbBuddiesRemoved = $nbBuddiesRemoved;
    }

    public function getNbBuddiesRefused(): int
    {
        return $this->nbBuddiesRefused;
    }

    public function setNbBuddiesRefused(int $nbBuddiesRefused): void
    {
        $this->nbBuddiesRefused = $nbBuddiesRefused;
    }

    public function getNbRefusedByBuddies(): int
    {
        return $this->nbRefusedByBuddies;
    }

    public function setNbRefusedByBuddies(int $nbRefusedByBuddies): void
    {
        $this->nbRefusedByBuddies = $nbRefusedByBuddies;
    }

    public function isOverrideMaxRefusedLimitation(): bool
    {
        return $this->overrideMaxRefusedLimitation;
    }

    public function setOverrideMaxRefusedLimitation(bool $overrideMaxRefusedLimitation): void
    {
        $this->overrideMaxRefusedLimitation = $overrideMaxRefusedLimitation;
    }

    public function getInstitute(): ?Institute
    {
        return $this->institute;
    }

    public function setInstitute(?Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): void
    {
        $this->association = $association;
    }

    public function isLocal(): ?bool
    {
        return $this->local;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    public function getNbBuddiesWanted(): int
    {
        return $this->nbBuddiesWanted;
    }

    public function setNbBuddiesWanted(int $nbBuddiesWanted): void
    {
        $this->nbBuddiesWanted = $nbBuddiesWanted;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(?string $nationality): void
    {
        $this->nationality = $nationality;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setArchived(bool $archived): void
    {
        $this->archived = $archived;
    }

    public function getMentors(): Collection
    {
        return $this->mentors;
    }

    public function getMentees(): Collection
    {
        return $this->mentees;
    }

    public function getSemester(): ?Semester
    {
        return $this->semester;
    }

    public function setSemester(?Semester $semester): void
    {
        $this->semester = $semester;
    }

    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function isAskNextSemester(): bool
    {
        return $this->isAskNextSemester;
    }

    public function setIsAskNextSemester(bool $isAskNextSemester): void
    {
        $this->isAskNextSemester = $isAskNextSemester;
    }

    public function getLocaleLanguage(): string
    {
        return $this->localeLanguage;
    }

    public function setLocaleLanguage(string $localeLanguage): void
    {
        $this->localeLanguage = $localeLanguage;
    }

    public function getUserNotification(): ?UserNotification
    {
        return $this->userNotification;
    }

    public function setUserNotification(UserNotification $userNotification): void
    {
        $this->userNotification = $userNotification;
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function getThreadMetadatas(): Collection
    {
        return $this->threadMetadatas;
    }

    public function getThreadFavorites(): Collection
    {
        return $this->threadFavorites;
    }

    public function getThreads(): Collection
    {
        return $this->threads;
    }

    public function isApproved(): bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): void
    {
        $this->approved = $approved;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    public function getLevelOfStudy(): ?string
    {
        return $this->levelOfStudy;
    }

    public function setLevelOfStudy(?string $levelOfStudy): void
    {
        $this->levelOfStudy = $levelOfStudy;
    }

    public function getLevelOfStudyOther(): ?string
    {
        return $this->levelOfStudyOther;
    }

    public function setLevelOfStudyOther(?string $levelOfStudyOther): void
    {
        $this->levelOfStudyOther = $levelOfStudyOther;
    }

    public function getTypeOfMobility(): ?string
    {
        return $this->isLocal() ? null : $this->typeOfMobility;
    }

    public function setTypeOfMobility(?string $typeOfMobility): void
    {
        $this->typeOfMobility = $typeOfMobility;
    }

    public function getTypeOfRegistration(): string
    {
        return $this->typeOfRegistration;
    }

    public function setTypeOfRegistration(string $typeOfRegistration): void
    {
        $this->typeOfRegistration = $typeOfRegistration;
    }

    public function getExtracts(): Collection
    {
        return $this->extracts;
    }

    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    public function setFirstName(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getAge(): ?int
    {
        $age = ($this->dob instanceof \DateTimeInterface)
            ? date_diff(new \DateTime('now'), $this->dob)->y
            : null
        ;

        return (\is_int($age) && $age > 1) ? $age : null;
    }

    public function setIsLocal(?bool $local): void
    {
        $this->local = $local;
    }

    public function addMentor(Buddy $buddy): void
    {
        if (!$this->mentors->contains($buddy)) {
            $this->mentors->add($buddy);
            $buddy->setMentor($this);
        }
    }

    public function removeMentor(Buddy $mentor): void
    {
        $this->mentors->removeElement($mentor);
    }

    public function addMentee(Buddy $buddy): void
    {
        if (!$this->mentees->contains($buddy)) {
            $this->mentees->add($buddy);
            $buddy->setMentee($this);
        }
    }

    public function removeMentee(Buddy $mentee): void
    {
        $this->mentees->removeElement($mentee);
    }

    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function getMotivationName(): ?string
    {
        return $this->motivation
            ? $this->motivation->getName($this->local)
            : null
        ;
    }

    public function addHobby(Hobby $hobby): void
    {
        $this->hobbies->add($hobby);
    }

    public function removeHobby(Hobby $hobby): void
    {
        $this->hobbies->removeElement($hobby);
    }

    public function addStudy(Study $study): void
    {
        $this->studies->add($study);
    }

    public function removeStudy(Study $study): void
    {
        $this->studies->removeElement($study);
    }

    public function isReadyForMatching(): bool
    {
        return !empty($this->getFirstname())
            && !empty($this->getInstitute())
            && !empty($this->getLanguages())
            && !empty($this->getSemester())
        ;
    }

    public function isMinimallyCompleted(): bool
    {
        return $this->isReadyForMatching()
            && !empty($this->getLastname())
            && !empty($this->getDob())
            && !empty($this->getSex())
            && !empty($this->getLevelOfStudy())
        ;
    }

    public function isFullyCompleted(): bool
    {
        return $this->isMinimallyCompleted()
            && !empty($this->getHobbies())
            && !empty($this->getMotivation())
            && !empty($this->getArrival())
        ;
    }

    public function getFullName(): string
    {
        return $this->getFirstname().($this->getLastname() ? ' '.$this->getLastname() : '');
    }

    public function addReport(Report $report): void
    {
        if (!$this->reports->contains($report)) {
            $this->reports->add($report);
            $report->setUser($this);
        }
    }

    public function removeReport(Report $report): void
    {
        $this->reports->removeElement($report);
    }

    public function incrementNbBuddies(int $increment = 1): void
    {
        $this->setNbBuddies($this->getNbBuddies() + $increment);
    }

    public function decrementNbBuddies(int $decrement = 1): void
    {
        if ($this->getNbBuddies() > 0) {
            $this->setNbBuddies($this->getNbBuddies() - $decrement);
            if (0 === $this->getNbBuddies()) {
                $this->setIsMatched(false);
            }
        }
    }

    public function incrementNbBuddiesArchived(int $increment = 1): void
    {
        $this->setNbBuddiesArchived($this->getNbBuddiesArchived() + $increment);
    }

    public function incrementNbBuddiesRemoved(int $increment = 1): void
    {
        $this->setNbBuddiesRemoved($this->getNbBuddiesRemoved() + $increment);
    }

    public function incrementNbBuddiesRefused(int $increment = 1): void
    {
        $this->setNbBuddiesRefused($this->getNbBuddiesRefused() + $increment);
    }

    public function decrementNbBuddiesRefused(int $decrement = 1): void
    {
        if ($this->getNbBuddiesRefused() > 0) {
            $this->setNbBuddiesRefused($this->getNbBuddiesRefused() - $decrement);
        }
    }

    public function incrementNbRefusedByBuddies(int $increment = 1): void
    {
        $this->setNbRefusedByBuddies($this->getNbRefusedByBuddies() + $increment);
    }

    public function decrementNbRefusedByBuddies(int $decrement = 1): void
    {
        if ($this->getNbRefusedByBuddies() > 0) {
            $this->setNbRefusedByBuddies($this->getNbRefusedByBuddies() - $decrement);
        }
    }

    public function incrementNbReport(int $increment = 1): void
    {
        $this->setNbReport($this->getNbReport() + $increment);
    }

    public function decrementNbReport(int $decrement = 1): void
    {
        if ($this->getNbReport() > 0) {
            $this->setNbReport($this->getNbReport() - $decrement);
        }
    }

    public function createHistory(): UserHistory
    {
        $history = new UserHistory();
        $history->setNbReport($this->nbReport);
        $history->setNbBuddiesRemoved($this->nbBuddiesRemoved);
        $history->setNbWarned($this->nbWarned);
        $history->setNbBuddiesRefused($this->nbBuddiesRefused);
        $history->setNbRefusedByBuddies($this->nbRefusedByBuddies);
        $history->setNbBuddies($this->nbBuddies + $this->nbBuddiesArchived);
        $history->setSemester($this->getSemester());
        $history->setInstitute($this->getInstitute());
        $history->setLocal($this->isLocal());
        $this->addHistory($history);

        $this->nbReport = 0;
        $this->nbBuddiesRemoved = 0;
        $this->nbWarned = 0;
        $this->nbBuddiesRefused = 0;
        $this->nbRefusedByBuddies = 0;
        $this->nbBuddies = 0;
        $this->nbBuddiesArchived = 0;
        $this->isMatched = false;

        return $history;
    }

    public function addHistory(UserHistory $history): void
    {
        if (!$this->histories->contains($history)) {
            $this->histories->add($history);
            $history->setUser($this);
        }
    }

    public function removeHistory(UserHistory $history): void
    {
        $this->histories->removeElement($history);
    }

    public function addMessage(Message $message): void
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setSender($this);
        }
    }

    public function removeMessage(Message $message): void
    {
        $this->messages->removeElement($message);
    }

    public function addThreadMetadata(ThreadMetadata $threadMetadata): void
    {
        if (!$this->threadMetadatas->contains($threadMetadata)) {
            $this->threadMetadatas->add($threadMetadata);
            $threadMetadata->setParticipant($this);
        }
    }

    public function removeThreadMetadata(ThreadMetadata $threadMetadata): void
    {
        $this->threadMetadatas->removeElement($threadMetadata);
    }

    public function addThreadFavorite(ThreadFavorite $threadFavorite): void
    {
        if (!$this->threadFavorites->contains($threadFavorite)) {
            $this->threadFavorites->add($threadFavorite);
            $threadFavorite->setUser($this);
        }
    }

    public function removeThreadFavorite(ThreadFavorite $threadFavorite): void
    {
        $this->threadFavorites->removeElement($threadFavorite);
    }

    public function privacyHasBeenAcceptedFully(): bool
    {
        return $this->privacyAccepted && $this->privacyAcceptedAt && $this->privacyAcceptedWith;
    }

    public function addThread(Thread $thread): void
    {
        if (!$this->threads->contains($thread)) {
            $this->threads->add($thread);
            $thread->addUser($this);
        }
    }

    public function removeThread(Thread $thread): void
    {
        $this->threads->removeElement($thread);
        $thread->removeUser($this);
    }

    public function addExtract(Extract $extract): void
    {
        if (!$this->extracts->contains($extract)) {
            $this->extracts->add($extract);
            $extract->setUser($this);
        }
    }

    public function removeExtract(Extract $extract): void
    {
        $this->extracts->removeElement($extract);
    }

    public function addRole(string $role): void
    {
        if (UserRoleEnum::ROLE_USER !== $role && !\in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
    }

    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    public function eraseCredentials(): void
    {
    }

    public function hasMaxBuddiesRefused(): bool
    {
        if (!$this->institute instanceof Institute) {
            return false;
        }

        return !$this->isOverrideMaxRefusedLimitation()
            && $this->institute->getNbBuddiesRefusedMax() > 0
            && $this->getNbBuddiesRefused() >= $this->institute->getNbBuddiesRefusedMax()
        ;
    }

    public function acceptPrivacy(): void
    {
        $this->setPrivacyAccepted(true);
        $this->setPrivacyAcceptedAt(new \DateTime());

        $languages = !empty($this->getLanguages())
            ? implode(', ', array_map(
                function ($language) {
                    return $language;
                },
                $this->getLanguages()
            ))
            : ''
        ;

        $country = $this->getCountry() ? $this->getCountry()->getName() : '';
        $city = $this->getCity() ? $this->getCity()->getName() : '';
        $local = $this->isLocal() ? 1 : 0;
        $semester = $this->getSemester()
            ? $this->getSemester()->getPeriod().' '.$this->getSemester()->getYear()
            : ''
        ;

        $acceptedWith = <<<EOF
        FirstName: {$this->getFirstname()}
        LastName: {$this->getLastname()}
        Email: {$this->getEmail()}
        Local: {$local}
        Country: {$country}
        City: {$city}
        Semester: {$semester}
        Languages: {$languages}
EOF;

        if ($institute = $this->getInstitute()) {
            $acceptedWith .= <<<EOF
            Institute: {$institute->getName()}
EOF;
        } elseif ($association = $this->getAssociation()) {
            $acceptedWith .= <<<EOF
            Association: {$association->getName()}
EOF;
        }

        if ($gender = $this->getSex()) {
            $acceptedWith .= <<<EOF
            Gender: {$gender}
EOF;
        }

        if ($dob = $this->getDob()) {
            $acceptedWith .= <<<EOF
            Birthday: {$dob->format('d/m/Y')}
EOF;
        }

        if ($levelOfStudy = $this->getLevelOfStudy()) {
            if (UserLevelStudyEnum::LEVEL_OTHER === $levelOfStudy) {
                $levelOfStudy = $this->getLevelOfStudyOther();
            }

            $acceptedWith .= <<<EOF
            Level of study: {$levelOfStudy}
EOF;
        }

        if ($typeOfMobility = $this->getTypeOfMobility()) {
            $acceptedWith .= <<<EOF
            Type of mobility: {$typeOfMobility}
EOF;
        }

        $this->setPrivacyAcceptedWith($acceptedWith);
    }

    public function setSamlAttributes(array $attributes): void
    {
    }

    public function serialize(): string
    {
        return serialize([
            $this->id,
            $this->email,
            $this->password,
            $this->localeLanguage,
        ]);
    }

    public function unserialize($data): void
    {
        [
            $this->id,
            $this->email,
            $this->password,
            $this->localeLanguage,
        ] = unserialize($data);
    }
}
