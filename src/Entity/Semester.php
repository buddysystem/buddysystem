<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SemesterRepository")
 */
class Semester
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="SemesterPeriodType")
     * @DoctrineAssert\Enum(entity="App\Enum\SemesterPeriodEnum")
     */
    private $period;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $year;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="semester", cascade={"persist"}))
     */
    private $users;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Buddy", mappedBy="semester", cascade={"persist"}))
     */
    private $buddies;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="UserHistory", mappedBy="semester", cascade={"persist", "remove"}))
     */
    private $histories;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Stat", mappedBy="semester", cascade={"persist", "remove"}))
     */
    private $stats;

    public function __construct()
    {
        $this->histories = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->buddies = new ArrayCollection();
        $this->stats = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function setPeriod(string $period): void
    {
        $this->period = $period;
    }

    public function getYear(): string
    {
        return $this->year;
    }

    public function setYear(string $year): void
    {
        $this->year = $year;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getBuddies(): Collection
    {
        return $this->buddies;
    }

    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function getStats(): Collection
    {
        return $this->stats;
    }

    public function getStartYear(): int
    {
        return (int) substr($this->year, 0, 4);
    }

    public function getEndYear(): int
    {
        return (int) substr($this->year, -4);
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setSemester($this);
        }
    }

    public function removeBuddy(Buddy $buddy): void
    {
        $this->buddies->removeElement($buddy);
    }

    public function addBuddy(Buddy $buddy): void
    {
        if (!$this->buddies->contains($buddy)) {
            $this->buddies->add($buddy);
            $buddy->setSemester($this);
        }
    }

    public function addHistory(UserHistory $history): void
    {
        if (!$this->histories->contains($history)) {
            $this->histories->add($history);
            $history->setSemester($this);
        }
    }

    public function removeHistory(UserHistory $history): void
    {
        $this->histories->removeElement($history);
    }

    public function addStat(Stat $stat): void
    {
        if (!$this->stats->contains($stat)) {
            $this->stats->add($stat);
            $stat->setSemester($this);
        }
    }

    public function removeStat(Stat $stat): void
    {
        $this->stats->removeElement($stat);
    }
}
