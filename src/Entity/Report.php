<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable
 */
class Report
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $deleteBuddy = false;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="reports", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $user;

    /**
     * @var Buddy
     *
     * @ORM\ManyToOne(targetEntity="Buddy", inversedBy="reports", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $buddy;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=65535)
     */
    private $comment;

    public function isDeleteBuddy(): bool
    {
        return $this->deleteBuddy;
    }

    public function setDeleteBuddy(bool $deleteBuddy): void
    {
        $this->deleteBuddy = $deleteBuddy;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getBuddy(): Buddy
    {
        return $this->buddy;
    }

    public function setBuddy(Buddy $buddy): void
    {
        $this->buddy = $buddy;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): void
    {
        $this->comment = $comment;
    }
}
