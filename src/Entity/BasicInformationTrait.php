<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

trait BasicInformationTrait
{
    /**
     * @var string
     *
     * @ORM\Column(nullable=false)
     */
    protected $name;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    protected $website;

    /**
     * @Vich\UploadableField(mapping="organization_logo", fileNameProperty="logoName")
     *
     * @Assert\File(
     *     maxSize="2M",
     *     mimeTypes={
     *         "image/png",
     *         "image/jpeg",
     *         "image/jpg",
     *         "image/gif",
     *         "image/bmp",
     *     }
     * )
     *
     * @var File
     */
    protected $logoFile;

    /**
     * @var string|null
     * @ORM\Column(nullable=true)
     */
    protected $logoName;

    /**
     * @var array
     * @ORM\Column(type="array")
     */
    protected $registration;

    public function __toString(): string
    {
        return (string) $this->getId();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getLogoFile(): ?File
    {
        return $this->logoFile;
    }

    public function setLogoFile(File $logo): void
    {
        $this->logoFile = $logo;
        $this->updatedAt = new \DateTime();
    }

    public function getLogoName(): ?string
    {
        return $this->logoName;
    }

    public function setLogoName(?string $logoName): void
    {
        $this->logoName = $logoName;
    }

    public function increaseRegistration(bool $isIframe = false): void
    {
        if (!\is_array($this->registration)
            || !\array_key_exists('count', $this->registration)
            || !\array_key_exists('iframe', $this->registration)
        ) {
            $this->registration = ['count' => 0, 'iframe' => 0];
        }

        ++$this->registration['count'];

        if ($isIframe) {
            ++$this->registration['iframe'];
        }
    }
}
