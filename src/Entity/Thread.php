<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ThreadRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "thread" : "Thread",
 *     "thread_association" : "ThreadAssociation",
 *     "thread_institute" : "ThreadInstitute",
 *     "thread_buddy" : "ThreadBuddy",
 *     "thread_user_manager" : "ThreadUserManager"
 * })
 */
class Thread implements ThreadInterface
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(nullable=false)
     */
    protected $subject;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="thread", cascade={"persist", "remove"})
     */
    protected $messages;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadMetadata", mappedBy="thread", cascade={"persist", "remove"})
     */
    protected $metadata;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="User", inversedBy="threads", cascade={"persist"})
     */
    private $users;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastMessageAt;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->metadata = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): void
    {
        $this->subject = $subject;
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function getMetadata(): Collection
    {
        return $this->metadata;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getLastMessageAt(): ?\DateTime
    {
        return $this->lastMessageAt;
    }

    public function setLastMessageAt(?\DateTime $lastMessageAt): void
    {
        $this->lastMessageAt = $lastMessageAt;
    }

    public function addMessage(Message $message): void
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
            $message->setThread($this);
        }
    }

    public function addMetadata(ThreadMetadata $meta): void
    {
        if (!$this->metadata->contains($meta)) {
            $this->metadata->add($meta);
            $meta->setThread($this);
        }
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function isParticipant(User $participant): bool
    {
        return $this->users->contains($participant);
    }

    public function getOtherUser(User $user): ?User
    {
        foreach ($this->getUsers() as $currentUser) {
            if ($currentUser !== $user) {
                return $currentUser;
            }
        }

        return null;
    }
}
