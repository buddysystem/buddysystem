<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @UniqueEntity(fields="name", message="name.unique")
 * @ORM\Entity(repositoryClass="App\Repository\CityRepository")
 */
class City
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(unique=true)
     */
    private $name;

    /**
     * @var Country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="cities", cascade={"persist"})
     */
    private $country;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Institute", mappedBy="cities", cascade={"persist"})
     * @ORM\OrderBy({"name" : "ASC"})
     */
    private $institutes;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Association", mappedBy="cities", cascade={"persist"})
     */
    private $associations;

    public function __construct()
    {
        $this->associations = new ArrayCollection();
        $this->institutes = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): void
    {
        $this->country = $country;
    }

    public function getInstitutes(): Collection
    {
        return $this->institutes;
    }

    public function getAssociations(): Collection
    {
        return $this->associations;
    }

    public function addAssociation(Association $association): void
    {
        if (!$this->associations->contains($association)) {
            $this->associations->add($association);
            $association->addCity($this);
        }
    }

    public function removeAssociation(Association $association): void
    {
        $this->associations->removeElement($association);
        if ($association->getCities()->contains($this)) {
            $association->removeCity($this);
        }
    }

    public function addInstitute(Institute $institute): void
    {
        if (!$this->institutes->contains($institute)) {
            $this->institutes->add($institute);
            $institute->addCity($this);
        }
    }

    public function removeInstitute(Institute $institute): void
    {
        $this->institutes->removeElement($institute);
        if ($institute->getCities()->contains($this)) {
            $institute->removeCity($this);
        }
    }
}
