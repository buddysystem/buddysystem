<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class UserNotification
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @deprecated
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $newMessage = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $newGroupMessage = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $newGlobalMessage = true;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    private $newIndividualMessage = true;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbNewMessageNotifSent = 0;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $newMessageSentAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function isNewGroupMessage(): bool
    {
        return $this->newGroupMessage;
    }

    public function setNewGroupMessage(bool $newGroupMessage): void
    {
        $this->newGroupMessage = $newGroupMessage;
    }

    public function isNewGlobalMessage(): bool
    {
        return $this->newGlobalMessage;
    }

    public function setNewGlobalMessage(bool $newGlobalMessage): void
    {
        $this->newGlobalMessage = $newGlobalMessage;
    }

    public function isNewIndividualMessage(): bool
    {
        return $this->newIndividualMessage;
    }

    public function setNewIndividualMessage(bool $newIndividualMessage): void
    {
        $this->newIndividualMessage = $newIndividualMessage;
    }

    public function getNbNewMessageNotifSent(): int
    {
        return $this->nbNewMessageNotifSent;
    }

    public function setNbNewMessageNotifSent(int $nbNewMessageNotifSent): void
    {
        $this->nbNewMessageNotifSent = $nbNewMessageNotifSent;
    }

    public function getNewMessageSentAt(): ?\DateTime
    {
        return $this->newMessageSentAt;
    }

    public function setNewMessageSentAt(?\DateTime $newMessageSentAt): void
    {
        $this->newMessageSentAt = $newMessageSentAt;
    }

    public function resetNewMessageSentAt(): void
    {
        $this->newMessageSentAt = new \DateTime();
    }

    public function increaseNbNewMessageNotifSent(): void
    {
        $this->setNbNewMessageNotifSent($this->getNbNewMessageNotifSent() + 1);
        $this->resetNewMessageSentAt();
    }

    public function resetNewMessage(): void
    {
        $this->setNbNewMessageNotifSent(0);
        $this->setNewMessageSentAt(null);
    }
}
