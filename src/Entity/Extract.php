<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExtractRepository")
 */
class Extract
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     */
    private $filename;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbDownloads = 0;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="extracts", cascade={"persist"})
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $generated = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    public function getNbDownloads(): int
    {
        return $this->nbDownloads;
    }

    public function setNbDownloads(int $nbDownloads): void
    {
        $this->nbDownloads = $nbDownloads;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function isGenerated(): bool
    {
        return $this->generated;
    }

    public function setGenerated(bool $generated): void
    {
        $this->generated = $generated;
    }

    public function incrementNbDownloads(int $increment = 1): void
    {
        $this->setNbDownloads($this->getNbDownloads() + $increment);
    }
}
