<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface ThreadManagerInterface
{
    public function getId(): int;

    public function getSubject(): string;

    public function setSubject(string $subject): void;

    public function addMessage(Message $message): void;

    public function getMessages(): Collection;

    public function addMetadata(ThreadMetadata $meta): void;

    public function getMetadata(): Collection;

    public function getRecipientType(): string;

    public function setRecipientType(string $type): void;

    public function getMatchingManager(): MatchingManagerInterface;

    public function getMatchingManagerType(): string;
}
