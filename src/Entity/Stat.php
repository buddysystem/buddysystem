<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatRepository")
 */
class Stat
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"unsigned" : true})
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Institute|null
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="stats")
     */
    private $institute;

    /**
     * @var Semester
     *
     * @ORM\ManyToOne(targetEntity="Semester", inversedBy="stats", cascade={"persist"})
     */
    private $semester;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbUsers;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbNewUsers;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMentees;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbNewMentees;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMenteesNotMatched;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMentors;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbNewMentors;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMentorsNotMatched;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMatchConfirmed;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMatchRefused;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default" : 0, "unsigned" : true})
     */
    private $nbMatchWaiting;

    /**
     * @var string
     *
     * @ORM\Column(type="StatTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\StatTypeEnum")
     */
    private $type;

    public function getId(): int
    {
        return $this->id;
    }

    public function getInstitute(): ?Institute
    {
        return $this->institute;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function getSemester(): Semester
    {
        return $this->semester;
    }

    public function setSemester(Semester $semester): void
    {
        $this->semester = $semester;
    }

    public function getNbUsers(): int
    {
        return $this->nbUsers;
    }

    public function setNbUsers(int $nbUsers): void
    {
        $this->nbUsers = $nbUsers;
    }

    public function getNbNewUsers(): int
    {
        return $this->nbNewUsers;
    }

    public function setNbNewUsers(int $nbNewUsers): void
    {
        $this->nbNewUsers = $nbNewUsers;
    }

    public function getNbMentees(): int
    {
        return $this->nbMentees;
    }

    public function setNbMentees(int $nbMentees): void
    {
        $this->nbMentees = $nbMentees;
    }

    public function getNbNewMentees(): int
    {
        return $this->nbNewMentees;
    }

    public function setNbNewMentees(int $nbNewMentees): void
    {
        $this->nbNewMentees = $nbNewMentees;
    }

    public function getNbMenteesNotMatched(): int
    {
        return $this->nbMenteesNotMatched;
    }

    public function setNbMenteesNotMatched(int $nbMenteesNotMatched): void
    {
        $this->nbMenteesNotMatched = $nbMenteesNotMatched;
    }

    public function getNbMentors(): int
    {
        return $this->nbMentors;
    }

    public function setNbMentors(int $nbMentors): void
    {
        $this->nbMentors = $nbMentors;
    }

    public function getNbNewMentors(): int
    {
        return $this->nbNewMentors;
    }

    public function setNbNewMentors(int $nbNewMentors): void
    {
        $this->nbNewMentors = $nbNewMentors;
    }

    public function getNbMentorsNotMatched(): int
    {
        return $this->nbMentorsNotMatched;
    }

    public function setNbMentorsNotMatched(int $nbMentorsNotMatched): void
    {
        $this->nbMentorsNotMatched = $nbMentorsNotMatched;
    }

    public function getNbMatchConfirmed(): int
    {
        return $this->nbMatchConfirmed;
    }

    public function setNbMatchConfirmed(int $nbMatchConfirmed): void
    {
        $this->nbMatchConfirmed = $nbMatchConfirmed;
    }

    public function getNbMatchRefused(): int
    {
        return $this->nbMatchRefused;
    }

    public function setNbMatchRefused(int $nbMatchRefused): void
    {
        $this->nbMatchRefused = $nbMatchRefused;
    }

    public function getNbMatchWaiting(): int
    {
        return $this->nbMatchWaiting;
    }

    public function setNbMatchWaiting(int $nbMatchWaiting): void
    {
        $this->nbMatchWaiting = $nbMatchWaiting;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
