<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\MessagingRecipientTypeEnum;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AssociationRepository")
 * @UniqueEntity(fields="name", message="name.unique")
 *
 * @Vich\Uploadable
 */
class Association implements MatchingManagerInterface, BasicInformationInterface, MatchingCriteriaInterface
{
    use BasicInformationTrait;
    use MatchingCriteriaTrait;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ThreadAssociation", mappedBy="association", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $threads;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AssociationInstitute", mappedBy="association", cascade={"persist"})
     */
    private $associationInstitutes;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="User", mappedBy="association", cascade={"persist"})
     */
    private $users;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $facebook;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $twitter;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     */
    private $instagram;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="City", inversedBy="associations", cascade={"persist"})
     */
    private $cities;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Buddy", mappedBy="association", cascade={"persist"})
     */
    private $buddies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Hobby", inversedBy="associations", cascade={"persist"})
     */
    private $hobbies;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="Motivation", inversedBy="associations", cascade={"persist"})
     */
    private $motivations;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Document", mappedBy="association", cascade={"persist", "remove"})
     */
    private $documents;

    public function __construct()
    {
        $this->associationInstitutes = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->hobbies = new ArrayCollection();
        $this->motivations = new ArrayCollection();
        $this->buddies = new ArrayCollection();
        $this->threads = new ArrayCollection();
        $this->documents = new ArrayCollection();

        $this->firstSemesterStart = new \DateTime('2014-09-01');
        $this->firstSemesterEnd = new \DateTime('2014-01-01');
        $this->secondSemesterStart = new \DateTime('2014-01-01');
        $this->secondSemesterEnd = new \DateTime('2014-07-01');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThreads(): Collection
    {
        return $this->threads;
    }

    public function getAssociationInstitutes(): Collection
    {
        return $this->associationInstitutes;
    }

    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function setFacebook(?string $facebook): void
    {
        $this->facebook = $facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function setTwitter(?string $twitter): void
    {
        $this->twitter = $twitter;
    }

    public function getInstagram(): ?string
    {
        return $this->instagram;
    }

    public function setInstagram(?string $instagram): void
    {
        $this->instagram = $instagram;
    }

    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function getBuddies(): Collection
    {
        return $this->buddies;
    }

    public function getHobbies(): Collection
    {
        return $this->hobbies;
    }

    public function getMotivations(): Collection
    {
        return $this->motivations;
    }

    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addAssociationInstitute(AssociationInstitute $associationInstitute): void
    {
        if (!$this->associationInstitutes->contains($associationInstitute)) {
            $this->associationInstitutes->add($associationInstitute);
            $associationInstitute->setAssociation($this);
        }
    }

    public function removeAssociationInstitute(AssociationInstitute $associationInstitute): void
    {
        $this->associationInstitutes->removeElement($associationInstitute);
    }

    public function hasSocialLink(): bool
    {
        return !empty($this->website) || !empty($this->facebook) || !empty($this->twitter) || !empty($this->instagram);
    }

    public function addCity(City $city): void
    {
        if (!$this->cities->contains($city)) {
            $this->cities->add($city);
            $city->addAssociation($this);
        }
    }

    public function removeCity(City $city): void
    {
        $this->cities->removeElement($city);
    }

    public function addBuddy(Buddy $buddy): void
    {
        if (!$this->buddies->contains($buddy)) {
            $this->buddies->add($buddy);
            $buddy->setAssociation($this);
        }
    }

    public function removeBuddy(Buddy $buddy): void
    {
        $this->buddies->removeElement($buddy);
    }

    public function addMotivation(Motivation $motivation): void
    {
        if (!$this->motivations->contains($motivation)) {
            $this->motivations->add($motivation);
        }
    }

    public function removeMotivation(Motivation $motivation): void
    {
        $this->motivations->removeElement($motivation);
    }

    public function addHobby(Hobby $hobby): void
    {
        if (!$this->hobbies->contains($hobby)) {
            $this->hobbies->add($hobby);
        }
    }

    public function removeHobby(Hobby $hobby): void
    {
        $this->hobbies->removeElement($hobby);
    }

    public function addDocument(Document $document): void
    {
        if (!$this->documents->contains($document)) {
            $this->documents->add($document);
            $document->setAssociation($this);
        }
    }

    public function removeDocument(Document $document): void
    {
        $this->documents->removeElement($document);
    }

    public function addUser(User $user): void
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->setAssociation($this);
        }
    }

    public function removeUser(User $user): void
    {
        $this->users->removeElement($user);
    }

    public function addThread(ThreadManagerInterface $thread): void
    {
        if ($thread instanceof ThreadAssociation
            && !$this->threads->contains($thread)
        ) {
            $this->threads->add($thread);
            $thread->setAssociation($this);
        }
    }

    public function removeThread(ThreadManagerInterface $thread): void
    {
        $this->threads->removeElement($thread);
    }

    public function canAddMessageInThread(ThreadManagerInterface $thread): bool
    {
        if (MatchingManagerInterface::TYPE_ASSOCIATION === $thread->getMatchingManagerType()) {
            return $this->getThreads()->contains($thread);
        }

        if (MessagingRecipientTypeEnum::ONLY_MANAGERS !== $thread->getRecipientType()) {
            /** @var AssociationInstitute $associationInstitute */
            foreach ($this->getAssociationInstitutes() as $associationInstitute) {
                if ($associationInstitute->getInstitute()->getThreads()->contains($thread)) {
                    return true;
                }
            }
        }

        return false;
    }
}
