<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;

interface MatchingManagerInterface
{
    public const TYPE_ASSOCIATION = 'association';
    public const TYPE_INSTITUTE = 'institute';

    public function getId(): ?int;

    public function setName(string $name): void;

    public function getName(): ?string;

    public function setDescription(?string $description): void;

    public function getDescription(): ?string;

    public function getWebsite(): ?string;

    public function setWebsite(?string $website): void;

    public function setLogoFile(File $logo): void;

    public function getLogoFile(): ?File;

    public function setLogoName(?string $logoName): void;

    public function getLogoName(): ?string;

    public function isActivated(): bool;

    public function setActivated(bool $activated): void;

    public function getNbMentors(): int;

    public function setNbMentors(int $nbMentors): void;

    public function getNbMentees(): int;

    public function setNbMentees(int $nbMentees): void;

    public function getNbBuddiesRefusedMax(): int;

    public function setNbBuddiesRefusedMax(int $nbBuddiesRefusedMax): void;

    public function isFullYearOnly(): bool;

    public function setFullYearOnly(bool $fullYearOnly): void;

    public function getFirstSemesterStart(): \DateTime;

    public function setFirstSemesterStart(\DateTime $firstSemesterStart): void;

    public function getFirstSemesterEnd(): ?\DateTime;

    public function setFirstSemesterEnd(?\DateTime $firstSemesterEnd): void;

    public function getSecondSemesterStart(): ?\DateTime;

    public function setSecondSemesterStart(?\DateTime $secondSemesterStart): void;

    public function getSecondSemesterEnd(): \DateTime;

    public function setSecondSemesterEnd(\DateTime $secondSemesterEnd): void;

    public function getMatchingHobbyValue(): int;

    public function setMatchingHobbyValue(int $matchingHobbyValue): void;

    public function getMatchingStudyValue(): int;

    public function setMatchingStudyValue(int $matchingStudyValue): void;

    public function getMatchingCityValue(): int;

    public function setMatchingCityValue(int $matchingCityValue): void;

    public function getMatchingLanguageValue(): int;

    public function setMatchingLanguageValue(int $matchingLanguageValue): void;

    public function getMatchingAgeValue(): int;

    public function setMatchingAgeValue(int $matchingAgeValue): void;

    public function getMatchingMotivationValue(): int;

    public function setMatchingMotivationValue(int $matchingMotivationValue): void;

    public function getMatchingAvailabilityValue(): int;

    public function setMatchingAvailabilityValue(int $matchingAvailabilityValue): void;

    public function getMatchingInstituteValues(): array;

    public function getMatchingInstituteValue(int $level = 0): int;

    public function setMatchingInstituteValues(array $matchingInstituteValues): void;

    public function getMatchingLevelOfStudyValue(): int;

    public function setMatchingLevelOfStudyValue(int $matchingLevelOfStudyValue): void;

    public function getRevivalDelay(): int;

    public function setRevivalDelay(int $revivalDelay): void;

    public function getUnmatchDelay(): int;

    public function setUnmatchDelay(int $unmatchDelay): void;

    public function addAssociationInstitute(AssociationInstitute $associationInstitute): void;

    public function removeAssociationInstitute(AssociationInstitute $associationInstitute): void;

    public function getAssociationInstitutes(): Collection;

    public function addUser(User $user): void;

    public function removeUser(User $user): void;

    public function getUsers(): Collection;

    public function hasSocialLink(): bool;

    public function addCity(City $city): void;

    public function removeCity(City $city): void;

    public function getCities(): Collection;

    public function addBuddy(Buddy $buddy): void;

    public function removeBuddy(Buddy $buddy): void;

    public function getBuddies(): Collection;

    public function getMotivations(): Collection;

    public function addMotivation(Motivation $motivation): void;

    public function removeMotivation(Motivation $motivation): void;

    public function getHobbies(): Collection;

    public function addHobby(Hobby $hobby): void;

    public function removeHobby(Hobby $hobby): void;

    public function increaseRegistration(bool $isIframe = false): void;

    public function getThreads(): Collection;

    public function addThread(ThreadManagerInterface $thread): void;

    public function removeThread(ThreadManagerInterface $thread): void;

    public function canAddMessageInThread(ThreadManagerInterface $thread): bool;

    public function isMentorValidationRequired(): bool;

    public function setMentorValidationRequired(bool $mentorValidationRequired): void;
}
