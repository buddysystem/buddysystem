<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity
 */
class ThreadInstitute extends Thread implements ThreadManagerInterface
{
    /**
     * @var Institute
     *
     * @ORM\ManyToOne(targetEntity="Institute", inversedBy="threads")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $institute;

    /**
     * @var string
     *
     * @ORM\Column(type="MessagingRecipientTypeType")
     * @DoctrineAssert\Enum(entity="App\Enum\MessagingRecipientTypeEnum")
     */
    private $recipientType;

    public function getInstitute(): Institute
    {
        return $this->institute;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;
    }

    public function getRecipientType(): string
    {
        return $this->recipientType;
    }

    public function setRecipientType(string $type): void
    {
        $this->recipientType = $type;
    }

    public function getMatchingManager(): MatchingManagerInterface
    {
        return $this->institute;
    }

    public function getMatchingManagerType(): string
    {
        return MatchingManagerInterface::TYPE_INSTITUTE;
    }
}
