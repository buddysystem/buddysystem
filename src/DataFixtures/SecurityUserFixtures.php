<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Institute;
use App\Entity\User;
use App\Enum\UserLevelStudyEnum;
use App\Enum\UserRoleEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityUserFixtures extends Fixture implements OrderedFixtureInterface
{
    public const ADMIN_USERNAME = 'admin';
    public const BUDDY_COORDINATOR_USERNAME = 'buddycoordinator';
    public const INSTITUTE_MANAGER_USERNAME = 'ri';
    public const TRANSLATOR_USERNAME = 'translator';
    public const LOCAL_STUDENT_USERNAME = 'local';
    public const INTERNATIONAL_STUDENT_USERNAME = 'international';

    private $passwordHasher;
    private $faker;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {
        $now = new \DateTime();
        $locale = 'en';

        /** @var Institute $institute */
        $institute = $this->getReference(InstituteFixtures::INSTITUTE_REFERENCE.'_0');
        $semester = $institute->getSemester();
        $city = $institute->getCities()[0];
        $country = $city->getCountry();

        $admin = new User();
        $admin->setEnabled(true);
        $admin->setEmail(self::ADMIN_USERNAME.'@buddysystem.eu');
        $admin->setPassword($this->passwordHasher->hashPassword($admin, self::ADMIN_USERNAME));
        $admin->setSemester($semester);
        $admin->setRoles([UserRoleEnum::ROLE_SUPER_ADMIN]);
        $admin->setArrival($now);
        $admin->setCity($city);
        $admin->setCountry($country);
        $admin->setTermsOfUse(true);
        $admin->setLocaleLanguage($locale);
        $admin->setPrivacyAccepted(true);
        $admin->setApproved(true);
        $admin->setCharterAccepted(true);

        $manager->persist($admin);

        $buddyCoordinator = new User();
        $buddyCoordinator->setEnabled(true);
        $buddyCoordinator->setEmail(self::BUDDY_COORDINATOR_USERNAME.'@buddysystem.eu');
        $buddyCoordinator->setPassword($this->passwordHasher->hashPassword($buddyCoordinator, self::BUDDY_COORDINATOR_USERNAME));
        $buddyCoordinator->setRoles([UserRoleEnum::ROLE_BUDDYCOORDINATOR]);
        $buddyCoordinator->setArrival($now);
        $buddyCoordinator->setTermsOfUse(true);
        $buddyCoordinator->setAssociation($this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_0'));
        $buddyCoordinator->setCity($city);
        $buddyCoordinator->setCountry($country);
        $buddyCoordinator->setLocaleLanguage($locale);
        $buddyCoordinator->setPrivacyAccepted(true);
        $buddyCoordinator->setApproved(true);
        $buddyCoordinator->setCharterAccepted(true);

        $manager->persist($buddyCoordinator);

        $ri = new User();
        $ri->setEnabled(true);
        $ri->setEmail(self::INSTITUTE_MANAGER_USERNAME.'@buddysystem.eu');
        $ri->setPassword($this->passwordHasher->hashPassword($ri, self::INSTITUTE_MANAGER_USERNAME));
        $ri->setRoles([UserRoleEnum::ROLE_INSTITUTE_MANAGER]);
        $ri->setArrival($now);
        $ri->setTermsOfUse(true);
        $ri->setInstitute($institute->getRoot());
        $ri->setCity($city);
        $ri->setCountry($country);
        $ri->setLocaleLanguage($locale);
        $ri->setPrivacyAccepted(true);
        $ri->setApproved(true);
        $ri->setCharterAccepted(true);

        $manager->persist($ri);

        $localStudent = new User();
        $localStudent->setEnabled(true);
        $localStudent->setEmail(self::LOCAL_STUDENT_USERNAME.'@buddysystem.eu');
        $localStudent->setFirstName($this->faker->firstName());
        $localStudent->setLastname($this->faker->lastName());
        $localStudent->setPassword($this->passwordHasher->hashPassword($localStudent, self::LOCAL_STUDENT_USERNAME));
        $localStudent->setLanguages([$locale]);
        $localStudent->setSemester($semester);
        $localStudent->setTermsOfUse(true);
        $localStudent->setInstitute($institute);
        $localStudent->setLocaleLanguage($locale);
        $localStudent->setCity($city);
        $localStudent->setCountry($country);
        $localStudent->setDob(new \DateTime());
        $localStudent->setSex('m');
        $localStudent->setLevelOfStudy(UserLevelStudyEnum::getRandomValue());
        $localStudent->setPrivacyAccepted(true);
        $localStudent->setApproved(true);
        $localStudent->setCharterAccepted(true);

        $manager->persist($localStudent);

        $internationalStudent = new User();
        $internationalStudent->setEnabled(true);
        $internationalStudent->setEmail(self::INTERNATIONAL_STUDENT_USERNAME.'@buddysystem.eu');
        $internationalStudent->setFirstName($this->faker->firstName());
        $internationalStudent->setLastname($this->faker->lastName());
        $internationalStudent->setPassword($this->passwordHasher->hashPassword($internationalStudent, self::INTERNATIONAL_STUDENT_USERNAME));
        $internationalStudent->setLanguages([$locale]);
        $internationalStudent->setSemester($semester);
        $internationalStudent->setTermsOfUse(true);
        $internationalStudent->setInstitute($institute);
        $internationalStudent->setCity($city);
        $internationalStudent->setCountry($country);
        $internationalStudent->setDob(new \DateTime());
        $internationalStudent->setSex('m');
        $internationalStudent->setLevelOfStudy(UserLevelStudyEnum::getRandomValue());
        $internationalStudent->setLocaleLanguage($locale);
        $internationalStudent->setPrivacyAccepted(true);
        $internationalStudent->setApproved(true);
        $internationalStudent->setCharterAccepted(true);

        $manager->persist($internationalStudent);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 12;
    }
}
