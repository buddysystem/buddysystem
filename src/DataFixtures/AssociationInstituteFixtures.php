<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\AssociationInstitute;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AssociationInstituteFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $ai = new AssociationInstitute();
        $ai->setCanMatch(false);
        $ai->setInstitute($this->getReference(InstituteFixtures::UNIVERSITY_REFERENCE.'_0'));
        $ai->setAssociation($this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_0'));

        $manager->persist($ai);
        $manager->flush();
    }

    public function getOrder(): int
    {
        return 4;
    }
}
