<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Study;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class StudyFixtures extends Fixture implements OrderedFixtureInterface
{
    private const STUDY_REFERENCE = 'study';

    public function load(ObjectManager $manager): void
    {
        $studies = ['Computing', 'Math', 'Law', 'LEA'];
        $ref = 0;

        foreach ($studies as $studyName) {
            $study = new Study();
            $study->setName($studyName);
            $manager->persist($study);

            $this->addReference(self::STUDY_REFERENCE.'_'.$ref, $study);
            ++$ref;
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 8;
    }
}
