<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Semester;
use App\Enum\SemesterPeriodEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SemesterFixtures extends Fixture implements OrderedFixtureInterface
{
    public const SEMESTER_REFERENCE = 'semester';

    public function load(ObjectManager $manager): void
    {
        $periodChoices = SemesterPeriodEnum::getChoicesToGenerate();

        $year = 2018;
        $currentYear = date('Y');
        $counter = 0;

        while ($year <= $currentYear) {
            for ($j = 0; $j < 3; ++$j) {
                $semester = new Semester();
                $semester->setPeriod($periodChoices[$j]);
                $semester->setYear($year.' - '.($year + 1));
                $manager->persist($semester);

                $this->addReference(self::SEMESTER_REFERENCE."_{$counter}", $semester);
                ++$counter;
            }
            ++$year;
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 2;
    }
}
