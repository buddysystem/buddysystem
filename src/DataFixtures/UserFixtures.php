<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Hobby;
use App\Entity\Institute;
use App\Entity\Motivation;
use App\Entity\Semester;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    public const USER_REFERENCE = 'user';

    private $passwordHasher;
    private $emailsUsed = [];
    private $faker;
    private $nbUsersByInstitute;

    public function __construct(UserPasswordHasherInterface $passwordHasher, int $nbUsersByInstitute)
    {
        $this->passwordHasher = $passwordHasher;
        $this->nbUsersByInstitute = $nbUsersByInstitute;
    }

    public function load(ObjectManager $manager): void
    {
        $gender = ['m', 'f', 'o'];
        $now = new \DateTime();
        $locale = 'en';
        $localeFr = 'fr';

        $isLocalWithoutConsentInit = false;

        for ($j = 0; $j < \count(InstituteFixtures::INSTITUTE_NAMES); ++$j) {
            /** @var Institute $institute */
            $institute = $this->getReference(InstituteFixtures::INSTITUTE_REFERENCE.'_'.$j);
            $semester = $institute->getRoot()->getSemester();

            for ($i = 0; $i < $this->nbUsersByInstitute; ++$i) {
                // IS
                [$firstName, $lastName] = $this->getFakerName();
                $manager->persist($is = $this->createUser(
                    $firstName,
                    $lastName,
                    [$locale],
                    $username = 'is_'.$j.'_'.$i,
                    false,
                    $gender[random_int(0, 2)],
                    true,
                    $now,
                    $institute,
                    $locale,
                    $semester,
                    0 === random_int(0, 2) ? ['fr'] : [],
                    $this->getReference(HobbyFixtures::HOBBY_REFERENCE.'_'.random_int(0, HobbyFixtures::NB_HOBBIES - 1)),
                    $this->getReference(MotivationFixtures::MOTIVATION_REFERENCE.'_'.random_int(0, MotivationFixtures::NB_MOTIVATIONS - 1))
                ));

                $this->addReference(self::USER_REFERENCE.'_'.$username, $is);

                // Local
                [$firstName, $lastName] = $this->getFakerName();
                $manager->persist($local = $this->createUser(
                    $firstName,
                    $lastName,
                    [$localeFr],
                    $username = 'local_'.$j.'_'.$i,
                    true,
                    $gender[random_int(0, 2)],
                    true,
                    $now,
                    $institute,
                    $locale,
                    $semester,
                    [],
                    $this->getReference(HobbyFixtures::HOBBY_REFERENCE.'_'.random_int(0, HobbyFixtures::NB_HOBBIES - 1)),
                    $this->getReference(MotivationFixtures::MOTIVATION_REFERENCE.'_'.random_int(0, MotivationFixtures::NB_MOTIVATIONS - 1))
                ));

                $this->addReference(self::USER_REFERENCE.'_'.$username, $local);
            }

            if (!$isLocalWithoutConsentInit) {
                [$firstName, $lastName] = $this->getFakerName();
                $manager->persist($this->createUser(
                    $firstName,
                    $lastName,
                    [$localeFr],
                    'localWithoutConsent',
                    false,
                    $gender[random_int(0, 2)],
                    true,
                    $now,
                    $institute,
                    $locale,
                    $semester,
                    [],
                    null,
                    null,
                    false,
                    'secret'
                ));

                $isLocalWithoutConsentInit = true;
            }

            $manager->flush();
        }
    }

    public function getOrder(): int
    {
        return 9;
    }

    private function getFakerName(): array
    {
        if (!$this->faker) {
            $this->faker = Factory::create('fr_FR');
        }

        do {
            $firstName = $this->faker->firstName();
            $lastName = $this->faker->lastName();
            $email = "{$firstName}.{$lastName}@buddysystem.eu";
        } while (\in_array($email, $this->emailsUsed));

        $this->emailsUsed[] = $email;

        return [$firstName, $lastName];
    }

    private function createUser(
        string $firstName,
        string $lastName,
        array $languages,
        string $username,
        bool $isLocal,
        string $sex,
        bool $termsOfUse,
        \DateTime $arrival,
        Institute $institute,
        string $localeLanguage,
        Semester $semester,
        array $languageWanted = [],
        Hobby $hobby = null,
        Motivation $motivation = null,
        bool $privacyAccepted = true,
        string $password = null
    ): User {
        $user = new User();
        $city = $institute->getCities()->first();

        $user->setFirstName($firstName);
        $user->setLastname($lastName);
        $user->setLanguages($languages);
        $user->setEmail("{$username}@buddysystem.eu");
        $user->setPassword($this->passwordHasher->hashPassword($user, $password ?? $firstName));
        $user->setIsLocal($isLocal);
        $user->setSex($sex);
        $user->setTermsOfUse($termsOfUse);
        $user->setArrival($arrival);
        $user->setInstitute($institute);
        $user->setCity($city);
        $user->setCountry($city->getCountry());
        $user->setNbBuddiesWanted(random_int(1, $institute->getNbMentors()));
        $user->setLocaleLanguage($localeLanguage);
        $user->setPrivacyAccepted($privacyAccepted);
        $user->setCharterAccepted($privacyAccepted);
        $user->setSemester($semester);
        $user->setEnabled(true);
        $user->setApproved(true);

        if ($languageWanted) {
            $user->setLanguagesWanted($languageWanted);
        }

        if ($hobby) {
            $user->addHobby($hobby);
        }

        if ($motivation) {
            $user->setMotivation($motivation);
        }

        return $user;
    }
}
