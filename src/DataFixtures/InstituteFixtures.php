<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Institute;
use App\Entity\RegistrationEmailDomain;
use App\Enum\RegistrationEmailDomainUserTypeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class InstituteFixtures extends Fixture implements OrderedFixtureInterface
{
    public const UNIVERSITY_REFERENCE = 'university';
    public const CAMPUS_REFERENCE = 'campus';
    public const INSTITUTE_REFERENCE = 'institute';
    public const INSTITUTE_NAMES = ['U.F.R. des Lettres et Sciences Humaines' => [0], 'Institut Universitaire de Technologie' => [1, 2], "Institut Supérieur d'Etudes Logistiques" => [1], 'Faculté des Affaires Internationales' => [0]];
    private const CAMPUS_NAMES = ['Site Lebon', 'Site Frissard', 'Site de Caucriauville'];

    public function load(ObjectManager $manager): void
    {
        /** @var City $city */
        $city = $this->getReference(CityFixtures::CITY_REFERENCE.'_0');

        $registrationEmailDomain = new RegistrationEmailDomain();
        $registrationEmailDomain->setDomain('buddysystem.eu');
        $registrationEmailDomain->setUserType(RegistrationEmailDomainUserTypeEnum::BOTH);

        $university = new Institute();
        $university->setName('Université Le Havre');
        $university->addCity($city);
        $university->addRegistrationEmailDomain($registrationEmailDomain);
        $manager->persist($university);
        $this->addReference(self::UNIVERSITY_REFERENCE.'_0', $university);

        for ($i = 0; $i < \count(self::CAMPUS_NAMES); ++$i) {
            $institute = new Institute();
            $institute->setName(self::CAMPUS_NAMES[$i]);
            $institute->addCity($city);
            $university->addChild($institute);

            $this->addReference(self::CAMPUS_REFERENCE.'_'.$i, $institute);

            $manager->persist($institute);
        }

        $k = 0;
        foreach (self::INSTITUTE_NAMES as $name => $campus) {
            $institute = new Institute();
            $institute->setName($name);
            $institute->addCity($city);
            $institute->setStudiesRequired(false);
            $this->addReference(self::INSTITUTE_REFERENCE.'_'.$k, $institute);

            foreach ($campus as $c) {
                /** @var Institute $campusEntity */
                $campusEntity = $this->getReference(self::CAMPUS_REFERENCE.'_'.$c);
                $campusEntity->addChild($institute);
            }

            $manager->persist($institute);
            ++$k;
        }

        $university->setLevel();

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3;
    }
}
