<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Hobby;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class HobbyFixtures extends Fixture implements OrderedFixtureInterface
{
    public const HOBBY_REFERENCE = 'hobby';
    public const NB_HOBBIES = 5;

    public function load(ObjectManager $manager): void
    {
        $hobbies = ['Sports', 'Music', 'Talking / Meet New People', 'Traveling', 'Cooking / Gastronomy'];

        for ($i = 0; $i < self::NB_HOBBIES; ++$i) {
            $hobby = new Hobby();
            $hobby->setName($hobbies[$i]);
            $hobby->addAssociation($this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_0'));
            $hobby->addInstitute($this->getReference(InstituteFixtures::UNIVERSITY_REFERENCE.'_0'));

            for ($j = 0; $j < \count(InstituteFixtures::INSTITUTE_NAMES); ++$j) {
                $hobby->addInstitute($this->getReference(InstituteFixtures::INSTITUTE_REFERENCE.'_'.$j));
            }

            $manager->persist($hobby);
            $this->addReference(self::HOBBY_REFERENCE.'_'.$i, $hobby);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 6;
    }
}
