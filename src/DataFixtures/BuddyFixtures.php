<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Services\MatchingManagerHelper;
use App\Services\MessagingHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class BuddyFixtures extends Fixture implements OrderedFixtureInterface
{
    private $matchingManagerHelper;
    private $messagingHelper;
    private $nbUsersByInstitute;

    public function __construct(
        MatchingManagerHelper $matchingManagerHelper,
        MessagingHelper $messagingHelper,
        int $nbUsersByInstitute
    ) {
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->messagingHelper = $messagingHelper;
        $this->nbUsersByInstitute = $nbUsersByInstitute;
    }

    public function load(ObjectManager $manager): void
    {
        $buddies = [];
        $buddyStatus = BuddyStatusEnum::getValues();

        for ($j = 0; $j < \count(InstituteFixtures::INSTITUTE_NAMES); ++$j) {
            $statusCreated = [];
            for ($i = 0; $i < \intval($this->nbUsersByInstitute / 3); ++$i) {
                /** @var User $is */
                $is = $this->getReference(UserFixtures::USER_REFERENCE.'_is_'.$j.'_'.$i);

                /** @var User $local */
                $local = $this->getReference(UserFixtures::USER_REFERENCE.'_local_'.$j.'_'.$i);

                $buddy = new Buddy($local, $is);

                if (\count($statusCreated) !== \count($buddyStatus)) {
                    do {
                        $status = BuddyStatusEnum::getRandomValue();
                    } while (\in_array($status, $statusCreated));

                    $statusCreated[] = $status;
                    $buddy->setStatus($status);
                } else {
                    $buddy->setStatus(BuddyStatusEnum::getRandomValue());
                }

                $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfUser($is);
                if ($matchingManager instanceof Institute) {
                    $buddy->setInstitute($matchingManager);
                } elseif ($matchingManager instanceof Association) {
                    $buddy->setAssociation($matchingManager);
                    $buddy->setInstitute($is->getInstitute());
                }

                $manager->persist($buddy);
                $buddies[] = $buddy;
            }
        }

        $manager->flush();

        foreach ($buddies as $buddy) {
            $mentor = $buddy->getMentor();
            $mentee = $buddy->getMentee();
            $status = $buddy->getStatus();

            if (BuddyStatusEnum::STATUS_CONFIRMED === $status) {
                $mentor->setIsMatched(true);
                $mentee->setIsMatched(true);
                $this->messagingHelper->createBuddyThread($buddy);
            } elseif (BuddyStatusEnum::STATUS_REFUSED === $status) {
                $mentor->incrementNbBuddiesRefused();
            }
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 10;
    }
}
