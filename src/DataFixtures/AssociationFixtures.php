<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Association;
use App\Entity\City;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AssociationFixtures extends Fixture implements OrderedFixtureInterface
{
    public const ASSOCIATION_REFERENCE = 'association';

    public function load(ObjectManager $manager): void
    {
        /** @var City $city */
        $city = $this->getReference(CityFixtures::CITY_REFERENCE.'_0');

        $association = new Association();
        $association->setName('Association ESN '.$city->getName());
        $association->setWebsite('https://www.'.str_replace(' ', '_', $association->getName()).'.eu');
        $association->setLogoName(str_replace(' ', '_', $association->getName()).'.png');
        $association->setNbMentees(random_int(1, 10));
        $association->setNbMentors(random_int(1, 10));
        $association->addCity($city);
        $manager->persist($association);
        $this->addReference(self::ASSOCIATION_REFERENCE.'_0', $association);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 3;
    }
}
