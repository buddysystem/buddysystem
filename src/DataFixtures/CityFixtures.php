<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\City;
use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CityFixtures extends Fixture implements OrderedFixtureInterface
{
    public const CITY_REFERENCE = 'city';
    public const NB_CITIES_TO_LOAD = 10;

    private const CITIES_NAMES = ['Le Havre', 'Paris', 'Marseille', 'Lyon', 'Bordeaux', 'Lille', 'Nantes', 'Toulouse', 'Rouen', 'Strasbourg'];

    public function load(ObjectManager $manager): void
    {
        $country = new Country();
        $country->setName('France');
        $country->setCode('FR');
        $manager->persist($country);

        for ($i = 0; $i < self::NB_CITIES_TO_LOAD; ++$i) {
            $city = new City();
            $city->setName(self::CITIES_NAMES[$i]);
            $city->setCountry($country);
            $manager->persist($city);
            $this->addReference(self::CITY_REFERENCE."_{$i}", $city);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 1;
    }
}
