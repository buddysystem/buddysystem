<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Thread;
use App\Services\MessagingHelper;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ThreadFixtures extends Fixture implements OrderedFixtureInterface
{
    private $messagingHelper;

    public function __construct(MessagingHelper $messagingHelper)
    {
        $this->messagingHelper = $messagingHelper;
    }

    public function load(ObjectManager $manager): void
    {
        $thread = new Thread();
        $thread->setSubject($this->messagingHelper->getBCSubject());
        $manager->persist($thread);

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 11;
    }
}
