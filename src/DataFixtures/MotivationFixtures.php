<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Motivation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class MotivationFixtures extends Fixture implements OrderedFixtureInterface
{
    public const MOTIVATION_REFERENCE = 'motivations';
    public const NB_MOTIVATIONS = 7;

    public function load(ObjectManager $manager): void
    {
        $motivations = [
            'Meet new people - live an intercultural experience',
            'Learn - increase language skills in an informal way',
            'Be helped on administrative considerations (accommodation, papers, etc.)',
            'Be helped in one/several field of studies linked to your academic program',
            'Share social events (parties, trips, etc.)',
            'Have someone to discover the city and/or the Institute with',
            'Have information on a country-city you are going to the semester-year after',
        ];

        for ($i = 0; $i < self::NB_MOTIVATIONS; ++$i) {
            $motivation = new Motivation();
            $motivation->setNameMentee($motivations[$i]);
            $motivation->setNameMentor($motivations[$i]);
            $motivation->addAssociation($this->getReference(AssociationFixtures::ASSOCIATION_REFERENCE.'_0'));
            $motivation->addInstitute($this->getReference(InstituteFixtures::UNIVERSITY_REFERENCE.'_0'));

            for ($j = 0; $j < \count(InstituteFixtures::INSTITUTE_NAMES); ++$j) {
                $motivation->addInstitute($this->getReference(InstituteFixtures::INSTITUTE_REFERENCE.'_'.$j));
            }

            $manager->persist($motivation);
            $this->addReference(self::MOTIVATION_REFERENCE.'_'.$i, $motivation);
        }

        $manager->flush();
    }

    public function getOrder(): int
    {
        return 7;
    }
}
