<?php

declare(strict_types=1);

namespace App\Twig\Extensions;

use App\Services\MessagingHelper;
use Symfony\Component\Security\Core\Security;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MessageExtension extends AbstractExtension
{
    private $security;
    private $messagingHelper;

    public function __construct(Security $security, MessagingHelper $messagingHelper)
    {
        $this->security = $security;
        $this->messagingHelper = $messagingHelper;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('message_has_unread', [$this, 'hasUnread']),
        ];
    }

    public function hasUnread(): bool
    {
        return $this->messagingHelper->hasUnread($this->security->getUser());
    }
}
