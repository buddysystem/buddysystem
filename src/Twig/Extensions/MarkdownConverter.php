<?php

declare(strict_types=1);

namespace App\Twig\Extensions;

use Twig\Extra\Markdown\MarkdownInterface;

class MarkdownConverter implements MarkdownInterface
{
    private $converter;

    public function __construct(MarkdownExtension $converter = null)
    {
        $this->converter = $converter ?: new MarkdownExtension();
    }

    public function convert(string $body): string
    {
        return $this->converter->text($body);
    }
}
