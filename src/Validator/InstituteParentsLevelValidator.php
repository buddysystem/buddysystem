<?php

declare(strict_types=1);

namespace App\Validator;

use App\Entity\Institute;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class InstituteParentsLevelValidator extends ConstraintValidator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof InstituteParentsLevel) {
            throw new UnexpectedTypeException($constraint, InstituteParentsLevel::class);
        }

        $depth = null;

        /** @var Institute $parent */
        foreach ($value as $parent) {
            $currentDepth = $parent->getDepth();
            if (!$parent->hasChildren()) {
                ++$currentDepth;
            }

            if (!$depth) {
                $depth = $currentDepth;
            } elseif ($currentDepth !== $depth) {
                /* @var $constraint InstituteParentsLevel */
                $this->context->buildViolation($this->translator->trans($constraint->message, [], 'validators'))->addViolation();
            }
        }
    }
}
