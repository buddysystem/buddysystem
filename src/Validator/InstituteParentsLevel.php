<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

class InstituteParentsLevel extends Constraint
{
    public $message = 'institute.institute_linked.level_error';
}
