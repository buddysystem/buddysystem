<?php

declare(strict_types=1);

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

class NotThrowawayEmail extends Constraint
{
    public $message = 'user.email.throwaway';
}
