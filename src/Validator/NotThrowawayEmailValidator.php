<?php

declare(strict_types=1);

namespace App\Validator;

use EmailChecker\EmailChecker;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotThrowawayEmailValidator extends ConstraintValidator
{
    private $translator;
    private $emailChecker;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->emailChecker = new EmailChecker();
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof NotThrowawayEmail) {
            throw new UnexpectedTypeException($constraint, NotThrowawayEmail::class);
        }

        if (!$this->emailChecker->isValid($value)) {
            $this->context->buildViolation($this->translator->trans($constraint->message, [], 'validators'))->addViolation();
        }
    }
}
