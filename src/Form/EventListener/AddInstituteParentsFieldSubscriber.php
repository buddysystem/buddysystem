<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Institute;
use App\Form\SearchCollectionType;
use App\Repository\CityRepository;
use App\Repository\InstituteRepository;
use App\Services\MatchingManagerHelper;
use App\Validator\InstituteParentsLevel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddInstituteParentsFieldSubscriber implements EventSubscriberInterface
{
    private $em;
    private $cityRepo;
    private $instituteRepo;
    private $matchingManagerHelper;
    private $institute;

    public function __construct(
        EntityManagerInterface $em,
        CityRepository $cityRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        $this->em = $em;
        $this->cityRepo = $cityRepo;
        $this->instituteRepo = $instituteRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->institute = null;
    }

    public function setInstitute(Institute $institute): void
    {
        $this->institute = $institute;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        /** @var Institute $institute */
        $institute = $event->getData();
        $form = $event->getForm();

        $this->addInstituteParentsInForm(
            $form,
            $institute->getCities()->toArray(),
            $this->em->contains($institute) ? $this->instituteRepo->getAllParents($institute) : []
        );
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $form = $event->getForm();

            $cities = [];
            if (\array_key_exists('cities', $data)) {
                foreach ($data['cities'] as $cityId) {
                    $cities[] = $this->cityRepo->findOneById($cityId);
                }
            }

            $parents = [];
            if (\array_key_exists('parents', $data)) {
                foreach ($data['parents'] as $i => $parentId) {
                    $parent = $this->instituteRepo->findOneById($parentId);

                    if ($parent && !\in_array($parent, $parents)
                        && !empty(array_intersect($parent->getCities()->toArray(), $cities))) {
                        $parents[] = $parent;
                    } else {
                        unset($data['parents'][$i]);
                    }
                }
            }

            $event->setData($data);
            $this->addInstituteParentsInForm($form, $cities, $parents);
        }
    }

    private function addInstituteParentsInForm(FormInterface $form, array $cities, array $parents = []): void
    {
        if ($this->institute instanceof Institute) {
            $institute = $this->institute;
        } else {
            $institute = new Institute();
        }

        $institute->setCities(new ArrayCollection($cities));

        $potentialParents = array_merge(
            $parents,
            $this->instituteRepo->getPotentialParentsQueryBuilder(
                $institute,
                $this->matchingManagerHelper->getMatchingManagerOfManager()
            )
        );

        $form->add('parents', CollectionType::class, [
            'entry_type' => SearchCollectionType::class,
            'mapped' => false,
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
            'prototype_name' => '__parent__name__',
            'required' => true,
            'by_reference' => false,
            'data' => $parents,
            'label_attr' => [
                'class' => 'col-sm-4',
            ],
            'entry_options' => [
                'choice_label' => 'name',
                'class' => Institute::class,
                'group_by' => function ($choice, $key, $value) {
                    if ($choice->getParent()) {
                        return $choice->getParent()->getName();
                    }

                    return null;
                },
                'choices' => $potentialParents,
            ],
            'constraints' => new InstituteParentsLevel(),
        ]);
    }
}
