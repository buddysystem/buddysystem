<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Association;
use App\Entity\City;
use App\Repository\AssociationRepository;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddAssociationFieldSubscriber implements EventSubscriberInterface
{
    private $cityRepo;
    private $countryRepo;
    private $associationRepo;
    private $translator;
    private $params;

    public function __construct(
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        AssociationRepository $associationRepo,
        TranslatorInterface $translator
    ) {
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->associationRepo = $associationRepo;
        $this->translator = $translator;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();

        $city = null;
        $association = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $association = $user->getAssociation();

                if ($association && !$association->getCities()->contains($city)) {
                    $association = null;
                }
            } else {
                $city = null;
            }
        }

        $this->addAssociationInForm($event->getForm(), $city, $association);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $city = null;
            $association = null;

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $association = (\array_key_exists('association', $data) && !empty($data['association']))
                        ? $this->associationRepo->find($data['association'])
                        : null;

                    if ($association && !$association->getCities()->contains($city)) {
                        $association = null;
                    }
                } else {
                    $city = null;
                }
            }

            $data['association'] = $association instanceof Association ? $association->getId() : null;
            $this->addAssociationInForm($event->getForm(), $city, $association);
        }
    }

    private function addAssociationInForm(FormInterface $form, City $city = null, Association $association = null): void
    {
        $options = $city
            ? ['placeholder' => $this->translator->trans('registration.association.city.placeholder', ['%city%' => $city->getName()])]
            : ['placeholder' => 'registration.association.placeholder',
                'disabled' => 'disabled',
            ]
        ;

        $form->add('association', EntityType::class, array_merge_recursive($options, $this->params, [
            'required' => true,
            'label' => 'registration.association.manager',
            'choice_label' => 'name',
            'class' => Association::class,
            'data' => $association,
            'query_builder' => function (EntityRepository $er) use ($city) {
                if ($city) {
                    return $er->createQueryBuilder('mm')
                        ->innerJoin('mm.cities', 'city')
                        ->where('city.id = :city')
                        ->andWhere('mm.activated = :activated')
                        ->setParameter('city', $city->getId())
                        ->setParameter('activated', true)
                        ->addOrderBy('mm.name', 'ASC')
                    ;
                }

                return null;
            },
        ]));
    }
}
