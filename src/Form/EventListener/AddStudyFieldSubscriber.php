<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Institute;
use App\Entity\Study;
use App\Form\DataTransformer\ObjectToIdTransformer;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use App\Repository\StudyRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddStudyFieldSubscriber implements EventSubscriberInterface
{
    private $registry;
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $studyRepo;
    private $params;

    public function __construct(
        ManagerRegistry $registry,
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo,
        StudyRepository $studyRepository
    ) {
        $this->registry = $registry;
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->instituteRepo = $instituteRepo;
        $this->studyRepo = $studyRepository;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();
        $institute = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();

                if ($institute && !$institute->getCities()->contains($city)) {
                    $institute = null;
                }
            }
        }

        $this->addStudyInForm($event->getForm(), $institute);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $institute = null;

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data) && !empty($data['institute']))
                        ? $this->instituteRepo->find($data['institute'])
                        : null
                    ;

                    if ($institute && !$institute->getCities()->contains($city)) {
                        $institute = null;
                    }
                }
            }

            $this->addStudyInForm($event->getForm(), $institute);
        }
    }

    private function addStudyInForm(FormInterface $form, Institute $institute = null): void
    {
        if ($institute && !$institute->hasChildren()) {
            if (false === $institute->getStudies()->isEmpty()) {
                $choices = $institute->getStudies()->toArray();
            } else {
                $choices = $this->studyRepo->findAll();
            }

            $form->add('studies', EntityType::class, array_merge_recursive($this->params, [
                'label' => 'user.studies',
                'multiple' => true,
                'choice_label' => 'name',
                'class' => Study::class,
                'required' => $institute->isStudiesRequired(),
                'choices' => $choices,
                'attr' => [
                    'class' => 'select2selector',
                ],
            ]));
        } else {
            $form->add('studies', HiddenType::class, [
                'model_transformer' => new ObjectToIdTransformer($this->registry, Study::class, true),
            ]);
        }
    }
}
