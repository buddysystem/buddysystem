<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Institute;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddNbBuddiesWantedFieldSubscriber implements EventSubscriberInterface
{
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $params;

    public function __construct(
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo
    ) {
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->instituteRepo = $instituteRepo;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();
        $institute = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();

                if ($institute && !$institute->getCities()->contains($city)) {
                    $institute = null;
                }
            }
        }

        $this->addNbBuddiesWantedInForm(
            $event->getForm(),
            $user->isLocal(),
            $institute,
            $user->getNbBuddiesWanted()
        );
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $institute = null;

            $isLocal = \array_key_exists('isLocal', $data) && \boolval($data['isLocal']);

            $nbBuddiesWanted = \array_key_exists('nbBuddiesWanted', $data) && !empty($data['nbBuddiesWanted'])
                ? (int) $data['nbBuddiesWanted']
                : 1
            ;

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data) && !empty($data['institute']))
                        ? $this->instituteRepo->find($data['institute'])
                        : null
                    ;

                    if ($institute && !$institute->getCities()->contains($city)) {
                        $institute = null;
                    }
                }
            }

            $this->addNbBuddiesWantedInForm(
                $event->getForm(),
                $isLocal,
                $institute,
                $nbBuddiesWanted
            );
        }
    }

    private function addNbBuddiesWantedInForm(
        FormInterface $form,
        bool $isLocal,
        Institute $institute = null,
        int $oldValue = 1
    ): void {
        $maxBuddiesWanted = [];
        if ($institute) {
            $maxBuddiesWanted = [
                'attr' => [
                    'max' => $isLocal ? $institute->getNbMentees() : $institute->getNbMentors(),
                ],
            ];
        }

        $form->add('nbBuddiesWanted', IntegerType::class, array_merge_recursive($this->params, $maxBuddiesWanted, [
            'empty_data' => 1,
            'data' => $oldValue,
            'attr' => [
                'min' => 1,
            ],
            'label' => 'profile.nb_buddy_wanted',
            'label_attr' => [
                'class' => 'col-sm-4',
            ],
        ]));
    }
}
