<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Institute;
use App\Entity\Semester;
use App\Enum\SemesterPeriodEnum;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use App\Repository\SemesterRepository;
use App\Services\MatchingManagerHelper;
use App\Services\SemesterHelper;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddSemesterFieldSubscriber implements EventSubscriberInterface
{
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $semesterHelper;
    private $semesterRepo;
    private $matchingManagerHelper;
    private $params;

    public function __construct(
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo,
        SemesterRepository $semesterRepo,
        SemesterHelper $semesterHelper,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->instituteRepo = $instituteRepo;
        $this->semesterRepo = $semesterRepo;
        $this->semesterHelper = $semesterHelper;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();
        $semester = null;
        $institute = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();
            $semester = $user->getSemester();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();
                if ($institute && !$institute->getCities()->contains($city)) {
                    $institute = null;
                }
            }
        }

        $this->addSemesterInForm($event->getForm(), $institute, $semester);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $institute = null;

            $semester = \array_key_exists('semester', $data) && !empty($data['semester'])
                ? $this->semesterRepo->find($data['semester'])
                : null
            ;

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data) && !empty($data['institute']))
                        ? $this->instituteRepo->find($data['institute'])
                        : null;

                    if ($institute && !$institute->getCities()->contains($city)) {
                        $institute = null;
                    }
                }
            }

            $this->addSemesterInForm($event->getForm(), $institute, $semester);
        }
    }

    private function addSemesterInForm(
        FormInterface $form,
        Institute $institute = null,
        Semester $oldSemester = null
    ): void {
        $options = [];
        if (!$institute) {
            $options = [
                'placeholder' => 'registration.semester.placeholder',
                'disabled' => 'disabled',
            ];
        }

        $form->add('semester', EntityType::class, array_merge_recursive($options, $this->params, [
            'required' => true,
            'label' => 'user.semester',
            'class' => Semester::class,
            'data' => $oldSemester,
            'choice_label' => function ($semester) use ($institute) {
                return $this->semesterHelper->getReadablePeriod($semester, $institute);
            },
            'query_builder' => function (EntityRepository $er) use ($institute, $oldSemester) {
                if ($institute && $institute->getSemester()) {
                    $qb = $er->createQueryBuilder('s')
                        ->where('s.id >= :idSemester')
                        ->setParameter('idSemester', $institute->getSemester()->getId())
                        ->orderBy('s.year', 'ASC')
                        ->addOrderBy("FIELD(s.period, '".implode("', '", SemesterPeriodEnum::getValues())."')")
                    ;

                    if ($oldSemester) {
                        $qb
                            ->orWhere('s.id = :oldSemester')
                            ->setParameter('oldSemester', $oldSemester->getId())
                        ;
                    }

                    if ($institute->isFullYearOnly()) {
                        $qb
                            ->andWhere('s.period = :period')
                            ->setParameter('period', SemesterPeriodEnum::PERIOD_ALL_YEAR)
                        ;
                    }

                    return $qb;
                }

                return null;
            },
            'group_by' => function ($choice, $key, $value) {
                return $choice->getYear();
            },
        ]));
    }
}
