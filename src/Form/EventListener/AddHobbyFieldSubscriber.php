<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Hobby;
use App\Entity\MatchingManagerInterface;
use App\Form\DataTransformer\ObjectToIdTransformer;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use App\Services\MatchingManagerHelper;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddHobbyFieldSubscriber implements EventSubscriberInterface
{
    private $registry;
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $matchingManagerHelper;
    private $params;

    public function __construct(
        ManagerRegistry $registry,
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        $this->registry = $registry;
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->instituteRepo = $instituteRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();
        $matchingManager = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();

                if ($institute && $institute->getCities()->contains($city)) {
                    $matchingManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute($institute, true);
                    foreach (array_reverse($matchingManagers) as $manager) {
                        if (null === $matchingManager || $matchingManager->getHobbies()->isEmpty()) {
                            $matchingManager = $manager;
                        }
                    }
                }
            }
        }

        $this->addHobbyInForm($event->getForm(), $matchingManager);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $matchingManager = null;

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data) && !empty($data['institute']))
                        ? $this->instituteRepo->find($data['institute'])
                        : null
                    ;

                    if ($institute && $institute->getCities()->contains($city)) {
                        $matchingManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute($institute, true);
                        foreach (array_reverse($matchingManagers) as $manager) {
                            if (null === $matchingManager || $matchingManager->getHobbies()->isEmpty()) {
                                $matchingManager = $manager;
                            }
                        }
                    }
                }
            }

            $this->addHobbyInForm($event->getForm(), $matchingManager);
        }
    }

    private function addHobbyInForm(FormInterface $form, MatchingManagerInterface $matchingManager = null): void
    {
        if ($matchingManager && !$matchingManager->getHobbies()->isEmpty()) {
            $form->add('hobbies', EntityType::class, array_merge_recursive($this->params, [
                'class' => Hobby::class,
                'multiple' => true,
                'choice_label' => 'name',
                'label' => 'user.hobbies',
                'required' => false,
                'choices' => $matchingManager->getHobbies()->toArray(),
                'attr' => [
                    'class' => 'select2selector',
                ],
            ]));
        } else {
            $form->add('hobbies', HiddenType::class, [
                'model_transformer' => new ObjectToIdTransformer($this->registry, Hobby::class, true),
            ]);
        }
    }
}
