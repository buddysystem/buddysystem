<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\City;
use App\Entity\Country;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddCityFieldSubscriber implements EventSubscriberInterface
{
    private $translator;
    private $countryRepo;
    private $cityRepo;
    private $params;

    public function __construct(
        TranslatorInterface $translator,
        CountryRepository $countryRepo,
        CityRepository $cityRepo
    ) {
        $this->translator = $translator;
        $this->countryRepo = $countryRepo;
        $this->cityRepo = $cityRepo;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();

        $country = null;
        $city = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() !== $country) {
                $city = null;
            }
        }

        $this->addCityInForm($event->getForm(), $country, $city);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $country = null;
            $city = null;

            if (\array_key_exists('country', $data) && !empty($data['country'])) {
                $country = $this->countryRepo->find($data['country']);

                $city = \array_key_exists('city', $data) && !empty($data['city'])
                    ? $this->cityRepo->find($data['city'])
                    : null;

                if ($city && $city->getCountry() !== $country) {
                    $city = null;
                }
            }

            $data['city'] = $city ? $city->getId() : null;
            $event->setData($data);

            $this->addCityInForm($event->getForm(), $country, $city);
        }
    }

    private function addCityInForm(FormInterface $form, Country $country = null, City $city = null): void
    {
        $options = $country
            ? ['placeholder' => $this->translator->trans('registration.city.country.placeholder', ['%country%' => $country->getName()])]
            : [
                'placeholder' => 'registration.city.placeholder',
                'disabled' => 'disabled',
            ]
        ;

        $form->add('city', EntityType::class, array_merge_recursive($this->params, $options, [
            'required' => true,
            'choice_label' => 'name',
            'class' => City::class,
            'data' => $city,
            'choices' => $country
                ? $this->cityRepo->getWithMatchingManager(
                    $country,
                    $form->getConfig()->getOption('type'),
                    $form->getConfig()->getOption('includeDeactivated'),
                    $form->getConfig()->getOption('matchingManager'),
                    $form->getConfig()->getOption('mode')
                )
                : [],
        ]));
    }
}
