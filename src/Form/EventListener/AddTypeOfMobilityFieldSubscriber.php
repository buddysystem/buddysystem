<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\User;
use App\Enum\UserTypeMobilityEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddTypeOfMobilityFieldSubscriber implements EventSubscriberInterface
{
    private $params;

    public function __construct()
    {
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        /** @var User $user */
        $user = $event->getData();

        $this->addTypeOfMobilityInForm(
            $event->getForm(),
            $user->isLocal(),
            $user->getTypeOfMobility()
        );
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $isLocal = \array_key_exists('isLocal', $data) && \boolval($data['isLocal']);

            $typeOfMobility = \array_key_exists('typeOfMobility', $data)
                ? $data['typeOfMobility']
                : null
            ;

            $this->addTypeOfMobilityInForm(
                $event->getForm(),
                $isLocal,
                $typeOfMobility
            );
        }
    }

    private function addTypeOfMobilityInForm(FormInterface $form, bool $isLocal, string $typeOfMobility = null): void
    {
        if (!$isLocal) {
            $form->add('typeOfMobility', ChoiceType::class, array_merge_recursive($this->params, [
                'empty_data' => null,
                'data' => $typeOfMobility,
                'placeholder' => null,
                'label' => 'user.type_of_mobility',
                'attr' => ['help' => 'registration.type_of_mobility.tooltip'],
                'choices' => UserTypeMobilityEnum::getChoices(),
            ]));
        } else {
            $form->add('typeOfMobility', HiddenType::class);
        }
    }
}
