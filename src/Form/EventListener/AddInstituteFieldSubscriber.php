<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\City;
use App\Entity\Institute;
use App\Enum\UserProfileTypeEnum;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AddInstituteFieldSubscriber implements EventSubscriberInterface
{
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $translator;
    private $params;

    public function __construct(
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo,
        TranslatorInterface $translator
    ) {
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->translator = $translator;
        $this->instituteRepo = $instituteRepo;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        $user = $event->getData();

        $city = null;
        $ancestors = [];
        $institute = null;
        $instituteParent = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();

                if ($institute instanceof Institute) {
                    if (\in_array($city, $institute->getCitiesWithParents())) {
                        $i = 0;

                        /** @var Institute $parent */
                        foreach ($institute->getParents() as $parent) {
                            $ancestors[$i] = $parent;
                            ++$i;
                        }

                        if ($institute->hasChildren()) {
                            $ancestors[$i] = $instituteParent = $institute;
                        } elseif ($i > 0) {
                            $instituteParent = $ancestors[$i - 1];
                        }
                    } else {
                        $institute = null;
                    }
                }
            } else {
                $city = null;
            }
        }

        foreach ($ancestors as $i => $ancestor) {
            $this->addInstituteAncestorsInForm(
                $form,
                'institute_'.$i,
                $city,
                $i > 0
                    ? $ancestors[$i - 1]
                    : null,
                $ancestor
            );
        }

        $this->addInstituteInForm($form, $city, $instituteParent, $institute);
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $form = $event->getForm();

            $city = null;
            $institute = null;
            $instituteParent = null;
            $ancestors = [];

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data))
                        ? $this->instituteRepo->find($data['institute'])
                        : null;

                    if ($institute instanceof Institute) {
                        if (\in_array($city, $institute->getCitiesWithParents())) {
                            $i = 0;

                            /** @var Institute $parent */
                            foreach ($institute->getParents() as $parent) {
                                $ancestors[$i] = $parent;
                                ++$i;
                            }

                            if ($institute->hasChildren()) {
                                $ancestors[$i] = $instituteParent = $institute;
                            } elseif ($i > 0) {
                                $instituteParent = $ancestors[$i - 1];
                            }
                        } else {
                            $institute = null;
                        }
                    }
                } else {
                    $city = null;
                }
            }

            for ($i = 0; $i < Institute::MAX_LEVEL - 1; ++$i) {
                $fieldName = 'institute_'.$i;

                if ($form->has($fieldName)) {
                    $form->remove($fieldName);
                }

                if (\array_key_exists($i, $ancestors)) {
                    $ancestor = $ancestors[$i];
                    $this->addInstituteAncestorsInForm(
                        $form,
                        'institute_'.$i,
                        $city,
                        $i > 0 ? $ancestors[$i - 1] : null,
                        $ancestor
                    );

                    $data[$fieldName] = $ancestor->getId();
                }
            }

            $data['institute'] = $institute ? $institute->getId() : null;
            $event->setData($data);

            $this->addInstituteInForm($form, $city, $instituteParent, $institute);
        }
    }

    private function addInstituteInForm(
        FormInterface $form,
        City $city = null,
        Institute $instituteParent = null,
        Institute $oldValue = null
    ): void {
        $options = $this->getDefaultOptions($form, $instituteParent);

        if (UserProfileTypeEnum::PROFILE_INSTITUTE === $form->getConfig()->getOption('mode')) {
            $options['empty_data'] = $instituteParent;
        }

        if ($city) {
            if ($instituteParent) {
                $options['placeholder'] = UserProfileTypeEnum::PROFILE_INSTITUTE === $form->getConfig()->getOption('mode')
                    ? false
                    : $this->translator->trans('registration.institute.parent.placeholder', ['%parent%' => $instituteParent->getName()]);
            } else {
                $options['placeholder'] = $this->translator->trans('registration.institute.city.placeholder', ['%city%' => $city->getName()]);
            }
        } else {
            $options['placeholder'] = 'registration.institute.placeholder';
            $options['disabled'] = 'disabled';
        }

        $this->addToForm($form, 'institute', $city, $instituteParent, $oldValue, $options);
    }

    private function addInstituteAncestorsInForm(
        FormInterface $form,
        string $fieldName,
        City $city = null,
        Institute $instituteParent = null,
        Institute $oldValue = null
    ): void {
        if ($city && $oldValue && (!$instituteParent || $instituteParent->hasChildren())) {
            $options = $this->getDefaultOptions($form, $instituteParent);

            if (UserProfileTypeEnum::PROFILE_INSTITUTE === $form->getConfig()->getOption('mode') && $instituteParent) {
                $options['placeholder'] = false;
            }

            $options['mapped'] = false;

            $this->addToForm($form, $fieldName, $city, $instituteParent, $oldValue, $options);
        }
    }

    private function addToForm(
        FormInterface $form,
        string $fieldName,
        City $city = null,
        Institute $instituteParent = null,
        Institute $oldValue = null,
        array $options = []
    ): void {
        $form->add($fieldName, EntityType::class, array_merge_recursive($options, $this->params, [
            'required' => true,
            'class' => Institute::class,
            'data' => $oldValue,
            'choices' => $city
                ? $this->instituteRepo->getByCity(
                    $city,
                    $instituteParent,
                    $form->getConfig()->getOption('includeDeactivated'),
                    $form->getConfig()->getOption('matchingManager'),
                    $form->getConfig()->getOption('mode')
                )
                : [],
        ]));
    }

    private function getDefaultOptions(FormInterface $form, Institute $instituteParent = null): array
    {
        return UserProfileTypeEnum::PROFILE_INSTITUTE === $form->getConfig()->getOption('mode')
            ? ['choice_label' => function ($choice, $key, $value) use ($instituteParent) {
                if ($instituteParent === $choice) {
                    return $this->translator->trans('registration.institute.manager.upper_level');
                }

                return $choice->getName();
            }]
            : [
                'preferred_choices' => function ($choice, $key, $value) {
                    return $choice->isLevel(1);
                },
                'choice_label' => 'name',
            ];
    }
}
