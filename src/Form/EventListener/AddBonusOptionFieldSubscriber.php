<?php

declare(strict_types=1);

namespace App\Form\EventListener;

use App\Entity\Institute;
use App\Form\DataTransformer\StringToBoolTransformer;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

class AddBonusOptionFieldSubscriber implements EventSubscriberInterface
{
    private $cityRepo;
    private $countryRepo;
    private $instituteRepo;
    private $params;

    public function __construct(
        CityRepository $cityRepo,
        CountryRepository $countryRepo,
        InstituteRepository $instituteRepo
    ) {
        $this->cityRepo = $cityRepo;
        $this->countryRepo = $countryRepo;
        $this->instituteRepo = $instituteRepo;
        $this->params = [];
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        ];
    }

    public function preSetData(FormEvent $event): void
    {
        $user = $event->getData();
        $institute = null;

        if ($user) {
            $country = $user->getCountry();
            $city = $user->getCity();

            if ($city && $city->getCountry() === $country) {
                $institute = $user->getInstitute();

                if ($institute && !$institute->getCities()->contains($city)) {
                    $institute = null;
                }
            }
        }

        $this->addBonusOptionInForm(
            $event->getForm(),
            $user->isLocal(),
            $institute,
            $user->isBonusOption()
        );
    }

    public function preSubmit(FormEvent $event): void
    {
        if ($data = $event->getData()) {
            $institute = null;

            $isLocal = \array_key_exists('isLocal', $data) && \boolval($data['isLocal']);
            $isBonusOption = \array_key_exists('bonusOption', $data) && \boolval($data['bonusOption']);

            if (\array_key_exists('country', $data) && !empty($data['country'])
                && \array_key_exists('city', $data) && !empty($data['city'])) {
                $country = $this->countryRepo->find($data['country']);
                $city = $this->cityRepo->find($data['city']);

                if ($city && $city->getCountry() === $country) {
                    $institute = (\array_key_exists('institute', $data) && !empty($data['institute']))
                        ? $this->instituteRepo->find($data['institute'])
                        : null
                    ;

                    if ($institute && !$institute->getCities()->contains($city)) {
                        $institute = null;
                    }
                }
            }

            $this->addBonusOptionInForm(
                $event->getForm(),
                $isLocal,
                $institute,
                $isBonusOption
            );
        }
    }

    private function addBonusOptionInForm(
        FormInterface $form,
        bool $isLocal,
        Institute $institute = null,
        bool $isBonusOption = false
    ): void {
        if ($isLocal && $institute && $institute->isShowBonusOption()) {
            $form->add('bonusOption', CheckboxType::class, array_merge_recursive($this->params, [
                'data' => $isBonusOption,
                'label' => 'profile.bonus_option',
                'required' => false,
            ]));
        } else {
            $form->add('bonusOption', HiddenType::class, [
                'model_transformer' => new StringToBoolTransformer(),
            ]);
        }
    }
}
