<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Buddy;
use App\Enum\BuddyRefuseEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RefusalBuddyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choices = [];
        foreach (BuddyRefuseEnum::getValues() as $value) {
            $choices['dashboard.matching.refusal.reason.'.$value] = $value;
        }

        $builder
            ->add('reasonRefusal', ChoiceType::class, [
                'multiple' => false,
                'label' => 'dashboard.matching.refusal.reason',
                'choices' => $choices,
            ])
            ->add('reasonRefusalOther', TextType::class, [
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Buddy::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'refusal_buddy';
    }
}
