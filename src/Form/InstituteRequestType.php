<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\InstituteRequest;
use App\Repository\CountryRepository;
use App\Services\LocaleManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstituteRequestType extends AbstractType
{
    private $countryRepo;
    private $localeManager;

    public function __construct(CountryRepository $countryRepo, LocaleManager $localeManager)
    {
        $this->countryRepo = $countryRepo;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $countries = $this->countryRepo->getAllSorted($this->localeManager->getCurrentLocale());
        $firstCountry = reset($countries);

        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'institute_finder.not_found.name',
            ])
            ->add('country', EntityType::class, [
                'required' => true,
                'choice_label' => 'name',
                'class' => Country::class,
                'choices' => $countries,
            ])
            ->add('city', EntityType::class, [
                'placeholder' => 'institute_finder.not_found.city.placeholder',
                'required' => false,
                'choice_label' => 'name',
                'class' => City::class,
                'choice_attr' => function ($choice, $key, $value) use ($firstCountry) {
                    $country = $choice->getCountry();
                    $params = ['data_country_id' => $choice->getCountry()->getId()];

                    if ($country !== $firstCountry) {
                        $params['style'] = 'display: none';
                    }

                    return $params;
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                        ->orderBy('c.name')
                    ;
                },
            ])
            ->add('cityOther', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'organization.city',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => InstituteRequest::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return parent::getBlockPrefix();
    }
}
