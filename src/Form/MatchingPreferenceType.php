<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Repository\InstituteRepository;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchingPreferenceType extends AbstractType
{
    protected $instituteRepo;
    protected $matchingManagerHelper;

    public function __construct(InstituteRepository $instituteRepo, MatchingManagerHelper $matchingManagerHelper)
    {
        $this->instituteRepo = $instituteRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('activated', CheckboxType::class, [
            'required' => false,
            'label' => 'organization.activated',
        ]);

        /** @var MatchingManagerInterface $matchingManager */
        $matchingManager = $builder->getData();

        if ($matchingManager->getId()) {
            if ($matchingManager instanceof Institute
                || !empty($this->matchingManagerHelper->getUpperLevelsCriteriasManaged($matchingManager))
            ) {
                $maxLevelManaged = $this->instituteRepo->getMaxLevelManaged($matchingManager);

                $attrRangeSlider = ['class' => 'range-slider'];
                $attrRangeSlider['data-slider-min'] = 1;
                $attrMatchingWeight = ['class' => 'range-slider__matching-weight'];
                $attrMaxValue = ['class' => 'range-slider__max-value'];
                $labelAttr = ['label_attr' => ['class' => 'col-sm-5']];

                $builder
                    ->add('fullYearOnly', CheckboxType::class, [
                        'required' => false,
                        'label' => 'organization.only_full_year',
                        'attr' => [
                            'class' => 'matching_preference_switch',
                        ],
                    ])
                    ->add('firstSemesterStart', DateType::class, [
                        'format' => \IntlDateFormatter::LONG,
                        'label' => false,
                        'attr' => [
                            'year' => 'disabled',
                        ],
                    ])
                    ->add('secondSemesterEnd', DateType::class, [
                        'format' => \IntlDateFormatter::LONG,
                        'label' => false,
                        'attr' => [
                            'year' => 'disabled',
                        ],
                    ])
                    ->add('nbMentors', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                        $attrRangeSlider,
                        [
                            'data-slider-value' => $matchingManager->getNbMentors(),
                            'data-slider-max' => 15,
                        ]
                    ),
                        'label' => 'organization.max_nb_mentors',
                    ]))
                    ->add('nbMentees', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrRangeSlider,
                            [
                                'data-slider-value' => $matchingManager->getNbMentees(),
                                'data-slider-max' => 15,
                            ]
                        ),
                        'label' => 'organization.max_nb_mentees',
                    ]))
                    ->add('nbBuddiesRefusedMax', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMaxValue,
                            [
                                'data-slider-value' => $matchingManager->getNbBuddiesRefusedMax(),
                                'help' => 'organization.max_buddies.tooltip',
                                'data-slider-min' => 1,
                            ]
                        ),
                        'label' => 'organization.max_nb_buddies_refused',
                    ]))
                    ->add('matchingHobbyValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingHobbyValue(),
                            ]
                        ),
                        'label' => 'organization.hobby_value',
                    ]))
                    ->add('matchingCityValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingCityValue(),
                            ]
                        ),
                        'label' => 'organization.city_value',
                    ]))
                    ->add('matchingStudyValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingStudyValue(),
                            ]
                        ),
                        'label' => 'organization.study_value',
                    ]))
                    ->add('matchingLanguageValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingLanguageValue(),
                            ]
                        ),
                        'label' => 'organization.language_value',
                    ]))
                    ->add('matchingAgeValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingAgeValue(),
                                'help' => 'organization.age_value.tooltip',
                            ]
                        ),
                        'label' => 'organization.age_value',
                    ]))
                    ->add('matchingAvailabilityValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingAvailabilityValue(),
                                'help' => 'organization.availability_value.tooltip',
                            ]
                        ),
                        'label' => 'organization.availability_value',
                    ]))
                    ->add('matchingMotivationValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingMotivationValue(),
                            ]
                        ),
                        'label' => 'organization.motivation_value',
                    ]))
                    ->add('matchingLevelOfStudyValue', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingLevelOfStudyValue(),
                            ]
                        ),
                        'label' => 'organization.level_of_study_value',
                    ]))
                    ->add('revivalDelay', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrRangeSlider,
                            [
                                'data-slider-max' => 15,
                                'data-slider-value' => $matchingManager->getRevivalDelay(),
                            ]
                        ),
                        'label' => 'organization.revival_delay',
                    ]))
                    ->add('unmatchDelay', IntegerType::class, array_merge_recursive($labelAttr, [
                        'attr' => array_merge(
                            $attrRangeSlider,
                            [
                                'data-slider-min' => 2,
                                'data-slider-max' => 30,
                                'data-slider-value' => $matchingManager->getUnmatchDelay(),
                            ]
                        ),
                        'label' => 'organization.unmatch_delay',
                    ]))
                    ->add('mentorValidationRequired', CheckboxType::class, array_merge_recursive($labelAttr, [
                        'required' => false,
                        'label' => 'organization.mentor_validation_required',
                        'attr' => [
                            'class' => 'matching_preference_switch',
                            'help' => 'organization.mentor_validation_required.tooltip',
                        ],
                    ]))
                ;

                for ($i = 1; $i <= $maxLevelManaged; ++$i) {
                    $builder->add('matchingInstituteValue_'.$i, IntegerType::class, array_merge_recursive($labelAttr, [
                        'mapped' => false,
                        'attr' => array_merge(
                            $attrMatchingWeight,
                            [
                                'data-slider-value' => $matchingManager->getMatchingInstituteValue($i),
                            ]
                        ),
                        'label' => 'organization.institute_value',
                        'label_translation_parameters' => [
                            '%level%' => $i,
                        ],
                    ]));
                }

                $this->manageFullYearParam($builder, $matchingManager->isFullYearOnly());
            }

            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($matchingManager): void {
                if ($data = $event->getData()) {
                    $matchingInstituteValues = [];

                    for ($i = 1; $i <= Institute::MAX_LEVEL; ++$i) {
                        if (\array_key_exists('matchingInstituteValue_'.$i, $data)) {
                            $matchingInstituteValues[$i] = (int) $data['matchingInstituteValue_'.$i];
                        }
                    }

                    $matchingManager->setMatchingInstituteValues($matchingInstituteValues);

                    if (\array_key_exists('nbBuddiesRefusedMax', $data)
                        && 1 === (int) $data['nbBuddiesRefusedMax']
                    ) {
                        $data['nbBuddiesRefusedMax'] = 0;
                    }

                    if (\array_key_exists('fullYearOnly', $data)) {
                        $fullYearOnly = \boolval($data['fullYearOnly']);
                        $data['fullYearOnly'] = $fullYearOnly;
                        $this->manageFullYearParam($event->getForm(), $fullYearOnly);
                    }

                    $event->setData($data);
                }
            });
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'inherit_data' => true,
        ]);
    }

    private function manageFullYearParam($form, bool $fullYearOnly): void
    {
        if ($fullYearOnly) {
            $form
                ->add('firstSemesterEnd', HiddenType::class)
                ->add('secondSemesterStart', HiddenType::class)
            ;
        } else {
            $form
                ->add('firstSemesterEnd', DateType::class, [
                    'format' => \IntlDateFormatter::LONG,
                    'label' => false,
                    'attr' => [
                        'year' => 'disabled',
                    ],
                ])
                ->add('secondSemesterStart', DateType::class, [
                    'format' => \IntlDateFormatter::LONG,
                    'label' => false,
                    'attr' => [
                        'year' => 'disabled',
                    ],
                ])
            ;
        }
    }
}
