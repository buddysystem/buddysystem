<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Buddy;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuddyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('languagePairing', CheckboxType::class, [
                'required' => false,
                'label' => 'buddy.languagePairing',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Buddy::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'buddy';
    }
}
