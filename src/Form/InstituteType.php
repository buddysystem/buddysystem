<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Institute;
use App\Enum\UserRoleEnum;
use App\Form\EventListener\AddInstituteParentsFieldSubscriber;
use App\Services\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class InstituteType extends MatchingManagerType
{
    private $security;
    private $addInstituteParentsFieldSubscriber;

    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        LocaleManager $localeManager,
        AddInstituteParentsFieldSubscriber $addInstituteParentsFieldSubscriber
    ) {
        parent::__construct($em, $localeManager);
        $this->security = $security;
        $this->addInstituteParentsFieldSubscriber = $addInstituteParentsFieldSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $institute = $builder->getData();

        if ($institute->getDepth() < Institute::MAX_LEVEL) {
            $this->addInstituteParentsFieldSubscriber->setInstitute($institute);
            $builder->addEventSubscriber($this->addInstituteParentsFieldSubscriber);
        }

        if ($this->security->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $builder->add('schacId', TextType::class, [
                'required' => false,
                'label' => 'institute.schac_id',
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Institute::class,
            'manager' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return parent::getBlockPrefix();
    }
}
