<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Length;

class UserEditPasswordType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'user.password.mismatch',
                'mapped' => false,
                'constraints' => [
                    new Length([
                        'min' => 8,
                        'minMessage' => 'user.password.short',
                        'max' => 4096,
                    ]),
                ],
                'options' => [
                    'attr' => [
                        'class' => 'password-field',
                    ],
                ],
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'user.password',
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'user.password_confirmation',
                    ],
                ],
                'required' => true,
            ])
            ->add('oldPassword', PasswordType::class, [
                'required' => true,
                'mapped' => false,
                'constraints' => [
                    new UserPassword([
                        'message' => 'user.password.edit.no_equals',
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([]);
    }

    public function getBlockPrefix(): string
    {
        return 'edit_password';
    }
}
