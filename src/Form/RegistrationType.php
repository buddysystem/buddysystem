<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Country;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Form\EventListener\AddCityFieldSubscriber;
use App\Repository\CountryRepository;
use App\Services\LocaleManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Languages;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class RegistrationType extends BaseType
{
    private $addCityFieldSubscriber;
    private $countryRepo;
    private $localeManager;

    public function __construct(
        AddCityFieldSubscriber $addCityFieldSubscriber,
        CountryRepository $countryRepo,
        LocaleManager $localeManager
    ) {
        $this->addCityFieldSubscriber = $addCityFieldSubscriber;
        $this->countryRepo = $countryRepo;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $countries = $this->countryRepo->getWithMatchingManager(
            $this->localeManager->getCurrentLocale(),
            $options['type'],
            $options['includeDeactivated'],
            $options['matchingManager']
        );

        if (1 === \count($countries)) {
            $user = $builder->getData();
            $user->setCountry($countries[0]);
        }

        $builder
            ->add('email', EmailType::class, [
                'attr' => [
                    'placeholder' => 'user.email',
                ],
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'user.password.mismatch',
                'mapped' => false,
                'constraints' => [
                    new Length([
                        'min' => 8,
                        'minMessage' => 'user.password.short',
                        'max' => 4096,
                    ]),
                ],
                'options' => [
                    'attr' => [
                        'class' => 'password-field',
                    ],
                ],
                'first_options' => [
                    'attr' => [
                        'placeholder' => 'user.password',
                    ],
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'user.password_confirmation',
                    ],
                ],
                'required' => true,
            ])
            ->add('firstName', TextType::class, [
                'label' => 'user.firstname',
                'attr' => [
                    'placeholder' => 'user.firstname',
                ],
            ])
            ->add('lastName', TextType::class, [
                'label' => 'user.lastname',
                'attr' => [
                    'placeholder' => 'user.lastname',
                ],
            ])
            ->add('country', EntityType::class, [
                'required' => true,
                'choice_label' => 'name',
                'class' => Country::class,
                'choices' => $countries,
                'placeholder' => 'registration.country.placeholder',
            ])
            ->add('languages', ChoiceType::class, [
                'multiple' => true,
                'label' => 'user.languages',
                'choices' => array_flip(Languages::getNames($this->localeManager->getCurrentLocale())),
                'required' => true,
                'attr' => [
                    'class' => 'select2selector',
                ],
            ])
            ->add('privacyAccepted', CheckboxType::class, [
                'required' => true,
                'attr' => [
                    'toggleMode' => false,
                ],
            ])
            ->add('termsOfUse', CheckboxType::class, [
                'required' => true,
                'attr' => [
                    'toggleMode' => false,
                ],
            ])
        ;

        if (UserProfileTypeEnum::PROFILE_ADMIN !== $options['mode']) {
            $builder->addEventSubscriber($this->addCityFieldSubscriber);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'mode' => UserProfileTypeEnum::PROFILE_USER,
            'data_class' => User::class,
            'type' => MatchingManagerInterface::TYPE_INSTITUTE,
            'includeDeactivated' => false,
            'matchingManager' => null,
            'validation_groups' => ['Default', 'Registration'],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'registration';
    }
}
