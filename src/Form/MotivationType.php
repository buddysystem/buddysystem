<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Motivation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MotivationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nameMentee', TextType::class, [
                'required' => $options['required'],
            ])
            ->add('nameMentor', TextType::class, [
                'required' => $options['required'],
            ])
            ->add('translatableLocale', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Motivation::class,
            'required' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'motivation';
    }
}
