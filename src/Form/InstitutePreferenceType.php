<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\RegistrationEmailDomain;
use App\Repository\InstituteRepository;
use App\Services\AllowedDomainHelper;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class InstitutePreferenceType extends MatchingPreferenceType
{
    protected $matchingManagerHelper;
    private $allowedDomainHelper;
    private $translator;

    public function __construct(
        InstituteRepository $instituteRepo,
        AllowedDomainHelper $allowedDomainHelper,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        parent::__construct($instituteRepo, $matchingManagerHelper);
        $this->allowedDomainHelper = $allowedDomainHelper;
        $this->translator = $translator;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Institute $institute */
        $institute = $builder->getData();
        parent::buildForm($builder, $options);

        if (!$institute->hasChildren()) {
            $builder->add('studiesRequired', CheckboxType::class, [
                'required' => false,
                'attr' => [
                    'help' => 'institute.studies_required.help',
                ],
                'label_attr' => [
                    'class' => 'col-sm-5',
                ],
            ]);
        }

        $builder
            ->add('registrationEmailDomains', CollectionType::class, [
                'label' => 'institute.allowed_domains',
                'entry_type' => RegistrationEmailDomainType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'required' => false,
                'delete_empty' => function (RegistrationEmailDomain $registrationEmailDomain) {
                    return empty($registrationEmailDomain->getDomain());
                },
                'by_reference' => false,
                'attr' => [
                    'help' => $this->translator->trans(
                        'institute.allowed_domains.help',
                        ['%domains%' => implode('<br>', $this->allowedDomainHelper->getAllowedDomains($institute, null, true))]
                    ),
                    'class' => 'collection-widget',
                ],
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ])
            ->add('showBonusOption', CheckboxType::class, [
                'required' => false,
                'label' => 'organization.show_bonus_option',
            ])
        ;

        $lowerLevelManager = $this->matchingManagerHelper->getLowerLevelCriteriasManager($institute);
        if ($institute->getParent() || $lowerLevelManager instanceof Association) {
            $builder->add('lowerLevelManagement', CheckboxType::class, [
                'required' => false,
                'label' => 'institute.lower_level_management',
                'attr' => [
                    'help' => 'institute.lower_level_management.help',
                ],
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Institute::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return parent::getBlockPrefix();
    }
}
