<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Services\LocaleManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Countries;

abstract class MatchingManagerType extends AbstractType
{
    protected $em;
    protected $countryRepo;
    protected $localeManager;

    public function __construct(EntityManagerInterface $em, LocaleManager $localeManager)
    {
        $this->em = $em;
        $this->countryRepo = $em->getRepository(Country::class);
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var MatchingManagerInterface $matchingManager */
        $matchingManager = $builder->getData();
        $manager = \array_key_exists('manager', $options)
            ? $options['manager']
            : null
        ;

        $builder
            ->add('name', TextType::class, [
                'label' => 'organization.name',
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ])
            ->add('cities', CollectionType::class, [
                'label' => 'organization.city',
                'entry_type' => SearchCollectionType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'prototype_name' => '__city__name__',
                'required' => true,
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
                'entry_options' => [
                    'choice_label' => 'name',
                    'class' => City::class,
                    'query_builder' => function (EntityRepository $er) use ($manager, $matchingManager) {
                        $qb = $er->createQueryBuilder('c')
                            ->orderBy('c.name', 'ASC')
                        ;

                        if ($manager) {
                            $qb->setParameter('manager', $manager);

                            if (!$this->em->contains($matchingManager) || $manager !== $matchingManager) {
                                if ($manager instanceof Institute) {
                                    $where = 'i1.id = :manager';
                                } else {
                                    $where = 'a1.id = :manager';
                                }

                                if ($this->em->contains($matchingManager)) {
                                    if ($matchingManager instanceof Institute) {
                                        $where .= ' or i1.id = :matchingManager';
                                    } else {
                                        $where .= ' or a1.id = :matchingManager';
                                    }

                                    $qb->setParameter('matchingManager', $matchingManager->getId());
                                }

                                $cities = $er->createQueryBuilder('c1')
                                    ->select('c1.id')
                                    ->leftJoin('c1.institutes', 'i1')
                                    ->leftJoin('c1.associations', 'a1')
                                    ->where($where)
                                    ->getQuery()
                                    ->getDQL()
                                ;

                                $qb->where($qb->expr()->in('c.id', $cities));
                            } else {
                                $qb
                                    ->innerJoin('c.country', 'country')
                                    ->innerJoin('country.cities', 'cityManager')
                                ;

                                if ($manager instanceof Institute) {
                                    $qb
                                        ->innerJoin('cityManager.institutes', 'i')
                                        ->where('i.id = :manager')
                                    ;
                                } else {
                                    $qb
                                        ->innerJoin('cityManager.associations', 'a')
                                        ->where('a.id = :manager')
                                    ;
                                }
                            }
                        }

                        return $qb;
                    },
                    'group_by' => function ($choice, $key, $value) {
                        return ucfirst(Countries::getName($choice->getCountry()->getCode()));
                    },
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'organization.description',
                'attr' => ['class' => 'tinymce'],
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ])
            ->add('logoFile', FileType::class, [
                'required' => false,
                'label' => 'organization.logo',
                'help' => 'file.max_upload_size',
            ])
            ->add('website', TextType::class, [
                'required' => false,
                'label' => 'organization.website',
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                if ($data = $event->getData()) {
                    if (\array_key_exists('cities', $data)) {
                        $data['cities'] = array_unique($data['cities']);
                        $event->setData($data);
                    }
                }
            })
        ;

        if (!$manager) {
            $defaultCountry = ($firstCity = $matchingManager->getCities()->first())
                ? $firstCity->getCountry()
                : null
            ;

            $builder->add('country', EntityType::class, [
                'choice_label' => 'name',
                'mapped' => false,
                'label' => 'organization.country',
                'class' => Country::class,
                'data' => $defaultCountry,
                'choices' => $this->countryRepo->getAllSorted(
                    $this->localeManager->getCurrentLocale()
                ),
                'label_attr' => [
                    'class' => 'col-sm-4',
                ],
            ]);
        }
    }

    public function getBlockPrefix(): string
    {
        return 'matching_manager';
    }
}
