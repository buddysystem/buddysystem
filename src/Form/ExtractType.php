<?php

declare(strict_types=1);

namespace App\Form;

use App\Enum\ExtractTypeEnum;
use App\Extract\ExtractModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtractType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', ChoiceType::class, [
                'label' => 'extract.type',
                'choices' => ExtractTypeEnum::getChoices(),
            ])
            ->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
                $data = $event->getData();

                $this->addFilters($event->getForm(), $data->getType());
            })
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event): void {
                $data = $event->getData();

                $this->addFilters($event->getForm(), $data['type']);
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ExtractModel::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'extract';
    }

    private function addFilters(FormInterface $form, string $type): void
    {
        $form->add('filters', CollectionType::class, [
            'entry_type' => ExtractFilterType::class,
            'label' => false,
            'entry_options' => [
                'label' => false,
                'type' => $type,
            ],
        ]);
    }
}
