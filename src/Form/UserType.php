<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Country;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserVoterEnum;
use App\Form\EventListener\AddCityFieldSubscriber;
use App\Repository\CountryRepository;
use App\Services\LocaleManager;
use App\Validator\NotThrowawayEmail;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Languages;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserType extends BaseType
{
    private $security;
    private $addCityFieldSubscriber;
    private $countryRepo;
    private $localeManager;

    public function __construct(
        Security $security,
        AddCityFieldSubscriber $addCityFieldSubscriber,
        CountryRepository $countryRepo,
        LocaleManager $localeManager
    ) {
        $this->security = $security;
        $this->addCityFieldSubscriber = $addCityFieldSubscriber;
        $this->countryRepo = $countryRepo;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var User $user */
        $user = $builder->getData();
        $email = $user->getEmail();

        $params = [];
        $paramsLocation = [];
        if (!$this->security->isGranted(UserVoterEnum::EDIT_ALL, $user)) {
            $params['attr'] = ['readonly' => 'readonly'];
        } else {
            $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($email): void {
                if ($data = $event->getData()) {
                    if (\array_key_exists('departure', $data)
                        && \array_key_exists('isLocal', $data)
                        && $data['isLocal']
                    ) {
                        $data['departure'] = null;
                        $event->setData($data);
                    }

                    if ($data['email'] !== $email) {
                        $event->getForm()->add('email', EmailType::class, [
                            'constraints' => new NotThrowawayEmail(),
                        ]);
                    }
                }
            });
        }

        if (!$this->security->isGranted(UserVoterEnum::EDIT_LOCATION, $user)) {
            $paramsLocation['attr'] = ['readonly' => 'readonly'];
        }

        $builder
            ->add('email', EmailType::class, $params)
            ->add('phone_number', PhoneNumberType::class, array_merge_recursive($params, [
                'widget' => PhoneNumberType::WIDGET_COUNTRY_CHOICE,
                'required' => false,
                'label' => 'user.phone',
            ]))
            ->add('firstname', TextType::class, array_merge_recursive($params, [
                'required' => true,
                'label' => 'user.firstname',
            ]))
            ->add('lastname', TextType::class, array_merge_recursive($params, [
                'required' => true,
                'label' => 'user.lastname',
            ]))
            ->add('languages', ChoiceType::class, array_merge_recursive($params, [
                'multiple' => true,
                'label' => 'user.languages',
                'choices' => array_flip(Languages::getNames($this->localeManager->getCurrentLocale())),
                'required' => true,
            ]))
            ->add('newsletter', CheckboxType::class, array_merge_recursive($params, [
                'required' => false,
                'label' => 'user.newsletter',
            ]))
            ->add('userNotification', UserNotificationType::class, array_merge_recursive($params, [
                'required' => false,
                'label' => false,
                'params' => $params,
            ]))
            ->add('country', EntityType::class, array_merge_recursive($paramsLocation, [
                'placeholder' => 'registration.country.placeholder',
                'required' => true,
                'choice_label' => 'name',
                'class' => Country::class,
                'choices' => $this->countryRepo->getWithMatchingManager(
                    $this->localeManager->getCurrentLocale(),
                    $options['type'],
                    $options['includeDeactivated']
                ),
            ]))
        ;

        if (UserProfileTypeEnum::PROFILE_ADMIN !== $options['mode']) {
            $this->addCityFieldSubscriber->setParams($paramsLocation);
            $builder->addEventSubscriber($this->addCityFieldSubscriber);
        }

        if ($this->security->isGranted(UserVoterEnum::EDIT_SENSITIVE_DATA, $user)) {
            $builder
                ->add('pictureFile', FileType::class, [
                    'required' => false,
                    'help' => 'file.max_upload_size',
                ])
            ;
        }

        $roles = $options['roles'];
        if (\count($roles) > 0) {
            $builder
                ->add('roles', ChoiceType::class, [
                    'choices' => $roles,
                    'required' => false,
                    'label' => 'user.roles',
                    'multiple' => true,
                    'choice_label' => function ($choiceValue, $key, $value) {
                        return strtolower(str_replace('_', ' ', $value));
                    },
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'roles' => [],
            'mode' => UserProfileTypeEnum::PROFILE_USER,
            'type' => MatchingManagerInterface::TYPE_INSTITUTE,
            'includeDeactivated' => false,
            'validation_groups' => ['Default', 'Profile'],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'user';
    }
}
