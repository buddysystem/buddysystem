<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\AssociationInstitute;
use App\Enum\AssociationInstitutePermEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class AssociationInstituteRightType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var AssociationInstitute $associationInstitute */
        $associationInstitute = $builder->getData();
        $association = $associationInstitute->getAssociation();

        $builder
            ->add('rights', ChoiceType::class, [
                'required' => true,
                'choices' => AssociationInstitutePermEnum::getChoices(),
                'disabled' => $options['disabled'],
                'attr' => [
                    'class' => 'rights_selector',
                    'data-id' => $association->getId(),
                ],
                'label' => false,
                'model_transformer' => new CallbackTransformer(
                    function ($rights) {
                        if (\in_array(AssociationInstitutePermEnum::PERM_PREFERENCES, $rights)) {
                            return AssociationInstitutePermEnum::PERM_PREFERENCES;
                        }

                        if (\in_array(AssociationInstitutePermEnum::PERM_MATCH, $rights)) {
                            return AssociationInstitutePermEnum::PERM_MATCH;
                        }

                        return AssociationInstitutePermEnum::PERM_VIEW;
                    },
                    function ($right) {
                        $rights = [$right];

                        if (AssociationInstitutePermEnum::PERM_VIEW !== $right) {
                            array_unshift($rights, AssociationInstitutePermEnum::PERM_VIEW);

                            if (AssociationInstitutePermEnum::PERM_PREFERENCES === $right) {
                                array_unshift($rights, AssociationInstitutePermEnum::PERM_MATCH);
                            }
                        }

                        return $rights;
                    }
                ),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AssociationInstitute::class,
            'disabled' => false,
            'csrf_protection' => false,
        ]);
    }
}
