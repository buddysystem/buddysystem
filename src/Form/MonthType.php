<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class MonthType extends AbstractType
{
    public function finishView(FormView $view, FormInterface $form, array $options): void
    {
        if ('choice' == $options['widget']) {
            if (empty($view['day']->vars['value'])) {
                $view['day']->vars['value'] = $view['day']->vars['choices'][0]->value;
            }

            if (empty($view['year']->vars['value'])) {
                $view['year']->vars['value'] = $view['year']->vars['choices'][0]->value;
            }

            $style = 'display:none';
            if (false == empty($view['day']->vars['attr']['style'])) {
                $style = $view['day']->vars['attr']['style'].'; '.$style;
            }
            $view['day']->vars['attr']['style'] = $style;

            $style = 'display:none';
            if (false == empty($view['year']->vars['attr']['style'])) {
                $style = $view['year']->vars['attr']['style'].'; '.$style;
            }
            $view['year']->vars['attr']['style'] = $style;
        }
    }

    public function getParent(): string
    {
        return DateType::class;
    }

    public function getName(): string
    {
        return 'month';
    }
}
