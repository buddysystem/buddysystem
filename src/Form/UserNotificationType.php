<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\UserNotification;
use Symfony\Component\Form\Extension\Core\Type\BaseType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserNotificationType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('newGlobalMessage', CheckboxType::class, array_merge_recursive($options['params'], [
                'label' => 'profile.notifications.email.new_global_message',
            ]))
            ->add('newGroupMessage', CheckboxType::class, array_merge_recursive($options['params'], [
                'label' => 'profile.notifications.email.new_group_message',
            ]))
            ->add('newIndividualMessage', CheckboxType::class, array_merge_recursive($options['params'], [
                'label' => 'profile.notifications.email.new_individual_message',
            ]))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserNotification::class,
            'params' => [],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'user_notification';
    }
}
