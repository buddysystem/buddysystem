<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Report;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['show_comment']) {
            $builder->add('comment', TextareaType::class, [
                'label' => false,
                'required' => true,
            ]);
        }

        $builder
            ->add('deleteBuddy', CheckboxType::class, [
                'required' => false,
                'label' => 'buddy.popup.report.new_buddy',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
            'show_comment' => true,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'report';
    }
}
