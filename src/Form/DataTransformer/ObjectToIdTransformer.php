<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ObjectToIdTransformer implements DataTransformerInterface
{
    protected $registry;
    protected $class;
    protected $multiple;
    protected $property;

    public function __construct(
        ManagerRegistry $registry,
        string $class,
        bool $multiple = false,
        string $property = 'id'
    ) {
        $this->registry = $registry;
        $this->class = $class;
        $this->multiple = $multiple;
        $this->property = $property;
    }

    public function transform($value)
    {
        if (null === $value) {
            return null;
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        $property = $this->getProperty();

        if ($this->isMultiple() && \is_array($value)) {
            $propertyValues = [];

            foreach ($value as $e) {
                if ($accessor->isReadable($value, $property)) {
                    $propertyValues[] = $accessor->getValue($e, $property);
                }
            }

            return implode(',', $propertyValues);
        }

        if (!$accessor->isReadable($value, $property)) {
            return null;
        }

        return $accessor->getValue($value, $property);
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            if ($this->isMultiple()) {
                return [];
            }

            return null;
        }

        $repo = $this->getRepository();
        $property = $this->getProperty();
        $class = $this->getClass();

        if ($this->isMultiple()) {
            $ids = explode(',', $value);

            return $repo->findBy([$property => $ids]);
        }

        $result = $repo->findOneBy([$property => $value]);

        if (null === $result) {
            throw new TransformationFailedException(sprintf('Can\'t find entity of class "%s" with property "%s" = "%s".', $class, $property, $value));
        }

        return $result;
    }

    protected function getClass(): string
    {
        return $this->class;
    }

    protected function isMultiple(): bool
    {
        return $this->multiple;
    }

    protected function getProperty(): string
    {
        return $this->property;
    }

    protected function getRepository(): ObjectRepository
    {
        return $this->registry->getRepository($this->getClass());
    }
}
