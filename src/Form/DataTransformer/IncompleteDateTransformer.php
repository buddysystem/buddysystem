<?php

declare(strict_types=1);

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class IncompleteDateTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        return $value;
    }

    public function reverseTransform($value)
    {
        if (!\is_array($value)) {
            return $value;
        }

        if (empty($value['year'])) {
            $value['year'] = date('Y');
        }

        if (empty($value['day'])) {
            $value['day'] = 1;
        }

        if (empty($value['month'])) {
            $value['month'] = 1;
        }

        return $value;
    }
}
