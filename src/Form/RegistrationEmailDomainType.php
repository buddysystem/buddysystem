<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\RegistrationEmailDomain;
use App\Enum\RegistrationEmailDomainUserTypeEnum;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationEmailDomainType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('domain', TextType::class, [
                'required' => true,
                'label' => false,
            ])
            ->add('userType', ChoiceType::class, [
                'label' => false,
                'choices' => RegistrationEmailDomainUserTypeEnum::getChoices(),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RegistrationEmailDomain::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'domain_collection_type';
    }
}
