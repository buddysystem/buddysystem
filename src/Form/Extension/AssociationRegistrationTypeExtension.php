<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\MatchingManagerInterface;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\EventListener\AddAssociationFieldSubscriber;
use App\Form\RegistrationType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssociationRegistrationTypeExtension extends AbstractTypeExtension
{
    private $addAssociationFieldSubscriber;

    public function __construct(AddAssociationFieldSubscriber $addAssociationFieldSubscriber)
    {
        $this->addAssociationFieldSubscriber = $addAssociationFieldSubscriber;
    }

    public static function getExtendedTypes(): iterable
    {
        return [RegistrationType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
            return;
        }

        $builder
            ->add('roles', CollectionType::class, [
                'entry_type' => HiddenType::class,
                'data' => [UserRoleEnum::ROLE_BUDDYCOORDINATOR],
                'label' => false,
            ])
            ->addEventSubscriber($this->addAssociationFieldSubscriber)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'RegistrationBC'];
        });

        $resolver->setDefault('type', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return MatchingManagerInterface::TYPE_ASSOCIATION;
        });

        $resolver->setDefault('includeDeactivated', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return true;
        });
    }
}
