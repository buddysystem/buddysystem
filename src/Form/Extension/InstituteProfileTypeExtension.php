<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\UserProfileTypeEnum;
use App\Enum\UserVoterEnum;
use App\Form\EventListener\AddInstituteFieldSubscriber;
use App\Form\UserType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class InstituteProfileTypeExtension extends AbstractTypeExtension
{
    private $security;
    private $addInstituteFieldSubscriber;

    public function __construct(Security $security, AddInstituteFieldSubscriber $addInstituteFieldSubscriber)
    {
        $this->security = $security;
        $this->addInstituteFieldSubscriber = $addInstituteFieldSubscriber;
    }

    public static function getExtendedTypes(): iterable
    {
        return [UserType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
            return;
        }

        $user = $builder->getData();
        $params = ['label' => 'user.institute'];

        if (!$this->security->isGranted(UserVoterEnum::EDIT_OTHER, $user)) {
            $params['attr'] = ['readonly' => 'readonly'];
        }

        $this->addInstituteFieldSubscriber->setParams($params);
        $builder->addEventSubscriber($this->addInstituteFieldSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'RegistrationRI'];
        });

        $resolver->setDefault('includeDeactivated', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
                return $previousValue;
            }

            return true;
        });
    }
}
