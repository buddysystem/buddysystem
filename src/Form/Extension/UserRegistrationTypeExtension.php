<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\User;
use App\Enum\UserGenderEnum;
use App\Enum\UserLevelStudyEnum;
use App\Enum\UserProfileTypeEnum;
use App\Form\EventListener\AddBonusOptionFieldSubscriber;
use App\Form\EventListener\AddInstituteFieldSubscriber;
use App\Form\EventListener\AddMotivationFieldSubscriber;
use App\Form\EventListener\AddNbBuddiesWantedFieldSubscriber;
use App\Form\EventListener\AddSemesterFieldSubscriber;
use App\Form\EventListener\AddStudyFieldSubscriber;
use App\Form\EventListener\AddTypeOfMobilityFieldSubscriber;
use App\Form\RegistrationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegistrationTypeExtension extends AbstractTypeExtension
{
    private $addInstituteUserSubscriber;
    private $addSemesterFieldSubscriber;
    private $addStudyFieldSubscriber;
    private $addMotivationFieldSubscriber;
    private $addNbBuddiesWantedFieldSubscriber;
    private $addBonusOptionFieldSubscriber;
    private $addTypeOfMobilityFieldSubscriber;

    public function __construct(
        AddInstituteFieldSubscriber $addInstituteFieldSubscriber,
        AddSemesterFieldSubscriber $addSemesterFieldSubscriber,
        AddStudyFieldSubscriber $addStudyFieldSubscriber,
        AddMotivationFieldSubscriber $addMotivationFieldSubscriber,
        AddNbBuddiesWantedFieldSubscriber $addNbBuddiesWantedFieldSubscriber,
        AddBonusOptionFieldSubscriber $addBonusOptionFieldSubscriber,
        AddTypeOfMobilityFieldSubscriber $addTypeOfMobilityFieldSubscriber
    ) {
        $this->addInstituteUserSubscriber = $addInstituteFieldSubscriber;
        $this->addSemesterFieldSubscriber = $addSemesterFieldSubscriber;
        $this->addStudyFieldSubscriber = $addStudyFieldSubscriber;
        $this->addMotivationFieldSubscriber = $addMotivationFieldSubscriber;
        $this->addNbBuddiesWantedFieldSubscriber = $addNbBuddiesWantedFieldSubscriber;
        $this->addBonusOptionFieldSubscriber = $addBonusOptionFieldSubscriber;
        $this->addTypeOfMobilityFieldSubscriber = $addTypeOfMobilityFieldSubscriber;
    }

    public static function getExtendedTypes(): iterable
    {
        return [RegistrationType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
            return;
        }

        $this->addInstituteUserSubscriber->setParams(['label' => 'registration.institute']);

        $builder
            ->add('levelOfStudy', ChoiceType::class, [
                'required' => true,
                'label' => 'user.level_of_study',
                'placeholder' => 'placeholder.empty',
                'choices' => UserLevelStudyEnum::getChoices(),
            ])
            ->add('levelOfStudyOther', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'user.level_of_study_other',
                ],
            ])
            ->add('isLocal', ChoiceType::class, [
                'choices' => [
                    'mentor' => true,
                    'mentee' => false,
                ],
                'attr' => [
                    'class' => 'btn-group btn-group-toggle mt-3',
                    'data-toggle' => 'buttons',
                ],
                'multiple' => false,
                'expanded' => true,
                'label' => false,
                'data' => null,
                'required' => true,
                'placeholder' => false,
            ])
            ->add('dob', BirthdayType::class, [
                'required' => true,
                'years' => range(idate('Y') - User::MAX_AGE, idate('Y') - User::MIN_AGE),
                'label' => 'user.dateofbirth',
                'label_attr' => [
                    'class' => 'col-sm-2',
                ],
                'placeholder' => [
                    'year' => '----', 'month' => '----', 'day' => '--',
                ],
            ])
            ->add('sex', ChoiceType::class, [
                'multiple' => false,
                'expanded' => false,
                'required' => true,
                'choices' => UserGenderEnum::getChoices(),
                'label' => 'user.gender',
                'placeholder' => 'placeholder.empty',
                'label_attr' => [
                    'class' => 'col-sm-2',
                ],
            ])
            ->addEventSubscriber($this->addSemesterFieldSubscriber)
            ->addEventSubscriber($this->addStudyFieldSubscriber)
            ->addEventSubscriber($this->addInstituteUserSubscriber)
            ->addEventSubscriber($this->addMotivationFieldSubscriber)
            ->addEventSubscriber($this->addNbBuddiesWantedFieldSubscriber)
            ->addEventSubscriber($this->addBonusOptionFieldSubscriber)
            ->addEventSubscriber($this->addTypeOfMobilityFieldSubscriber)
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $form = $event->getForm();
            $options = $form->get('country')->getConfig()->getOptions();
            $options['attr']['help'] = 'registration.country.tooltip';
            $options['label'] = 'registration.country';
            $form->add('country', EntityType::class, $options);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'RegistrationUser'];
        });
    }
}
