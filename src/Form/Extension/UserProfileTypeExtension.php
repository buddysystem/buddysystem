<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\User;
use App\Enum\UserGenderEnum;
use App\Enum\UserLevelStudyEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserVoterEnum;
use App\Form\DataTransformer\StringToBoolTransformer;
use App\Form\EventListener\AddBonusOptionFieldSubscriber;
use App\Form\EventListener\AddHobbyFieldSubscriber;
use App\Form\EventListener\AddInstituteFieldSubscriber;
use App\Form\EventListener\AddMotivationFieldSubscriber;
use App\Form\EventListener\AddNbBuddiesWantedFieldSubscriber;
use App\Form\EventListener\AddSemesterFieldSubscriber;
use App\Form\EventListener\AddStudyFieldSubscriber;
use App\Form\EventListener\AddTypeOfMobilityFieldSubscriber;
use App\Form\UserType;
use App\Services\LocaleManager;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Countries;
use Symfony\Component\Intl\Languages;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class UserProfileTypeExtension extends AbstractTypeExtension
{
    public const NB_YEARS_VISIBLE = 3;

    private $security;
    private $addInstituteFieldSubscriber;
    private $addSemesterFieldSubscriber;
    private $addStudyFieldSubscriber;
    private $addMotivationFieldSubscriber;
    private $addHobbyFieldSubscriber;
    private $addNbBuddiesWantedFieldSubscriber;
    private $localeManager;
    private $addBonusOptionFieldSubscriber;
    private $addTypeOfMobilityFieldSubscriber;

    public function __construct(
        Security $security,
        AddInstituteFieldSubscriber $addInstituteFieldSubscriber,
        AddSemesterFieldSubscriber $addSemesterFieldSubscriber,
        AddStudyFieldSubscriber $addStudyFieldSubscriber,
        AddMotivationFieldSubscriber $addMotivationFieldSubscriber,
        AddHobbyFieldSubscriber $addHobbyFieldSubscriber,
        AddNbBuddiesWantedFieldSubscriber $addNbBuddiesWantedFieldSubscriber,
        AddBonusOptionFieldSubscriber $addBonusOptionFieldSubscriber,
        AddTypeOfMobilityFieldSubscriber $addTypeOfMobilityFieldSubscriber,
        LocaleManager $localeManager
    ) {
        $this->security = $security;
        $this->addInstituteFieldSubscriber = $addInstituteFieldSubscriber;
        $this->addSemesterFieldSubscriber = $addSemesterFieldSubscriber;
        $this->addStudyFieldSubscriber = $addStudyFieldSubscriber;
        $this->addMotivationFieldSubscriber = $addMotivationFieldSubscriber;
        $this->addHobbyFieldSubscriber = $addHobbyFieldSubscriber;
        $this->addNbBuddiesWantedFieldSubscriber = $addNbBuddiesWantedFieldSubscriber;
        $this->addBonusOptionFieldSubscriber = $addBonusOptionFieldSubscriber;
        $this->addTypeOfMobilityFieldSubscriber = $addTypeOfMobilityFieldSubscriber;
        $this->localeManager = $localeManager;
    }

    public static function getExtendedTypes(): iterable
    {
        return [UserType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
            return;
        }

        $user = $builder->getData();
        $yearsRange = range(idate('Y'), idate('Y') + self::NB_YEARS_VISIBLE);

        $params = [];
        $paramsArchived = [];
        if (!$this->security->isGranted(UserVoterEnum::EDIT_ALL, $user)) {
            $params['attr'] = ['readonly' => 'readonly'];

            if ($user->isArchived()) {
                $paramsArchived['attr'] = $params['attr'];
            }
        }

        $this->addInstituteFieldSubscriber->setParams(array_merge_recursive($paramsArchived, ['label' => 'user.institute']));
        $this->addSemesterFieldSubscriber->setParams($paramsArchived);
        $this->addStudyFieldSubscriber->setParams($paramsArchived);
        $this->addMotivationFieldSubscriber->setParams($params);
        $this->addHobbyFieldSubscriber->setParams($params);
        $this->addNbBuddiesWantedFieldSubscriber->setParams($params);
        $this->addBonusOptionFieldSubscriber->setParams($paramsArchived);
        $this->addTypeOfMobilityFieldSubscriber->setParams($paramsArchived);

        $builder
            ->add('isLocal', ChoiceType::class, array_merge_recursive($paramsArchived, [
                'choices' => [
                    'mentor' => true,
                    'mentee' => false,
                ],
                'data' => $user->isLocal(),
                'attr' => [
                    'class' => 'btn-group btn-group-toggle',
                    'data-toggle' => 'buttons',
                ],
                'multiple' => false,
                'expanded' => true,
                'required' => false,
                'placeholder' => false,
            ]))
            ->add('arrival', DateType::class, array_merge_recursive($params, [
                'label' => $user->isLocal() ? 'user.arrival.mentor' : 'user.arrival',
                'required' => false,
                'years' => $user->getArrival()
                    ? array_merge([$user->getArrival()->format('Y')], $yearsRange)
                    : $yearsRange,
            ]))
            ->add('departure', DateType::class, array_merge_recursive($params, [
                'required' => false,
                'label' => 'user.departure',
                'years' => $user->getDeparture()
                    ? array_merge([$user->getDeparture()->format('Y')], $yearsRange)
                    : $yearsRange,
            ]))
            ->add('languagesWanted', ChoiceType::class, array_merge_recursive($params, [
                'multiple' => true,
                'label' => 'profile.languages_wanted',
                'choices' => array_flip(Languages::getNames($this->localeManager->getCurrentLocale())),
                'required' => false,
            ]))
            ->add('sexWanted', CheckboxType::class, array_merge_recursive($params, [
                'label' => 'profile.same_gender',
                'required' => false,
            ]))
            ->add('comment', TextareaType::class, array_merge_recursive($params, [
                'required' => false,
                'label' => 'user.comments',
                'attr' => [
                    'placeholder' => 'profile.comments.placeholder',
                ],
            ]))
            ->add('levelOfStudy', ChoiceType::class, [
                'required' => true,
                'label' => 'user.level_of_study',
                'choices' => UserLevelStudyEnum::getChoices(),
                'placeholder' => 'placeholder.empty',
            ])
            ->add('levelOfStudyOther', TextType::class, array_merge_recursive($paramsArchived, [
                'required' => false,
                'label' => false,
                'attr' => [
                    'placeholder' => 'user.level_of_study_other',
                ],
            ]))
            ->add('nationality', ChoiceType::class, array_merge_recursive($params, [
                'choices' => array_flip(Countries::getNames()),
                'required' => false,
            ]))
            ->add('dob', BirthdayType::class, array_merge_recursive($params, [
                'required' => true,
                'years' => range(idate('Y') - User::MAX_AGE, idate('Y') - User::MIN_AGE),
                'label' => 'user.dateofbirth',
            ]))
            ->add('sex', ChoiceType::class, array_merge_recursive($params, [
                'multiple' => false,
                'expanded' => false,
                'required' => true,
                'choices' => UserGenderEnum::getChoices(),
                'label' => 'user.gender',
                'placeholder' => 'placeholder.empty',
            ]))
            ->addEventSubscriber($this->addSemesterFieldSubscriber)
            ->addEventSubscriber($this->addStudyFieldSubscriber)
            ->addEventSubscriber($this->addInstituteFieldSubscriber)
            ->addEventSubscriber($this->addMotivationFieldSubscriber)
            ->addEventSubscriber($this->addHobbyFieldSubscriber)
            ->addEventSubscriber($this->addNbBuddiesWantedFieldSubscriber)
            ->addEventSubscriber($this->addBonusOptionFieldSubscriber)
            ->addEventSubscriber($this->addTypeOfMobilityFieldSubscriber)
        ;

        if ($this->security->isGranted(UserVoterEnum::EDIT_OTHER, $user)
            && !$this->security->isGranted(UserVoterEnum::IS_MANAGER, $user)
        ) {
            $builder->add(
                'archived',
                ChoiceType::class,
                [
                    'choices' => [
                        'no' => false,
                        'yes' => true,
                    ],
                    'data' => $user->isArchived(),
                    'label' => 'user.archived',
                    'model_transformer' => new StringToBoolTransformer(),
                ]
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_USER !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'ProfileUser'];
        });
    }
}
