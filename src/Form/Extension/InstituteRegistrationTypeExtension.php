<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Form\EventListener\AddInstituteFieldSubscriber;
use App\Form\RegistrationType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InstituteRegistrationTypeExtension extends AbstractTypeExtension
{
    private $addInstituteFieldSubscriber;

    public function __construct(AddInstituteFieldSubscriber $addInstituteFieldSubscriber)
    {
        $this->addInstituteFieldSubscriber = $addInstituteFieldSubscriber;
    }

    public static function getExtendedTypes(): iterable
    {
        return [RegistrationType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
            return;
        }

        $this->addInstituteFieldSubscriber->setParams(['label' => 'registration.institute.manager']);

        $builder
            ->add('roles', CollectionType::class, [
                'entry_type' => HiddenType::class,
                'data' => [UserRoleEnum::ROLE_INSTITUTE_MANAGER],
                'label' => false,
            ])
            ->addEventSubscriber($this->addInstituteFieldSubscriber)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'RegistrationRI'];
        });

        $resolver->setDefault('includeDeactivated', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_INSTITUTE !== $options['mode']) {
                return $previousValue;
            }

            return true;
        });
    }
}
