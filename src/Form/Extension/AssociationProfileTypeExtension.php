<?php

declare(strict_types=1);

namespace App\Form\Extension;

use App\Entity\MatchingManagerInterface;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserVoterEnum;
use App\Form\EventListener\AddAssociationFieldSubscriber;
use App\Form\UserType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class AssociationProfileTypeExtension extends AbstractTypeExtension
{
    private $security;
    private $addAssociationFieldSubscriber;

    public function __construct(Security $security, AddAssociationFieldSubscriber $addAssociationFieldSubscriber)
    {
        $this->security = $security;
        $this->addAssociationFieldSubscriber = $addAssociationFieldSubscriber;
    }

    public static function getExtendedTypes(): iterable
    {
        return [UserType::class];
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
            return;
        }

        $user = $builder->getData();

        if (!$this->security->isGranted(UserVoterEnum::EDIT_ALL, $user)
            || !$this->security->isGranted(UserVoterEnum::EDIT_LOCATION, $user)
        ) {
            $this->addAssociationFieldSubscriber->setParams(['attr' => ['readonly' => 'readonly']]);
        }

        $builder->addEventSubscriber($this->addAssociationFieldSubscriber);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefault('validation_groups', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return ['Default', 'Profile', 'RegistrationBC'];
        });

        $resolver->setDefault('type', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return MatchingManagerInterface::TYPE_ASSOCIATION;
        });

        $resolver->setDefault('includeDeactivated', function (Options $options, $previousValue) {
            if (UserProfileTypeEnum::PROFILE_ASSOCIATION !== $options['mode']) {
                return $previousValue;
            }

            return true;
        });
    }
}
