<?php

declare(strict_types=1);

namespace App\Form;

use App\Enum\ExtractTypeEnum;
use App\Enum\SemesterPeriodEnum;
use App\Enum\UserTypeMobilityEnum;
use App\Repository\SemesterRepository;
use App\Services\LocaleManager;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Languages;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExtractFilterType extends AbstractType
{
    private $semesterRepo;
    private $matchingManagerHelper;
    private $localeManager;

    public function __construct(
        SemesterRepository $semesterRepo,
        MatchingManagerHelper $matchingManagerHelper,
        LocaleManager $localeManager
    ) {
        $this->semesterRepo = $semesterRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->localeManager = $localeManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $type = $options['type'];
        $filters = $this->getFiltersByType($type);

        foreach ($filters as $filter) {
            switch ($filter) {
                case 'archived':
                    $builder->add('archived', ChoiceType::class, [
                        'required' => false,
                        'choices' => [
                            'no' => 0,
                            'yes' => 1,
                        ],
                        'data' => 0,
                        'label' => 'user.archived',
                    ]);
                    break;
                case 'semester':
                    $builder->add('semester', ChoiceType::class, [
                        'required' => false,
                        'label' => 'user.semester',
                        'choices' => SemesterPeriodEnum::getChoices(),
                    ]);
                    break;
                case 'year':
                    $years = $this->semesterRepo->getDistinctYears(
                        $this->matchingManagerHelper->getMatchingManagerOfManager()
                    );

                    $builder->add('year', ChoiceType::class, [
                        'required' => false,
                        'label' => 'semester.year',
                        'choices' => array_combine($years, $years),
                    ]);
                    break;
                case 'languages':
                    $builder->add('languages', ChoiceType::class, [
                        'multiple' => true,
                        'label' => 'user.languages',
                        'choices' => array_flip(Languages::getNames($this->localeManager->getCurrentLocale())),
                        'required' => false,
                        'attr' => [
                            'class' => 'select2selector',
                        ],
                    ]);
                    break;
                case 'local':
                    $builder->add('local', ChoiceType::class, [
                        'choices' => [
                            'mentor' => true,
                            'mentee' => false,
                        ],
                        'label' => 'user.list.is_local',
                        'required' => false,
                    ]);
                    break;
                case 'typeOfMobility':
                    $builder->add('typeOfMobility', ChoiceType::class, [
                        'label' => 'user.type_of_mobility',
                        'choices' => UserTypeMobilityEnum::getChoices(),
                        'required' => false,
                    ]);
                    break;
                case 'languagePairing':
                    $builder->add('languagePairing', ChoiceType::class, [
                        'required' => false,
                        'choices' => [
                            'no' => 0,
                            'yes' => 1,
                        ],
                        'label' => 'buddy.languagePairing',
                    ]);
                    break;
            }
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'type' => ExtractTypeEnum::TYPE_USERS,
        ]);
    }

    private function getFiltersByType(string $type): array
    {
        $filters = [];

        switch ($type) {
            case ExtractTypeEnum::TYPE_USERS:
                $filters = ['archived', 'semester', 'year', 'languages', 'local', 'typeOfMobility'];
                break;
            case ExtractTypeEnum::TYPE_BUDDIES:
                $filters = ['archived', 'semester', 'year', 'languagePairing'];
                break;
        }

        return $filters;
    }
}
