<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\City;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Motivation;
use App\Entity\Semester;
use App\Entity\User;
use App\Enum\SemesterPeriodEnum;
use App\Enum\UserLevelStudyEnum;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\Inflector\EnglishInflector;

class MatchingHelper
{
    public const CRITERIA_DISABLED = 0;
    public const CRITERIA_MANDATORY = 10;
    public const WARNING_AGE_DIFF = 5;
    private const DIFF_AGE = 3;

    private $propertyAccessor;

    public function __construct()
    {
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
    }

    public function matching(
        User $international,
        array $locals,
        string $orderDir = null,
        int $offset = 0,
        int $limit = 0
    ): array {
        /** @var User $local */
        foreach ($locals as $i => &$local) {
            $score = $this->getScore($international, $local);

            if ($score > 0) {
                $local->setScore($score);
            } else {
                unset($locals[$i]);
            }
        }

        $nbTotal = \count($locals);

        if ($orderDir) {
            uasort($locals, function ($local1, $local2) use ($orderDir) {
                if ('desc' === $orderDir) {
                    return $local1->getScore() > $local2->getScore() ? -1 : 1;
                }

                return $local1->getScore() < $local2->getScore() ? -1 : 1;
            });
        }

        if ($limit > 0) {
            $locals = \array_slice($locals, $offset, $limit);
        }

        return [$locals, $nbTotal];
    }

    public function compareBuddies(User $international, User $local): array
    {
        $languageMentee = $international->getLanguages();
        $languageMentor = $local->getLanguages();
        $languageWantedMentee = $international->getLanguagesWanted();
        $languageWantedMentor = $local->getLanguagesWanted();

        $instituteLocal = $local->getInstitute();
        $instituteInternational = $international->getInstitute();
        $allInstituteLocal = array_merge($instituteLocal->getParents(), [$instituteLocal]);
        $allInstituteInternational = array_merge($instituteInternational->getParents(), [$instituteInternational]);
        $maxLevel = max(\count($allInstituteInternational), \count($allInstituteLocal));

        $institutesEqual = [];
        for ($i = 0; $i < $maxLevel; ++$i) {
            $institutesEqual[] = $this->isInstituteEqual(
                $allInstituteInternational[$i] ?? null,
                $allInstituteLocal[$i] ?? null
            );
        }

        return [
            'ageEqual' => $this->isAgeEqual($international->getAge(), $local->getAge()),
            'dateArrivalOk' => $this->dateArrivalOk($international->getArrival(), $local->getArrival()),
            'availabilityScore' => $this->availabilityScore($international->getSemester(), $international->getArrival(), $local->getSemester(), $local->getArrival()),
            'languageInternationalWanted' => $this->isArrayIntersect($languageWantedMentee, $languageMentor),
            'languageLocalWanted' => $this->isArrayIntersect($languageWantedMentor, $languageMentee),
            'languageScore' => $this->languageScore($languageMentee, $languageMentor, $languageWantedMentee, $languageWantedMentor),
            'hobbiesScore' => $this->hobbyScore($international->getHobbies()->getValues(), $local->getHobbies()->getValues()),
            'motivationEqual' => $this->isMotivationEqual($international->getMotivation(), $local->getMotivation()),
            'semesterEqual' => $this->isSemesterEqual($international->getSemester(), $local->getSemester()),
            'cityEqual' => $this->isCityEqual($international->getCity(), $local->getCity()),
            'studiesEqual' => $this->studyScore($international->getStudies()->getValues(), $local->getStudies()->getValues()),
            'levelOfStudyEqual' => $this->levelOfStudyScore($international->getLevelOfStudy(), $local->getLevelOfStudy()),
            'score' => $this->getScore($international, $local),
            'instituteLevelToShow' => $maxLevel - 1,
            'institutesEqual' => $institutesEqual,
            'institutesInternational' => $allInstituteInternational,
            'institutesLocal' => $allInstituteLocal,
        ];
    }

    public function getScore(User $international, User $local): int
    {
        $result = 0;

        $sexEquality = $this->isSexEqual($international->getSex(), $international->getSexWanted(), $local->getSex(), $local->getSexWanted());
        $semesterEqual = $this->isSemesterEqual($international->getSemester(), $local->getSemester());

        if ($semesterEqual && $sexEquality) {
            $score = 0;

            $instituteLocal = $local->getInstitute();
            $instituteInternational = $international->getInstitute();
            $allInstituteLocal = array_merge($instituteLocal->getParents(), [$instituteLocal]);
            $allInstituteInternational = array_merge($instituteInternational->getParents(), [$instituteInternational]);
            $maxLevel = max(\count($allInstituteInternational), \count($allInstituteLocal));

            // loop through levels of institutes from lvl 1 to the deepest level of both students institutes
            for ($i = 1; $i <= $maxLevel; ++$i) {
                $instituteInternationalCriteriaValue = $instituteInternational->getMatchingInstituteValue($i);
                if (self::CRITERIA_DISABLED !== $instituteInternationalCriteriaValue) {
                    $isEqual = $this->isInstituteEqual(
                        $allInstituteInternational[$i - 1] ?? null,
                        $allInstituteLocal[$i - 1] ?? null
                    );

                    $instituteLocalCriteriaValue = $instituteLocal->getMatchingInstituteValue($i);

                    if (!$isEqual
                        && (self::CRITERIA_MANDATORY === $instituteInternationalCriteriaValue || self::CRITERIA_MANDATORY === $instituteLocalCriteriaValue)
                    ) {
                        return $result;
                    }

                    if ($isEqual) {
                        $score += $instituteInternationalCriteriaValue;
                    }
                }
            }

            foreach ($this->getCriteriaNames() as $criteriaName) {
                $criteriaValueInternational = $this->propertyAccessor->getValue($instituteInternational, 'getMatching'.$criteriaName.'Value');
                $criteriaValueLocal = $this->propertyAccessor->getValue($instituteLocal, 'getMatching'.$criteriaName.'Value');

                if (self::CRITERIA_DISABLED !== $criteriaValueInternational) {
                    $methodEqual = 'is'.$criteriaName.'Equal';
                    if (method_exists($this, $methodEqual)) {
                        $isEqual = \call_user_func_array([$this, $methodEqual], [
                            $this->getValueOfProperty($international, $criteriaName),
                            $this->getValueOfProperty($local, $criteriaName),
                        ]);

                        if (!$isEqual
                            && (self::CRITERIA_MANDATORY === $criteriaValueInternational || self::CRITERIA_MANDATORY === $criteriaValueLocal)
                        ) {
                            return $result;
                        }

                        if ($isEqual) {
                            $score += $criteriaValueInternational;
                        }
                    } else {
                        $methodScore = strtolower($criteriaName).'Score';
                        if (method_exists($this, $methodScore)) {
                            switch ($criteriaName) {
                                case 'Language':
                                    $params = [
                                        $international->getLanguages(),
                                        $local->getLanguages(),
                                        $international->getLanguagesWanted(),
                                        $local->getLanguagesWanted(),
                                    ];
                                    break;
                                case 'Availability':
                                    $params = [
                                        $international->getSemester(),
                                        $international->getArrival(),
                                        $local->getSemester(),
                                        $local->getArrival(),
                                    ];
                                    break;
                                default:
                                    $params = [
                                        $this->getValueOfProperty($international, $criteriaName),
                                        $this->getValueOfProperty($local, $criteriaName),
                                    ];
                                    break;
                            }

                            $criteriaScore = \call_user_func_array([$this, $methodScore], $params);

                            if (!$criteriaScore
                                && (self::CRITERIA_MANDATORY === $criteriaValueInternational || self::CRITERIA_MANDATORY === $criteriaValueLocal)
                            ) {
                                return $result;
                            }

                            $score += $criteriaValueInternational * $criteriaScore;
                        }
                    }
                }
            }

            if ($score > 0) {
                $result = (int) round($score / $this->scoreMax($instituteInternational) * 100);
            }
        }

        return $result;
    }

    public function isSexEqual(
        ?string $sexMentee,
        ?bool $sexMenteeWanted,
        ?string $sexMentor,
        ?bool $sexMentorWanted
    ): bool {
        if ($sexMenteeWanted || $sexMentorWanted) {
            if (!empty($sexMentee) && !empty($sexMentor) && $sexMentee !== $sexMentor) {
                return false;
            }
        }

        return true;
    }

    public function isSemesterEqual(Semester $semesterMentee, Semester $semesterMentor): bool
    {
        return $semesterMentee === $semesterMentor
            || (
                $semesterMentor->getYear() == $semesterMentee->getYear()
                && (SemesterPeriodEnum::PERIOD_ALL_YEAR === $semesterMentor->getPeriod()
                    || SemesterPeriodEnum::PERIOD_ALL_YEAR === $semesterMentee->getPeriod())
            );
    }

    public function getAgeDiff(?int $ageMentee, ?int $ageMentor): ?int
    {
        if ($ageMentee && $ageMentor) {
            if ($ageMentor === $ageMentee) {
                return 0;
            }

            return $ageMentee > $ageMentor ? $ageMentee - $ageMentor : $ageMentor - $ageMentee;
        }

        return null;
    }

    private function scoreMax(Institute $institute): int
    {
        $scoreMax = 0;

        foreach ($this->getCriteriaNames() as $criteriaName) {
            $scoreMax += $this->propertyAccessor->getValue($institute, 'getMatching'.$criteriaName.'Value');
        }

        foreach ($institute->getMatchingInstituteValues() as $instituteCriteriaValue) {
            $scoreMax += $instituteCriteriaValue;
        }

        return $scoreMax;
    }

    private function languageScore(
        array $languageMentee,
        array $languageMentor,
        array $languageWantedMentee,
        array $languageWantedMentor
    ): float {
        return $this->arrayCriteriaScore($languageMentee, $languageMentor, $languageWantedMentee, $languageWantedMentor);
    }

    private function isArrayIntersect(array $array1, array $array2): bool
    {
        return \count(array_intersect($array1, $array2)) > 0;
    }

    private function isInstituteEqual(?Institute $instituteMentee, ?Institute $instituteMentor): bool
    {
        if ($instituteMentee && $instituteMentee->getDuplicate()) {
            $instituteMentee = $instituteMentee->getDuplicate();
        }

        if ($instituteMentor && $instituteMentor->getDuplicate()) {
            $instituteMentor = $instituteMentor->getDuplicate();
        }

        return $instituteMentee === $instituteMentor;
    }

    private function isCityEqual(?City $cityMentee, ?City $cityMentor): bool
    {
        return $cityMentee === $cityMentor;
    }

    private function hobbyScore(array $hobbiesMentee, array $hobbiesMentor): float
    {
        return $this->arrayCriteriaScore($hobbiesMentee, $hobbiesMentor);
    }

    private function studyScore(array $studiesMentee, array $studiesMentor): float
    {
        return $this->arrayCriteriaScore($studiesMentee, $studiesMentor);
    }

    private function levelOfStudyScore(?string $levelMentee, ?string $levelMentor): float
    {
        $score = 0;
        if (!empty($levelMentee) && !empty($levelMentor) && $levelMentor === $levelMentee) {
            if (UserLevelStudyEnum::LEVEL_OTHER === $levelMentee) {
                $score = 0.5;
            } else {
                $score = 1;
            }
        }

        return (float) $score;
    }

    private function isAgeEqual(?int $ageMentee, ?int $ageMentor): bool
    {
        $diffAge = $this->getAgeDiff($ageMentee, $ageMentor);

        return \is_int($diffAge) && $diffAge <= self::DIFF_AGE;
    }

    private function isMotivationEqual(?Motivation $motivationMentee, ?Motivation $motivationMentor): bool
    {
        return null !== $motivationMentee && $motivationMentee === $motivationMentor;
    }

    private function arrayCriteriaScore(
        array $valuesMentee,
        array $valuesMentor,
        array $valuesMenteeWanted = null,
        array $valuesMentorWanted = null
    ): float {
        $score = 0;

        if (!\is_array($valuesMenteeWanted) && !\is_array($valuesMentorWanted)) {
            $valuesMenteeWanted = $valuesMentee;
            $valuesMentorWanted = $valuesMentor;
        }

        if (\count($valuesMentee) > 0 && \count($valuesMentor) > 0) {
            $valuesMenteeFound = \count(array_intersect($valuesMentor, $valuesMenteeWanted));
            $valuesMentorFound = \count(array_intersect($valuesMentee, $valuesMentorWanted));

            if ($valuesMenteeFound > 0 || $valuesMentorFound > 0) {
                $score = 0.5;

                if (\count($valuesMenteeWanted) > 0 && \count($valuesMentorWanted) > 0) {
                    if ($valuesMenteeFound === \count($valuesMenteeWanted) || $valuesMentorFound === \count($valuesMentorWanted)) {
                        $score = 0.75;

                        if ($valuesMenteeFound === \count($valuesMenteeWanted) && $valuesMentorFound === \count($valuesMentorWanted)) {
                            $score = 1;
                        }
                    }
                }
            }
        }

        return (float) $score;
    }

    private function availabilityScore(
        Semester $semesterMentee,
        ?\DateTime $arrivalMentee,
        Semester $semesterMentor,
        ?\DateTime $arrivalMentor
    ): float {
        $score = 0.5;

        if ($this->dateArrivalOk($arrivalMentee, $arrivalMentor)) {
            if ($semesterMentee === $semesterMentor) {
                $score = 1;
            } else {
                $score = 0.75;
            }
        } elseif ($semesterMentee === $semesterMentor) {
            $score = 0.75;
        }

        return (float) $score;
    }

    private function dateArrivalOk(?\DateTime $arrivalMentee, ?\DateTime $arrivalMentor): bool
    {
        return null !== $arrivalMentee && null !== $arrivalMentor && $arrivalMentor <= $arrivalMentee;
    }

    private function getCriteriaNames(): array
    {
        $criteriaNames = [];

        $methods = preg_grep('/^getMatching.*Value$/', get_class_methods(MatchingManagerInterface::class));
        foreach ($methods as $method) {
            $matches = [];
            if (preg_match('/^getMatching(.*)Value$/', $method, $matches)) {
                $criteriaNames[] = $matches[1];
            }
        }

        return $criteriaNames;
    }

    private function getValueOfProperty(User $user, string $property)
    {
        $value = null;
        $getter = 'get'.$property;

        if ($this->propertyAccessor->isReadable($user, $getter)) {
            $value = $this->propertyAccessor->getValue($user, $getter);
        } else {
            $inflector = new EnglishInflector();
            $pluralizedName = $inflector->pluralize($property);
            $getter = 'get'.$pluralizedName[0];

            if ($this->propertyAccessor->isReadable($user, $getter)) {
                $value = $this->propertyAccessor->getValue($user, $getter);
            }
        }

        if ($value instanceof Collection) {
            $value = $value->getValues();
        }

        return $value;
    }
}
