<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\User;
use App\Enum\UserRoleEnum;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;

class GrantedService
{
    private $accessDecisionManager;

    public function __construct(AccessDecisionManagerInterface $accessDecisionManager)
    {
        $this->accessDecisionManager = $accessDecisionManager;
    }

    public function isGranted(User $user, string $role, $object = null): bool
    {
        $token = new UsernamePasswordToken($user, 'none', 'none', $user->getRoles());

        return $this->accessDecisionManager->decide($token, [$role], $object);
    }

    public function getRole(User $user): string
    {
        if ($this->isGranted($user, UserRoleEnum::ROLE_SUPER_ADMIN)) {
            $role = UserRoleEnum::ROLE_SUPER_ADMIN;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_ADMIN)) {
            $role = UserRoleEnum::ROLE_ADMIN;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_BUDDYCOORDINATOR)) {
            $role = UserRoleEnum::ROLE_BUDDYCOORDINATOR;
        } elseif ($this->isGranted($user, UserRoleEnum::ROLE_INSTITUTE_MANAGER)) {
            $role = UserRoleEnum::ROLE_INSTITUTE_MANAGER;
        } else {
            $role = UserRoleEnum::ROLE_USER;
        }

        return $role;
    }
}
