<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Repository\HobbyRepository;
use App\Repository\MotivationRepository;

class MatchingManagerInitiator
{
    protected $semesterHelper;
    protected $hobbyRepo;
    protected $motivationRepo;
    protected $messagingHelper;

    public function __construct(
        SemesterHelper $semesterHelper,
        HobbyRepository $hobbyRepo,
        MotivationRepository $motivationRepo,
        MessagingHelper $messagingHelper
    ) {
        $this->semesterHelper = $semesterHelper;
        $this->hobbyRepo = $hobbyRepo;
        $this->motivationRepo = $motivationRepo;
        $this->messagingHelper = $messagingHelper;
    }

    public function initMatchingManager(MatchingManagerInterface $matchingManager): void
    {
        foreach ($this->hobbyRepo->findAll() as $hobby) {
            $matchingManager->addHobby($hobby);
        }

        foreach ($this->motivationRepo->findAll() as $motivation) {
            $matchingManager->addMotivation($motivation);
        }

        $this->messagingHelper->createMatchingManagerThreads($matchingManager);

        if ($matchingManager instanceof Institute) {
            $this->setSemester($matchingManager);
        }
    }

    public function setSemester(Institute $institute, bool $forcePersist = false): void
    {
        if (!$institute->getSemester() || $forcePersist) {
            $institute->setSemester($this->semesterHelper->getSemester($institute));
        }
    }
}
