<?php

declare(strict_types=1);

namespace App\Services;

use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Symfony\Component\HttpFoundation\Cookie;

class JWTFactory
{
    private string $jwtKey;
    private string $host;

    public function __construct(string $jwtKey, string $baseHost)
    {
        $this->jwtKey = $jwtKey;
        $this->host = $baseHost;
    }

    public function getCookie(array $topics): Cookie
    {
        return Cookie::create(
            'mercureAuthorization',
            $this->getToken($topics),
            0,
            '/.well-known/mercure',
            $this->host,
            true,
            true,
            false,
            Cookie::SAMESITE_LAX
        );
    }

    private function getToken(array $topics): string
    {
        $config = Configuration::forSymmetricSigner(new Sha256(), InMemory::plainText($this->jwtKey));

        return $config->builder()
            ->withClaim('mercure', ['subscribe' => $topics])
            ->getToken($config->signer(), $config->signingKey())
            ->toString()
        ;
    }
}
