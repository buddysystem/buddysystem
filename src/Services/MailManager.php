<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Buddy;
use App\Entity\Extract;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Report;
use App\Entity\User;
use App\Enum\SemesterPeriodEnum;
use App\Repository\UserRepository;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\LoginLink\LoginLinkDetails;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\ResetPassword\Model\ResetPasswordToken;

class MailManager
{
    private $from;
    private $mailer;
    private $translator;
    private $logger;
    private $userRepo;
    private $router;
    private $fromName;
    private $host;

    public function __construct(
        MailerInterface $mailer,
        TranslatorInterface $translator,
        LoggerHelper $logger,
        UrlGeneratorInterface $router,
        UserRepository $userRepo,
        string $mailerFrom,
        string $mailerFromName,
        string $host
    ) {
        $this->from = $mailerFrom;
        $this->fromName = $mailerFromName;
        $this->mailer = $mailer;
        $this->translator = $translator;
        $this->logger = $logger;
        $this->userRepo = $userRepo;
        $this->router = $router;
        $this->host = $host;
    }

    public function sendWarningToLocal(User $user, string $type, MatchingManagerInterface $matchingManager = null): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.matching.mentor.subject.'.$type),
            'Emails/cron_warning_mentor.html.twig',
            [
                'user' => $user,
                'manager' => $matchingManager ? $matchingManager->getName() : '',
                'type' => $type,
                'path' => $this->router->generate(
                    'messaging_homepage',
                    ['_locale' => $user->getLocaleLanguage()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ]
        );
    }

    public function askNextSemester(User $user, MatchingManagerInterface $matchingManager): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.ask_next_semester.subject'),
            'Emails/ask_next_semester.html.twig',
            [
                'user' => $user->getFirstname(),
                'manager' => $matchingManager,
                'locale' => $user->getLocaleLanguage(),
                'path' => $this->router->generate('dashboard', ['_locale' => $user->getLocaleLanguage()], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    public function reportBuddy(Report $report, MatchingManagerInterface $matchingManager): void
    {
        $buddy = $report->getBuddy();
        $user = $report->getUser();
        $userType = $user->isLocal() ? 'mentor' : 'mentee';
        $managers = $this->userRepo->getGrantedManager($matchingManager);

        foreach ($managers as $manager) {
            $this->sendEmail(
                $manager->getEmail(),
                $this->translator->trans('mail.report_'.$userType.'.subject'),
                'Emails/report_buddy.html.twig',
                [
                    'mentor' => $buddy->getMentor(),
                    'mentee' => $buddy->getMentee(),
                    'comment' => $report->getComment(),
                    'locale' => $manager->getLocaleLanguage(),
                    'userType' => $userType,
                ]
            );
        }
    }

    public function sendNotification(User $user, string $type, MatchingManagerInterface $matchingManager = null): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.notification.subject.'.$type),
            'Emails/cron_notification.html.twig',
            [
                'user' => $user,
                'manager' => $matchingManager ? $matchingManager->getName() : '',
                'type' => $type,
                'path' => $this->router->generate(
                    'messaging_homepage',
                    ['_locale' => $user->getLocaleLanguage()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
            ]
        );
    }

    public function sendAnonymisationEmail(User $user): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.anonymisation.subject', [], 'messages', $user->getLocaleLanguage()),
            'Emails/user_anonymisation.html.twig',
            [
                'user' => $user,
            ]
        );
    }

    public function sendUserApprovedEmail(User $user): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.approved.subject'),
            'Emails/user_approved.html.twig',
            [
                'user' => $user,
            ]
        );
    }

    public function notifyNewBuddyToValidate(Buddy $buddy, User $userToNotify): void
    {
        $mentor = $buddy->getMentor();
        $mentee = $buddy->getMentee();

        $this->sendEmail(
            $userToNotify->getEmail(),
            $this->translator->trans('mail.matching.proposal.subject'),
            'Emails/matching_proposal.html.twig',
            [
                'userToNotify' => $userToNotify,
                'buddy' => $userToNotify === $mentor ? $mentee : $mentor,
                'matchingManager' => $buddy->getMatchingManager(),
                'baseUrl' => $this->router->generate('dashboard', ['_locale' => $userToNotify->getLocaleLanguage()], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    public function notifyNewBuddy(Buddy $buddy, User $userToNotify): void
    {
        $mentor = $buddy->getMentor();
        $mentee = $buddy->getMentee();

        $this->sendEmail(
            $userToNotify->getEmail(),
            $this->translator->trans('mail.matching.new_buddy.subject'),
            'Emails/matching_new_buddy.html.twig',
            [
                'userToNotify' => $userToNotify,
                'buddy' => $userToNotify === $mentor ? $mentee : $mentor,
                'matchingManager' => $buddy->getMatchingManager(),
                'baseUrl' => $this->router->generate('dashboard', ['_locale' => $userToNotify->getLocaleLanguage()], UrlGeneratorInterface::ABSOLUTE_URL),
            ]
        );
    }

    public function notifyManagersSwitchSemester(Institute $institute): void
    {
        $semester = $institute->getSemester();
        $managers = $this->userRepo->getGrantedManager($institute);
        foreach ($managers as $manager) {
            $this->sendEmail(
                $manager->getEmail(),
                $this->translator->trans('mail.notify_switch_semester.subject'),
                'Emails/switch_semester.html.twig',
                [
                    'managerName' => $institute->getName(),
                    'semesterPeriod' => $this->translator->trans(SemesterPeriodEnum::getReadableValue($semester->getPeriod())),
                    'semesterYear' => $semester->getYear(),
                    'locale' => $manager->getLocaleLanguage(),
                ]
            );
        }
    }

    public function sendRightsChangedNotification(array $mailBodies, array $mailRecipients): void
    {
        foreach ($mailRecipients as $mailRecipient) {
            $this->sendEmail(
                $mailRecipient['mail'],
                $this->translator->trans('mail.notification.subject.rights_changed'),
                'Emails/rights_changed.html.twig',
                [
                    'changed' => implode('<br>', array_intersect_key($mailBodies, $mailRecipient['mailBodies'])),
                    'locale' => $mailRecipient['locale'],
                ]
            );
        }
    }

    public function sendPasswordResetEmail(string $emailRecipient, ResetPasswordToken $resetToken): void
    {
        $this->sendEmail(
            $emailRecipient,
            $this->translator->trans('user.resetting.title'),
            'Emails/resetting.html.twig',
            [
                'resetToken' => $resetToken,
            ]
        );
    }

    public function sendLoginLinkEmail(string $emailRecipient, LoginLinkDetails $loginLinkDetails): void
    {
        $this->sendEmail(
            $emailRecipient,
            $this->translator->trans('user.login_link.title'),
            'Emails/login_link.html.twig',
            [
                'loginLinkDetails' => $loginLinkDetails,
            ]
        );
    }

    public function accountDeletedByManager(User $user, User $manager): void
    {
        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.account_deleted.subject'),
            'Emails/account_deleted.html.twig',
            [
                'manager' => $manager,
            ]
        );
    }

    public function sendExtractLink(Extract $extract): void
    {
        $user = $extract->getUser();

        $this->sendEmail(
            $user->getEmail(),
            $this->translator->trans('mail.extract.subject'),
            'Emails/extract.html.twig',
            [
                'filepath' => $this->router->generate('extract_download', ['filename' => $extract->getFilename()], UrlGeneratorInterface::ABSOLUTE_URL),
                'locale' => $user->getLocaleLanguage(),
            ]
        );
    }

    public function getEmailConfirmation(string $emailRecipient): TemplatedEmail
    {
        $this->logger->logEmail("FROM:{$this->from}, TO:{$emailRecipient}, SUBJECT:registration");

        return $this->getDefaultEmail()
            ->to($emailRecipient)
            ->subject($this->translator->trans('registration.email.subject'))
            ->htmlTemplate('Emails/registration.html.twig')
        ;
    }

    public function sendEmail(string $to, string $subject, string $template, array $params = null): void
    {
        if (!\array_key_exists('baseUrl', $params)) {
            $params['baseUrl'] = $this->router->generate('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $this->logger->logEmail("FROM:{$this->from}, TO:{$to}, SUBJECT:{$subject}");

        $this->mailer->send(
            $this->getDefaultEmail()
                ->to($to)
                ->subject($subject)
                ->htmlTemplate($template)
                ->context($params)
        );
    }

    private function getDefaultEmail(): TemplatedEmail
    {
        return (new TemplatedEmail())->from(new Address($this->from, $this->fromName));
    }
}
