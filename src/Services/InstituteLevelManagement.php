<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Entity\MatchingCriteriaTrait;
use App\Entity\MatchingManagerInterface;
use App\EventListener\UnitOfWorkTrait;
use App\Repository\InstituteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

class InstituteLevelManagement
{
    use UnitOfWorkTrait;

    private $em;
    private $instituteRepo;
    private $matchingManagerHelper;
    private $matchingManagerInitiator;
    private $propertyAccessor;
    private $propertyInfo;

    public function __construct(
        EntityManagerInterface $em,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper,
        MatchingManagerInitiator $matchingManagerInitiator
    ) {
        $this->em = $em;
        $this->instituteRepo = $instituteRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->matchingManagerInitiator = $matchingManagerInitiator;
        $this->propertyAccessor = PropertyAccess::createPropertyAccessor();
        $this->propertyInfo = new PropertyInfoExtractor([new ReflectionExtractor()]);
    }

    public function setLevels(Institute $institute): void
    {
        $root = $institute->getRoot() ?? $institute;
        $root->setLevel();

        /** @var Institute $copy */
        foreach ($institute->getCopies() as $copy) {
            $root = $copy->getRoot() ?? $copy;
            $root->setLevel();
        }
    }

    public function applyPreferencesOnChildren(
        MatchingManagerInterface $matchingManager,
        Institute $institute = null
    ): void {
        if ($institute) {
            $properties = $this->propertyInfo->getProperties(MatchingCriteriaTrait::class);

            foreach ($properties as $property) {
                if ($this->propertyAccessor->isReadable($matchingManager, $property)
                    && $this->propertyAccessor->isWritable($institute, $property)
                ) {
                    $newValue = $this->propertyAccessor->getValue($matchingManager, $property);

                    if ('fullYearOnly' === $property) {
                        $oldValue = $this->propertyAccessor->getValue($institute, $property);

                        if ($oldValue !== $newValue) {
                            $this->matchingManagerInitiator->setSemester($institute, true);
                        }
                    }

                    $this->propertyAccessor->setValue(
                        $institute,
                        $property,
                        $newValue
                    );
                }
            }
        } else {
            $institutes = $this->matchingManagerHelper->getUpperLevelsCriteriasManaged($matchingManager);

            foreach ($institutes as $institute) {
                $this->applyPreferencesOnChildren($matchingManager, $institute);
            }
        }
    }

    public function setLowerLevelManagement(Institute $institute, bool $value): void
    {
        if (!$institute->getParent()
            && $institute->isLowerLevelManagement() === !$value
            && (!$value || !$this->instituteRepo->hasManagers($institute))
        ) {
            $institute->setLowerLevelManagement($value);
        }
    }

    public function setShowBonusOptionOnChildren(Institute $institute, bool $isShowBonusOption): void
    {
        $institute->setShowBonusOption($isShowBonusOption);
        $this->addUpdateToUOW($this->em, $institute);

        /** @var Institute $child */
        foreach ($institute->getChildren() as $child) {
            $this->setShowBonusOptionOnChildren($child, $isShowBonusOption);
        }
    }
}
