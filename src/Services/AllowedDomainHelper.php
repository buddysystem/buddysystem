<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Entity\RegistrationEmailDomain;
use App\Enum\RegistrationEmailDomainUserTypeEnum;
use Symfony\Contracts\Translation\TranslatorInterface;

class AllowedDomainHelper
{
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function isEmailAllowed(string $email, Institute $institute, string $userType): bool
    {
        $email = trim(strtolower($email));
        $allowedDomains = $this->getAllowedDomains($institute, $userType);

        if (\count($allowedDomains) > 0) {
            foreach ($allowedDomains as $allowedDomain) {
                if (strpos($email, trim(strtolower($allowedDomain)))) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    public function getAllowedDomains(Institute $institute, string $userType = null, bool $onlyParents = false): array
    {
        if ($onlyParents) {
            $allowedDomains = [];
        } else {
            $allowedDomains = $this->getDomains($institute, $userType);
        }

        /** @var Institute $parent */
        foreach ($institute->getParents() as $parent) {
            $allowedDomains = array_merge($allowedDomains, $this->getDomains($parent, $userType, $onlyParents));
        }

        return array_unique(array_map(function ($domain) {
            if ('@' === substr($domain, 0, 1)) {
                return $domain;
            }

            return '@'.$domain;
        }, $allowedDomains));
    }

    private function getDomains(Institute $institute, string $userType = null, bool $concatUserType = false): array
    {
        $domains = [];

        /** @var RegistrationEmailDomain $registrationEmailDomain */
        foreach ($institute->getRegistrationEmailDomains() as $registrationEmailDomain) {
            $domainUserType = $registrationEmailDomain->getUserType();
            if (!$userType
                || RegistrationEmailDomainUserTypeEnum::BOTH === $userType
                || $domainUserType === $userType
                || RegistrationEmailDomainUserTypeEnum::BOTH === $domainUserType
            ) {
                $domain = $registrationEmailDomain->getDomain();
                if ($concatUserType) {
                    $domain .= ' ('.$this->translator->trans(RegistrationEmailDomainUserTypeEnum::getReadableValue($domainUserType)).')';
                }
                $domains[] = $domain;
            }
        }

        return $domains;
    }
}
