<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Message;
use App\Entity\ThreadBuddy;
use App\Entity\ThreadInterface;
use App\Entity\ThreadManagerInterface;
use App\Entity\ThreadUserManager;
use App\Entity\User;
use App\Enum\MessagingRecipientTypeEnum;
use App\Enum\ThreadChannelTypeEnum;
use App\Enum\ThreadVoterEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Messaging\DTO\Author;
use App\Messaging\DTO\ThreadGroup;
use App\Messaging\DTO\Topic;
use App\Messaging\DTO\TopicMatchingManager;
use App\Repository\ThreadRepository;
use Doctrine\ORM\EntityNotFoundException;
use Liip\ImagineBundle\Service\FilterService;
use Misd\Linkify\Linkify;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class MessagingProvider
{
    private const MATCHING_MANAGER_IRI = 'matching_managers';

    private $threadRepo;
    private $messagingHelper;
    private $matchingManagerHelper;
    private $security;
    private $translator;
    private $uploaderHelper;
    private $linkify;
    private $grantedService;
    private $assetsManager;
    private $router;
    private $imagine;
    private $filesystem;
    private $projectDir;

    public function __construct(
        ThreadRepository $threadRepo,
        MatchingManagerHelper $matchingManagerHelper,
        MessagingHelper $messagingHelper,
        Security $security,
        TranslatorInterface $translator,
        UploaderHelper $uploaderHelper,
        GrantedService $grantedService,
        Packages $assetsManager,
        UrlGeneratorInterface $router,
        FilterService $imagine,
        Filesystem $filesystem,
        string $projectDir
    ) {
        $this->threadRepo = $threadRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->messagingHelper = $messagingHelper;
        $this->security = $security;
        $this->translator = $translator;
        $this->uploaderHelper = $uploaderHelper;
        $this->grantedService = $grantedService;
        $this->assetsManager = $assetsManager;
        $this->router = $router;
        $this->imagine = $imagine;
        $this->filesystem = $filesystem;
        $this->projectDir = $projectDir;
        $this->linkify = new Linkify();
    }

    public function getAllowedThreads(User $user): array
    {
        $threads = [];

        if ($threadManagers = $this->getThreadGroupMatchingManagersData($user)) {
            $threads[] = $threadManagers;
        }

        if ($threadInstituteManagers = $this->getThreadGroupInstituteManagersData($user)) {
            $threads[] = $threadInstituteManagers;
        }

        if ($threadAssociationManagers = $this->getThreadGroupAssociationManagersData($user)) {
            $threads[] = $threadAssociationManagers;
        }

        foreach ($this->messagingHelper->getSubscribedThread($user, ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL) as $thread) {
            try {
                $threads[] = $this->getThreadGroupData($thread, $user);
            } catch (EntityNotFoundException $e) {
                // the linked buddy entity has been soft deleted, ignore it
                continue;
            }
        }

        foreach ($this->messagingHelper->getSubscribedThread($user, ThreadChannelTypeEnum::GROUP_CHANNEL) as $thread) {
            $threads[] = $this->getThreadGroupData($thread, $user);
        }

        usort($threads, function ($a, $b) {
            if ($a->getLastMessageAt() === $b->getLastMessageAt()) {
                return 0;
            }

            if ($a->isFavorite() && !$b->isFavorite()) {
                return -1;
            }

            if ($b->isFavorite() && !$a->isFavorite()) {
                return 1;
            }

            return ($a->getLastMessageAt() < $b->getLastMessageAt()) ? 1 : -1;
        });

        return $threads;
    }

    public function getMessageDTO(Message $message): \App\Messaging\DTO\Message
    {
        $sender = $message->getSender();
        $thread = $message->getThread();

        if ($thread instanceof ThreadManagerInterface
            && MessagingRecipientTypeEnum::ONLY_MANAGERS !== $thread->getRecipientType()
        ) {
            $threadMatchingManager = $thread->getMatchingManager();
            $threadGroupId = self::MATCHING_MANAGER_IRI;
            $recipientType = $thread->getRecipientType();
            $recipientName = $threadMatchingManager->getName();
            $recipientNameTooltip = '';

            if ($threadMatchingManager instanceof Institute) {
                foreach ($threadMatchingManager->getParents() as $matchingManager) {
                    $recipientNameTooltip .= $matchingManager->getName()."\n";
                }
            }
        } else {
            $threadGroupId = (string) $thread->getId();
            $recipientType = '';
            $recipientName = '';
            $recipientNameTooltip = '';
        }

        $author = new Author(
            (string) $sender->getId(),
            $sender->getFirstname() ?? $sender->getEmail(),
            $sender->getLastname() ?? '',
            $this->getAvatar($sender)
        );

        return new \App\Messaging\DTO\Message(
            (string) $message->getId(),
            $threadGroupId,
            $author,
            $message->getCreatedAt(),
            $this->linkify->process($message->getBody(), ['attr' => ['target' => '_blank']]),
            $recipientType,
            $recipientName,
            $recipientNameTooltip
        );
    }

    public function getThreadIRI(ThreadInterface $thread): string
    {
        return '/threads/'.$thread->getId();
    }

    public function getUserIRI(User $user): string
    {
        return '/users/'.$user->getId();
    }

    public function getAllTopicsIRI(User $user): array
    {
        $topics = [$this->getUserIRI($user)];

        foreach ($this->messagingHelper->getSubscribedThread($user) as $thread) {
            $topics[] = $this->getThreadIRI($thread);
        }

        return $topics;
    }

    public function getThreadsByThreadGroupId(string $id, User $user): array
    {
        if (self::MATCHING_MANAGER_IRI === $id) {
            return $this->messagingHelper->getThreadMatchingManagers($user);
        }

        return $this->threadRepo->findBy(['id' => $id]);
    }

    public function getThreadGroupData(ThreadInterface $thread, User $user): ThreadGroup
    {
        $icon = '';
        $avatar = '';
        $canReply = true;
        $channelTypeTooltip = '';
        $profileUrl = null;

        $isManager = \in_array($this->grantedService->getRole($user), UserRoleEnum::$managers);

        if ($thread instanceof ThreadBuddy) {
            $buddy = $thread->getBuddy();
            if ($user === $buddy->getMentee()) {
                $otherParticipant = $buddy->getMentor();
            } else {
                $otherParticipant = $buddy->getMentee();
            }

            $avatar = $this->getAvatar($otherParticipant);
            $name = $otherParticipant->getFullName();
            $channelType = ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL;
        } elseif ($thread instanceof ThreadUserManager) {
            if ($isManager) {
                $otherParticipant = $thread->getUser();
                $name = $otherParticipant->getFullName();
                $avatar = $this->getAvatar($otherParticipant);
                $profileUrl = $this->security->isGranted(UserVoterEnum::SHOW, $otherParticipant)
                    ? $this->router->generate('user_edit', ['id' => $otherParticipant->getId()])
                    : null
                ;
            } else {
                $icon = 'life-ring';
                $name = MatchingManagerInterface::TYPE_INSTITUTE === $thread->getMatchingManagerType()
                    ? $this->translator->trans('inbox.channel.institute_managers')
                    : $this->translator->trans('inbox.channel.association_managers')
                ;
            }

            $canReply = $this->security->isGranted(ThreadVoterEnum::UNCHANGED_RECIPIENTS, $thread);
            $channelType = ThreadChannelTypeEnum::GROUP_CHANNEL;

            if ($matchingManagersThread = $thread->getMatchingManagers()) {
                foreach ($matchingManagersThread as $matchingManager) {
                    $channelTypeTooltip .= $matchingManager->getName()."\n";
                }
            }
        } else {
            $otherParticipant = $thread->getOtherUser($user);

            $avatar = $this->getAvatar($otherParticipant);
            $name = $otherParticipant->getFullName();
            $channelType = ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL;
            $profileUrl = $this->security->isGranted(UserVoterEnum::SHOW, $otherParticipant)
                ? $this->router->generate('user_edit', ['id' => $otherParticipant->getId()])
                : null
            ;
        }

        return new ThreadGroup(
            (string) $thread->getId(),
            $name,
            $channelType,
            $channelTypeTooltip,
            $icon,
            $avatar,
            $thread->getLastMessageAt(),
            [new Topic($this->getThreadIRI($thread), $thread->getId())],
            $this->messagingHelper->isRead($thread, $user),
            $this->messagingHelper->isFavorite((string) $thread->getId(), $user),
            $canReply,
            $profileUrl
        );
    }

    private function getThreadGroupAssociationManagersData(User $user): ?ThreadGroup
    {
        if ($thread = $this->messagingHelper->getThreadAssociationManagers($user)) {
            return new ThreadGroup(
                (string) $thread->getId(),
                $this->translator->trans('inbox.channel.buddycoordinators'),
                ThreadChannelTypeEnum::GLOBAL_CHANNEL,
                '',
                'brain',
                '',
                $thread->getLastMessageAt(),
                [new Topic($this->getThreadIRI($thread), $thread->getId())],
                $this->messagingHelper->isRead($thread, $user),
                $this->messagingHelper->isFavorite((string) $thread->getId(), $user)
            );
        }

        return null;
    }

    private function getThreadGroupInstituteManagersData(User $user): ?ThreadGroup
    {
        if ($thread = $this->messagingHelper->getThreadInstituteManagers($user)) {
            return new ThreadGroup(
                (string) $thread->getId(),
                $this->translator->trans('inbox.channel.institute.managers'),
                ThreadChannelTypeEnum::GLOBAL_CHANNEL,
                '',
                'university',
                '',
                $thread->getLastMessageAt(),
                [new Topic($this->getThreadIRI($thread), $thread->getId())],
                $this->messagingHelper->isRead($thread, $user),
                $this->messagingHelper->isFavorite((string) $thread->getId(), $user)
            );
        }

        return null;
    }

    private function getThreadGroupMatchingManagersData(User $user): ?ThreadGroup
    {
        $topics = [];
        $lastMessageDate = null;
        $isRead = true;

        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($user);

        foreach ($this->messagingHelper->getThreadMatchingManagers($user) as $thread) {
            $lastMessageDate = max($lastMessageDate, $thread->getLastMessageAt());

            if ($isRead) {
                $isRead = $this->messagingHelper->isRead($thread, $user);
            }

            $threadMatchingManager = $thread->getMatchingManager();
            $recipientMatchingManagerType = $thread->getMatchingManagerType();

            $topicMatchingManager = null;
            if ($matchingManager instanceof Association
                || (
                    $matchingManager instanceof Institute
                    && MatchingManagerInterface::TYPE_INSTITUTE === $recipientMatchingManagerType
                    && (
                        $threadMatchingManager === $matchingManager
                        || $threadMatchingManager->getAncestors()->contains($matchingManager)
                    )
                )
            ) {
                $label = $threadMatchingManager->getName();
                if ($threadMatchingManager instanceof Institute && $threadMatchingManager !== $matchingManager) {
                    $parent = $threadMatchingManager->getParent();

                    while ($parent && $parent !== $matchingManager) {
                        $label = $parent->getName().' > '.$label;
                        $parent = $parent->getParent();
                    }
                }

                $topicMatchingManager = new TopicMatchingManager(
                    (string) $threadMatchingManager->getId(),
                    $recipientMatchingManagerType,
                    $thread->getRecipientType(),
                    $label
                );
            }

            $topics[] = new Topic($this->getThreadIRI($thread), $thread->getId(), $topicMatchingManager);
        }

        if (empty($topics)) {
            return null;
        }

        $isManager = \in_array($this->grantedService->getRole($user), UserRoleEnum::$managers);

        return new ThreadGroup(
            self::MATCHING_MANAGER_IRI,
            $isManager
                ? $this->translator->trans('inbox.channel.students')
                : $this->translator->trans('inbox.channel.matching_managers'),
            ThreadChannelTypeEnum::GLOBAL_CHANNEL,
            '',
            $isManager
                ? 'users'
                : 'university',
            '',
            $lastMessageDate,
            $topics,
            $isRead,
            $this->messagingHelper->isFavorite(self::MATCHING_MANAGER_IRI, $user),
            $isManager
        );
    }

    private function getAvatar(User $user): string
    {
        $url = $this->uploaderHelper->asset($user, 'pictureFile');

        if (empty($url) || !$this->filesystem->exists($this->projectDir.'/public'.$url)) {
            return $this->assetsManager->getUrl('images/pictos/no_photo.png');
        }

        return $this->imagine->getUrlOfFilteredImage($url, 'square50');
    }
}
