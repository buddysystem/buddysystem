<?php

declare(strict_types=1);

namespace App\Services;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LoggerHelper
{
    private $registrationLogger;
    private $debugLogger;
    private $emailLogger;
    private $switchSemesterLogger;

    public function __construct(string $logsDir)
    {
        $this->registrationLogger = new Logger('registration');
        $this->registrationLogger->pushHandler(new StreamHandler($logsDir.'/registration.log'));

        $this->emailLogger = new Logger('email');
        $this->emailLogger->pushHandler(new StreamHandler($logsDir.'/email.log'));

        $this->debugLogger = new Logger('debug');
        $this->debugLogger->pushHandler(new StreamHandler($logsDir.'/debug.log'));

        $this->switchSemesterLogger = new Logger('switchSemester');
        $this->switchSemesterLogger->pushHandler(new StreamHandler($logsDir.'/switch_semester.log'));
    }

    public function logRegistration(string $message): void
    {
        $this->registrationLogger->info($message);
    }

    public function logEmail(string $message): void
    {
        $this->emailLogger->info($message);
    }

    public function logDebug(string $message): void
    {
        $this->debugLogger->info($message);
    }

    public function logSwitchSemester(string $message): void
    {
        $this->switchSemesterLogger->info($message);
    }
}
