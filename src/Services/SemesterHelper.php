<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Entity\Semester;
use App\Entity\User;
use App\Enum\SemesterPeriodEnum;
use App\Enum\StatTypeEnum;
use App\Repository\SemesterRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SemesterHelper
{
    public const PREOPENING = '1 month';

    private $em;
    private $statsService;
    private $userRepo;
    private $semesterRepo;
    private $localeManager;
    private $translator;
    private $mailManager;

    public function __construct(
        EntityManagerInterface $em,
        StatsService $statsService,
        UserRepository $userRepo,
        SemesterRepository $semesterRepo,
        LocaleManager $localeManager,
        TranslatorInterface $translator,
        MailManager $mailManager
    ) {
        $this->em = $em;
        $this->statsService = $statsService;
        $this->userRepo = $userRepo;
        $this->semesterRepo = $semesterRepo;
        $this->localeManager = $localeManager;
        $this->translator = $translator;
        $this->mailManager = $mailManager;
    }

    public function getSemester(Institute $institute, string $date = 'now'): ?Semester
    {
        $currentDate = $this->getDate($date);
        $currentYear = \intval($currentDate->format('Y'));

        $earliestDateSecondSemester = $this->getEarliestEndDate($institute->getFirstSemesterStart(), $institute->getSecondSemesterEnd());
        $earliestDateSecondSemester->setDate($currentYear, \intval($earliestDateSecondSemester->format('m')), \intval($earliestDateSecondSemester->format('d')));

        if ($currentDate > $earliestDateSecondSemester) {
            $year = $currentYear.' - '.($currentYear + 1);
        } else {
            $year = ($currentYear - 1).' - '.$currentYear;
        }

        if ($institute->isFullYearOnly()) {
            $period = SemesterPeriodEnum::PERIOD_ALL_YEAR;
        } else {
            $earliestDateFirstSemester = $this->getEarliestEndDate($institute->getSecondSemesterStart(), $institute->getFirstSemesterEnd());
            $earliestDateFirstSemester->setDate($currentYear, \intval($earliestDateFirstSemester->format('m')), \intval($earliestDateFirstSemester->format('d')));

            if ($currentDate >= $earliestDateSecondSemester) {
                if ($earliestDateFirstSemester < $earliestDateSecondSemester) {
                    $earliestDateFirstSemester->modify('+ 1 year');
                }
            } else {
                if ($earliestDateFirstSemester > $earliestDateSecondSemester) {
                    $earliestDateFirstSemester->modify('- 1 year');
                }
            }

            if ($currentDate >= $earliestDateFirstSemester) {
                $period = SemesterPeriodEnum::PERIOD_SECOND_SEMESTER;
            } else {
                $period = SemesterPeriodEnum::PERIOD_FIRST_SEMESTER;
            }
        }

        return $this->semesterRepo->findOneBy([
            'year' => $year,
            'period' => $period,
        ]);
    }

    public function getDateNextSwitch(Institute $institute, string $date = 'now'): \DateTime
    {
        $endSemesterDate = $this->getEndSemesterDate($institute, $date);
        $currentDate = $this->getDate($date);

        if ($endSemesterDate > $currentDate) {
            $dateNextSwitch = $endSemesterDate
                ->modify('- 3 day')
                ->modify('next monday')
            ;

            if ($currentDate <= $dateNextSwitch) {
                return $dateNextSwitch;
            }

            return $dateNextSwitch->modify('next monday');
        }

        return $currentDate->modify('next monday');
    }

    public function isEndOfSemesterReached(Institute $institute, string $date = 'now'): bool
    {
        return $this->getDate($date) >= $this->getEndSemesterDate($institute, $date);
    }

    public function switchSemester(Institute $institute): void
    {
        if ($institute->isFullYearOnly()) {
            $this->statsService->generateNewStat($institute, StatTypeEnum::TYPE_YEAR);
        } else {
            $this->statsService->generateNewStat($institute, StatTypeEnum::TYPE_SEMESTER);

            if (SemesterPeriodEnum::PERIOD_SECOND_SEMESTER === $institute->getSemester()->getPeriod()) {
                $this->statsService->generateNewStat($institute, StatTypeEnum::TYPE_YEAR);
            }
        }

        $this->archiveUsers($institute);
        $this->manageSemesterOptions($institute);
        $institute->setSemester($this->getSemester($institute));
        $this->em->flush();
        $this->statsService->generateNewStat($institute);
        $this->em->flush();
    }

    public function getReadablePeriod(Semester $semester, Institute $institute = null): string
    {
        $response = $this->translator->trans(SemesterPeriodEnum::getReadableValue($semester->getPeriod()));

        $dateFormatter = new \IntlDateFormatter(
            $this->localeManager->getCurrentLocale(),
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::NONE
        );

        if ($institute) {
            switch ($semester->getPeriod()) {
                case SemesterPeriodEnum::PERIOD_FIRST_SEMESTER:
                    $start = $dateFormatter->formatObject($institute->getFirstSemesterStart(), 'MMMM');
                    $end = $dateFormatter->formatObject($institute->getFirstSemesterEnd(), 'MMMM');
                    break;

                case SemesterPeriodEnum::PERIOD_SECOND_SEMESTER:
                    $start = $dateFormatter->formatObject($institute->getSecondSemesterStart(), 'MMMM');
                    $end = $dateFormatter->formatObject($institute->getSecondSemesterEnd(), 'MMMM');
                    break;

                case SemesterPeriodEnum::PERIOD_ALL_YEAR:
                    $start = $dateFormatter->formatObject($institute->getFirstSemesterStart(), 'MMMM');
                    $end = $dateFormatter->formatObject($institute->getSecondSemesterEnd(), 'MMMM');
                    break;

                default:
                    $start = '';
                    $end = '';
                    break;
            }

            $response = ucfirst($start).' - '.ucfirst($end).' ('.$response.')';
        }

        return $response;
    }

    private function archiveUsers(Institute $institute): void
    {
        $users = $this->userRepo->getUsersToArchive($institute);

        /** @var User $user */
        foreach ($users as $user) {
            $user->createHistory();
            $user->setArchived(true);
            $user->setIsAskNextSemester(true);
            $this->em->persist($user);

            if (true === $institute->isActivated()) {
                $this->mailManager->askNextSemester($user, $institute);
            }
        }
    }

    private function manageSemesterOptions(Institute $institute): void
    {
        $semester = $institute->getSemester();
        $currentPeriod = $semester->getPeriod();
        $nbSemestersAvailable = $this->semesterRepo->getNbAvailableChoices($institute);

        if (
            (
                SemesterPeriodEnum::PERIOD_SECOND_SEMESTER === $currentPeriod
                && $nbSemestersAvailable < 3
            )
            || (
                SemesterPeriodEnum::PERIOD_ALL_YEAR == $currentPeriod
                && $nbSemestersAvailable < 2
            )
        ) {
            $lastSemester = $this->semesterRepo->findOneBy([], ['id' => 'desc']);
            $this->generateNewYearSemester($lastSemester->getEndYear());
        }
    }

    private function getEndSemesterDate(Institute $institute, string $date = 'now'): \DateTime
    {
        $currentDate = $this->getDate($date);

        $expectedSemester = $this->getSemester($institute, $date);
        $currentSemester = $institute->getSemester();

        if ($expectedSemester->getStartYear() > $currentSemester->getStartYear()
            || (
                SemesterPeriodEnum::PERIOD_SECOND_SEMESTER === $expectedSemester->getPeriod()
                && SemesterPeriodEnum::PERIOD_FIRST_SEMESTER === $currentSemester->getPeriod()
            )
        ) {
            return $currentDate;
        }

        if (SemesterPeriodEnum::PERIOD_FIRST_SEMESTER === $currentSemester->getPeriod()
            && !$institute->isFullYearOnly()
        ) {
            $endDate = $this->getEarliestEndDate(
                $institute->getSecondSemesterStart(),
                $institute->getFirstSemesterEnd()
            );
        } else {
            $endDate = $this->getEarliestEndDate(
                $institute->getFirstSemesterStart(),
                $institute->getSecondSemesterEnd()
            );
        }

        $endDayDate = \intval($endDate->format('d'));
        $endMonthDate = \intval($endDate->format('m'));
        $currentYear = \intval($currentDate->format('Y'));

        if ($currentDate->format('m') <= $endMonthDate) {
            $endDate->setDate($currentYear, $endMonthDate, $endDayDate);
        } else {
            $endDate->setDate($currentYear + 1, $endMonthDate, $endDayDate);
        }

        return $endDate;
    }

    private function generateNewYearSemester(int $baseYear): void
    {
        foreach (SemesterPeriodEnum::getChoicesToGenerate() as $period) {
            $semester = new Semester();
            $semester->setPeriod($period);
            $semester->setYear($baseYear.' - '.($baseYear + 1));
            $this->em->persist($semester);
        }
    }

    private function getDate(string $date = 'now'): \DateTime
    {
        return (new \DateTime($date))->setTime(0, 0);
    }

    private function getEarliestEndDate(\DateTime $startDate, \DateTime $endDate): \DateTime
    {
        $nextSemesterStart = (clone $startDate)->modify('- '.self::PREOPENING);

        return ($endDate < $nextSemesterStart)
            ? clone $endDate
            : $nextSemesterStart
        ;
    }
}
