<?php

declare(strict_types=1);

namespace App\Services;

class FileWriterHelper
{
    public const EXTRACT_FOLDER = '/public/extract/';

    private $extractDir;

    public function __construct(string $projectDir)
    {
        $this->extractDir = $projectDir.self::EXTRACT_FOLDER;
    }

    public function getExtractDir(): string
    {
        return $this->extractDir;
    }

    public function getStream(string $filepath)
    {
        $stream = fopen($filepath, 'a+');

        if ($this->getFlock($stream)) {
            return $stream;
        }

        $this->closeStream($stream, false);

        return null;
    }

    public function closeStream($stream, bool $release = true): void
    {
        if ($release) {
            flock($stream, \LOCK_UN);
        }

        fclose($stream);
    }

    private function getFlock($stream): bool
    {
        $count = 0;
        $timeout_secs = 10;
        $gotLock = true;
        while (!flock($stream, \LOCK_EX | \LOCK_NB, $wouldBlock)) {
            if ($wouldBlock && $count++ < $timeout_secs) {
                sleep(1);
            } else {
                $gotLock = false;
                break;
            }
        }

        return $gotLock;
    }
}
