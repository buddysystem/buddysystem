<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationRepository;
use App\Repository\InstituteRepository;
use Symfony\Component\Security\Core\Security;

class MatchingManagerHelper
{
    protected $security;
    protected $instituteRepo;
    protected $associationRepo;
    protected $grantedService;

    public function __construct(
        Security $security,
        InstituteRepository $instituteRepo,
        AssociationRepository $associationRepo,
        GrantedService $grantedService
    ) {
        $this->security = $security;
        $this->instituteRepo = $instituteRepo;
        $this->associationRepo = $associationRepo;
        $this->grantedService = $grantedService;
    }

    public function getMatchingManagerOfManager(User $user = null): ?MatchingManagerInterface
    {
        if (!$user && $this->security->getUser()) {
            /** @var User $user */
            $user = $this->security->getUser();
        }

        if ($user) {
            switch ($this->grantedService->getRole($user)) {
                case UserRoleEnum::ROLE_BUDDYCOORDINATOR:
                    return $user->getAssociation();
                case UserRoleEnum::ROLE_INSTITUTE_MANAGER:
                    return $user->getInstitute();
            }
        }

        return null;
    }

    public function getMatchingManagerOfUser(User $user, bool $onlyCanMatch = true): ?MatchingManagerInterface
    {
        if ($matchingManager = $this->getMatchingManagerOfManager($user)) {
            return $matchingManager;
        }

        if ($institute = $user->getInstitute()) {
            $matchingManager = $this->getMatchingManagers(
                $institute,
                true,
                false,
                $onlyCanMatch
            );

            if (!empty($matchingManager)) {
                return end($matchingManager);
            }
        }

        return null;
    }

    public function getAllMatchingManagerOfUser(
        User $user,
        bool $onlyActivated = true,
        bool $onlyCanMatch = true,
        string $type = null
    ): array {
        if ($institute = $user->getInstitute()) {
            return $this->getMatchingManagersOfInstitute($institute, $onlyActivated, $onlyCanMatch, $type);
        }

        return [];
    }

    public function getMatchingManagersOfInstitute(
        Institute $institute,
        bool $onlyActivated = false,
        bool $onlyCanMatch = true,
        string $type = null
    ): array {
        switch ($type) {
            case MatchingManagerInterface::TYPE_INSTITUTE:
                return $this->getInstitutes($institute, $onlyActivated, true);
            case MatchingManagerInterface::TYPE_ASSOCIATION:
                return $this->getAssociations($institute, $onlyActivated, true, $onlyCanMatch);
            default:
                return $this->getMatchingManagers($institute, $onlyActivated, true, $onlyCanMatch);
        }
    }

    public function getAllMatchingManager(bool $onlyManaged = false): array
    {
        if ($onlyManaged) {
            return array_merge(
                $this->associationRepo->getWithManager(),
                $this->instituteRepo->getWithManager()
            );
        }

        return array_merge(
            $this->associationRepo->findAll(),
            $this->instituteRepo->findAll()
        );
    }

    public function getMatchingManagerTypeWithManager(Institute $institute): array
    {
        $types = [];

        foreach ($institute->getAssociationInstitutes() as $associationInstitute) {
            $association = $associationInstitute->getAssociation();
            if ($this->associationRepo->hasManagers($association)) {
                $types[] = MatchingManagerInterface::TYPE_ASSOCIATION;
                break;
            }
        }

        if ($this->instituteRepo->hasManagers($institute)) {
            $types[] = MatchingManagerInterface::TYPE_INSTITUTE;
        } else {
            foreach ($institute->getParents() as $i) {
                if ($this->instituteRepo->hasManagers($i)) {
                    $types[] = MatchingManagerInterface::TYPE_INSTITUTE;
                    break;
                }
            }
        }

        return $types;
    }

    public function getLowerLevelCriteriasManager(Institute $institute): MatchingManagerInterface
    {
        $associations = $this->getAssociations($institute, false);

        if (\in_array(MatchingManagerInterface::TYPE_INSTITUTE, $this->getMatchingManagerTypeWithManager($institute))
            || empty($associations)
        ) {
            /** @var Institute $parent */
            foreach (array_reverse($institute->getParents()) as $parent) {
                if (!$parent->isLowerLevelManagement()) {
                    return $parent;
                }
            }

            return $institute;
        }

        return reset($associations);
    }

    public function getUpperLevelsCriteriasManaged(MatchingManagerInterface $matchingManager): array
    {
        $institutes = [];

        if ($matchingManager instanceof Institute) {
            /** @var Institute $child */
            foreach ($matchingManager->getChildren() as $child) {
                if ($child->isLowerLevelManagement()) {
                    $institutes[] = $child;
                    $institutes = array_merge($institutes, $this->getUpperLevelsCriteriasManaged($child));
                }
            }
        } elseif ($matchingManager instanceof Association) {
            /** @var AssociationInstitute $associationInstitute */
            foreach ($matchingManager->getAssociationInstitutes() as $associationInstitute) {
                $institute = $associationInstitute->getInstitute();
                if ($institute->isLowerLevelManagement()
                    && !\in_array(MatchingManagerInterface::TYPE_INSTITUTE, $this->getMatchingManagerTypeWithManager($institute))
                ) {
                    $institutes[] = $institute;
                }
            }
        }

        return $institutes;
    }

    public function canManage(User $userToManage, bool $canMatchOnly = false): bool
    {
        $matchingManager = $this->getMatchingManagerOfManager();
        $userInstitute = $userToManage->getInstitute();

        if ($matchingManager) {
            if ($matchingManager === $this->getMatchingManagerOfManager($userToManage)) {
                return true;
            }

            if ($matchingManager instanceof Institute) {
                $userManager = $this->getAllMatchingManagerOfUser(
                    $userToManage,
                    true,
                    $canMatchOnly,
                    MatchingManagerInterface::TYPE_INSTITUTE
                );

                return \in_array($matchingManager, $userManager, true);
            }

            /** @var AssociationInstitute $associationInstitute */
            foreach ($matchingManager->getAssociationInstitutes() as $associationInstitute) {
                $institute = $associationInstitute->getInstitute();

                if ($institute === $userInstitute
                    && (
                        !$canMatchOnly
                        || $associationInstitute->canMatch()
                    )
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    private function getAssociations(
        Institute $institute,
        bool $onlyActivated = true,
        bool $allowMultipleManager = false,
        bool $onlyCanMatch = true
    ): array {
        $managers = [];

        /** @var AssociationInstitute $associationInstitute */
        foreach ($institute->getAssociationInstitutes() as $associationInstitute) {
            $association = $associationInstitute->getAssociation();
            if ((!$onlyCanMatch || $associationInstitute->canMatch())
                && (!$onlyActivated || $association->isActivated())
            ) {
                if (!$allowMultipleManager) {
                    return [$association];
                }

                $managers[] = $association;
            }
        }

        return $managers;
    }

    private function getInstitutes(
        Institute $institute,
        bool $onlyActivated = true,
        bool $allowMultipleManager = false
    ): array {
        $managers = [];

        $institutes = array_reverse($institute->getParents());
        array_unshift($institutes, $institute);

        /** @var Institute $i */
        foreach ($institutes as $i) {
            if ($this->instituteRepo->hasManagers($i)
                && (!$onlyActivated || $i->isActivated())) {
                if (!$allowMultipleManager) {
                    return [$i];
                }

                if (!\in_array($i, $managers)) {
                    $managers[] = $i;
                }
            }
        }

        return $managers;
    }

    private function getMatchingManagers(
        Institute $institute,
        bool $onlyActivated = true,
        bool $allowMultipleManager = false,
        bool $onlyCanMatch = true
    ): array {
        $managers = array_merge(
            $this->getAssociations($institute, $onlyActivated, $allowMultipleManager, $onlyCanMatch),
            $this->getInstitutes($institute, $onlyActivated, $allowMultipleManager)
        );

        if (!empty($managers)) {
            return array_reverse($managers);
        }

        if (!$onlyActivated || $institute->isActivated()) {
            return [$institute];
        }

        return [];
    }
}
