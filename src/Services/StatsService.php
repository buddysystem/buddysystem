<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Stat;
use App\Enum\BuddyStatusEnum;
use App\Enum\SemesterPeriodEnum;
use App\Enum\StatTypeEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\InstituteRepository;
use App\Repository\MotivationRepository;
use App\Repository\SemesterRepository;
use App\Repository\StatRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class StatsService
{
    protected $em;
    protected $userRepo;
    protected $buddyRepo;
    protected $instituteRepo;
    protected $associationRepo;
    protected $motivationRepo;
    protected $statRepo;
    protected $localeManager;
    protected $semesterRepo;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepo,
        BuddyRepository $buddyRepo,
        InstituteRepository $instituteRepo,
        AssociationRepository $associationRepo,
        MotivationRepository $motivationRepo,
        StatRepository $statRepo,
        SemesterRepository $semesterRepo,
        LocaleManager $localeManager
    ) {
        $this->em = $em;
        $this->userRepo = $userRepo;
        $this->buddyRepo = $buddyRepo;
        $this->instituteRepo = $instituteRepo;
        $this->motivationRepo = $motivationRepo;
        $this->statRepo = $statRepo;
        $this->localeManager = $localeManager;
        $this->associationRepo = $associationRepo;
        $this->semesterRepo = $semesterRepo;
    }

    public function generateAllWeekStats(): void
    {
        $institutes = $this->instituteRepo->findAll();
        /** @var Institute $institute */
        foreach ($institutes as $institute) {
            $this->generateNewStat($institute);
        }

        $this->em->flush();
    }

    public function generateNewStat(Institute $institute, string $type = StatTypeEnum::TYPE_WEEK): Stat
    {
        $stat = new Stat();
        $institute->addStat($stat);
        $currentSemester = $institute->getSemester();
        $semester = StatTypeEnum::TYPE_YEAR === $type
            ? $this->semesterRepo->findOneBy([
                'year' => $currentSemester->getYear(),
                'period' => SemesterPeriodEnum::PERIOD_ALL_YEAR,
            ])
            : $currentSemester
        ;
        $stat->setSemester($semester);
        $stat->setType($type);

        $lastStats = $this->getStatsValues($institute, $type);
        $stat->setNbUsers($lastStats['nbUsers']);
        $stat->setNbNewUsers($lastStats['nbNewUsers']);
        $stat->setNbMentees($lastStats['nbMentees']);
        $stat->setNbNewMentees($lastStats['nbNewMentees']);
        $stat->setNbMenteesNotMatched($lastStats['nbMenteesNotMatched']);
        $stat->setNbMentors($lastStats['nbMentors']);
        $stat->setNbNewMentors($lastStats['nbNewMentors']);
        $stat->setNbMentorsNotMatched($lastStats['nbMentorsNotMatched']);
        $stat->setNbMatchConfirmed($lastStats['nbMatchConfirmed']);
        $stat->setNbMatchRefused($lastStats['nbMatchRefused']);
        $stat->setNbMatchWaiting($lastStats['nbMatchWaiting']);

        $this->em->persist($stat);

        return $stat;
    }

    public function getStatsValues(
        MatchingManagerInterface $matchingManager = null,
        string $type = StatTypeEnum::TYPE_WEEK
    ): array {
        $startDate = null;
        $isYearStats = StatTypeEnum::TYPE_YEAR === $type;
        $rolesToExclude = UserRoleEnum::$managerAndAdmin;
        $periods = [SemesterPeriodEnum::PERIOD_ALL_YEAR];

        if ($isYearStats) {
            $periods[] = SemesterPeriodEnum::PERIOD_FIRST_SEMESTER;
        }

        if (StatTypeEnum::TYPE_WEEK !== $type) {
            $startDate = $this->statRepo->getDateOfFirstStat($matchingManager, $isYearStats);
        }

        if (!$startDate) {
            $startDate = date('Y-m-d', strtotime('-1 week'));
        }

        return [
            'nbUsers' => $this->userRepo->getNbUsers([], $matchingManager, $rolesToExclude, false, true, $periods),
            'nbMentees' => $this->userRepo->getNbUsers(['local' => 0], $matchingManager, $rolesToExclude, false, true, $periods),
            'nbMentors' => $this->userRepo->getNbUsers(['local' => 1], $matchingManager, $rolesToExclude, false, true, $periods),
            'nbNewUsers' => $this->userRepo->getNbWeekUsers($startDate, $matchingManager, $rolesToExclude),
            'nbNewMentees' => $this->userRepo->getNbWeekUsers($startDate, $matchingManager, $rolesToExclude, false),
            'nbNewMentors' => $this->userRepo->getNbWeekUsers($startDate, $matchingManager, $rolesToExclude, true),
            'nbMenteesNotMatched' => $this->userRepo->getNbUsers(
                [
                    'local' => 0,
                    'nbBuddies' => 0,
                ],
                $matchingManager,
                $rolesToExclude,
                true,
                true,
                $periods
            ),
            'nbMentorsNotMatched' => $this->userRepo->getNbUsers(
                [
                    'local' => 1,
                    'nbBuddies' => 0,
                ],
                $matchingManager,
                $rolesToExclude,
                true,
                true,
                $periods
            ),
            'nbMatchConfirmed' => $this->buddyRepo->getNbBuddies(
                [
                    'status' => BuddyStatusEnum::STATUS_CONFIRMED,
                ],
                $matchingManager,
                true,
                $periods
            ),
            'nbMatchRefused' => $this->buddyRepo->getNbBuddies(
                [
                    'status' => BuddyStatusEnum::STATUS_REFUSED,
                ],
                $matchingManager,
                true,
                $periods
            ),
            'nbMatchWaiting' => $this->buddyRepo->getNbBuddies(
                [
                    'status' => BuddyStatusEnum::STATUS_PENDING,
                ],
                $matchingManager,
                true,
                $periods
            ),
            'institutes' => $this->instituteRepo->getStats($matchingManager, $periods),
            'motivations' => $this->motivationRepo->getStats($matchingManager, $periods),
            'nationalities' => $this->userRepo->getNationalitiesStats($matchingManager, $periods),
        ];
    }

    public function getLastWeekStats(MatchingManagerInterface $matchingManager = null, bool $isYearStats = false): array
    {
        $registration = [];
        $lastStat = null;

        $lastStats = $this->statRepo->getLastWeeksValues($matchingManager, $isYearStats);

        if (!empty($lastStats)) {
            $lastStat = reset($lastStats);

            $fmt = datefmt_create(
                $this->localeManager->getCurrentLocale(),
                \IntlDateFormatter::SHORT,
                \IntlDateFormatter::NONE
            );

            $registration['labels'] = [];
            $serieUsers = [];
            $serieMentors = [];
            $serieMentees = [];

            foreach ($lastStats as $stat) {
                $createdAt = $stat['createdAt'];
                if (!$createdAt instanceof \DateTime) {
                    $createdAt = \DateTime::createFromFormat('Y-m-d', $createdAt);
                }

                if ($createdAt) {
                    $meta = datefmt_format($fmt, $createdAt);
                    array_push($registration['labels'], $meta);
                    array_push($serieUsers, ['meta' => $meta, 'value' => $stat['nbNewUsers']]);
                    array_push($serieMentors, ['meta' => $meta, 'value' => $stat['nbNewMentors']]);
                    array_push($serieMentees, ['meta' => $meta, 'value' => $stat['nbNewMentees']]);
                }
            }

            $registration['series'] = [
                $serieUsers,
                $serieMentors,
                $serieMentees,
            ];
        }

        return [
            'lastStats' => $lastStat,
            'registration' => $registration,
        ];
    }
}
