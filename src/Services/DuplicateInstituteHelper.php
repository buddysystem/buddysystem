<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Institute;
use App\Repository\InstituteRepository;
use Doctrine\ORM\EntityManagerInterface;

class DuplicateInstituteHelper
{
    private $em;
    private $instituteRepo;
    private $instituteLevelManagement;

    public function __construct(
        EntityManagerInterface $em,
        InstituteRepository $instituteRepo,
        InstituteLevelManagement $instituteLevelManagement
    ) {
        $this->em = $em;
        $this->instituteRepo = $instituteRepo;
        $this->instituteLevelManagement = $instituteLevelManagement;
    }

    public function manageDuplicates(Institute $institute, array $parents): void
    {
        if ($duplicate = $institute->getDuplicate()) {
            $institute = $duplicate;
        }

        $existingParents = $this->em->contains($institute) ? $this->instituteRepo->getAllParents($institute) : [];
        $duplicates = $this->em->contains($institute) ? $this->instituteRepo->getAllDuplicates($institute) : [];

        foreach ($existingParents as $existingParent) {
            if (!\in_array($existingParent, $parents)) {
                foreach ($duplicates as $k => $duplicate) {
                    if ($duplicate->getParent() === $existingParent) {
                        unset($duplicates[$k]);

                        $existingParent->removeChild($duplicate);

                        if ($duplicate !== $institute || !empty($duplicates)) {
                            $this->em->remove($duplicate);

                            if (!empty($duplicates)) {
                                $institute = reset($duplicates);
                                $institute->setDuplicate(null);
                            }
                        }

                        $this->instituteLevelManagement->setLevels($existingParent);
                        break;
                    }
                }
            }
        }

        foreach ($duplicates as $duplicate) {
            $institute->addCopy($duplicate);
        }

        /** @var Institute $parent */
        foreach ($parents as $parent) {
            if (!\in_array($parent, $existingParents)) {
                if (!$institute->getParent()) {
                    $parent->addChild($institute);
                } else {
                    $duplicate = clone $institute;
                    $parent->addChild($duplicate);
                    $institute->addCopy($duplicate);
                    $this->em->persist($duplicate);
                }
            }
        }

        $this->updateDuplicatesInformation($institute);
        $this->instituteLevelManagement->setLevels($institute);
    }

    private function updateDuplicatesInformation(Institute $institute): void
    {
        /** @var Institute $copy */
        foreach ($institute->getCopies() as $copy) {
            $copy->setName($institute->getName());
            $copy->setDescription($institute->getDescription());
            $copy->setLogoName($institute->getLogoName());
            $copy->setWebsite($institute->getWebsite());
            $copy->setCities($institute->getCities());
        }
    }
}
