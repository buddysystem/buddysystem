<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Association;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\ThreadAssociation;
use App\Entity\ThreadBuddy;
use App\Entity\ThreadFavorite;
use App\Entity\ThreadInstitute;
use App\Entity\ThreadInterface;
use App\Entity\ThreadManagerInterface;
use App\Entity\ThreadMetadata;
use App\Entity\ThreadUserManager;
use App\Entity\User;
use App\Enum\MessagingRecipientTypeEnum;
use App\Enum\ThreadChannelTypeEnum;
use App\Enum\UserRoleEnum;
use App\Repository\InstituteRepository;
use App\Repository\ThreadBuddyRepository;
use App\Repository\ThreadFavoriteRepository;
use App\Repository\ThreadMetadataRepository;
use App\Repository\ThreadRepository;
use App\Repository\ThreadUserManagerRepository;
use Doctrine\ORM\EntityManagerInterface;
use HtmlSanitizer\Sanitizer;

class MessagingHelper
{
    public const CHANNEL_GLOBAL = 'CHANNEL';
    public const CHANNEL_BUDDY = 'BUDDY';
    public const CHANNEL_ASSOCIATION_USER = 'ASSOCIATION_USER';
    public const CHANNEL_INSTITUTE_USER = 'INSTITUTE_USER';
    public const DIRECT_MESSAGE = 'DEFAULT';

    protected $em;
    protected $threadRepo;
    protected $threadMetadataRepo;
    protected $threadFavoriteRepo;
    protected $threadUserManagerRepo;
    protected $threadBuddyRepo;
    protected $matchingManagerHelper;
    protected $instituteRepo;
    protected $grantedService;

    public function __construct(
        EntityManagerInterface $em,
        ThreadRepository $threadRepo,
        ThreadMetadataRepository $threadMetadataRepo,
        ThreadFavoriteRepository $threadFavoriteRepo,
        ThreadUserManagerRepository $threadUserManagerRepo,
        ThreadBuddyRepository $threadBuddyRepo,
        MatchingManagerHelper $matchingManagerHelper,
        InstituteRepository $instituteRepo,
        GrantedService $grantedService
    ) {
        $this->em = $em;
        $this->threadRepo = $threadRepo;
        $this->threadMetadataRepo = $threadMetadataRepo;
        $this->threadFavoriteRepo = $threadFavoriteRepo;
        $this->threadUserManagerRepo = $threadUserManagerRepo;
        $this->threadBuddyRepo = $threadBuddyRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->instituteRepo = $instituteRepo;
        $this->grantedService = $grantedService;
    }

    public function createMatchingManagerThreads(MatchingManagerInterface $matchingManager): void
    {
        $recipientTypeFound = [];

        /** @var ThreadManagerInterface $thread */
        foreach ($matchingManager->getThreads() as $thread) {
            $recipientTypeFound[] = $thread->getRecipientType();
        }

        $missingRecipientType = array_diff(MessagingRecipientTypeEnum::getChoices(), $recipientTypeFound);

        foreach ($missingRecipientType as $recipientType) {
            /** @var ThreadManagerInterface $thread */
            $thread = $this->createThread(
                $this->getMatchingManagerSubject($matchingManager, $recipientType),
                $matchingManager instanceof Institute
                    ? ThreadInstitute::class
                    : ThreadAssociation::class
            );

            $thread->setRecipientType($recipientType);

            $matchingManager->addThread($thread);
        }
    }

    public function createBuddyThread(Buddy $buddy): ThreadBuddy
    {
        $thread = $buddy->getThread();
        if (!$thread) {
            $thread = $this->createThread($this->getBuddySubject($buddy), ThreadBuddy::class);
            $buddy->setThread($thread);

            $buddy->getMentor()->addThread($thread);
            $buddy->getMentee()->addThread($thread);
        }

        return $thread;
    }

    public function createMessage(Thread $thread, string $body, User $sender): Message
    {
        $sanitizer = Sanitizer::create(['extensions' => ['basic']]);

        $message = new Message();
        $message->setBody($sanitizer->sanitize($body));
        $message->setSender($sender);
        $thread->addMessage($message);

        $this->em->persist($message);

        $this->markAsRead($thread, $sender);

        return $message;
    }

    public function getMatchingManagerSubject(MatchingManagerInterface $manager, string $recipientType): string
    {
        $managerType = $manager instanceof Institute
            ? strtoupper(MatchingManagerInterface::TYPE_INSTITUTE)
            : strtoupper(MatchingManagerInterface::TYPE_ASSOCIATION);

        return self::CHANNEL_GLOBAL.'_'.$managerType.'_'.$manager->getId().'_'.$recipientType;
    }

    public function markAsRead(Thread $thread, User $user): void
    {
        if ($lastMessage = $thread->getMessages()->last()) {
            /** @var ThreadMetadata $meta */
            $meta = $this->threadMetadataRepo->findOneBy(['thread' => $thread, 'participant' => $user]);

            if (!$meta) {
                $meta = new ThreadMetadata();
                $meta->setThread($thread);
                $meta->setParticipant($user);
            }

            $meta->setLastMessageReadAt($lastMessage->getCreatedAt());

            if (!$this->hasUnread($user)) {
                if ($userNotif = $user->getUserNotification()) {
                    $userNotif->resetNewMessage();
                }
            }

            $this->em->persist($meta);
        }
    }

    public function isRead(ThreadInterface $thread, User $user): bool
    {
        if ($thread->getMessages()->isEmpty()) {
            return true;
        }

        /** @var ThreadMetadata $meta */
        if ($meta = $this->threadMetadataRepo->findOneBy(['thread' => $thread, 'participant' => $user])) {
            return $meta->getLastMessageReadAt() == $thread->getLastMessageAt();
        }

        return false;
    }

    public function hasUnread(User $user): bool
    {
        return $this->threadRepo->hasUnread($user, $this->getSubscribedThread($user));
    }

    public function isFavorite(string $thread, User $user): bool
    {
        return null !== $this->threadFavoriteRepo->findOneBy(['user' => $user, 'thread' => $thread]);
    }

    public function toggleFavorite(string $thread, User $user): void
    {
        $threadFavorite = $this->threadFavoriteRepo->findOneBy(['user' => $user, 'thread' => $thread]);

        if ($threadFavorite) {
            $this->em->remove($threadFavorite);
        } else {
            $threadFavorite = new ThreadFavorite();
            $threadFavorite->setUser($user);
            $threadFavorite->setThread($thread);
            $this->em->persist($threadFavorite);
        }

        $this->em->flush();
    }

    public function getThreadMatchingManagers(User $user): array
    {
        $threads = [];
        $matchingManagers = [];

        $isManager = \in_array($this->grantedService->getRole($user), UserRoleEnum::$managers);

        if ($association = $user->getAssociation()) {
            $matchingManagers[] = $association;

            $matchingManagers = array_merge(
                $matchingManagers,
                $this->instituteRepo->getByAssociationOrderByChildren($association)
            );
        } elseif ($institute = $user->getInstitute()) {
            $matchingManagers = $this->matchingManagerHelper->getMatchingManagersOfInstitute(
                $institute,
                false,
                false
            );

            if ($isManager) {
                $matchingManagers = array_merge($matchingManagers, $institute->getAllChildren());
            }
        }

        if (!empty($matchingManagers)) {
            /** @var MatchingManagerInterface $matchingManager */
            foreach ($matchingManagers as $matchingManager) {
                foreach ($matchingManager->getThreads() as $thread) {
                    if (MessagingRecipientTypeEnum::ONLY_MANAGERS !== ($recipientType = $thread->getRecipientType())) {
                        if ($isManager || MessagingRecipientTypeEnum::ALL_USERS === $recipientType
                            || ($user->isLocal() && MessagingRecipientTypeEnum::ONLY_MENTORS === $recipientType)
                            || (!$user->isLocal() && MessagingRecipientTypeEnum::ONLY_MENTEES === $recipientType)
                        ) {
                            $threads[] = $thread;
                        }
                    }
                }
            }
        }

        return $threads;
    }

    public function getThreadAssociationManagers(User $user): ?Thread
    {
        if ($this->grantedService->isGranted($user, UserRoleEnum::ROLE_BUDDYCOORDINATOR)) {
            return $this->threadRepo->findOneBy(['subject' => $this->getBCSubject()]);
        }

        return null;
    }

    public function getThreadInstituteManagers(User $user): ?ThreadInstitute
    {
        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($user);

        if ($matchingManager instanceof Institute) {
            $institute = $matchingManager->getRoot() ?? $matchingManager;

            return $this->em->getRepository(ThreadInstitute::class)->findOneBy([
                'institute' => $institute,
                'recipientType' => MessagingRecipientTypeEnum::ONLY_MANAGERS,
            ]);
        }

        return null;
    }

    public function getThreadBySubjectAndParticipants(string $subject, array $participants): Thread
    {
        $thread = $this->threadRepo->getBySubjectAndUsers($subject, $participants);

        if (!$thread) {
            $thread = $this->createThread($subject);
            foreach ($participants as $participant) {
                $thread->addUser($participant);
            }
        }

        return $thread;
    }

    public function getThreadUserManagers(string $subject, array $matchingManagers, User $user): Thread
    {
        $thread = $this->threadUserManagerRepo->getBySubjectMatchingManagersAndUser($subject, $matchingManagers, $user);

        if (!$thread) {
            /** @var ThreadUserManager $thread */
            $thread = $this->createThread($subject, ThreadUserManager::class);
            $thread->setUser($user);

            foreach ($matchingManagers as $matchingManager) {
                $thread->addMatchingManager($matchingManager);
            }
        }

        return $thread;
    }

    public function getThreadsManagerUsers(User $user): array
    {
        $threads = [];

        if ($matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($user)) {
            if ($matchingManager instanceof Association) {
                $subject = self::CHANNEL_ASSOCIATION_USER;
            } else {
                $subject = self::CHANNEL_INSTITUTE_USER;
            }

            $threads = $this->threadUserManagerRepo->getBySubjectAndMatchingManagers($subject, [$matchingManager]);
        }

        return $threads;
    }

    public function getBCSubject(): string
    {
        return self::CHANNEL_GLOBAL.'_BCS';
    }

    public function getBuddySubject(Buddy $buddy): string
    {
        return self::CHANNEL_BUDDY.'_'.$buddy->getId();
    }

    public function getSubscribedThread(User $user, string $threadChannelType = null): array
    {
        $threads = [];

        if (!$threadChannelType || ThreadChannelTypeEnum::INDIVIDUAL_CHANNEL === $threadChannelType) {
            $threads = array_merge(
                $this->threadRepo->getByUserAndSubject($user, self::DIRECT_MESSAGE),
                $this->threadBuddyRepo->getByUser($user)
            );
        }

        if (!$threadChannelType || ThreadChannelTypeEnum::GROUP_CHANNEL === $threadChannelType) {
            $threads = array_merge(
                $threads,
                $this->threadUserManagerRepo->getByUser($user),
                $this->getThreadsManagerUsers($user)
            );
        }

        if (!$threadChannelType || ThreadChannelTypeEnum::GLOBAL_CHANNEL === $threadChannelType) {
            $threads = array_merge(
                $threads,
                $this->getThreadMatchingManagers($user)
            );

            if ($threadAssociationManager = $this->getThreadAssociationManagers($user)) {
                $threads[] = $threadAssociationManager;
            } elseif ($threadInstituteManager = $this->getThreadInstituteManagers($user)) {
                $threads[] = $threadInstituteManager;
            }
        }

        return $threads;
    }

    private function createThread(string $subject, string $threadType = Thread::class): Thread
    {
        $thread = new $threadType();
        $thread->setSubject($subject);
        $this->em->persist($thread);

        return $thread;
    }
}
