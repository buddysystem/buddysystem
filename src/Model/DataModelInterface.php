<?php

declare(strict_types=1);

namespace App\Model;

interface DataModelInterface
{
    public function getSort(): array;

    public function getSearch(): array;

    public function getFirstResult(): int;

    public function getMaxResults(): int;
}
