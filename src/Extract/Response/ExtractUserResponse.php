<?php

declare(strict_types=1);

namespace App\Extract\Response;

use App\DataTable\Response\UserListResponse;
use App\Entity\User;
use App\Extract\AbstractExtract;

class ExtractUserResponse extends AbstractExtract
{
    public function getRows(): array
    {
        $rows = [];

        if (0 === $this->getFirstResult()) {
            $rows[] = [
                $this->translator->trans('user.firstname'),
                $this->translator->trans('user.lastname'),
                $this->translator->trans('user.email'),
                $this->translator->trans('user.semester'),
                $this->translator->trans('semester.year'),
                $this->translator->trans('user.roles'),
                $this->translator->trans('user.nationality'),
                $this->translator->trans('user.city'),
                $this->translator->trans('user.institute'),
                $this->translator->trans('user.studies'),
                $this->translator->trans('user.level_of_study'),
                $this->translator->trans('user.type_of_mobility'),
                $this->translator->trans('user.languages'),
                $this->translator->trans('user.list.is_local'),
                $this->translator->trans('user.arrival'),
                $this->translator->trans('created_at'),
                $this->translator->trans('user.list.nb_buddy'),
                $this->translator->trans('matching.nb_buddy_wanted'),
                $this->translator->trans('user.list.has_max_buddies'),
                $this->translator->trans('matching.nb_refusal'),
                $this->translator->trans('matching.nb_refusal_by_buddies'),
                $this->translator->trans('user.gender'),
                $this->translator->trans('user.list.same_gender'),
                $this->translator->trans('user.list.bonus_option'),
                $this->translator->trans('user.archived'),
            ];
        }

        $repo = $this->em->getRepository(User::class);
        $data = $this->responseFactory->getResponse(
            UserListResponse::class,
            $repo->getListInfo(
                $this,
                $this->getMatchingManager()
            ),
            ['expanded' => true]
        );

        foreach ($data as $user) {
            $rows[] = [
                $user['firstname'],
                $user['lastname'],
                $user['email'],
                $user['semester'],
                $user['year'],
                $user['roles'],
                $user['nationality'],
                $user['city'],
                $this->br2nl($user['institute']),
                $user['studies'],
                $user['levelOfStudy'],
                $user['typeOfMobility'],
                $this->br2nl($user['languages']),
                $user['local'],
                $user['arrival'],
                $user['createdAt'],
                $user['nbBuddies'],
                $user['nbBuddiesWanted'],
                $user['hasMaxBuddies'],
                $user['nbBuddiesRefused'],
                $user['nbRefusedByBuddies'],
                $user['sex'],
                $user['sexWanted'],
                $user['bonusOption'],
                $user['archived'],
            ];
        }

        return $rows;
    }
}
