<?php

declare(strict_types=1);

namespace App\Extract\Response;

use App\DataTable\Response\BuddyListResponse;
use App\Entity\Buddy;
use App\Extract\AbstractExtract;

class ExtractBuddyResponse extends AbstractExtract
{
    public function getRows(): array
    {
        $rows = [];

        if (0 === $this->getFirstResult()) {
            $rows[] = [
                $this->translator->trans('mentor'),
                $this->translator->trans('mentor.email'),
                $this->translator->trans('mentor.institute'),
                $this->translator->trans('mentee'),
                $this->translator->trans('mentee.email'),
                $this->translator->trans('mentee.institute'),
                $this->translator->trans('user.semester'),
                $this->translator->trans('semester.year'),
                $this->translator->trans('buddy.languagePairing'),
                $this->translator->trans('buddy.confirmed'),
                $this->translator->trans('buddy.refused'),
                $this->translator->trans('buddy.warned_at'),
                $this->translator->trans('buddy.response_at'),
                $this->translator->trans('buddy.last_message_at'),
                $this->translator->trans('created_at'),
                $this->translator->trans('buddy.archive'),
            ];
        }

        $repo = $this->em->getRepository(Buddy::class);
        $data = $this->responseFactory->getResponse(
            BuddyListResponse::class,
            $repo->getListInfo(
                $this,
                $this->getMatchingManager()
            )
        );

        foreach ($data as $buddy) {
            $rows[] = [
                $buddy['mentor'],
                $buddy['mentorEmail'],
                $this->br2nl($buddy['mentorInstitute']),
                $buddy['mentee'],
                $buddy['menteeEmail'],
                $this->br2nl($buddy['menteeInstitute']),
                $buddy['semester'],
                $buddy['year'],
                $buddy['languagePairing'],
                $buddy['status'],
                $buddy['reasonRefusal'],
                $buddy['warnedAt'],
                $buddy['responseAt'],
                $buddy['lastMessageAt'],
                $buddy['createdAt'],
                $buddy['archived'],
            ];
        }

        return $rows;
    }
}
