<?php

declare(strict_types=1);

namespace App\Extract;

use App\DataTable\DataTableResponseFactory;
use App\Enum\ExtractTypeEnum;
use App\Extract\Response\ExtractBuddyResponse;
use App\Extract\Response\ExtractUserResponse;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExtractFactory
{
    private $em;
    private $responseFactory;
    private $translator;
    private $matchingManagerHelper;

    public function __construct(
        EntityManagerInterface $em,
        DataTableResponseFactory $responseFactory,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper
    ) {
        $this->em = $em;
        $this->responseFactory = $responseFactory;
        $this->translator = $translator;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    public function create(ExtractModel $extractModel): ?ExtractInterface
    {
        switch ($extractModel->getType()) {
            case ExtractTypeEnum::TYPE_USERS:
                $type = ExtractUserResponse::class;
                break;
            case ExtractTypeEnum::TYPE_BUDDIES:
                $type = ExtractBuddyResponse::class;
                break;
            default:
                $type = null;
                break;
        }

        if ($type) {
            return new $type(
                $this->em,
                $this->responseFactory,
                $this->translator,
                $this->matchingManagerHelper,
                $extractModel
            );
        }

        return null;
    }
}
