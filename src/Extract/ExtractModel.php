<?php

declare(strict_types=1);

namespace App\Extract;

use App\Entity\User;
use App\Enum\ExtractTypeEnum;
use App\Model\DataModelInterface;

class ExtractModel implements DataModelInterface
{
    public const MAX_RESULTS = 100;

    private string $type = ExtractTypeEnum::TYPE_USERS;
    private array $sort = [];
    private array $search = [];
    private int $firstResult = 0;
    private int $maxResults = self::MAX_RESULTS;
    private User $user;
    private array $filters = [[]];
    private string $filename = '';

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getSort(): array
    {
        return $this->sort;
    }

    public function setSort(array $sort): void
    {
        $this->sort = $sort;
    }

    public function getSearch(): array
    {
        if (empty($this->search)) {
            foreach (reset($this->filters) as $filter => $value) {
                if (null !== $value) {
                    $this->search[$filter] = $value;
                }
            }
        }

        return $this->search;
    }

    public function setSearch(array $search): void
    {
        $this->search = $search;
    }

    public function getFirstResult(): int
    {
        return $this->firstResult;
    }

    public function setFirstResult(int $firstResult): void
    {
        $this->firstResult = $firstResult;
    }

    public function getMaxResults(): int
    {
        return $this->maxResults;
    }

    public function setMaxResults(int $maxResults): void
    {
        $this->maxResults = $maxResults;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    public function getFilename(): string
    {
        if (!$this->filename) {
            $this->filename = $this->getType().'_'.date('Y-m-d_h-i-s').'.csv';
        }

        return $this->filename;
    }

    public function setFilename(string $filename = null): void
    {
        $this->filename = $filename;
    }
}
