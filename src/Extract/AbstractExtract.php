<?php

declare(strict_types=1);

namespace App\Extract;

use App\DataTable\DataTableResponseFactory;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractExtract extends ExtractModel implements ExtractInterface
{
    protected $em;
    protected $responseFactory;
    protected $translator;
    protected $matchingManagerHelper;

    public function __construct(
        EntityManagerInterface $em,
        DataTableResponseFactory $responseFactory,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        ExtractModel $extractModel
    ) {
        $this->em = $em;
        $this->responseFactory = $responseFactory;
        $this->translator = $translator;
        $this->matchingManagerHelper = $matchingManagerHelper;

        $userRepo = $this->em->getRepository(User::class);

        $this->setUser($userRepo->findOneBy(['id' => $extractModel->getUser()->getId()]));
        $this->setFirstResult($extractModel->getFirstResult());
        $this->setMaxResults($extractModel->getMaxResults());
        $this->setFilename($extractModel->getFilename());
        $this->setSearch($extractModel->getSearch());
        $this->setSort($extractModel->getSort());
    }

    abstract public function getRows(): array;

    protected function br2nl(string $value): string
    {
        return str_replace(['<br>', '<br />'], "\n", $value);
    }

    protected function getMatchingManager(): ?MatchingManagerInterface
    {
        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager($this->getUser());

        if ($matchingManager) {
            $this->em->refresh($matchingManager);
        }

        return $matchingManager;
    }
}
