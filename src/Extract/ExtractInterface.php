<?php

declare(strict_types=1);

namespace App\Extract;

use Symfony\Component\Security\Core\User\UserInterface;

interface ExtractInterface
{
    public function getFilename(): string;

    public function getRows(): array;

    public function getUser(): UserInterface;
}
