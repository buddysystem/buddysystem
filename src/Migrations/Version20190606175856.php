<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190606175856 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83B03A8386');
        $this->addSql('DROP INDEX IDX_31204C83B03A8386 ON thread');
        $this->addSql('ALTER TABLE thread DROP created_by_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE thread ADD created_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE thread ADD CONSTRAINT FK_31204C83B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_31204C83B03A8386 ON thread (created_by_id)');
    }
}
