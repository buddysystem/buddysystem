<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190116094947 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_history (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, nb_warned INT UNSIGNED DEFAULT 0 NOT NULL, nb_report INT UNSIGNED DEFAULT 0 NOT NULL, nb_buddies_removed INT UNSIGNED DEFAULT 0 NOT NULL, nb_buddies_refused INT UNSIGNED DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_7FB76E41A76ED395 (user_id), PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE user_history ADD CONSTRAINT FK_7FB76E41A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE user_history');
    }
}
