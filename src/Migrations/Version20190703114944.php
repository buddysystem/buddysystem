<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190703114944 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          section 
        DROP 
          INDEX UNIQ_2D737AEFF4AD20B6, 
        ADD 
          INDEX IDX_2D737AEFF4AD20B6 (current_semester_id)');
        $this->addSql('ALTER TABLE semester DROP FOREIGN KEY FK_F7388EEDD823E37A');
        $this->addSql('DROP INDEX IDX_F7388EEDD823E37A ON semester');
        $this->addSql('ALTER TABLE semester DROP section_id, DROP activated');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          section 
        DROP 
          INDEX IDX_2D737AEFF4AD20B6, 
        ADD 
          UNIQUE INDEX UNIQ_2D737AEFF4AD20B6 (current_semester_id)');
        $this->addSql('ALTER TABLE semester ADD section_id INT DEFAULT NULL, ADD activated TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE 
          semester 
        ADD 
          CONSTRAINT FK_F7388EEDD823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
        $this->addSql('CREATE INDEX IDX_F7388EEDD823E37A ON semester (section_id)');
    }
}
