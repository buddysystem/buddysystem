<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220328122203 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO study (name) VALUES (\'Other\')');
        $this->addSql('UPDATE institute SET studies_required=1 WHERE studies_required=0');
    }

    public function down(Schema $schema): void
    {
    }
}
