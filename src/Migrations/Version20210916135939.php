<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210916135939 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP FOREIGN KEY FK_FD8521CC4A798B6F');
        $this->addSql('DROP INDEX IDX_FD8521CC4A798B6F ON association');
        $this->addSql('ALTER TABLE association DROP semester_id');
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_EAC2F091EFB9C8A5');
        $this->addSql('DROP INDEX IDX_20B8FF21EFB9C8A5 ON stat');
        $this->addSql('ALTER TABLE stat DROP association_id');
        $this->addSql('update buddy b set b.institute_id = (select u.institute_id from user u where b.mentee_id = u.id) where b.institute_id is null');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association ADD semester_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          association
        ADD
          CONSTRAINT FK_FD8521CC4A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE INDEX IDX_FD8521CC4A798B6F ON association (semester_id)');
        $this->addSql('ALTER TABLE stat ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_EAC2F091EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_20B8FF21EFB9C8A5 ON stat (association_id)');
    }
}
