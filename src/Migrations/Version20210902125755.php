<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210902125755 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE report ADD deleted_at DATETIME DEFAULT NULL');
        $this->addSql('UPDATE report r set deleted_at = (select deleted_at from buddy b where b.id = r.buddy_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE report DROP deleted_at');
    }
}
