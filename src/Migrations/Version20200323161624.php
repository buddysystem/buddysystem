<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200323161624 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_E06073ED5E237E06 ON motivation');
        $this->addSql('ALTER TABLE 
          motivation 
        ADD 
          name_mentor VARCHAR(255) NOT NULL, 
          CHANGE name name_mentee VARCHAR(255) NOT NULL');
        $this->addSql('update ext_translations set field = \'name_mentee\' where field = \'name\' and object_class = \'App\\Entity\\Motivation\'');
        $this->addSql('update motivation set name_mentor = name_mentee');
        $this->addSql('INSERT INTO ext_translations (locale, object_class, field, foreign_key, content) SELECT locale, object_class, \'name_mentor\', foreign_key, content FROM ext_translations where object_class = \'App\\Entity\\Motivation\' and field = \'name_mentee\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          motivation 
        CHANGE 
          name_mentee name VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,  
        DROP 
          name_mentor');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E06073ED5E237E06 ON motivation (name)');
        $this->addSql('update ext_translations set field = \'name\' where field = \'name_mentee\' and object_class = \'App\\Entity\\Motivation\'');
        $this->addSql('delete from ext_translations where object_class = \'App\\Entity\\Motivation\' and field = \'name_mentor\'');
    }
}
