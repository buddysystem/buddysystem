<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201105163326 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE thread_favorite (
          id INT AUTO_INCREMENT NOT NULL,
          user_id INT DEFAULT NULL,
          thread VARCHAR(255) NOT NULL,
          INDEX IDX_6774CD05A76ED395 (user_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          thread_favorite
        ADD
          CONSTRAINT FK_6774CD05A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE thread_favorite');
    }
}
