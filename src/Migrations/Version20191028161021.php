<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191028161021 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5783E3463');
        $this->addSql('DROP INDEX IDX_6EB2BCA5783E3463 ON association_institute');
        $this->addSql('ALTER TABLE association_institute DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          id INT AUTO_INCREMENT NOT NULL, 
        DROP 
          manager_id, 
        DROP 
          can_match, 
          CHANGE association_id association_id INT DEFAULT NULL, 
          CHANGE institute_id institute_id INT DEFAULT NULL,
          ADD PRIMARY KEY (id)');
        $this->addSql('CREATE TABLE association_institute_right (
          id INT AUTO_INCREMENT NOT NULL, 
          association_institute_id INT DEFAULT NULL, 
          manager_id INT DEFAULT NULL, 
          can_match TINYINT(1) NOT NULL, 
          INDEX IDX_3136C203D4ED69C1 (association_institute_id), 
          INDEX IDX_3136C203783E3463 (manager_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_institute_right_institute (
          association_institute_right_id INT NOT NULL, 
          institute_id INT NOT NULL, 
          INDEX IDX_B779701C52715B0D (
            association_institute_right_id
          ), 
          INDEX IDX_B779701C697B0F4C (institute_id), 
          PRIMARY KEY(
            association_institute_right_id, 
            institute_id
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          association_institute_right 
        ADD 
          CONSTRAINT FK_3136C203D4ED69C1 FOREIGN KEY (association_institute_id) REFERENCES association_institute (id)');
        $this->addSql('ALTER TABLE 
          association_institute_right 
        ADD 
          CONSTRAINT FK_3136C203783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_B779701C52715B0D FOREIGN KEY (
            association_institute_right_id
          ) REFERENCES association_institute_right (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_B779701C697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_57269CFBD4ED69C1 ON association_institute_right (association_institute_id)');
        $this->addSql('CREATE INDEX IDX_57269CFB783E3463 ON association_institute_right (manager_id)');
        $this->addSql('CREATE INDEX IDX_9AAF6E61D02919BD ON association_institute_right_institute (association_institute_right_id)');
        $this->addSql('CREATE INDEX IDX_9AAF6E61697B0F4C ON association_institute_right_institute (institute_id)');
        $this->addSql('ALTER TABLE 
          association_institute_right 
        ADD 
          CONSTRAINT FK_57269CFBD4ED69C1 FOREIGN KEY (association_institute_id) REFERENCES association_institute (id)');
        $this->addSql('ALTER TABLE 
          association_institute_right 
        ADD 
          CONSTRAINT FK_57269CFB783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_9AAF6E61D02919BD FOREIGN KEY (association_institute_right_id) REFERENCES association_institute_right (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_9AAF6E61697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_57269CFBD4ED69C1');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_57269CFB783E3463');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_9AAF6E61D02919BD');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_9AAF6E61697B0F4C');
        $this->addSql('DROP TABLE association_institute_right');
        $this->addSql('DROP TABLE association_institute_right_institute');
        $this->addSql('ALTER TABLE association_institute MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE association_institute DROP PRIMARY KEY');
        $this->addSql('DROP INDEX IDX_3136c203d4ed69c1 ON association_institute_right');
        $this->addSql('DROP INDEX idx_3136c203783e3463 ON association_institute_right');
        $this->addSql('DROP INDEX idx_b779701c52715b0d ON association_institute_right_institute');
        $this->addSql('DROP INDEX idx_b779701c697b0f4c ON association_institute_right_institute');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_3136c203d4ed69c1');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_3136c203783e3463');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_b779701c52715b0d');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_b779701c697b0f4c');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          manager_id INT DEFAULT NULL, 
        ADD 
          can_match TINYINT(1) NOT NULL, 
        DROP 
          id, 
          CHANGE association_id association_id INT NOT NULL, 
          CHANGE institute_id institute_id INT NOT NULL');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_6EB2BCA5783E3463 ON association_institute (manager_id)');
        $this->addSql('ALTER TABLE association_institute ADD PRIMARY KEY (association_id, institute_id)');
    }
}
