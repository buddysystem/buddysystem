<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190703114946 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_institute (
          user_id INT NOT NULL, 
          institute_id INT NOT NULL, 
          INDEX IDX_CAB53834A76ED395 (user_id), 
          INDEX IDX_CAB53834697B0F4C (institute_id), 
          PRIMARY KEY(user_id, institute_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matching_manager (
          id INT AUTO_INCREMENT NOT NULL, 
          current_semester_id INT DEFAULT NULL, 
          name VARCHAR(255) NOT NULL, 
          description LONGTEXT DEFAULT NULL, 
          website VARCHAR(255) DEFAULT NULL, 
          nbMentors INT UNSIGNED DEFAULT 1 NOT NULL, 
          nbMentees INT UNSIGNED DEFAULT 1 NOT NULL, 
          first_semester_start DATE NOT NULL, 
          first_semester_end DATE NOT NULL, 
          second_semester_start DATE NOT NULL, 
          second_semester_end DATE NOT NULL, 
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL, 
          matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL, 
          matching_institute_value INT UNSIGNED DEFAULT 10 NOT NULL, 
          matching_language_value INT UNSIGNED DEFAULT 6 NOT NULL, 
          matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL, 
          activated TINYINT(1) NOT NULL, 
          logo_name VARCHAR(255) DEFAULT NULL, 
          revival_delay INT UNSIGNED DEFAULT 7 NOT NULL, 
          unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          type VARCHAR(255) NOT NULL, 
          facebook VARCHAR(255) DEFAULT NULL, 
          twitter VARCHAR(255) DEFAULT NULL, 
          instagram VARCHAR(255) DEFAULT NULL,
          level INT UNSIGNED DEFAULT 0,
          INDEX IDX_7E2AE106F4AD20B6 (current_semester_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matching_manager_city (
          matching_manager_id INT NOT NULL, 
          city_id INT NOT NULL, 
          INDEX IDX_159B369CFE12B287 (matching_manager_id), 
          INDEX IDX_159B369C8BAC62AF (city_id), 
          PRIMARY KEY(matching_manager_id, city_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matching_manager_hobby (
          matching_manager_id INT NOT NULL, 
          hobby_id INT NOT NULL, 
          INDEX IDX_E15118D9FE12B287 (matching_manager_id), 
          INDEX IDX_E15118D9322B2123 (hobby_id), 
          PRIMARY KEY(matching_manager_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matching_manager_motivation (
          matching_manager_id INT NOT NULL, 
          motivation_id INT NOT NULL, 
          INDEX IDX_1DBBA406FE12B287 (matching_manager_id), 
          INDEX IDX_1DBBA4068EDBCD4E (motivation_id), 
          PRIMARY KEY(
            matching_manager_id, motivation_id
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute_institute (
          institute_source INT NOT NULL, 
          institute_target INT NOT NULL, 
          INDEX IDX_AF802331AE7E57AA (institute_source), 
          INDEX IDX_AF802331B79B0725 (institute_target), 
          PRIMARY KEY(
            institute_source, institute_target
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_institute (
          association_id INT NOT NULL, 
          institute_id INT NOT NULL, 
          can_match TINYINT(1) NOT NULL, 
          INDEX IDX_6EB2BCA5EFB9C8A5 (association_id), 
          INDEX IDX_6EB2BCA5697B0F4C (institute_id), 
          PRIMARY KEY(association_id, institute_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          user_institute 
        ADD 
          CONSTRAINT FK_CAB53834A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          user_institute 
        ADD 
          CONSTRAINT FK_CAB53834697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager 
        ADD 
          CONSTRAINT FK_7E2AE106F4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          matching_manager_city 
        ADD 
          CONSTRAINT FK_159B369CFE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager_city 
        ADD 
          CONSTRAINT FK_159B369C8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager_hobby 
        ADD 
          CONSTRAINT FK_E15118D9FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager_hobby 
        ADD 
          CONSTRAINT FK_E15118D9322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager_motivation 
        ADD 
          CONSTRAINT FK_1DBBA406FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          matching_manager_motivation 
        ADD 
          CONSTRAINT FK_1DBBA4068EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331AE7E57AA FOREIGN KEY (institute_source) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331B79B0725 FOREIGN KEY (institute_target) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5EFB9C8A5 FOREIGN KEY (association_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE user ADD institute_id INT DEFAULT NULL, ADD matching_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649697B0F4C ON user (institute_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649FE12B287 ON user (matching_manager_id)');
        $this->addSql('ALTER TABLE buddy ADD matching_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_1EA78759FE12B287 ON buddy (matching_manager_id)');
        $this->addSql('ALTER TABLE week_stat ADD matching_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F091FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_EAC2F091FE12B287 ON week_stat (matching_manager_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649697B0F4C');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FE12B287');
        $this->addSql('ALTER TABLE user_institute DROP FOREIGN KEY FK_CAB53834697B0F4C');
        $this->addSql('ALTER TABLE matching_manager_city DROP FOREIGN KEY FK_159B369CFE12B287');
        $this->addSql('ALTER TABLE matching_manager_hobby DROP FOREIGN KEY FK_E15118D9FE12B287');
        $this->addSql('ALTER TABLE matching_manager_motivation DROP FOREIGN KEY FK_1DBBA406FE12B287');
        $this->addSql('ALTER TABLE institute_institute DROP FOREIGN KEY FK_AF802331AE7E57AA');
        $this->addSql('ALTER TABLE institute_institute DROP FOREIGN KEY FK_AF802331B79B0725');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759FE12B287');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F091FE12B287');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5EFB9C8A5');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5697B0F4C');
        $this->addSql('DROP TABLE user_institute');
        $this->addSql('DROP TABLE matching_manager');
        $this->addSql('DROP TABLE matching_manager_city');
        $this->addSql('DROP TABLE matching_manager_hobby');
        $this->addSql('DROP TABLE matching_manager_motivation');
        $this->addSql('DROP TABLE institute_institute');
        $this->addSql('DROP TABLE association_institute');
        $this->addSql('DROP INDEX IDX_1EA78759FE12B287 ON buddy');
        $this->addSql('ALTER TABLE buddy DROP matching_manager_id');
        $this->addSql('DROP INDEX IDX_8D93D649697B0F4C ON user');
        $this->addSql('DROP INDEX IDX_8D93D649FE12B287 ON user');
        $this->addSql('ALTER TABLE user DROP institute_id, DROP matching_manager_id');
        $this->addSql('DROP INDEX IDX_EAC2F091FE12B287 ON week_stat');
        $this->addSql('ALTER TABLE week_stat DROP matching_manager_id');
    }
}
