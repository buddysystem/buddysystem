<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190227132137 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy ADD status ENUM(\'pending\', \'confirmed\', \'refused\') DEFAULT \'pending\' NOT NULL COMMENT \'(DC2Type:BuddyStatusType)\', CHANGE confirmed confirmed TINYINT(1) DEFAULT NULL');
        $this->addSql('UPDATE buddy set status = \'confirmed\' where confirmed = 1');
        $this->addSql('UPDATE buddy set status = \'refused\' where confirmed = 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP status, CHANGE confirmed confirmed INT DEFAULT NULL');
    }
}
