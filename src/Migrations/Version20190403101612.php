<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190403101612 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE week_stat (id INT UNSIGNED AUTO_INCREMENT NOT NULL, section_id INT DEFAULT NULL, semester INT UNSIGNED DEFAULT 1 NOT NULL, nb_users INT UNSIGNED DEFAULT 0 NOT NULL, nb_new_users INT UNSIGNED DEFAULT 0 NOT NULL, nb_mentees INT UNSIGNED DEFAULT 0 NOT NULL, nb_new_mentees INT UNSIGNED DEFAULT 0 NOT NULL, nb_mentees_not_matched INT UNSIGNED DEFAULT 0 NOT NULL, nb_mentors INT UNSIGNED DEFAULT 0 NOT NULL, nb_new_mentors INT UNSIGNED DEFAULT 0 NOT NULL, nb_mentors_not_matched INT UNSIGNED DEFAULT 0 NOT NULL, nb_match_confirmed INT UNSIGNED DEFAULT 0 NOT NULL, nb_match_refused INT UNSIGNED DEFAULT 0 NOT NULL, nb_match_waiting INT UNSIGNED DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_EAC2F091D823E37A (section_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE week_stat ADD CONSTRAINT FK_EAC2F091D823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE section CHANGE semester semester INT UNSIGNED DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE week_stat');
        $this->addSql('ALTER TABLE section CHANGE semester semester INT DEFAULT NULL');
    }
}
