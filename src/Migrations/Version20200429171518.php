<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200429171518 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('Update user set username = email, username_canonical = email_canonical');
    }

    public function down(Schema $schema): void
    {
    }
}
