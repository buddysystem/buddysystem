<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210112101130 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          user
        ADD
          type_of_registration ENUM(\'classic\', \'mse\') NOT NULL COMMENT \'(DC2Type:UserRegistrationType)\'');
        $this->addSql('UPDATE user set type_of_registration=\'classic\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP type_of_registration');
    }
}
