<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503160339 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('update association_institute set rights = \'["canView","canMatch"]\' where rights = \'["canMatch"]\'');
        $this->addSql('update association_institute set rights = \'["canView"]\' where rights = \'[]\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('update association_institute set rights = \'["canMatch"]\' where rights = \'["canView","canMatch"]\'');
        $this->addSql('update association_institute set rights = \'[]\' where rights = \'["canView"]\'');
    }
}
