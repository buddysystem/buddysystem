<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200324130406 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          matching_manager 
        ADD 
          matching_campus_value INT UNSIGNED DEFAULT 0 NOT NULL, 
        ADD 
          matching_university_value INT UNSIGNED DEFAULT 0 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE matching_manager DROP matching_campus_value, DROP matching_university_value');
    }
}
