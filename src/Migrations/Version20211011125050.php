<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211011125050 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association ADD mentor_validation_required TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('ALTER TABLE institute ADD mentor_validation_required TINYINT(1) DEFAULT \'1\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP mentor_validation_required');
        $this->addSql('ALTER TABLE institute DROP mentor_validation_required');
    }
}
