<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201208111819 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        CHANGE nbMentors nb_mentors INT UNSIGNED DEFAULT 2 NOT NULL,
        CHANGE nbMentees nb_mentees INT UNSIGNED DEFAULT 2 NOT NULL');

        $this->addSql('ALTER TABLE
          institute
        CHANGE nbMentors nb_mentors INT UNSIGNED DEFAULT 2 NOT NULL,
        CHANGE nbMentees nb_mentees INT UNSIGNED DEFAULT 2 NOT NULL');
        $this->addSql('ALTER TABLE
          user
        CHANGE dateofbirthday date_of_birthday DATE DEFAULT NULL,
        CHANGE dateofarrival date_of_arrival DATE DEFAULT NULL,
        CHANGE dateofdepart date_of_depart DATE DEFAULT NULL,
        CHANGE termsOfUse terms_of_use TINYINT(1) NOT NULL,
        CHANGE isMatched is_matched TINYINT(1) NOT NULL,
        DROP
          facebookid,
        DROP
          googleid');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association
        CHANGE nb_mentors nbMentors INT UNSIGNED DEFAULT 2 NOT NULL,
        CHANGE nb_mentees nbMentees INT UNSIGNED DEFAULT 2 NOT NULL');

        $this->addSql('ALTER TABLE
          institute
        CHANGE nb_mentors nbMentors INT UNSIGNED DEFAULT 2 NOT NULL,
        CHANGE nb_mentees nbMentees INT UNSIGNED DEFAULT 2 NOT NULL');

        $this->addSql('ALTER TABLE
          user
        CHANGE date_of_birthday dateofbirthday DATE DEFAULT NULL,
        CHANGE date_of_arrival dateofarrival DATE DEFAULT NULL,
        CHANGE date_of_depart dateofdepart DATE DEFAULT NULL,
        CHANGE terms_of_use termsOfUse TINYINT(1) NOT NULL,
        CHANGE is_matched isMatched TINYINT(1) NOT NULL,
        ADD
          facebookid VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          googleid VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
