<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200721145225 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association CHANGE nbMentors nbMentors INT UNSIGNED DEFAULT 2 NOT NULL,
          CHANGE nbMentees nbMentees INT UNSIGNED DEFAULT 2 NOT NULL,
          CHANGE matching_study_value matching_study_value INT UNSIGNED DEFAULT 7 NOT NULL,
          CHANGE matching_city_value matching_city_value INT UNSIGNED DEFAULT 10 NOT NULL,
          CHANGE matching_age_value matching_age_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_availability_value matching_availability_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_institute_values matching_institute_values LONGTEXT DEFAULT \'[7]\' NOT NULL COMMENT \'(DC2Type:json)\',
          CHANGE matching_level_of_study_value matching_level_of_study_value INT UNSIGNED DEFAULT 5 NOT NULL');
        $this->addSql('ALTER TABLE
          institute CHANGE nbMentors nbMentors INT UNSIGNED DEFAULT 2 NOT NULL,
          CHANGE nbMentees nbMentees INT UNSIGNED DEFAULT 2 NOT NULL,
          CHANGE matching_study_value matching_study_value INT UNSIGNED DEFAULT 7 NOT NULL,
          CHANGE matching_city_value matching_city_value INT UNSIGNED DEFAULT 10 NOT NULL,
          CHANGE matching_age_value matching_age_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_availability_value matching_availability_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_institute_values matching_institute_values LONGTEXT DEFAULT \'[7]\' NOT NULL COMMENT \'(DC2Type:json)\',
          CHANGE matching_level_of_study_value matching_level_of_study_value INT UNSIGNED DEFAULT 5 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          association CHANGE nbMentors nbMentors INT UNSIGNED DEFAULT 1 NOT NULL,
          CHANGE nbMentees nbMentees INT UNSIGNED DEFAULT 1 NOT NULL,
          CHANGE matching_study_value matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL,
          CHANGE matching_city_value matching_city_value INT UNSIGNED DEFAULT 5 NOT NULL,
          CHANGE matching_institute_values matching_institute_values LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[10]\' NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\',
          CHANGE matching_level_of_study_value matching_level_of_study_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_age_value matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL,
          CHANGE matching_availability_value matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL');
        $this->addSql('ALTER TABLE
          institute CHANGE nbMentors nbMentors INT UNSIGNED DEFAULT 1 NOT NULL,
          CHANGE nbMentees nbMentees INT UNSIGNED DEFAULT 1 NOT NULL,
          CHANGE matching_study_value matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL,
          CHANGE matching_city_value matching_city_value INT UNSIGNED DEFAULT 5 NOT NULL,
          CHANGE matching_institute_values matching_institute_values LONGTEXT CHARACTER SET utf8mb4 DEFAULT \'[10]\' NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\',
          CHANGE matching_level_of_study_value matching_level_of_study_value INT UNSIGNED DEFAULT 3 NOT NULL,
          CHANGE matching_age_value matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL,
          CHANGE matching_availability_value matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL');
    }
}
