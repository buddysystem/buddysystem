<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200109174358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE section_hobby DROP FOREIGN KEY FK_AE339453D823E37A');
        $this->addSql('ALTER TABLE section_motivation DROP FOREIGN KEY FK_7DD04932D823E37A');
        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_9AB9BEEBD823E37A');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_1A67AC92D823E37A');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F091D823E37A');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649309D1878');
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE section_hobby');
        $this->addSql('DROP TABLE section_motivation');
        $this->addSql('DROP TABLE university');
        $this->addSql('DROP INDEX IDX_8D93D649D823E37A ON user');
        $this->addSql('DROP INDEX IDX_8D93D649309D1878 ON user');
        $this->addSql('ALTER TABLE user DROP university_id, DROP section_id');
        $this->addSql('ALTER TABLE city DROP country');
        $this->addSql('DROP INDEX IDX_EAC2F091D823E37A ON week_stat');
        $this->addSql('ALTER TABLE week_stat DROP section_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE section (
          id INT AUTO_INCREMENT NOT NULL, 
          city_id INT DEFAULT NULL, 
          current_semester_id INT DEFAULT NULL, 
          association_id INT DEFAULT NULL, 
          name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, 
          description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, 
          facebook VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, 
          twitter VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, 
          instagram VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, 
          website VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, 
          email VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, 
          nbMentors INT UNSIGNED DEFAULT 1 NOT NULL, 
          nbMentees INT UNSIGNED DEFAULT 1 NOT NULL, 
          mailing TINYINT(1) NOT NULL, 
          semester INT UNSIGNED DEFAULT 1 NOT NULL, 
          matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL, 
          matching_university_value INT UNSIGNED DEFAULT 4 NOT NULL, 
          matching_language_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          activated TINYINT(1) NOT NULL, 
          logo_name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, 
          updated_at DATETIME NOT NULL, 
          matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          revival_delay INT UNSIGNED DEFAULT 7 NOT NULL, 
          unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL, 
          created_at DATETIME NOT NULL, 
          matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL, 
          first_semester_start DATE NOT NULL, 
          first_semester_end DATE NOT NULL, 
          second_semester_start DATE NOT NULL, 
          second_semester_end DATE NOT NULL, 
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL, 
          UNIQUE INDEX UNIQ_2D737AEF5E237E06 (name), 
          INDEX IDX_2D737AEF8BAC62AF (city_id), 
          UNIQUE INDEX UNIQ_2D737AEFEFB9C8A5 (association_id), 
          INDEX IDX_2D737AEFF4AD20B6 (current_semester_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE section_hobby (
          section_id INT NOT NULL, 
          hobby_id INT NOT NULL, 
          INDEX IDX_AE339453322B2123 (hobby_id), 
          INDEX IDX_AE339453D823E37A (section_id), 
          PRIMARY KEY(section_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE section_motivation (
          section_id INT NOT NULL, 
          motivation_id INT NOT NULL, 
          INDEX IDX_7DD049328EDBCD4E (motivation_id), 
          INDEX IDX_7DD04932D823E37A (section_id), 
          PRIMARY KEY(section_id, motivation_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE university (
          id INT AUTO_INCREMENT NOT NULL, 
          section_id INT DEFAULT NULL, 
          institute_id INT DEFAULT NULL, 
          name VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, 
          UNIQUE INDEX UNIQ_A07A85EC697B0F4C (institute_id), 
          INDEX IDX_A07A85ECD823E37A (section_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          CONSTRAINT FK_2D737AEFEFB9C8A5 FOREIGN KEY (association_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          CONSTRAINT FK_2D737AEFF4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          CONSTRAINT FK_C3AF07D08BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE 
          section_hobby 
        ADD 
          CONSTRAINT FK_AE339453322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          section_hobby 
        ADD 
          CONSTRAINT FK_AE339453D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          section_motivation 
        ADD 
          CONSTRAINT FK_7DD049328EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          section_motivation 
        ADD 
          CONSTRAINT FK_7DD04932D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          university 
        ADD 
          CONSTRAINT FK_9AB9BEEBD823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE 
          university 
        ADD 
          CONSTRAINT FK_A07A85EC697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE city ADD country VARCHAR(2) NOT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE user ADD university_id INT DEFAULT NULL, ADD section_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_1A67AC92D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649309D1878 FOREIGN KEY (university_id) REFERENCES university (id) ON DELETE 
        SET 
          NULL');
        $this->addSql('CREATE INDEX IDX_8D93D649D823E37A ON user (section_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649309D1878 ON user (university_id)');
        $this->addSql('ALTER TABLE week_stat ADD section_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F091D823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
        $this->addSql('CREATE INDEX IDX_EAC2F091D823E37A ON week_stat (section_id)');
    }
}
