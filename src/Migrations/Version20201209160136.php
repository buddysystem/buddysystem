<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201209160136 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759E2904019');
        $this->addSql('DROP INDEX UNIQ_1EA78759E2904019 ON buddy');
        $this->addSql('ALTER TABLE buddy DROP thread_id');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83395CE8D6');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy ADD thread_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          buddy
        ADD
          CONSTRAINT FK_1EA78759E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1EA78759E2904019 ON buddy (thread_id)');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83395CE8D6');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id) ON DELETE CASCADE');
    }
}
