<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210628134823 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy ADD semester_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          buddy
        ADD
          CONSTRAINT FK_1EA787594A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE INDEX IDX_1EA787594A798B6F ON buddy (semester_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA787594A798B6F');
        $this->addSql('DROP INDEX IDX_1EA787594A798B6F ON buddy');
        $this->addSql('ALTER TABLE buddy DROP semester_id');
    }
}
