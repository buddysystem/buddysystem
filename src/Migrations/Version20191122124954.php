<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191122124954 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE institute_parent (
          institute_source INT NOT NULL, 
          institute_target INT NOT NULL, 
          INDEX IDX_CC7A4896AE7E57AA (institute_source), 
          INDEX IDX_CC7A4896B79B0725 (institute_target), 
          PRIMARY KEY(
            institute_source, institute_target
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          institute_parent 
        ADD 
          CONSTRAINT FK_CC7A4896AE7E57AA FOREIGN KEY (institute_source) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_parent 
        ADD 
          CONSTRAINT FK_CC7A4896B79B0725 FOREIGN KEY (institute_target) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE institute_institute');
        $this->addSql('ALTER TABLE matching_manager 
            ADD grandparent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          matching_manager 
        ADD 
          CONSTRAINT FK_7E2AE1061F29726C FOREIGN KEY (grandparent_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_7E2AE1061F29726C ON matching_manager (grandparent_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE institute_institute (
          institute_source INT NOT NULL, 
          institute_target INT NOT NULL, 
          INDEX IDX_AF802331B79B0725 (institute_target), 
          INDEX IDX_AF802331AE7E57AA (institute_source), 
          PRIMARY KEY(
            institute_source, institute_target
          )
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331AE7E57AA FOREIGN KEY (institute_source) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331B79B0725 FOREIGN KEY (institute_target) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE institute_parent');
        $this->addSql('ALTER TABLE matching_manager DROP FOREIGN KEY FK_7E2AE1061F29726C');
        $this->addSql('DROP INDEX IDX_7E2AE1061F29726C ON matching_manager');
        $this->addSql('ALTER TABLE matching_manager 
            DROP grandparent_id');
    }
}
