<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190601203337 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user ADD privacy_accepted TINYINT(1) NOT NULL, ADD privacy_accepted_at DATETIME DEFAULT NULL, ADD privacy_accepted_with LONGTEXT DEFAULT NULL');
        $this->addSql('UPDATE user SET privacy_accepted = false');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP privacy_accepted, DROP privacy_accepted_at, DROP privacy_accepted_with');
    }
}
