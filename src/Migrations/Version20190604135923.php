<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190604135923 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user CHANGE semester semester VARCHAR(255) DEFAULT NULL');
        $this->addSql('update city set country = "FR" where country = "France"');
        $this->addSql('update city set country = "DE" where country = "Germany"');
        $this->addSql('update city set country = "GB" where country = "England"');
        $this->addSql('update city set country = "AT" where country = "Austria"');
        $this->addSql('update city set country = "IE" where country = "Ireland"');
        $this->addSql('update city set country = "PL" where country = "Poland"');
        $this->addSql('update city set country = "BG" where country = "Bulgaria"');
        $this->addSql('update city set country = "IT" where country = "Italy"');
        $this->addSql('update city set country = "CH" where country = "Swiss"');
        $this->addSql('update city set country = "RO" where country = "Romania"');
        $this->addSql('ALTER TABLE city CHANGE country country VARCHAR(2) NOT NULL');
        $this->addSql('ALTER TABLE language CHANGE code code VARCHAR(3) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user CHANGE semester semester VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE city CHANGE country country VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE language CHANGE code code TINYTEXT NOT NULL COLLATE utf8_unicode_ci');
    }
}
