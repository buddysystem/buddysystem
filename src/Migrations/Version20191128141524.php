<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191128141524 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE matching_manager ADD referent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          matching_manager 
        ADD 
          CONSTRAINT FK_7E2AE10635E47E35 FOREIGN KEY (referent_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_7E2AE10635E47E35 ON matching_manager (referent_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE matching_manager DROP FOREIGN KEY FK_7E2AE10635E47E35');
        $this->addSql('DROP INDEX IDX_7E2AE10635E47E35 ON matching_manager');
        $this->addSql('ALTER TABLE matching_manager DROP referent_id');
    }
}
