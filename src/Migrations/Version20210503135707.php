<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503135707 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute DROP can_match');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute ADD can_match TINYINT(1) NOT NULL');
    }
}
