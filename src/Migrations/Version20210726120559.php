<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210726120559 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE institute ADD lower_level_management TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('UPDATE institute SET lower_level_management = 0 WHERE id in (select i.id from institute i left join user u on u.institute_id = i.id left join association_institute ai on ai.institute_id = i.id where i.level in (0,1) and (u.roles like \'%ROLE_INSTITUTE_MANAGER%\' or ai.association_id is null))');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE institute DROP lower_level_management');
    }
}
