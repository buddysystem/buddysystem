<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210426120813 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE week_stat
            RENAME TO stat,
            ADD
                type ENUM(\'week\', \'semester\', \'year\') NOT NULL COMMENT \'(DC2Type:StatTypeType)\'
        ');
        $this->addSql('ALTER TABLE association DROP nb_weeks_pre_opening');
        $this->addSql('ALTER TABLE institute DROP nb_weeks_pre_opening');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE stat
            RENAME TO week_stat,
            DROP 
                type
        ');
        $this->addSql('ALTER TABLE association ADD nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL');
        $this->addSql('ALTER TABLE institute ADD nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL');
    }
}
