<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200930120206 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE institute_request (
          id INT AUTO_INCREMENT NOT NULL,
          country_id INT DEFAULT NULL,
          city_id INT DEFAULT NULL,
          name VARCHAR(255) NOT NULL,
          city_other VARCHAR(255) DEFAULT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          INDEX IDX_C46911C7F92F3E70 (country_id),
          INDEX IDX_C46911C78BAC62AF (city_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          institute_request
        ADD
          CONSTRAINT FK_C46911C7F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE
          institute_request
        ADD
          CONSTRAINT FK_C46911C78BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE institute_request');
    }
}
