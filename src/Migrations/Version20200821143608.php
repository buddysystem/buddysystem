<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200821143608 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE thread_user (
          thread_id INT NOT NULL,
          user_id INT NOT NULL,
          INDEX IDX_922CAC7E2904019 (thread_id),
          INDEX IDX_922CAC7A76ED395 (user_id),
          PRIMARY KEY(thread_id, user_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          thread_user
        ADD
          CONSTRAINT FK_922CAC7E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user
        ADD
          CONSTRAINT FK_922CAC7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE message_metadata');
        $this->addSql('ALTER TABLE buddy ADD thread_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          buddy
        ADD
          CONSTRAINT FK_1EA78759E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1EA78759E2904019 ON buddy (thread_id)');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE2904019');
        $this->addSql('ALTER TABLE message ADD updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE
          message
        ADD
          CONSTRAINT FK_B6BD307FE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread
        ADD
          association_id INT DEFAULT NULL,
        ADD
          institute_id INT DEFAULT NULL,
        ADD
          buddy_id INT DEFAULT NULL,
        ADD
          updated_at DATETIME NOT NULL,
        ADD
          type VARCHAR(255) NOT NULL,
        ADD
          recipient_type ENUM(\'MENTORS\', \'MENTEES\', \'ALL\', \'MANAGERS\') DEFAULT NULL COMMENT \'(DC2Type:MessagingRecipientTypeType)\',
        ADD
          last_message_at DATETIME DEFAULT NULL,
        DROP
          is_spam');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_31204C83EFB9C8A5 ON thread (association_id)');
        $this->addSql('CREATE INDEX IDX_31204C83697B0F4C ON thread (institute_id)');
        $this->addSql('CREATE INDEX IDX_31204C83395CE8D6 ON thread (buddy_id)');
        $this->addSql('ALTER TABLE thread_metadata DROP FOREIGN KEY FK_40A577C8E2904019');
        $this->addSql('DELETE FROM thread_metadata where is_deleted = 1');
        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
            last_message_read_at DATETIME DEFAULT NULL,
        DROP
          is_deleted,
        DROP
          last_participant_message_date,
        DROP
          last_message_date');

        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
          CONSTRAINT FK_40A577C8E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE thread_user_manager_association (
          thread_user_manager_id INT NOT NULL,
          association_id INT NOT NULL,
          INDEX IDX_A8C32E474F32D28E (thread_user_manager_id),
          INDEX IDX_A8C32E47EFB9C8A5 (association_id),
          PRIMARY KEY(
            thread_user_manager_id, association_id
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thread_user_manager_institute (
          thread_user_manager_id INT NOT NULL,
          institute_id INT NOT NULL,
          INDEX IDX_8A1868B84F32D28E (thread_user_manager_id),
          INDEX IDX_8A1868B8697B0F4C (institute_id),
          PRIMARY KEY(
            thread_user_manager_id, institute_id
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          thread_user_manager_association
        ADD
          CONSTRAINT FK_A8C32E474F32D28E FOREIGN KEY (thread_user_manager_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user_manager_association
        ADD
          CONSTRAINT FK_A8C32E47EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user_manager_institute
        ADD
          CONSTRAINT FK_8A1868B84F32D28E FOREIGN KEY (thread_user_manager_id) REFERENCES thread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          thread_user_manager_institute
        ADD
          CONSTRAINT FK_8A1868B8697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_notification
        ADD
          new_group_message TINYINT(1) DEFAULT \'1\' NOT NULL,
        ADD
          new_global_message TINYINT(1) DEFAULT \'1\' NOT NULL,
        ADD
          new_individual_message TINYINT(1) DEFAULT \'1\' NOT NULL');
        $this->addSql('update thread set type = \'thread\'');
        $this->addSql('update thread set type = \'thread_buddy\' where subject like \'%BUDDY%\'');
        $this->addSql('UPDATE thread SET type = \'thread_user_manager\' WHERE subject = \'BC_USER\'');
        $this->addSql('update thread t set t.last_message_at = (select created_at from message m where m.thread_id = t.id order by m.id desc limit 1)');
        $this->addSql('update user_notification set new_group_message = 0, new_global_message = 0, new_individual_message = 0 where new_message = 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE message_metadata (
          id INT AUTO_INCREMENT NOT NULL,
          message_id INT DEFAULT NULL,
          participant_id INT DEFAULT NULL,
          is_read TINYINT(1) NOT NULL,
          INDEX IDX_4632F005537A1329 (message_id),
          INDEX IDX_4632F0059D1C3019 (participant_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE
          message_metadata
        ADD
          CONSTRAINT FK_4632F005537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE
          message_metadata
        ADD
          CONSTRAINT FK_4632F0059D1C3019 FOREIGN KEY (participant_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE thread_user');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759E2904019');
        $this->addSql('DROP INDEX UNIQ_1EA78759E2904019 ON buddy');
        $this->addSql('ALTER TABLE buddy DROP thread_id');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE2904019');
        $this->addSql('ALTER TABLE message DROP updated_at');
        $this->addSql('ALTER TABLE
          message
        ADD
          CONSTRAINT FK_B6BD307FE2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83EFB9C8A5');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83697B0F4C');
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83395CE8D6');
        $this->addSql('DROP INDEX IDX_31204C83EFB9C8A5 ON thread');
        $this->addSql('DROP INDEX IDX_31204C83697B0F4C ON thread');
        $this->addSql('DROP INDEX IDX_31204C83395CE8D6 ON thread');
        $this->addSql('ALTER TABLE
          thread
        ADD
          is_spam TINYINT(1) NOT NULL,
        DROP
          association_id,
        DROP
          institute_id,
        DROP
          buddy_id,
        DROP
          updated_at,
        DROP
          type,
        DROP
          recipient_type,
        DROP
          last_message_at');
        $this->addSql('ALTER TABLE thread_metadata DROP FOREIGN KEY FK_40A577C8E2904019');
        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
          is_deleted TINYINT(1) NOT NULL,
        ADD
          last_participant_message_date DATETIME DEFAULT NULL,
        ADD
          last_message_date DATETIME DEFAULT NULL,
        DROP
          last_message_read_ata');
        $this->addSql('ALTER TABLE
          thread_metadata
        ADD
          CONSTRAINT FK_40A577C8E2904019 FOREIGN KEY (thread_id) REFERENCES thread (id)');
        $this->addSql('DROP TABLE thread_user_manager_association');
        $this->addSql('DROP TABLE thread_user_manager_institute');
        $this->addSql('ALTER TABLE
          user_notification
        DROP
          new_group_message,
        DROP
          new_global_message,
        DROP
          new_individual_message');
    }
}
