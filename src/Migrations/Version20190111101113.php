<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190111101113 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user CHANGE islocal local TINYINT(1) NOT NULL, ADD locale_language VARCHAR(255) NOT NULL');
        $this->addSql('UPDATE user SET locale_language = "en"');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user CHANGE local isLocal TINYINT(1) NOT NULL, DROP locale_language');
    }
}
