<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200505145840 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_B779701C52715B0D');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_3136C203783E3463');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY FK_3136C203D4ED69C1');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY FK_B779701C697B0F4C');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5697B0F4C');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5EFB9C8A5');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759FE12B287');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649697B0F4C');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FE12B287');
        $this->addSql('ALTER TABLE user_institute DROP FOREIGN KEY FK_CAB53834697B0F4C');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F091FE12B287');
        $this->addSql('ALTER TABLE week_stat_institute DROP FOREIGN KEY FK_A0058D93697B0F4C');
        $this->addSql('CREATE TABLE institute (
          id INT AUTO_INCREMENT NOT NULL, 
          parent_id INT DEFAULT NULL, 
          duplicate_id INT DEFAULT NULL, 
          root_id INT DEFAULT NULL, 
          semester_id INT DEFAULT NULL, 
          level INT UNSIGNED DEFAULT 0 NOT NULL, 
          studies_required TINYINT(1) DEFAULT \'0\' NOT NULL, 
          allowed_domains LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', 
          name VARCHAR(255) NOT NULL, 
          description LONGTEXT DEFAULT NULL, 
          website VARCHAR(255) DEFAULT NULL, 
          logo_name VARCHAR(255) DEFAULT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          activated TINYINT(1) DEFAULT \'1\' NOT NULL, 
          nbMentors INT UNSIGNED DEFAULT 1 NOT NULL, 
          nbMentees INT UNSIGNED DEFAULT 1 NOT NULL, 
          first_semester_start DATE NOT NULL, 
          first_semester_end DATE NOT NULL, 
          second_semester_start DATE NOT NULL, 
          second_semester_end DATE NOT NULL, 
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL, 
          matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL, 
          matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL, 
          matching_city_value INT UNSIGNED DEFAULT 5 NOT NULL,  
          matching_language_value INT UNSIGNED DEFAULT 6 NOT NULL, 
          matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL, 
          matching_institute_values LONGTEXT DEFAULT \'[10]\' NOT NULL COMMENT \'(DC2Type:json)\', 
          matching_level_of_study_value INT UNSIGNED DEFAULT 3 NOT NULL,
          revival_delay INT UNSIGNED DEFAULT 7 NOT NULL, 
          unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL, 
          schac_id VARCHAR(255) DEFAULT NULL,
          INDEX IDX_CA55B5D0727ACA70 (parent_id), 
          INDEX IDX_CA55B5D0BC12F48A (duplicate_id), 
          INDEX IDX_CA55B5D079066886 (root_id), 
          INDEX IDX_CA55B5D04A798B6F (semester_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute_city (
          institute_id INT NOT NULL, 
          city_id INT NOT NULL, 
          INDEX IDX_88456041697B0F4C (institute_id), 
          INDEX IDX_884560418BAC62AF (city_id), 
          PRIMARY KEY(institute_id, city_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute_hobby (
          institute_id INT NOT NULL, 
          hobby_id INT NOT NULL, 
          INDEX IDX_19AE68E6697B0F4C (institute_id), 
          INDEX IDX_19AE68E6322B2123 (hobby_id), 
          PRIMARY KEY(institute_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute_motivation (
          institute_id INT NOT NULL, 
          motivation_id INT NOT NULL, 
          INDEX IDX_37087495697B0F4C (institute_id), 
          INDEX IDX_370874958EDBCD4E (motivation_id), 
          PRIMARY KEY(institute_id, motivation_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association (
          id INT AUTO_INCREMENT NOT NULL, 
          semester_id INT DEFAULT NULL, 
          facebook VARCHAR(255) DEFAULT NULL, 
          twitter VARCHAR(255) DEFAULT NULL, 
          instagram VARCHAR(255) DEFAULT NULL, 
          name VARCHAR(255) NOT NULL, 
          description LONGTEXT DEFAULT NULL, 
          website VARCHAR(255) DEFAULT NULL, 
          logo_name VARCHAR(255) DEFAULT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          activated TINYINT(1) DEFAULT \'1\' NOT NULL, 
          nbMentors INT UNSIGNED DEFAULT 1 NOT NULL, 
          nbMentees INT UNSIGNED DEFAULT 1 NOT NULL, 
          first_semester_start DATE NOT NULL, 
          first_semester_end DATE NOT NULL, 
          second_semester_start DATE NOT NULL, 
          second_semester_end DATE NOT NULL, 
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL, 
          matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL, 
          matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL, 
          matching_city_value INT UNSIGNED DEFAULT 5 NOT NULL,  
          matching_language_value INT UNSIGNED DEFAULT 6 NOT NULL, 
          matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL, 
          matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL,
          matching_institute_values LONGTEXT DEFAULT \'[10]\' NOT NULL COMMENT \'(DC2Type:json)\', 
          matching_level_of_study_value INT UNSIGNED DEFAULT 3 NOT NULL, 
          revival_delay INT UNSIGNED DEFAULT 7 NOT NULL, 
          unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL, 
          INDEX IDX_FD8521CC4A798B6F (semester_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_city (
          association_id INT NOT NULL, 
          city_id INT NOT NULL, 
          INDEX IDX_2F9F935EFB9C8A5 (association_id), 
          INDEX IDX_2F9F9358BAC62AF (city_id), 
          PRIMARY KEY(association_id, city_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_hobby (
          association_id INT NOT NULL, 
          hobby_id INT NOT NULL, 
          INDEX IDX_4E4C615AEFB9C8A5 (association_id), 
          INDEX IDX_4E4C615A322B2123 (hobby_id), 
          PRIMARY KEY(association_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE association_motivation (
          association_id INT NOT NULL, 
          motivation_id INT NOT NULL, 
          INDEX IDX_C0AB1157EFB9C8A5 (association_id), 
          INDEX IDX_C0AB11578EDBCD4E (motivation_id), 
          PRIMARY KEY(association_id, motivation_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE institute_institute (
          institute_source INT NOT NULL, 
          institute_target INT NOT NULL, 
          INDEX IDX_AF802331AE7E57AA (institute_source), 
          INDEX IDX_AF802331B79B0725 (institute_target), 
          PRIMARY KEY(
            institute_source, institute_target
          )
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331AE7E57AA FOREIGN KEY (institute_source) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_institute 
        ADD 
          CONSTRAINT FK_AF802331B79B0725 FOREIGN KEY (institute_target) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute 
        ADD 
          CONSTRAINT FK_CA55B5D0727ACA70 FOREIGN KEY (parent_id) REFERENCES institute (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE 
          institute 
        ADD 
          CONSTRAINT FK_CA55B5D0BC12F48A FOREIGN KEY (duplicate_id) REFERENCES institute (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE 
          institute 
        ADD 
          CONSTRAINT FK_CA55B5D079066886 FOREIGN KEY (root_id) REFERENCES institute (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE 
          institute 
        ADD 
          CONSTRAINT FK_CA55B5D04A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          institute_city 
        ADD 
          CONSTRAINT FK_88456041697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_city 
        ADD 
          CONSTRAINT FK_884560418BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_hobby 
        ADD 
          CONSTRAINT FK_19AE68E6697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_hobby 
        ADD 
          CONSTRAINT FK_19AE68E6322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_motivation 
        ADD 
          CONSTRAINT FK_37087495697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_motivation 
        ADD 
          CONSTRAINT FK_370874958EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association 
        ADD 
          CONSTRAINT FK_FD8521CC4A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          association_city 
        ADD 
          CONSTRAINT FK_2F9F935EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_city 
        ADD 
          CONSTRAINT FK_2F9F9358BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_hobby 
        ADD 
          CONSTRAINT FK_4E4C615AEFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_hobby 
        ADD 
          CONSTRAINT FK_4E4C615A322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_motivation 
        ADD 
          CONSTRAINT FK_C0AB1157EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_motivation 
        ADD 
          CONSTRAINT FK_C0AB11578EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649F4AD20B6');
        $this->addSql('DROP INDEX IDX_8D93D649FE12B287 ON user');
        $this->addSql('DROP INDEX IDX_8D93D649F4AD20B6 ON user');

        $this->addSql('INSERT INTO institute (`id`, `name`, `description`, `logo_name`, `allowed_domains`, `website`, `level`, `created_at`, `updated_at`, `activated`, `semester_id`, `first_semester_start`, `first_semester_end`, `second_semester_start`, `second_semester_end`, `nb_weeks_pre_opening`, `matching_hobby_value`, `matching_study_value`, `matching_city_value`, `matching_institute_values`, `matching_language_value`, `matching_age_value`, `matching_motivation_value`, `matching_availability_value`, `revival_delay`, `unmatch_delay`, `nbMentors`, `nbMentees`) Select id, name, description, logo_name, allowed_domains, website, level, created_at, updated_at, activated, current_semester_id, first_semester_start, first_semester_end, second_semester_start, second_semester_end, nb_weeks_pre_opening, matching_hobby_value, matching_study_value, matching_city_value, concat("[", matching_institute_value,",", matching_campus_value,",", matching_university_value,"]"), matching_language_value, matching_age_value, matching_motivation_value, matching_availability_value, revival_delay, unmatch_delay, nbMentors, nbMentees from matching_manager where type = "institute"');
        $this->addSql('INSERT INTO association (`id`, `name`, `description`, `logo_name`, `website`, `created_at`, `updated_at`, `activated`, `semester_id`, `first_semester_start`, `first_semester_end`, `second_semester_start`, `second_semester_end`, `nb_weeks_pre_opening`, `matching_hobby_value`, `matching_study_value`, `matching_city_value`, `matching_institute_values`, `matching_language_value`, `matching_age_value`, `matching_motivation_value`, `matching_availability_value`, `revival_delay`, `unmatch_delay`, `nbMentors`, `nbMentees`) Select id, name, description, logo_name, website, created_at, updated_at, activated, current_semester_id, first_semester_start, first_semester_end, second_semester_start, second_semester_end, nb_weeks_pre_opening, matching_hobby_value, matching_study_value, matching_city_value, concat("[", matching_institute_value,",", matching_campus_value,",", matching_university_value,"]"), matching_language_value, matching_age_value, matching_motivation_value, matching_availability_value, revival_delay, unmatch_delay, nbMentors, nbMentees from matching_manager where type = "association"');

        $this->addSql('ALTER TABLE user 
            ADD
                association_id INT DEFAULT NULL,
            CHANGE 
                current_semester_id semester_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D6494A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          level_of_study ENUM(
            \'bachelor\', \'master\', \'doctorate\', 
            \'alumni\', \'other\'
          ) DEFAULT NULL COMMENT \'(DC2Type:UserLevelStudyType)\', 
        ADD 
          level_of_study_other LONGTEXT DEFAULT NULL,
        ADD 
          type_of_mobility ENUM(
            \'erasmus\', \'out_of_europe\', \'free_mover\', 
            \'digital_mobility\'
          ) DEFAULT NULL COMMENT \'(DC2Type:UserTypeMobilityType)\'');
        $this->addSql('CREATE INDEX IDX_8D93D649EFB9C8A5 ON user (association_id)');
        $this->addSql('CREATE INDEX IDX_8D93D6494A798B6F ON user (semester_id)');
        $this->addSql('ALTER TABLE institute_study DROP FOREIGN KEY FK_C6B50C98697B0F4C');
        $this->addSql('ALTER TABLE 
          institute_study 
        ADD 
          CONSTRAINT FK_C6B50C98697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_1EA78759FE12B287 ON buddy');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          association_id INT DEFAULT NULL, 
        ADD
          institute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_1EA78759697B0F4C ON buddy (institute_id)');
        $this->addSql('CREATE INDEX IDX_1EA78759EFB9C8A5 ON buddy (association_id)');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          can_match TINYINT(1) NOT NULL, 
        ADD 
          rights LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('DROP INDEX IDX_EAC2F091FE12B287 ON week_stat');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          association_id INT DEFAULT NULL, 
        ADD
          institute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F091697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F091EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('CREATE INDEX IDX_EAC2F091697B0F4C ON week_stat (institute_id)');
        $this->addSql('CREATE INDEX IDX_EAC2F091EFB9C8A5 ON week_stat (association_id)');

        $this->addSql('INSERT INTO institute_city (`institute_id`, `city_id`) Select mmc.matching_manager_id, mmc.city_id from matching_manager_city mmc inner join matching_manager mm on mm.id = mmc.matching_manager_id where mm.type = "institute"');
        $this->addSql('INSERT INTO association_city (`association_id`, `city_id`) Select mmc.matching_manager_id, mmc.city_id from matching_manager_city mmc inner join matching_manager mm on mm.id = mmc.matching_manager_id where mm.type = "association"');
        $this->addSql('INSERT INTO institute_hobby (`institute_id`, `hobby_id`) Select mmh.matching_manager_id, mmh.hobby_id from matching_manager_hobby mmh inner join matching_manager mm on mm.id = mmh.matching_manager_id where mm.type = "institute"');
        $this->addSql('INSERT INTO association_hobby (`association_id`, `hobby_id`) Select mmh.matching_manager_id, mmh.hobby_id from matching_manager_hobby mmh inner join matching_manager mm on mm.id = mmh.matching_manager_id where mm.type = "association"');
        $this->addSql('INSERT INTO institute_motivation (`institute_id`, `motivation_id`) Select mmm.matching_manager_id, mmm.motivation_id from matching_manager_motivation mmm inner join matching_manager mm on mm.id = mmm.matching_manager_id where mm.type = "institute"');
        $this->addSql('INSERT INTO association_motivation (`association_id`, `motivation_id`) Select mmm.matching_manager_id, mmm.motivation_id from matching_manager_motivation mmm inner join matching_manager mm on mm.id = mmm.matching_manager_id where mm.type = "association"');
        $this->addSql('update buddy b set b.institute_id = b.matching_manager_id where b.matching_manager_id = (select mm.id from matching_manager mm where mm.id = b.matching_manager_id and mm.type = "institute")');
        $this->addSql('update buddy b set b.association_id = b.matching_manager_id where b.matching_manager_id = (select mm.id from matching_manager mm where mm.id = b.matching_manager_id and mm.type = "association")');
        $this->addSql('update week_stat ws set ws.institute_id = ws.matching_manager_id where ws.matching_manager_id = (select mm.id from matching_manager mm where mm.id = ws.matching_manager_id and mm.type = "institute")');
        $this->addSql('update week_stat ws set ws.association_id = ws.matching_manager_id where ws.matching_manager_id = (select mm.id from matching_manager mm where mm.id = ws.matching_manager_id and mm.type = "association")');
        $this->addSql('update user u set u.institute_id = u.matching_manager_id where u.matching_manager_id = (select mm.id from matching_manager mm where mm.id = u.matching_manager_id and mm.type = "institute")');
        $this->addSql('update user u set u.association_id = u.matching_manager_id where u.matching_manager_id = (select mm.id from matching_manager mm where mm.id = u.matching_manager_id and mm.type = "association")');
        $this->addSql('update user set institute_id = null where roles like "%ADMIN%";');
        $this->addSql('update user set semester_id = null where roles like "%ADMIN%" or roles like "%BUDDYCOORDINATOR%" or roles like "%INSTITUTE_MANAGER%"');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649697B0F4C');
        $this->addSql('ALTER TABLE institute DROP FOREIGN KEY FK_CA55B5D0727ACA70');
        $this->addSql('ALTER TABLE institute DROP FOREIGN KEY FK_CA55B5D0BC12F48A');
        $this->addSql('ALTER TABLE institute DROP FOREIGN KEY FK_CA55B5D079066886');
        $this->addSql('ALTER TABLE institute DROP FOREIGN KEY FK_CA55B5D035E47E35');
        $this->addSql('ALTER TABLE institute_study DROP FOREIGN KEY FK_C6B50C98697B0F4C');
        $this->addSql('ALTER TABLE institute_city DROP FOREIGN KEY FK_88456041697B0F4C');
        $this->addSql('ALTER TABLE institute_hobby DROP FOREIGN KEY FK_19AE68E6697B0F4C');
        $this->addSql('ALTER TABLE institute_motivation DROP FOREIGN KEY FK_37087495697B0F4C');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759697B0F4C');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5697B0F4C');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F091697B0F4C');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649EFB9C8A5');
        $this->addSql('ALTER TABLE association_city DROP FOREIGN KEY FK_2F9F935EFB9C8A5');
        $this->addSql('ALTER TABLE association_hobby DROP FOREIGN KEY FK_4E4C615AEFB9C8A5');
        $this->addSql('ALTER TABLE association_motivation DROP FOREIGN KEY FK_C0AB1157EFB9C8A5');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759EFB9C8A5');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5EFB9C8A5');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F091EFB9C8A5');
        $this->addSql('ALTER TABLE 
          association_institute_right 
        ADD 
          CONSTRAINT FK_3136C203783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_B779701C52715B0D FOREIGN KEY (association_institute_right_id) REFERENCES association_institute_right (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          association_institute_right_institute 
        ADD 
          CONSTRAINT FK_B779701C697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          association_institute_right 
        ADD 
          CONSTRAINT FK_3136C203D4ED69C1 FOREIGN KEY (association_institute_id) REFERENCES association_institute (id)');
        $this->addSql('ALTER TABLE
          user_institute
        ADD
          CONSTRAINT FK_CAB53834697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          week_stat_institute
        ADD
          CONSTRAINT FK_A0058D93697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE institute');
        $this->addSql('DROP TABLE institute_city');
        $this->addSql('DROP TABLE institute_hobby');
        $this->addSql('DROP TABLE institute_motivation');
        $this->addSql('DROP TABLE association');
        $this->addSql('DROP TABLE association_city');
        $this->addSql('DROP TABLE association_hobby');
        $this->addSql('DROP TABLE association_motivation');
        $this->addSql('DROP TABLE institute_institute');
        $this->addSql('ALTER TABLE association_institute DROP can_match, DROP rights');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5EFB9C8A5 FOREIGN KEY (association_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('DROP INDEX IDX_1EA78759697B0F4C ON buddy');
        $this->addSql('DROP INDEX IDX_1EA78759EFB9C8A5 ON buddy');
        $this->addSql('ALTER TABLE 
          buddy 
        DROP 
          institute_id, 
        DROP 
          association_id');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_1EA78759FE12B287 ON buddy (matching_manager_id)');
        $this->addSql('ALTER TABLE 
          institute_study 
        ADD 
          CONSTRAINT FK_C6B50C98697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6494A798B6F');
        $this->addSql('DROP INDEX IDX_8D93D649EFB9C8A5 ON user');
        $this->addSql('DROP INDEX IDX_8D93D6494A798B6F ON user');
        $this->addSql('ALTER TABLE user 
            DROP
                association_id,
            CHANGE 
                semester_id current_semester_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649F4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE 
          user 
        DROP 
          level_of_study, 
        DROP 
          level_of_study_other,
        DROP
            type_of_mobility');
        $this->addSql('CREATE INDEX IDX_8D93D649FE12B287 ON user (matching_manager_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649F4AD20B6 ON user (current_semester_id)');
        $this->addSql('DROP INDEX IDX_EAC2F091697B0F4C ON week_stat');
        $this->addSql('DROP INDEX IDX_EAC2F091EFB9C8A5 ON week_stat');
        $this->addSql('ALTER TABLE 
          week_stat 
        DROP 
          institute_id, 
        DROP 
          association_id');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F091FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_EAC2F091FE12B287 ON week_stat (matching_manager_id)');
    }
}
