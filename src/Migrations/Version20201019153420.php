<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201019153420 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE report ADD user_id INT NOT NULL');
        $this->addSql('UPDATE report SET user_id = mentor_id');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F77845C3E47C3');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784DB403044');
        $this->addSql('DROP INDEX IDX_C42F7784DB403044 ON report');
        $this->addSql('DROP INDEX IDX_C42F77845C3E47C3 ON report');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C42F7784A76ED395 ON report (user_id)');
        $this->addSql('ALTER TABLE report DROP mentor_id, DROP mentee_id');
        $this->addSql('ALTER TABLE report DROP INDEX UNIQ_C42F7784395CE8D6, ADD INDEX IDX_C42F7784395CE8D6 (buddy_id)');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784395CE8D6');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784A76ED395');
        $this->addSql('ALTER TABLE report CHANGE buddy_id buddy_id INT NOT NULL');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784A76ED395');
        $this->addSql('DROP INDEX IDX_C42F7784A76ED395 ON report');
        $this->addSql('ALTER TABLE report ADD mentee_id INT NOT NULL, CHANGE user_id mentor_id INT NOT NULL');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F77845C3E47C3 FOREIGN KEY (mentee_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784DB403044 FOREIGN KEY (mentor_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_C42F7784DB403044 ON report (mentor_id)');
        $this->addSql('CREATE INDEX IDX_C42F77845C3E47C3 ON report (mentee_id)');
        $this->addSql('ALTER TABLE
          report
        DROP
          INDEX IDX_C42F7784395CE8D6,
        ADD
          UNIQUE INDEX UNIQ_C42F7784395CE8D6 (buddy_id)');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784A76ED395');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784395CE8D6');
        $this->addSql('ALTER TABLE report CHANGE buddy_id buddy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE
          report
        ADD
          CONSTRAINT FK_C42F7784395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id)');
    }
}
