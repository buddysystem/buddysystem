<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191107100352 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE week_stat_institute (
          week_stat_id INT UNSIGNED NOT NULL, 
          institute_id INT NOT NULL, 
          INDEX IDX_A0058D93FB46553C (week_stat_id), 
          INDEX IDX_A0058D93697B0F4C (institute_id), 
          PRIMARY KEY(week_stat_id, institute_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          week_stat_institute 
        ADD 
          CONSTRAINT FK_A0058D93FB46553C FOREIGN KEY (week_stat_id) REFERENCES week_stat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          week_stat_institute 
        ADD 
          CONSTRAINT FK_A0058D93697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE week_stat_institute');
    }
}
