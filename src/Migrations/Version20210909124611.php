<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210909124611 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE registration_email_domain (
          id INT AUTO_INCREMENT NOT NULL,
          institute_id INT DEFAULT NULL,
          user_type ENUM(\'both\', \'mentors\', \'mentees\') NOT NULL COMMENT \'(DC2Type:RegistrationEmailDomainUserTypeType)\',
          domain VARCHAR(255) NOT NULL,
          INDEX IDX_7CCD153F697B0F4C (institute_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE
          registration_email_domain
        ADD
          CONSTRAINT FK_7CCD153F697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE registration_email_domain');
    }
}
