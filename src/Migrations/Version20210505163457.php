<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210505163457 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE
          user
        ADD
          languages LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\',
        ADD
          languages_wanted LONGTEXT DEFAULT \'[]\' NOT NULL COMMENT \'(DC2Type:json)\'');

        $this->addSql('update user u set languages = 
                    (SELECT COALESCE(CONCAT(\'[\',GROUP_CONCAT(CONCAT(\'"\', l.code, \'"\')), \']\'), \'[]\')
                     from language l 
                     inner join user_language ul on ul.language_id = l.id
                     where u.id = ul.user_id)');

        $this->addSql('update user u set languages_wanted = 
                    (SELECT COALESCE(CONCAT(\'[\',GROUP_CONCAT(CONCAT(\'"\', l.code, \'"\')), \']\'), \'[]\')
                     from language l 
                     inner join user_language_wanted ulw on ulw.language_id = l.id
                     where u.id = ulw.user_id)');
        $this->addSql('ALTER TABLE user_language DROP FOREIGN KEY FK_345695B582F1BAF4');
        $this->addSql('ALTER TABLE user_language_wanted DROP FOREIGN KEY FK_D72CEDD182F1BAF4');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE user_language');
        $this->addSql('DROP TABLE user_language_wanted');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP languages, DROP languages_wanted');
        $this->addSql('CREATE TABLE language (
          id INT AUTO_INCREMENT NOT NULL,
          name VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`,
          code VARCHAR(3) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE user_language (
          user_id INT NOT NULL,
          language_id INT NOT NULL,
          INDEX IDX_345695B5A76ED395 (user_id),
          INDEX IDX_345695B582F1BAF4 (language_id),
          PRIMARY KEY(user_id, language_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE user_language_wanted (
          user_id INT NOT NULL,
          language_id INT NOT NULL,
          INDEX IDX_D72CEDD1A76ED395 (user_id),
          INDEX IDX_D72CEDD182F1BAF4 (language_id),
          PRIMARY KEY(user_id, language_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE
          user_language
        ADD
          CONSTRAINT FK_345695B582F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_language
        ADD
          CONSTRAINT FK_345695B5A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_language_wanted
        ADD
          CONSTRAINT FK_D72CEDD182F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_language_wanted
        ADD
          CONSTRAINT FK_D72CEDD1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
    }
}
