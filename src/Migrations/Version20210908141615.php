<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210908141615 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user_history ADD institute_id INT DEFAULT NULL, ADD local TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE
          user_history
        ADD
          CONSTRAINT FK_7FB76E41697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id) ON DELETE
        SET
          NULL');
        $this->addSql('CREATE INDEX IDX_7FB76E41697B0F4C ON user_history (institute_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user_history DROP FOREIGN KEY FK_7FB76E41697B0F4C');
        $this->addSql('DROP INDEX IDX_7FB76E41697B0F4C ON user_history');
        $this->addSql('ALTER TABLE user_history DROP institute_id, DROP local');
    }
}
