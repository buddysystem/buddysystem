<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190301141110 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_notification (id INT AUTO_INCREMENT NOT NULL, new_message TINYINT(1) DEFAULT \'1\' NOT NULL, new_message_sent_at DATETIME DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE user ADD user_notification_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649FDC6F10B FOREIGN KEY (user_notification_id) REFERENCES user_notification (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649FDC6F10B ON user (user_notification_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649FDC6F10B');
        $this->addSql('DROP TABLE user_notification');
        $this->addSql('DROP INDEX UNIQ_8D93D649FDC6F10B ON user');
        $this->addSql('ALTER TABLE user DROP user_notification_id');
    }
}
