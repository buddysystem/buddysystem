<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190108104402 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy ADD archive TINYINT(1) NOT NULL, CHANGE reason_refusal reason_refusal ENUM(\'time\', \'buddy\', \'other\') DEFAULT NULL COMMENT \'(DC2Type:BuddyRefusedType)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP archive, CHANGE reason_refusal reason_refusal VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
