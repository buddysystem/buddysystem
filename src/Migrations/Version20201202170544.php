<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201202170544 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association ADD nb_buddies_refused_max INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE ext_translations CHANGE object_class object_class VARCHAR(191) NOT NULL');
        $this->addSql('ALTER TABLE institute ADD nb_buddies_refused_max INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE user ADD nb_refused_by_buddies INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE user_history ADD nb_refused_by_buddies INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE user ADD override_max_refused_limitation TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP nb_buddies_refused_max');
        $this->addSql('ALTER TABLE
          ext_translations
        CHANGE
          object_class object_class VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_general_ci`');
        $this->addSql('ALTER TABLE institute DROP nb_buddies_refused_max');
        $this->addSql('ALTER TABLE user DROP nb_refused_by_buddies');
        $this->addSql('ALTER TABLE user_history DROP nb_refused_by_buddies');
        $this->addSql('ALTER TABLE user DROP override_max_refused_limitation');
    }
}
