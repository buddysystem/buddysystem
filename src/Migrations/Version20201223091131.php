<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201223091131 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83395CE8D6');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE thread DROP FOREIGN KEY FK_31204C83395CE8D6');
        $this->addSql('ALTER TABLE
          thread
        ADD
          CONSTRAINT FK_31204C83395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id)');
    }
}
