<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200207140212 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          translatable_locales LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:simple_array)\', 
        DROP 
          semester');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          semester VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, 
        DROP 
          translatable_locales');
    }
}
