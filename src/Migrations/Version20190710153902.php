<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190710153902 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE institute_study (
          institute_id INT NOT NULL, 
          study_id INT NOT NULL, 
          INDEX IDX_C6B50C98697B0F4C (institute_id), 
          INDEX IDX_C6B50C98E7B003E9 (study_id), 
          PRIMARY KEY(institute_id, study_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE study (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          institute_study 
        ADD 
          CONSTRAINT FK_C6B50C98697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          institute_study 
        ADD 
          CONSTRAINT FK_C6B50C98E7B003E9 FOREIGN KEY (study_id) REFERENCES study (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matching_manager ADD matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL');

        $this->addSql('CREATE TABLE user_study (
          user_id INT NOT NULL, 
          study_id INT NOT NULL, 
          INDEX IDX_4BD6C11A76ED395 (user_id), 
          INDEX IDX_4BD6C11E7B003E9 (study_id), 
          PRIMARY KEY(user_id, study_id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          user_study 
        ADD 
          CONSTRAINT FK_4BD6C11A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          user_study 
        ADD 
          CONSTRAINT FK_4BD6C11E7B003E9 FOREIGN KEY (study_id) REFERENCES study (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matching_manager ADD studies_required TINYINT(1) DEFAULT \'0\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE institute_study DROP FOREIGN KEY FK_C6B50C98E7B003E9');
        $this->addSql('DROP TABLE institute_study');
        $this->addSql('DROP TABLE study');
        $this->addSql('DROP TABLE user_study');
        $this->addSql('ALTER TABLE matching_manager DROP matching_study_value');
        $this->addSql('ALTER TABLE matching_manager DROP studies_required');
    }
}
