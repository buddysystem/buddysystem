<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181212140635 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE section_hobby ADD CONSTRAINT FK_AE339453D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE section_hobby ADD CONSTRAINT FK_AE339453322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE section_motivation ADD CONSTRAINT FK_7DD04932D823E37A FOREIGN KEY (section_id) REFERENCES section (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE section_motivation ADD CONSTRAINT FK_7DD049328EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE section_hobby DROP FOREIGN KEY FK_AE339453D823E37A');
        $this->addSql('ALTER TABLE section_hobby DROP FOREIGN KEY FK_AE339453322B2123');
        $this->addSql('ALTER TABLE section_motivation DROP FOREIGN KEY FK_7DD04932D823E37A');
        $this->addSql('ALTER TABLE section_motivation DROP FOREIGN KEY FK_7DD049328EDBCD4E');
    }
}
