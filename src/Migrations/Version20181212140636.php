<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181212140636 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, mentor_id INT NOT NULL, mentee_id INT NOT NULL, buddy_id INT DEFAULT NULL, comment VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_C42F7784DB403044 (mentor_id), INDEX IDX_C42F77845C3E47C3 (mentee_id), UNIQUE INDEX UNIQ_C42F7784395CE8D6 (buddy_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784DB403044 FOREIGN KEY (mentor_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F77845C3E47C3 FOREIGN KEY (mentee_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784395CE8D6 FOREIGN KEY (buddy_id) REFERENCES buddy (id)');
        $this->addSql('ALTER TABLE user ADD nb_report INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE nb_warned nb_warned INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE nb_buddies_removed nb_buddies_removed INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE nb_buddies_refused nb_buddies_refused INT UNSIGNED DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE section CHANGE matching_hobby_value matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL, CHANGE matching_university_value matching_university_value INT UNSIGNED DEFAULT 4 NOT NULL, CHANGE matching_language_value matching_language_value INT UNSIGNED DEFAULT 8 NOT NULL, CHANGE matching_age_value matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL, CHANGE matching_motivation_value matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL, CHANGE revival_delay revival_delay INT UNSIGNED DEFAULT 7 NOT NULL, CHANGE unmatch_delay unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL, CHANGE matching_availability_value matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE report');
        $this->addSql('ALTER TABLE section CHANGE matching_hobby_value matching_hobby_value INT DEFAULT 2 NOT NULL, CHANGE matching_university_value matching_university_value INT DEFAULT 4 NOT NULL, CHANGE matching_language_value matching_language_value INT DEFAULT 8 NOT NULL, CHANGE matching_age_value matching_age_value INT DEFAULT 8 NOT NULL, CHANGE matching_motivation_value matching_motivation_value INT DEFAULT 8 NOT NULL, CHANGE matching_availability_value matching_availability_value INT DEFAULT 1 NOT NULL, CHANGE revival_delay revival_delay INT DEFAULT 7 NOT NULL, CHANGE unmatch_delay unmatch_delay INT DEFAULT 14 NOT NULL');
        $this->addSql('ALTER TABLE user DROP nb_report, CHANGE nb_warned nb_warned INT DEFAULT 0 NOT NULL, CHANGE nb_buddies_removed nb_buddies_removed INT DEFAULT 0 NOT NULL, CHANGE nb_buddies_refused nb_buddies_refused INT DEFAULT 0 NOT NULL');
    }
}
