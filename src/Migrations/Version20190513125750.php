<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190513125750 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          current_semester_id INT DEFAULT NULL, 
        ADD 
          first_semester_start DATE DEFAULT NULL, 
        ADD 
          first_semester_end DATE DEFAULT NULL, 
        ADD 
          second_semester_start DATE DEFAULT NULL, 
        ADD 
          second_semester_end DATE DEFAULT NULL, 
        ADD 
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL');
        $this->addSql("UPDATE section SET first_semester_start = '2014-09-01', first_semester_end = '2014-01-01', second_semester_start = '2014-01-01', second_semester_end = '2014-07-01'");
        $this->addSql('ALTER TABLE
          section
        CHANGE 
          first_semester_start first_semester_start DATE NOT NULL, 
        CHANGE 
          first_semester_end first_semester_end DATE NOT NULL, 
        CHANGE 
          second_semester_start second_semester_start DATE NOT NULL, 
        CHANGE 
          second_semester_end second_semester_end DATE NOT NULL');
        $this->addSql('CREATE TABLE semester (
          id INT AUTO_INCREMENT NOT NULL, 
          section_id INT DEFAULT NULL, 
          period ENUM(
            \'first_semester\', \'second_semester\', 
            \'all_year\'
          ) NOT NULL COMMENT \'(DC2Type:SemesterPeriodType)\', 
          year VARCHAR(255) NOT NULL, 
          activated TINYINT(1) NOT NULL, 
          INDEX IDX_F7388EEDD823E37A (section_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          semester 
        ADD 
          CONSTRAINT FK_F7388EEDD823E37A FOREIGN KEY (section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          current_semester_id INT DEFAULT NULL, 
          CHANGE archive archived TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE 
          user 
        ADD 
          CONSTRAINT FK_8D93D649F4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE INDEX IDX_8D93D649F4AD20B6 ON user (current_semester_id)');
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          CONSTRAINT FK_2D737AEFF4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFF4AD20B6 ON section (current_semester_id)');
        $this->addSql('ALTER TABLE user_history ADD semester_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          user_history 
        ADD 
          CONSTRAINT FK_7FB76E414A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE INDEX IDX_7FB76E414A798B6F ON user_history (semester_id)');
        $this->addSql('ALTER TABLE buddy CHANGE archive archived TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE week_stat ADD semester_id INT DEFAULT NULL, DROP semester');
        $this->addSql('ALTER TABLE 
          week_stat 
        ADD 
          CONSTRAINT FK_EAC2F0914A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('CREATE INDEX IDX_EAC2F0914A798B6F ON week_stat (semester_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          section 
        DROP 
          current_semester_id, 
        DROP 
          first_semester_start, 
        DROP 
          first_semester_end, 
        DROP 
          second_semester_start, 
        DROP 
          second_semester_end, 
        DROP 
          nb_weeks_pre_opening');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649F4AD20B6');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFF4AD20B6');
        $this->addSql('ALTER TABLE user_history DROP FOREIGN KEY FK_7FB76E414A798B6F');
        $this->addSql('DROP TABLE semester');
        $this->addSql('DROP INDEX UNIQ_2D737AEFF4AD20B6 ON section');
        $this->addSql('DROP INDEX IDX_8D93D649F4AD20B6 ON user');
        $this->addSql('ALTER TABLE user DROP current_semester_id, CHANGE archived archive TINYINT(1) NOT NULL');
        $this->addSql('DROP INDEX IDX_7FB76E414A798B6F ON user_history');
        $this->addSql('ALTER TABLE user_history DROP semester_id');
        $this->addSql('ALTER TABLE buddy CHANGE archived archive TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE week_stat DROP FOREIGN KEY FK_EAC2F0914A798B6F');
        $this->addSql('DROP INDEX IDX_EAC2F0914A798B6F ON week_stat');
        $this->addSql('ALTER TABLE week_stat ADD semester INT UNSIGNED DEFAULT 1 NOT NULL, DROP semester_id');
    }
}
