<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181122163137 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'init db based on the version before doctrine migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE `buddy` (  `id` int(11) NOT NULL,`mentor_id` int(11) DEFAULT NULL,`mentee_id` int(11) DEFAULT NULL,`reason_refusal` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,`response_at` datetime DEFAULT NULL,`warned_at` datetime DEFAULT NULL,`created_at` datetime NOT NULL,`updated_at` datetime NOT NULL,`confirmed` int(11) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `city` (  `id` int(11) NOT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `ext_translations` (  `id` int(11) NOT NULL,`locale` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,`object_class` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`field` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,`foreign_key` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,`content` longtext COLLATE utf8mb4_unicode_ci) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC');
        $this->addSql('CREATE TABLE `hobby` (  `id` int(11) NOT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `language` (  `id` int(11) NOT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`code` tinytext COLLATE utf8mb4_unicode_ci NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `motivation` (  `id` int(11) NOT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `section` (  `id` int(11) NOT NULL,`city_id` int(11) DEFAULT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`description` longtext COLLATE utf8mb4_unicode_ci,`facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`nbMentors` int(11) DEFAULT NULL,`nbMentees` int(11) DEFAULT NULL,`mailing` tinyint(1) NOT NULL,`semester` int(11) DEFAULT NULL,`matching_hobby_value` int(11) NOT NULL DEFAULT \'2\',`matching_university_value` int(11) NOT NULL DEFAULT \'4\',`matching_language_value` int(11) NOT NULL DEFAULT \'8\',`matching_age_value` int(11) NOT NULL DEFAULT \'8\',`matching_motivation_value` int(11) NOT NULL DEFAULT \'8\',`activated` tinyint(1) NOT NULL,`logo_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`revival_delay` int(11) NOT NULL DEFAULT \'7\',`unmatch_delay` int(11) NOT NULL DEFAULT \'14\',`created_at` datetime NOT NULL,`updated_at` datetime NOT NULL,`emails` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `section_hobby` (  `section_id` int(11) NOT NULL,`hobby_id` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `section_motivation` (  `section_id` int(11) NOT NULL,`motivation_id` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `university` (  `id` int(11) NOT NULL,`section_id` int(11) DEFAULT NULL,`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `user` (  `id` int(11) NOT NULL,`motivation_id` int(11) DEFAULT NULL,`university_id` int(11) NOT NULL,`city_id` int(11) DEFAULT NULL,`section_id` int(11) DEFAULT NULL,`username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,`username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,`email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,`email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,`enabled` tinyint(1) NOT NULL,`salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`last_login` datetime DEFAULT NULL,`confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`password_requested_at` datetime DEFAULT NULL,`roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT \'(DC2Type: array)\',`firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`gender` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`sex_wanted` tinyint(1) DEFAULT NULL,`dateofbirthday` date DEFAULT NULL,`dateofarrival` date DEFAULT NULL,`dateofdepart` date DEFAULT NULL,`esncard` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`facebookid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`googleid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`termsOfUse` tinyint(1) NOT NULL,`newsletter` tinyint(1) DEFAULT NULL,`isMatched` tinyint(1) NOT NULL,`nb_buddy` int(11) DEFAULT NULL,`picture_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,`nb_warned` int(11) NOT NULL DEFAULT \'0\',`nb_buddies_removed` int(11) NOT NULL DEFAULT \'0\',`nb_buddies_refused` int(11) NOT NULL DEFAULT \'0\',`isLocal` tinyint(1) NOT NULL,`comment` longtext COLLATE utf8mb4_unicode_ci,`nb_buddy_wanted` int(11) NOT NULL,`archive` tinyint(1) NOT NULL,`semester` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,`created_at` datetime NOT NULL,`updated_at` datetime NOT NULL,`activities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `user_hobby` (  `user_id` int(11) NOT NULL,`hobby_id` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `user_language` (  `user_id` int(11) NOT NULL,`language_id` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('CREATE TABLE `user_language_wanted` (  `user_id` int(11) NOT NULL,`language_id` int(11) NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');
        $this->addSql('ALTER TABLE `buddy`ADD PRIMARY KEY (`id`),ADD KEY `IDX_1EA78759DB403044` (`mentor_id`),ADD KEY `IDX_1EA787595C3E47C3` (`mentee_id`)');
        $this->addSql('ALTER TABLE `city`ADD PRIMARY KEY (`id`),ADD UNIQUE KEY `UNIQ_2D5B02345E237E06` (`name`)');
        $this->addSql('ALTER TABLE `ext_translations`ADD PRIMARY KEY (`id`),ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),ADD KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)');
        $this->addSql('ALTER TABLE `hobby`ADD PRIMARY KEY (`id`)');
        $this->addSql('ALTER TABLE `language`ADD PRIMARY KEY (`id`)');
        $this->addSql('ALTER TABLE `motivation`ADD PRIMARY KEY (`id`),ADD UNIQUE KEY `UNIQ_E06073ED5E237E06` (`name`)');
        $this->addSql('ALTER TABLE `section`ADD PRIMARY KEY (`id`),ADD UNIQUE KEY `UNIQ_2D737AEF5E237E06` (`name`),ADD KEY `IDX_2D737AEF8BAC62AF` (`city_id`)');
        $this->addSql('ALTER TABLE `section_hobby`ADD PRIMARY KEY (`section_id`,`hobby_id`),ADD KEY `IDX_AE339453D823E37A` (`section_id`),ADD KEY `IDX_AE339453322B2123` (`hobby_id`)');
        $this->addSql('ALTER TABLE `section_motivation`ADD PRIMARY KEY (`section_id`,`motivation_id`),ADD KEY `IDX_7DD04932D823E37A` (`section_id`),ADD KEY `IDX_7DD049328EDBCD4E` (`motivation_id`)');
        $this->addSql('ALTER TABLE `university`ADD PRIMARY KEY (`id`),ADD KEY `IDX_9AB9BEEBD823E37A` (`section_id`)');
        $this->addSql('ALTER TABLE `user`ADD PRIMARY KEY (`id`),ADD UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),ADD UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),ADD UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`),ADD KEY `IDX_8D93D6498EDBCD4E` (`motivation_id`),ADD KEY `IDX_8D93D649309D1878` (`university_id`),ADD KEY `IDX_8D93D6498BAC62AF` (`city_id`),ADD KEY `IDX_8D93D649D823E37A` (`section_id`)');
        $this->addSql('ALTER TABLE `user_hobby`ADD PRIMARY KEY (`user_id`,`hobby_id`),ADD KEY `IDX_DBA6086FA76ED395` (`user_id`),ADD KEY `IDX_DBA6086F322B2123` (`hobby_id`)');
        $this->addSql('ALTER TABLE `user_language`ADD PRIMARY KEY (`user_id`,`language_id`),ADD KEY `IDX_345695B5A76ED395` (`user_id`),ADD KEY `IDX_345695B582F1BAF4` (`language_id`)');
        $this->addSql('ALTER TABLE `user_language_wanted`ADD PRIMARY KEY (`user_id`,`language_id`),ADD KEY `IDX_D72CEDD1A76ED395` (`user_id`),ADD KEY `IDX_D72CEDD182F1BAF4` (`language_id`)');
        $this->addSql('ALTER TABLE `buddy`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `city`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `ext_translations`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `hobby`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `language`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `motivation`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `section`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `university`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `user`MODIFY `id` int(11) NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE `buddy`ADD CONSTRAINT `FK_1EA787595C3E47C3` FOREIGN KEY (`mentee_id`) REFERENCES `user` (`id`),ADD CONSTRAINT `FK_1EA78759DB403044` FOREIGN KEY (`mentor_id`) REFERENCES `user` (`id`)');
        $this->addSql('ALTER TABLE `section`ADD CONSTRAINT `FK_2D737AEF8BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)');
        $this->addSql('ALTER TABLE `university`ADD CONSTRAINT `FK_9AB9BEEBD823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`)');
        $this->addSql('ALTER TABLE `user`ADD CONSTRAINT `FK_1A67AC92309D1878` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,ADD CONSTRAINT `FK_8D93D6498BAC62AF` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),ADD CONSTRAINT `FK_8D93D6498EDBCD4E` FOREIGN KEY (`motivation_id`) REFERENCES `motivation` (`id`),ADD CONSTRAINT `FK_1A67AC92D823E37A` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`)');
        $this->addSql('ALTER TABLE `user_hobby`ADD CONSTRAINT `FK_DBA6086F322B2123` FOREIGN KEY (`hobby_id`) REFERENCES `hobby` (`id`) ON DELETE CASCADE,ADD CONSTRAINT `FK_DBA6086FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `user_language`ADD CONSTRAINT `FK_345695B582F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE,ADD CONSTRAINT `FK_345695B5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `user_language_wanted`ADD CONSTRAINT `FK_D72CEDD182F1BAF4` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE,ADD CONSTRAINT `FK_D72CEDD1A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `buddy` DROP CONSTRAINT `FK_1EA787595C3E47C3`,DROP CONSTRAINT `FK_1EA78759DB403044`');
        $this->addSql('ALTER TABLE `section` DROP CONSTRAINT `FK_2D737AEF8BAC62AF`');
        $this->addSql('ALTER TABLE `university` DROP CONSTRAINT `FK_9AB9BEEBD823E37A`');
        $this->addSql('ALTER TABLE `user` DROP CONSTRAINT `FK_1A67AC92309D1878`,DROP CONSTRAINT `FK_8D93D6498BAC62AF`,DROP CONSTRAINT `FK_8D93D6498EDBCD4E`,DROP CONSTRAINT `FK_1A67AC92D823E37A`');
        $this->addSql('ALTER TABLE `user_hobby` DROP CONSTRAINT `FK_DBA6086F322B2123`,DROP CONSTRAINT `FK_DBA6086FA76ED395`');
        $this->addSql('ALTER TABLE `user_language` DROP CONSTRAINT `FK_345695B582F1BAF4`,DROP CONSTRAINT `FK_345695B5A76ED395`');
        $this->addSql('ALTER TABLE `user_language_wanted` DROP CONSTRAINT `FK_D72CEDD182F1BAF4`,DROP CONSTRAINT `FK_D72CEDD1A76ED395`');
        $this->addSql('ALTER TABLE `buddy` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `city` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `ext_translations` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `hobby` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `language` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `motivation` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `section` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `university` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `user` MODIFY `id` int(11) NOT NULL');
        $this->addSql('ALTER TABLE `buddy` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `city` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `ext_translations` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `hobby` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `language` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `motivation` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `section` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `section_hobby` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `section_motivation` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `university` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `user` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `user_hobby` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `user_language` DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE `user_language_wanted` DROP PRIMARY KEY');
        $this->addSql('DROP TABLE `buddy`');
        $this->addSql('DROP TABLE `city`');
        $this->addSql('DROP TABLE `ext_translations`');
        $this->addSql('DROP TABLE `hobby`');
        $this->addSql('DROP TABLE `language`');
        $this->addSql('DROP TABLE `motivation`');
        $this->addSql('DROP TABLE `section`');
        $this->addSql('DROP TABLE `section_hobby`');
        $this->addSql('DROP TABLE `section_motivation`');
        $this->addSql('DROP TABLE `university`');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE `user_hobby`');
        $this->addSql('DROP TABLE `user_language`');
        $this->addSql('DROP TABLE `user_language_wanted`');
    }
}
