<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190423145119 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP confirmed');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy ADD confirmed TINYINT(1) DEFAULT NULL');
    }
}
