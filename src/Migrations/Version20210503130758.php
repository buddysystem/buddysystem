<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503130758 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_EAC2F0914A798B6F');
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_EAC2F091697B0F4C');
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_EAC2F091EFB9C8A5');
        $this->addSql('DROP INDEX idx_eac2f091697b0f4c ON stat');
        $this->addSql('CREATE INDEX IDX_20B8FF21697B0F4C ON stat (institute_id)');
        $this->addSql('DROP INDEX idx_eac2f091efb9c8a5 ON stat');
        $this->addSql('CREATE INDEX IDX_20B8FF21EFB9C8A5 ON stat (association_id)');
        $this->addSql('DROP INDEX idx_eac2f0914a798b6f ON stat');
        $this->addSql('CREATE INDEX IDX_20B8FF214A798B6F ON stat (semester_id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_EAC2F0914A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_EAC2F091697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_EAC2F091EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_20B8FF21697B0F4C');
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_20B8FF21EFB9C8A5');
        $this->addSql('ALTER TABLE stat DROP FOREIGN KEY FK_20B8FF214A798B6F');
        $this->addSql('DROP INDEX idx_20b8ff21697b0f4c ON stat');
        $this->addSql('CREATE INDEX IDX_EAC2F091697B0F4C ON stat (institute_id)');
        $this->addSql('DROP INDEX idx_20b8ff21efb9c8a5 ON stat');
        $this->addSql('CREATE INDEX IDX_EAC2F091EFB9C8A5 ON stat (association_id)');
        $this->addSql('DROP INDEX idx_20b8ff214a798b6f ON stat');
        $this->addSql('CREATE INDEX IDX_EAC2F0914A798B6F ON stat (semester_id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_20B8FF21697B0F4C FOREIGN KEY (institute_id) REFERENCES institute (id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_20B8FF21EFB9C8A5 FOREIGN KEY (association_id) REFERENCES association (id)');
        $this->addSql('ALTER TABLE
          stat
        ADD
          CONSTRAINT FK_20B8FF214A798B6F FOREIGN KEY (semester_id) REFERENCES semester (id)');
    }
}
