<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200325124553 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA787595C3E47C3');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759DB403044');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA787595C3E47C3 FOREIGN KEY (mentee_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759DB403044 FOREIGN KEY (mentor_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA78759DB403044');
        $this->addSql('ALTER TABLE buddy DROP FOREIGN KEY FK_1EA787595C3E47C3');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA78759DB403044 FOREIGN KEY (mentor_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE 
          buddy 
        ADD 
          CONSTRAINT FK_1EA787595C3E47C3 FOREIGN KEY (mentee_id) REFERENCES user (id)');
    }
}
