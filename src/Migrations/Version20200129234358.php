<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200129234358 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE sessions MODIFY sess_lifetime INTEGER UNSIGNED NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE sessions');
        $this->addSql('ALTER TABLE sessions MODIFY sess_lifetime MEDIUMINT NOT NULL');
    }
}
