<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210506120606 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association ADD full_year_only TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE institute ADD full_year_only TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE
          association
        CHANGE
          first_semester_end first_semester_end DATE DEFAULT NULL,
        CHANGE
          second_semester_start second_semester_start DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE
          institute
        CHANGE
          first_semester_end first_semester_end DATE DEFAULT NULL,
        CHANGE
          second_semester_start second_semester_start DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association DROP full_year_only');
        $this->addSql('ALTER TABLE institute DROP full_year_only');
        $this->addSql('ALTER TABLE
          association
        CHANGE
          first_semester_end first_semester_end DATE NOT NULL,
        CHANGE
          second_semester_start second_semester_start DATE NOT NULL');
        $this->addSql('ALTER TABLE
          institute
        CHANGE
          first_semester_end first_semester_end DATE NOT NULL,
        CHANGE
          second_semester_start second_semester_start DATE NOT NULL');
    }
}
