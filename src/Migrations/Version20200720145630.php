<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200720145630 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY IF EXISTS FK_9AAF6E61D02919BD');
        $this->addSql('ALTER TABLE association_institute_right DROP FOREIGN KEY IF EXISTS FK_57269CFB783E3463');
        $this->addSql('ALTER TABLE association_institute_right_institute DROP FOREIGN KEY IF EXISTS FK_9AAF6E61697B0F4C');
        $this->addSql('ALTER TABLE institute_parent DROP FOREIGN KEY FK_CC7A4896AE7E57AA');
        $this->addSql('ALTER TABLE institute_parent DROP FOREIGN KEY FK_CC7A4896B79B0725');
        $this->addSql('ALTER TABLE matching_manager DROP FOREIGN KEY FK_7E2AE1061F29726C');
        $this->addSql('ALTER TABLE matching_manager DROP FOREIGN KEY FK_7E2AE10635E47E35');
        $this->addSql('ALTER TABLE matching_manager_city DROP FOREIGN KEY FK_159B369CFE12B287');
        $this->addSql('ALTER TABLE matching_manager_hobby DROP FOREIGN KEY FK_E15118D9FE12B287');
        $this->addSql('ALTER TABLE matching_manager_motivation DROP FOREIGN KEY FK_1DBBA406FE12B287');
        $this->addSql('DROP TABLE association_institute_right');
        $this->addSql('DROP TABLE association_institute_right_institute');
        $this->addSql('DROP TABLE institute_parent');
        $this->addSql('DROP TABLE matching_manager');
        $this->addSql('DROP TABLE matching_manager_city');
        $this->addSql('DROP TABLE matching_manager_hobby');
        $this->addSql('DROP TABLE matching_manager_motivation');
        $this->addSql('DROP TABLE user_institute');
        $this->addSql('DROP TABLE week_stat_institute');
        $this->addSql('ALTER TABLE buddy DROP matching_manager_id');
        $this->addSql('ALTER TABLE user DROP phone, DROP matching_manager_id');
        $this->addSql('ALTER TABLE week_stat DROP matching_manager_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE TABLE association_institute_right (
          id INT AUTO_INCREMENT NOT NULL,
          association_institute_id INT DEFAULT NULL,
          manager_id INT DEFAULT NULL,
          can_match TINYINT(1) NOT NULL,
          INDEX IDX_3136C203783E3463 (manager_id),
          INDEX IDX_57269CFBD4ED69C1 (association_institute_id),
          INDEX IDX_3136C203D4ED69C1 (association_institute_id),
          INDEX IDX_57269CFB783E3463 (manager_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE association_institute_right_institute (
          association_institute_right_id INT NOT NULL,
          institute_id INT NOT NULL,
          INDEX IDX_B779701C52715B0D (association_institute_right_id),
          INDEX IDX_9AAF6E61697B0F4C (institute_id),
          INDEX IDX_B779701C697B0F4C (institute_id),
          INDEX IDX_9AAF6E61D02919BD (association_institute_right_id),
          PRIMARY KEY(
            association_institute_right_id,
            institute_id
          )
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE institute_parent (
          institute_source INT NOT NULL,
          institute_target INT NOT NULL,
          INDEX IDX_CC7A4896AE7E57AA (institute_source),
          INDEX IDX_CC7A4896B79B0725 (institute_target),
          PRIMARY KEY(
            institute_source, institute_target
          )
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE matching_manager (
          id INT AUTO_INCREMENT NOT NULL,
          current_semester_id INT DEFAULT NULL,
          grandparent_id INT DEFAULT NULL,
          referent_id INT DEFAULT NULL,
          name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
          description LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          website VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          nbMentors INT UNSIGNED DEFAULT 1 NOT NULL,
          nbMentees INT UNSIGNED DEFAULT 1 NOT NULL,
          first_semester_start DATE NOT NULL,
          first_semester_end DATE NOT NULL,
          second_semester_start DATE NOT NULL,
          second_semester_end DATE NOT NULL,
          nb_weeks_pre_opening INT UNSIGNED DEFAULT 4 NOT NULL,
          matching_hobby_value INT UNSIGNED DEFAULT 2 NOT NULL,
          matching_institute_value INT UNSIGNED DEFAULT 10 NOT NULL,
          matching_language_value INT UNSIGNED DEFAULT 6 NOT NULL,
          matching_age_value INT UNSIGNED DEFAULT 8 NOT NULL,
          matching_motivation_value INT UNSIGNED DEFAULT 8 NOT NULL,
          matching_availability_value INT UNSIGNED DEFAULT 1 NOT NULL,
          activated TINYINT(1) NOT NULL,
          logo_name VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          revival_delay INT UNSIGNED DEFAULT 7 NOT NULL,
          unmatch_delay INT UNSIGNED DEFAULT 14 NOT NULL,
          created_at DATETIME NOT NULL,
          updated_at DATETIME NOT NULL,
          type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`,
          facebook VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          twitter VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          instagram VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
          level INT UNSIGNED DEFAULT 0,
          matching_study_value INT UNSIGNED DEFAULT 6 NOT NULL,
          studies_required TINYINT(1) DEFAULT \'0\',
          matching_city_value INT UNSIGNED DEFAULT 5 NOT NULL,
          allowed_domains LONGTEXT CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:simple_array)\',
          matching_campus_value INT UNSIGNED DEFAULT 0 NOT NULL,
          matching_university_value INT UNSIGNED DEFAULT 0 NOT NULL,
          INDEX IDX_7E2AE1061F29726C (grandparent_id),
          INDEX IDX_7E2AE10635E47E35 (referent_id),
          INDEX IDX_7E2AE106F4AD20B6 (current_semester_id),
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE matching_manager_city (
          matching_manager_id INT NOT NULL,
          city_id INT NOT NULL,
          INDEX IDX_159B369CFE12B287 (matching_manager_id),
          INDEX IDX_159B369C8BAC62AF (city_id),
          PRIMARY KEY(matching_manager_id, city_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE matching_manager_hobby (
          matching_manager_id INT NOT NULL,
          hobby_id INT NOT NULL,
          INDEX IDX_E15118D9FE12B287 (matching_manager_id),
          INDEX IDX_E15118D9322B2123 (hobby_id),
          PRIMARY KEY(matching_manager_id, hobby_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE matching_manager_motivation (
          matching_manager_id INT NOT NULL,
          motivation_id INT NOT NULL,
          INDEX IDX_1DBBA406FE12B287 (matching_manager_id),
          INDEX IDX_1DBBA4068EDBCD4E (motivation_id),
          PRIMARY KEY(
            matching_manager_id, motivation_id
          )
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE user_institute (
          user_id INT NOT NULL,
          institute_id INT NOT NULL,
          INDEX IDX_CAB53834A76ED395 (user_id),
          INDEX IDX_CAB53834697B0F4C (institute_id),
          PRIMARY KEY(user_id, institute_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('CREATE TABLE week_stat_institute (
          week_stat_id INT UNSIGNED NOT NULL,
          institute_id INT NOT NULL,
          INDEX IDX_A0058D93FB46553C (week_stat_id),
          INDEX IDX_A0058D93697B0F4C (institute_id),
          PRIMARY KEY(week_stat_id, institute_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\'');
        $this->addSql('ALTER TABLE
          association_institute_right
        ADD
          CONSTRAINT FK_57269CFB783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE
          association_institute_right
        ADD
          CONSTRAINT FK_57269CFBD4ED69C1 FOREIGN KEY (association_institute_id) REFERENCES association_institute (id)');
        $this->addSql('ALTER TABLE
          association_institute_right_institute
        ADD
          CONSTRAINT FK_9AAF6E61697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          association_institute_right_institute
        ADD
          CONSTRAINT FK_9AAF6E61D02919BD FOREIGN KEY (association_institute_right_id) REFERENCES association_institute_right (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          institute_parent
        ADD
          CONSTRAINT FK_CC7A4896AE7E57AA FOREIGN KEY (institute_source) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          institute_parent
        ADD
          CONSTRAINT FK_CC7A4896B79B0725 FOREIGN KEY (institute_target) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager
        ADD
          CONSTRAINT FK_7E2AE1061F29726C FOREIGN KEY (grandparent_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE
          matching_manager
        ADD
          CONSTRAINT FK_7E2AE10635E47E35 FOREIGN KEY (referent_id) REFERENCES matching_manager (id)');
        $this->addSql('ALTER TABLE
          matching_manager
        ADD
          CONSTRAINT FK_7E2AE106F4AD20B6 FOREIGN KEY (current_semester_id) REFERENCES semester (id)');
        $this->addSql('ALTER TABLE
          matching_manager_city
        ADD
          CONSTRAINT FK_159B369C8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager_city
        ADD
          CONSTRAINT FK_159B369CFE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager_hobby
        ADD
          CONSTRAINT FK_E15118D9322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager_hobby
        ADD
          CONSTRAINT FK_E15118D9FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager_motivation
        ADD
          CONSTRAINT FK_1DBBA4068EDBCD4E FOREIGN KEY (motivation_id) REFERENCES motivation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          matching_manager_motivation
        ADD
          CONSTRAINT FK_1DBBA406FE12B287 FOREIGN KEY (matching_manager_id) REFERENCES matching_manager (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          user_institute
        ADD
          CONSTRAINT FK_CAB53834A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE
          week_stat_institute
        ADD
          CONSTRAINT FK_A0058D93FB46553C FOREIGN KEY (week_stat_id) REFERENCES week_stat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE buddy ADD matching_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE
          user
        ADD
          phone VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`,
        ADD
          matching_manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE week_stat ADD matching_manager_id INT DEFAULT NULL');
    }
}
