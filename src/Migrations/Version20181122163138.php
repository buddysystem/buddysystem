<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181122163138 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_1A67AC92309D1878');
        $this->addSql('ALTER TABLE user CHANGE university_id university_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649309D1878 FOREIGN KEY (university_id) REFERENCES university (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE section ADD matching_availability_value INT DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649309D1878');
        $this->addSql('ALTER TABLE user CHANGE university_id university_id INT NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_1A67AC92309D1878 FOREIGN KEY (university_id) REFERENCES university (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE section DROP matching_availability_value');
    }
}
