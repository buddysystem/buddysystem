<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190919145825 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE user_approved_history (
          id INT AUTO_INCREMENT NOT NULL, 
          user_approved_id INT DEFAULT NULL, 
          user_validator_id INT DEFAULT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          UNIQUE INDEX UNIQ_7CDC8FB2674AB7BA (user_approved_id), 
          INDEX IDX_7CDC8FB230A836EE (user_validator_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          user_approved_history 
        ADD 
          CONSTRAINT FK_7CDC8FB2674AB7BA FOREIGN KEY (user_approved_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE 
          user_approved_history 
        ADD 
          CONSTRAINT FK_7CDC8FB230A836EE FOREIGN KEY (user_validator_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE user_approved_history');
    }
}
