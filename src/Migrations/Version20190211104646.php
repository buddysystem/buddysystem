<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190211104646 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE user CHANGE nb_buddy nb_buddies INT UNSIGNED DEFAULT 0 NOT NULL, CHANGE nb_buddy_wanted nb_buddies_wanted INT UNSIGNED DEFAULT 1 NOT NULL, DROP activities');
        $this->addSql('ALTER TABLE section DROP emails');
        $this->addSql('ALTER TABLE user_history ADD nb_buddies INT UNSIGNED DEFAULT 0 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE section ADD emails VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE user CHANGE nb_buddies nb_buddy INT DEFAULT NULL, CHANGE nb_buddies_wanted nb_buddy_wanted INT NOT NULL, ADD activities VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE user_history DROP nb_buddies');
    }
}
