<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20191004135605 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          buddy CHANGE mentor_id mentor_id INT NOT NULL, 
          CHANGE mentee_id mentee_id INT NOT NULL');
        $this->addSql('ALTER TABLE university ADD institute_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          university 
        ADD 
          CONSTRAINT FK_A07A85EC697B0F4C FOREIGN KEY (institute_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A07A85EC697B0F4C ON university (institute_id)');
        $this->addSql('ALTER TABLE section ADD association_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          section 
        ADD 
          CONSTRAINT FK_2D737AEFEFB9C8A5 FOREIGN KEY (association_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2D737AEFEFB9C8A5 ON section (association_id)');
        $this->addSql('ALTER TABLE association_institute ADD manager_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE 
          association_institute 
        ADD 
          CONSTRAINT FK_6EB2BCA5783E3463 FOREIGN KEY (manager_id) REFERENCES matching_manager (id)');
        $this->addSql('CREATE INDEX IDX_6EB2BCA5783E3463 ON association_institute (manager_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE 
          buddy CHANGE mentor_id mentor_id INT DEFAULT NULL, 
          CHANGE mentee_id mentee_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE university DROP FOREIGN KEY FK_A07A85EC697B0F4C');
        $this->addSql('DROP INDEX UNIQ_A07A85EC697B0F4C ON university');
        $this->addSql('ALTER TABLE university DROP institute_id');
        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEFEFB9C8A5');
        $this->addSql('DROP INDEX UNIQ_2D737AEFEFB9C8A5 ON section');
        $this->addSql('ALTER TABLE section DROP association_id');
        $this->addSql('ALTER TABLE association_institute DROP FOREIGN KEY FK_6EB2BCA5783E3463');
        $this->addSql('DROP INDEX IDX_6EB2BCA5783E3463 ON association_institute');
        $this->addSql('ALTER TABLE association_institute DROP manager_id');
    }
}
