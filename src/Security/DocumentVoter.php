<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Document;
use App\Entity\User;
use App\Enum\DocumentVoterEnum;
use App\Enum\UserVoterEnum;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class DocumentVoter extends Voter
{
    private $matchingManagerHelper;
    private $authorizationChecker;

    public function __construct(
        MatchingManagerHelper $matchingManagerHelper,
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->authorizationChecker = $authorizationChecker;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, DocumentVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Document) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case DocumentVoterEnum::EDIT:
                return $this->canEdit($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canEdit(TokenInterface $token, Document $document): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        return $this->authorizationChecker->isGranted(UserVoterEnum::IS_MANAGER, $user)
            && $this->matchingManagerHelper->getMatchingManagerOfManager($user) === $document->getMatchingManager()
        ;
    }
}
