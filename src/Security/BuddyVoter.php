<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Buddy;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Enum\BuddyVoterEnum;
use App\Enum\UserRoleEnum;
use App\Repository\BuddyRepository;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class BuddyVoter extends Voter
{
    private $grantedService;
    private $matchingManagerHelper;
    private $buddyRepo;

    public function __construct(
        GrantedService $grantedService,
        MatchingManagerHelper $matchingManagerHelper,
        BuddyRepository $buddyRepo
    ) {
        $this->grantedService = $grantedService;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->buddyRepo = $buddyRepo;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, BuddyVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Buddy) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case BuddyVoterEnum::CREATE:
                return $this->canCreate($token, $subject);
            case BuddyVoterEnum::SHOW:
                return $this->canShow($token, $subject);
            case BuddyVoterEnum::CONFIRM:
                return $this->canConfirm($token, $subject);
            case BuddyVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canCreate(TokenInterface $token, Buddy $buddy): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        $mentor = $buddy->getMentor();
        $mentee = $buddy->getMentee();

        $buddyExist = $this->buddyRepo->findOneBy(['mentor' => $mentor, 'mentee' => $mentee]);

        return !$buddyExist
            && (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || (
                $this->matchingManagerHelper->canManage($mentor, true)
                && !$mentor->hasMaxBuddiesRefused()
                && $this->matchingManagerHelper->canManage($mentee, true)
            )
            )
        ;
    }

    private function canShow(TokenInterface $token, Buddy $buddy): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
                || $this->matchingManagerHelper->canManage($buddy->getMentor())
                || $this->matchingManagerHelper->canManage($buddy->getMentee())
        ;
    }

    private function canConfirm(TokenInterface $token, Buddy $buddy): bool
    {
        return $buddy->getMentor() === $token->getUser() && BuddyStatusEnum::STATUS_PENDING === $buddy->getStatus();
    }

    private function canDelete(TokenInterface $token, Buddy $buddy): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || (
                !$buddy->isArchived()
                && (
                    $this->matchingManagerHelper->canManage($buddy->getMentor(), true)
                    || $this->matchingManagerHelper->canManage($buddy->getMentee(), true)
                )
            )
        ;
    }
}
