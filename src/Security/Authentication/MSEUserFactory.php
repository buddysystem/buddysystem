<?php

declare(strict_types=1);

namespace App\Security\Authentication;

use App\Entity\User;
use App\Enum\UserRegistrationTypeEnum;
use App\Services\LocaleManager;
use Hslavich\OneloginSamlBundle\Security\User\SamlUserFactoryInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MSEUserFactory implements SamlUserFactoryInterface
{
    private $localeManager;
    private $passwordHasher;

    public function __construct(LocaleManager $localeManager, UserPasswordHasherInterface $passwordHasher)
    {
        $this->localeManager = $localeManager;
        $this->passwordHasher = $passwordHasher;
    }

    public function createUser($username, array $attributes = []): UserInterface
    {
        $user = new User();
        $user->setPassword($this->passwordHasher->hashPassword($user, bin2hex(random_bytes(10))));
        $user->setEmail($attributes['mail'][0]);
        $user->setFirstname($attributes['givenName'][0]);
        $user->setLastname($attributes['sn'][0]);
        $user->setPrivacyAccepted(false);
        $user->setTermsOfUse(true);
        $user->setEnabled(true);
        $user->setApproved(true);
        $user->setLocaleLanguage($this->localeManager->getCurrentLocale());
        $user->setTypeOfRegistration(UserRegistrationTypeEnum::REGISTRATION_MSE);

        return $user;
    }
}
