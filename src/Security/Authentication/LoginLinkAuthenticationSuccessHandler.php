<?php

declare(strict_types=1);

namespace App\Security\Authentication;

use App\Entity\User;
use App\Event\ImplicitLoginEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class LoginLinkAuthenticationSuccessHandler implements AuthenticationSuccessHandlerInterface
{
    private $em;
    private $urlGenerator;
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $urlGenerator,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token): RedirectResponse
    {
        $loginEvent = new ImplicitLoginEvent($request, $token);
        $this->eventDispatcher->dispatch($loginEvent);

        /** @var User $user */
        $user = $token->getUser();

        if (!$user->isEnabled()) {
            $user->setEnabled(true);
            $this->em->flush();
        }

        return new RedirectResponse($this->urlGenerator->generate(LoginAuthenticator::TARGET_ROUTE));
    }
}
