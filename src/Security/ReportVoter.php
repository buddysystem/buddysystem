<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Report;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Enum\ReportVoterEnum;
use App\Enum\UserRoleEnum;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ReportVoter extends Voter
{
    private $grantedService;
    private $matchingManagerHelper;

    public function __construct(GrantedService $grantedService, MatchingManagerHelper $matchingManagerHelper)
    {
        $this->grantedService = $grantedService;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, ReportVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Report) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case ReportVoterEnum::CREATE:
                return $this->canCreate($token, $subject);
            case ReportVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canCreate(TokenInterface $token, Report $report): bool
    {
        $buddy = $report->getBuddy();
        $user = $token->getUser();

        return BuddyStatusEnum::STATUS_CONFIRMED === $buddy->getStatus()
            && ($user === $buddy->getMentee() || $user === $buddy->getMentor());
    }

    private function canDelete(TokenInterface $token, Report $report): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || $this->matchingManagerHelper->canManage($report->getBuddy()->getMentor(), true)
            || $this->matchingManagerHelper->canManage($report->getBuddy()->getMentee(), true)
        ;
    }
}
