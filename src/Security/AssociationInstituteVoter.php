<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\User;
use App\Enum\AssociationInstitutePermEnum;
use App\Enum\AssociationInstituteVoterEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationInstituteRepository;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AssociationInstituteVoter extends Voter
{
    private $grantedService;
    private $matchingManagerHelper;
    private $associationInstituteRepo;

    public function __construct(
        GrantedService $grantedService,
        MatchingManagerHelper $matchingManagerHelper,
        AssociationInstituteRepository $associationInstituteRepo
    ) {
        $this->grantedService = $grantedService;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->associationInstituteRepo = $associationInstituteRepo;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, AssociationInstituteVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof AssociationInstitute) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case AssociationInstituteVoterEnum::CHANGE_RIGHTS:
                return $this->canChangeRights($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canChangeRights(TokenInterface $token, AssociationInstitute $ai): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        $institute = $ai->getInstitute();
        $associationInstitutes = $institute->getAssociationInstitutes();
        $hasMultipleAssociations = $associationInstitutes->count() > 1;
        $associationInstitutesWithMatchingRight = $this->associationInstituteRepo->getWithRight(
            $institute,
            AssociationInstitutePermEnum::PERM_MATCH
        );

        if ($hasMultipleAssociations
            && !empty($associationInstitutesWithMatchingRight)
            && !\in_array($ai, $associationInstitutesWithMatchingRight)
        ) {
            return false;
        }

        if ($this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)) {
            return true;
        }

        if (UserRoleEnum::ROLE_INSTITUTE_MANAGER === $this->grantedService->getRole($currentUser)) {
            /** @var Institute $matchingManager */
            $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager();

            if ($matchingManager === $institute
                || $matchingManager->isParentOf($institute)
                || $matchingManager === $institute->getRoot()
            ) {
                return true;
            }
        }

        return false;
    }
}
