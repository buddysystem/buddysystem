<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Association;
use App\Entity\Institute;
use App\Entity\User;
use App\Enum\InstituteVoterEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationInstituteRepository;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class InstituteVoter extends Voter
{
    private $grantedService;
    private $matchingManagerHelper;
    private $associationInstituteRepo;

    public function __construct(
        GrantedService $grantedService,
        MatchingManagerHelper $matchingManagerHelper,
        AssociationInstituteRepository $associationInstituteRepo
    ) {
        $this->grantedService = $grantedService;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->associationInstituteRepo = $associationInstituteRepo;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, InstituteVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Institute) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case InstituteVoterEnum::CREATE:
                return $this->canCreate($token);
            case InstituteVoterEnum::EDIT:
                return $this->canEdit($token, $subject);
            case InstituteVoterEnum::EDIT_PREFERENCES:
                return $this->canEditPreferences($token, $subject);
            case InstituteVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canCreate(TokenInterface $token): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $manager = $this->matchingManagerHelper->getMatchingManagerOfManager();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || $manager instanceof Association
            || ($manager instanceof Institute && !$manager->isLevel(Institute::MAX_LEVEL))
        ;
    }

    private function canEdit(TokenInterface $token, Institute $institute): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager();
        $instituteManager = $this->matchingManagerHelper->getMatchingManagersOfInstitute(
            $institute,
            false,
            false
        );

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || (
                \in_array($matchingManager, $instituteManager, true)
                && ($matchingManager instanceof Institute || 1 === \count($instituteManager))
            )
        ;
    }

    private function canEditPreferences(TokenInterface $token, Institute $institute): bool
    {
        if ($this->canEdit($token, $institute)) {
            return true;
        }

        $manager = $this->matchingManagerHelper->getMatchingManagerOfManager();

        if (!$manager instanceof Association) {
            return false;
        }

        $associationInstitute = $this->associationInstituteRepo->findOneBy([
            'association' => $manager,
            'institute' => $institute,
        ]);

        return $associationInstitute && $associationInstitute->canEditPreferences();
    }

    private function canDelete(TokenInterface $token, Institute $institute): bool
    {
        return $this->canEdit($token, $institute) && $institute->getUsers()->isEmpty() && !$institute->hasChildren();
    }
}
