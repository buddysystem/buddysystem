<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Thread;
use App\Entity\ThreadManagerInterface;
use App\Entity\ThreadUserManager;
use App\Entity\User;
use App\Enum\ThreadVoterEnum;
use App\Services\MatchingManagerHelper;
use App\Services\MessagingHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ThreadVoter extends Voter
{
    private $matchingManagerHelper;
    private $messagingHelper;

    public function __construct(MatchingManagerHelper $matchingManagerHelper, MessagingHelper $messagingHelper)
    {
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->messagingHelper = $messagingHelper;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, ThreadVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Thread) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case ThreadVoterEnum::SEE:
                return $this->canSee($token, $subject);
            case ThreadVoterEnum::REPLY:
                return $this->canReply($token, $subject);
            case ThreadVoterEnum::UNCHANGED_RECIPIENTS:
                return $this->isRecipientsUnchanged($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canSee(TokenInterface $token, Thread $thread): bool
    {
        /** @var User $user */
        $user = $token->getUser();

        return \in_array($thread, $this->messagingHelper->getSubscribedThread($user), true);
    }

    private function canReply(TokenInterface $token, Thread $thread): bool
    {
        if (!$this->canSee($token, $thread)) {
            return false;
        }

        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager();
        if ($thread instanceof ThreadManagerInterface
            && (
                !$matchingManager
                || !$matchingManager->canAddMessageInThread($thread)
            )
        ) {
            return false;
        }

        return $this->isRecipientsUnchanged($token, $thread);
    }

    private function isRecipientsUnchanged(TokenInterface $token, Thread $thread): bool
    {
        if ($thread instanceof ThreadUserManager) {
            if (($user = $thread->getUser()) && $thread->getMatchingManagers()) {
                $matchingManagersUser = $this->matchingManagerHelper->getAllMatchingManagerOfUser(
                    $user,
                    true,
                    false,
                    $thread->getMatchingManagerType()
                );

                $matchingManagersThread = $thread->getMatchingManagers()->toArray();

                return $matchingManagersUser === $matchingManagersThread;
            }

            return false;
        }

        return true;
    }
}
