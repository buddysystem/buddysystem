<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Association;
use App\Entity\User;
use App\Enum\AssociationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class AssociationVoter extends Voter
{
    private $grantedService;
    private $matchingManagerHelper;

    public function __construct(GrantedService $grantedService, MatchingManagerHelper $matchingManagerHelper)
    {
        $this->grantedService = $grantedService;
        $this->matchingManagerHelper = $matchingManagerHelper;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, AssociationVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof Association) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case AssociationVoterEnum::EDIT:
                return $this->canEdit($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canEdit(TokenInterface $token, Association $association): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();
        $matchingManager = $this->matchingManagerHelper->getMatchingManagerOfManager();

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
            || $matchingManager === $association
        ;
    }
}
