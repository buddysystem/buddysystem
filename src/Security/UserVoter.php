<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Repository\BuddyRepository;
use App\Repository\InstituteRepository;
use App\Services\GrantedService;
use App\Services\MatchingManagerHelper;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{
    private $instituteRepo;
    private $buddyRepo;
    private $matchingManagerHelper;
    private $grantedService;

    public function __construct(
        InstituteRepository $instituteRepo,
        BuddyRepository $buddyRepo,
        MatchingManagerHelper $matchingManagerHelper,
        GrantedService $grantedService
    ) {
        $this->instituteRepo = $instituteRepo;
        $this->buddyRepo = $buddyRepo;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->grantedService = $grantedService;
    }

    protected function supports(string $attribute, $subject): bool
    {
        if (!\in_array($attribute, UserVoterEnum::getValues())) {
            return false;
        }

        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        switch ($attribute) {
            case UserVoterEnum::SHOW:
                return $this->canShow($token, $subject);
            case UserVoterEnum::EDIT_OTHER:
                return $this->canEditOther($token, $subject);
            case UserVoterEnum::EDIT_ALL:
                return $this->canEditAll($token, $subject);
            case UserVoterEnum::EDIT_SENSITIVE_DATA:
                return $this->canEditSensitiveData($token, $subject);
            case UserVoterEnum::DELETE:
                return $this->canDelete($token, $subject);
            case UserVoterEnum::MATCH:
                return $this->canMatch($token, $subject);
            case UserVoterEnum::BE_MATCHED:
                return $this->canBeMatched($subject);
            case UserVoterEnum::IS_MANAGER:
                return $this->isManager($subject);
            case UserVoterEnum::HAS_MATCHING_RIGHT:
                return $this->hasMatchingRight($subject);
            case UserVoterEnum::EDIT_LOCATION:
                return $this->canEditLocation($token, $subject);
            case UserVoterEnum::APPROVE:
                return $this->canApprove($token, $subject);
            case UserVoterEnum::SEND_MESSAGE:
                return $this->canSendMessage($token, $subject);
        }

        throw new \LogicException('Voter Error.');
    }

    private function canShow(TokenInterface $token, User $userToShow): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return UserRoleEnum::ROLE_SUPER_ADMIN === $this->grantedService->getRole($currentUser)
            || (
                UserRoleEnum::ROLE_ADMIN === $this->grantedService->getRole($currentUser)
                && UserRoleEnum::ROLE_SUPER_ADMIN !== $this->grantedService->getRole($userToShow)
            )
            || (
                $this->matchingManagerHelper->canManage($userToShow)
                && \in_array($this->grantedService->getRole($userToShow), [
                    UserRoleEnum::ROLE_USER,
                    $this->grantedService->getRole($currentUser),
                ])
            );
    }

    private function canEditOther(TokenInterface $token, User $userToEdit): bool
    {
        return $token->getUser() !== $userToEdit && $this->canShow($token, $userToEdit);
    }

    private function canEditAll(TokenInterface $token, User $userToEdit): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $token->getUser() === $userToEdit
            || $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SUPER_ADMIN)
            || (
                UserRoleEnum::ROLE_ADMIN === $this->grantedService->getRole($currentUser)
                && !$this->grantedService->isGranted($userToEdit, UserRoleEnum::ROLE_ADMIN)
            )
        ;
    }

    private function canEditLocation(TokenInterface $token, User $userToEdit): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        if ($currentUser === $userToEdit) {
            return !\in_array($this->grantedService->getRole($userToEdit), UserRoleEnum::$managers);
        }

        return $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_SUPER_ADMIN)
                || (
                    UserRoleEnum::ROLE_ADMIN === $this->grantedService->getRole($currentUser)
                    && !$this->grantedService->isGranted($userToEdit, UserRoleEnum::ROLE_ADMIN)
                )
            ;
    }

    private function canEditSensitiveData(TokenInterface $token, User $userToEdit): bool
    {
        return $token->getUser() === $userToEdit;
    }

    private function canDelete(TokenInterface $token, User $userToDelete): bool
    {
        return $token->getUser() === $userToDelete || $this->canShow($token, $userToDelete);
    }

    private function canMatch(TokenInterface $token, User $userToMatch): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return $this->canBeMatched($userToMatch)
            && $userToMatch->isEnabled() && $userToMatch->isApproved()
            && $this->buddyRepo->getRealNbBuddiesByUser($userToMatch) < $userToMatch->getNbBuddiesWanted()
            && (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
                || ($this->matchingManagerHelper->canManage($userToMatch, true) && !$userToMatch->hasMaxBuddiesRefused())
            );
    }

    private function canBeMatched(User $userToMatch): bool
    {
        return !$this->isManager($userToMatch);
    }

    private function isManager(User $user): bool
    {
        return \in_array($this->grantedService->getRole($user), UserRoleEnum::$managerAndAdmin);
    }

    private function hasMatchingRight(User $user): bool
    {
        $manager = $this->matchingManagerHelper->getMatchingManagerOfManager();

        return $this->grantedService->isGranted($user, UserRoleEnum::ROLE_ADMIN)
            || $this->grantedService->isGranted($user, UserRoleEnum::ROLE_INSTITUTE_MANAGER)
            || $this->instituteRepo->hasMatchableInstitute($manager);
    }

    private function canApprove(TokenInterface $token, User $userToApprove): bool
    {
        /** @var User $currentUser */
        $currentUser = $token->getUser();

        return !$userToApprove->isApproved()
            && $currentUser !== $userToApprove
            && (
                $this->grantedService->isGranted($currentUser, UserRoleEnum::ROLE_ADMIN)
                || $this->matchingManagerHelper->canManage($userToApprove)
            );
    }

    private function canSendMessage(TokenInterface $token, User $userToContact): bool
    {
        return $userToContact->isApproved()
            && $this->canEditOther($token, $userToContact)
        ;
    }
}
