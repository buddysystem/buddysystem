<?php

declare(strict_types=1);

namespace App\Message;

use App\Extract\ExtractModel;

class ExtractMessage
{
    private $extractModel;

    public function __construct(ExtractModel $extractModel)
    {
        $this->extractModel = $extractModel;
    }

    public function getExtractModel(): ExtractModel
    {
        return $this->extractModel;
    }
}
