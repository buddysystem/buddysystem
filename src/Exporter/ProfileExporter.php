<?php

declare(strict_types=1);

namespace App\Exporter;

use App\Entity\Hobby;
use App\Entity\Message;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ProfileExporter extends Exporter
{
    private $user;
    private $rows;
    private $translator;
    private $categories = [
        'account',
        'profile',
        'situation',
        'preferences',
        'messages',
        'histories',
    ];

    public function __construct(TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->translator = $translator;
        $this->rows = [];
    }

    public function getContent(): string
    {
        foreach ($this->categories as $category) {
            $this->addNewCategory($category);
        }

        return implode("\n", $this->rows);
    }

    public function getHeaders(string $category): array
    {
        switch ($category) {
            case 'profile':
                return [
                    'firstname',
                    'lastname',
                    'date of birth',
                    'gender',
                ];
            case 'situation':
                return [
                    'institute',
                    'languages',
                    'local student',
                    'semester',
                    'date of arrival',
                    'date of departure',
                ];
            case 'preferences':
                return [
                    'buddies languages wanted',
                    'number of buddies wanted',
                    'hobbies',
                    'motivation',
                    'same gender requested',
                    'comment',
                ];
            case 'messages':
                return [
                    'date',
                    'body',
                ];
            case 'histories':
                return [
                    'createdAt',
                    'number of buddies refused',
                    'number of buddies removed',
                    'number of buddies',
                    'number of report',
                    'number of warning',
                ];
            default:
                return [
                    'email',
                    'phone',
                ];
        }
    }

    public function getData(string $category): array
    {
        $phoneNumberUtil = PhoneNumberUtil::getInstance();

        switch ($category) {
            case 'profile':
                return [
                    $this->user->getFirstname(),
                    $this->user->getLastname(),
                    $this->user->getDob() ? $this->user->getDob()->format('d/m/Y') : '',
                    $this->user->getSex(),
                ];
            case 'situation':
                return [
                    $this->user->getInstitute() ? $this->user->getInstitute()->getName() : '',
                    implode('|', array_map(function ($language) {
                        return Languages::getName($language);
                    }, $this->user->getLanguages())),
                    $this->user->isLocal(),
                    $this->user->getSemester() ? $this->translator->trans('semester.period.'.$this->user->getSemester()->getPeriod()).' '.$this->user->getSemester()->getYear() : '',
                    $this->user->getArrival() ? $this->user->getArrival()->format('d/m/Y') : '',
                    $this->user->getDeparture() ? $this->user->getDeparture()->format('d/m/Y') : '',
                ];
            case 'preferences':
                return [
                    implode('|', array_map(function ($language) {
                        return Languages::getName($language);
                    }, $this->user->getLanguagesWanted())),
                    $this->user->getNbBuddiesWanted(),
                    implode('|', array_map(function (Hobby $hobby) {
                        return $hobby->getName();
                    }, $this->user->getHobbies()->toArray())),
                    $this->user->getMotivation() ? $this->user->getMotivation()->getName($this->user->isLocal()) : '',
                    $this->user->getSexWanted(),
                    $this->user->getComment(),
                ];
            case 'messages':
                $messages = [];

                /** @var Message $message */
                foreach ($this->user->getMessages() as $message) {
                    $messages[] = [
                        $message->getCreatedAt()->format('d/m/Y H:i:s'),
                        $this->cleanContent($message->getBody()),
                    ];
                }

                return $messages;
            case 'histories':
                $histories = [];

                foreach ($this->user->getHistories() as $history) {
                    $histories[] = [
                        $history->getCreatedAt()->format('d/m/Y'),
                        $history->getNbBuddiesRefused(),
                        $history->getNbBuddiesRemoved(),
                        $history->getNbBuddies(),
                        $history->getNbReport(),
                        $history->getNbWarned(),
                    ];
                }

                return $histories;
            default:
                return [
                    $this->user->getEmail(),
                    $this->user->getPhoneNumber() instanceof PhoneNumber
                        ? $phoneNumberUtil->format($this->user->getPhoneNumber(), PhoneNumberFormat::INTERNATIONAL)
                        : '',
                ];
        }
    }

    public function getFileName(): string
    {
        return 'buddysystem_export.csv';
    }

    private function addNewCategory(string $category): void
    {
        $datas = $this->getData($category);

        if (\count($datas)) {
            $this->rows[] = ucfirst($category);
            $this->rows[] = implode(',', $this->getHeaders($category));

            if (\is_array($datas[0])) {
                foreach ($datas as $data) {
                    $this->rows[] = implode(',', $data);
                }
            } else {
                $this->rows[] = implode(',', $datas);
            }

            $this->rows[] = null;
        }
    }

    private function cleanContent(string $originalContent): string
    {
        $content = str_replace(',', ' ', $originalContent);
        $content = str_replace("\n", '.', $content);

        return str_replace("\r", '.', $content);
    }
}
