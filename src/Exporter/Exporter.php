<?php

declare(strict_types=1);

namespace App\Exporter;

use Symfony\Component\HttpFoundation\Response;

abstract class Exporter
{
    public function export(): Response
    {
        $response = new Response();
        $response->setContent($this->getContent());

        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$this->getFileName());

        return $response;
    }

    abstract public function getContent(): string;

    abstract public function getFileName(): string;
}
