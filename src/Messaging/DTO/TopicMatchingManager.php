<?php

declare(strict_types=1);

namespace App\Messaging\DTO;

class TopicMatchingManager
{
    private $id;
    private $type;
    private $recipientType;
    private $label;

    public function __construct(string $id, string $type, string $recipientType, string $label)
    {
        $this->id = $id;
        $this->type = $type;
        $this->recipientType = $recipientType;
        $this->label = $label;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getRecipientType(): string
    {
        return $this->recipientType;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
