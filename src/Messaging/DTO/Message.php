<?php

declare(strict_types=1);

namespace App\Messaging\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Message
{
    private const TYPE = 'MESSAGE';

    private $id;
    private $threadGroupId;
    private $author;
    private $datetime;
    private $body;
    private $recipientType;
    private $recipientName;
    private $recipientNameTooltip;

    public function __construct(
        string $id,
        string $threadGroupId,
        Author $author,
        \DateTime $datetime,
        string $body,
        string $recipientType = null,
        string $recipientName = null,
        string $recipientNameTooltip = null
    ) {
        $this->id = $id;
        $this->threadGroupId = $threadGroupId;
        $this->author = $author;
        $this->datetime = $datetime;
        $this->body = $body;
        $this->recipientType = $recipientType;
        $this->recipientName = $recipientName;
        $this->recipientNameTooltip = $recipientNameTooltip;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @SerializedName("thread")
     */
    public function getThreadGroupId(): string
    {
        return $this->threadGroupId;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function getDatetime(): \DateTime
    {
        return $this->datetime;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getRecipientType(): ?string
    {
        return $this->recipientType;
    }

    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    public function getRecipientNameTooltip(): ?string
    {
        return $this->recipientNameTooltip;
    }

    /**
     * @SerializedName("@var")
     */
    public function getType(): string
    {
        return self::TYPE;
    }
}
