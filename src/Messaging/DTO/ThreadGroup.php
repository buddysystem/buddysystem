<?php

declare(strict_types=1);

namespace App\Messaging\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class ThreadGroup
{
    private $id;
    private $name;
    private $channelType;
    private $channelTypeTooltip;
    private $icon;
    private $avatar;
    private $lastMessageAt;
    private $topics;
    private $isRead;
    private $canReply;
    private $isFavorite;
    private $profileUrl;

    public function __construct(
        string $id,
        string $name,
        string $channelType,
        string $channelTypeTooltip,
        string $icon,
        string $avatar,
        \DateTime $lastMessageAt = null,
        array $topics = [],
        bool $isRead = true,
        bool $isFavorite = false,
        bool $canReply = true,
        string $profileUrl = null
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->channelType = $channelType;
        $this->channelTypeTooltip = $channelTypeTooltip;
        $this->avatar = $avatar;
        $this->icon = $icon;
        $this->lastMessageAt = $lastMessageAt;
        $this->topics = $topics;
        $this->isRead = $isRead;
        $this->isFavorite = $isFavorite;
        $this->canReply = $canReply;
        $this->profileUrl = $profileUrl;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getChannelType(): string
    {
        return $this->channelType;
    }

    public function getChannelTypeTooltip(): string
    {
        return $this->channelTypeTooltip;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function getAvatar(): string
    {
        return $this->avatar;
    }

    public function getLastMessageAt(): ?\DateTime
    {
        return $this->lastMessageAt;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function isRead(): bool
    {
        return $this->isRead;
    }

    public function isCanReply(): bool
    {
        return $this->canReply;
    }

    public function isFavorite(): bool
    {
        return $this->isFavorite;
    }

    public function getProfileUrl(): ?string
    {
        return $this->profileUrl;
    }
}
