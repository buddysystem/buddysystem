<?php

declare(strict_types=1);

namespace App\Messaging\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class Topic
{
    private $id;
    private $threadId;
    private $matchingManager;

    public function __construct(string $id, int $threadId, TopicMatchingManager $matchingManager = null)
    {
        $this->id = $id;
        $this->threadId = $threadId;
        $this->matchingManager = $matchingManager;
    }

    /**
     * @SerializedName("@id")
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getThreadId(): int
    {
        return $this->threadId;
    }

    public function getMatchingManager(): ?TopicMatchingManager
    {
        return $this->matchingManager;
    }
}
