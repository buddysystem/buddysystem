<?php

declare(strict_types=1);

namespace App\Command\Migration;

use App\Entity\Association;
use App\Entity\Institute;
use App\Repository\AssociationRepository;
use App\Repository\InstituteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConvertMatchingInstituteValuesCommand extends Command
{
    private EntityManagerInterface $em;
    private AssociationRepository $assoRepo;
    private InstituteRepository $instRepo;

    public function __construct(
        EntityManagerInterface $em,
        AssociationRepository $assoRepo,
        InstituteRepository $instRepo,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->assoRepo = $assoRepo;
        $this->instRepo = $instRepo;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('bs:migration:convert-matching-institutes')
            ->setDescription('Convert "matching_institute_values" column in database')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->process($io, $this->assoRepo->findAll());
        $this->process($io, $this->instRepo->findAll());

        return 0;
    }

    private function process($io, array $entities): void
    {
        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($entities));
        }

        /** @var Association|Institute $entity */
        foreach ($entities as $entity) {
            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
                $io->comment("\t<comment>".$entity->getId().'|'.$entity->getName().'</comment>');
            }

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }

            $miv = $entity->getMatchingInstituteValues();

            if (true === isset($miv[3])) {
                // already converted, skip
                continue;
            }

            $entity->setMatchingInstituteValues([
                1 => $miv[2] ?? 0,
                2 => $miv[1] ?? 0,
                3 => $miv[0] ?? 0,
            ]);
        }

        $this->em->flush();
    }
}
