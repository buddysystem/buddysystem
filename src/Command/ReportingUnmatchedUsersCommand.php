<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Association;
use App\Entity\Institute;
use App\Enum\StatTypeEnum;
use App\Repository\AssociationRepository;
use App\Repository\InstituteRepository;
use App\Services\StatsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReportingUnmatchedUsersCommand extends Command
{
    private EntityManagerInterface $em;
    private StatsService $statsService;
    private AssociationRepository $associationRepo;
    private InstituteRepository $instituteRepo;

    public function __construct(
        EntityManagerInterface $em,
        StatsService $statsService,
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo
    ) {
        $this->em = $em;
        $this->statsService = $statsService;
        $this->associationRepo = $associationRepo;
        $this->instituteRepo = $instituteRepo;

        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->setName('report:users:unmatched')
            ->setDescription('')
            ->addArgument('entity', InputArgument::REQUIRED, 'institutes OR associations')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
            $io->text('Reporting');
        }

        $entities = [];
        if ('institutes' === $input->getArgument('entity')) {
            $entities = $this->instituteRepo->findAll();
        } elseif ('associations' === $input->getArgument('entity')) {
            $entities = $this->associationRepo->findAll();
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($entities));
        }

        $result = [];

        foreach ($entities as $entity) {
            if ( // add stats only if the entity is a level 0 institute or an association. It also has to be activated.
                (
                    ($entity instanceof Institute && $entity->getLevel() <= 1)
                    || $entity instanceof Association
                )
                && $entity->isActivated()
            ) {
                $statsType = !$entity->isFullYearOnly() ? 'semester' : StatTypeEnum::TYPE_YEAR;

                $stats = $this->statsService->getStatsValues($entity, $statsType);

                $result[] = [
                    $entity->getName().';'.
                    $stats['nbUsers'].';'.
                    $stats['nbMentees'].';'.
                    $stats['nbMentors'].';'.
                    $stats['nbMenteesNotMatched'].';'.
                    $stats['nbMentorsNotMatched'],
                ];
            }

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        foreach ($result as $item) {
            $output->writeln($item);
        }

        $io->success('Finish');

        $this->em->flush();

        return 0;
    }
}
