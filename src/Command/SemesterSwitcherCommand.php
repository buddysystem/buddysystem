<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Institute;
use App\Repository\AssociationRepository;
use App\Repository\InstituteRepository;
use App\Services\LoggerHelper;
use App\Services\MailManager;
use App\Services\SemesterHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SemesterSwitcherCommand extends Command
{
    private $mailManager;
    private $associationRepo;
    private $instituteRepo;
    private $semesterHelper;
    private $loggerHelper;

    public function __construct(
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        MailManager $mailManager,
        SemesterHelper $semesterHelper,
        LoggerHelper $loggerHelper,
        ?string $name = null
    ) {
        $this->associationRepo = $associationRepo;
        $this->instituteRepo = $instituteRepo;
        $this->mailManager = $mailManager;
        $this->semesterHelper = $semesterHelper;
        $this->loggerHelper = $loggerHelper;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('cron:semester:switch')
            ->setDescription('If the date of end of semester is reached, switch to the next one. This cron will be executed every monday morning at 4am')
            ->addOption('simulate', null, InputOption::VALUE_OPTIONAL, 'Run the command without actually doing something', false)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $institutes = $this->instituteRepo->findAll();
        $this->testSemesterEnd($io, $institutes, (bool) $input->getOption('simulate'));

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->text("Switch semester done\n");
        }

        return 0;
    }

    private function testSemesterEnd(SymfonyStyle $io, array $institutes, bool $simulate): void
    {
        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($institutes));
        }

        /** @var Institute $institute */
        foreach ($institutes as $institute) {
            if ($this->semesterHelper->isEndOfSemesterReached($institute)) {
                if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                    $io->comment("\t<comment>".$institute->getName().' reached the end of semester</comment>');
                }

                if (false === $simulate) {
                    $this->semesterHelper->switchSemester($institute);
                    if (true === $institute->isActivated()) {
                        $this->mailManager->notifyManagersSwitchSemester($institute);
                    }
                }

                $semester = $institute->getSemester();
                $this->loggerHelper->logSwitchSemester('Institute : '.$institute->getId());
                $this->loggerHelper->logSwitchSemester('New Semester : '.$semester->getPeriod().' '.$semester->getYear());
            }

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }
    }
}
