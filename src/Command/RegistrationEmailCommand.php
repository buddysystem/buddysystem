<?php

declare(strict_types=1);

namespace App\Command;

use App\Repository\UserRepository;
use App\Security\Authentication\EmailVerifier;
use App\Services\MailManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

class RegistrationEmailCommand extends Command
{
    private $userRepo;
    private $emailVerifier;
    private $mailManager;

    public function __construct(
        UserRepository $userRepo,
        EmailVerifier $emailVerifier,
        MailManager $mailManager,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->userRepo = $userRepo;
        $this->emailVerifier = $emailVerifier;
        $this->mailManager = $mailManager;
    }

    public function configure(): void
    {
        $this
            ->setName('user:registration:resend_confirmation')
            ->setDescription('resend confirmation email')
            ->addOption('date-start', null, InputOption::VALUE_OPTIONAL, 'Date from')
            ->addOption('date-end', null, InputOption::VALUE_OPTIONAL, 'Date to')
            ->addOption('email', null, InputOption::VALUE_OPTIONAL, 'Email of a specific user')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $dateStart = $input->getOption('date-start');
        $dateEnd = $input->getOption('date-end');
        $email = $input->getOption('email');

        $qb = $this->userRepo->createQueryBuilder('u')
            ->where('u.enabled = :enabled')
            ->setParameter('enabled', false)
        ;

        if (\is_string($email)) {
            $qb
                ->andWhere('u.email = :email')
                ->setParameter('email', $email)
            ;
        }

        if (\is_string($dateStart)) {
            $qb
                ->andWhere('u.createdAt >= :dateStart')
                ->setParameter('dateStart', $dateStart)
            ;
        }

        if (\is_string($dateEnd)) {
            $qb
                ->andWhere('u.createdAt < :dateEnd')
                ->setParameter('dateEnd', $dateEnd)
            ;
        }

        $users = $qb->getQuery()->getResult();
        $nbUsers = \count($users);

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Would you really like to resend registration email to '.$nbUsers.' users?', false);

        if (!$helper->ask($input, $output, $question)) {
            return 0;
        }

        $io->createProgressBar($nbUsers);
        $io->progressStart();

        foreach ($users as $user) {
            $this->emailVerifier->sendEmailConfirmation(
                'register_confirm',
                $user,
                $this->mailManager->getEmailConfirmation($user->getEmail())
            );
            $io->progressAdvance();
        }

        $io->progressFinish();

        return 0;
    }
}
