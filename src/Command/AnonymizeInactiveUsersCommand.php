<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\MailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AnonymizeInactiveUsersCommand extends Command
{
    private const RGPD_LEGAL_MONTHS_INTERVAL = 36;

    private $em;
    private $userRepo;
    private $mailManager;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepo,
        MailManager $mailManager,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->userRepo = $userRepo;
        $this->mailManager = $mailManager;

        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('cron:user:anonymize')
            ->setDescription('Anonymize user who didn\'t log into the buddysystem since 36 months. Has to be executed once a day.')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Maximum users count to handle', 1000)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
            $io->text("User who didn't log since ".self::RGPD_LEGAL_MONTHS_INTERVAL.' months will be anonymized');
        }

        $users = $this->userRepo->getInactiveNonAnonymizedUsers(self::RGPD_LEGAL_MONTHS_INTERVAL, (int) $input->getOption('limit'));

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($users));
        }

        foreach ($users as $user) {
//            $this->mailManager->sendAnonymisationEmail($user);
            $this->anonymize($user);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->progressFinish();
        }

        $io->success('Finish');

        $this->em->flush();

        return 0;
    }

    private function anonymize(User $user): void
    {
        $user->setFirstName(null);
        $user->setLastname(null);
        $user->setPhoneNumber(null);
        $user->setEsncard(null);
        $user->setPictureName(null);
        $user->setPictureFile(null);
        $user->setPrivacyAcceptedWith(null);

        if (false !== empty($user->getComment())) {
            $user->setComment('---');
        }

        $email = $user->getEmail();
        $atPosition = strpos($email, '@');
        $id = $user->getId() ?: random_int(0, 9);

        if (false !== $atPosition) {
            $localPart = substr($email, 0, $atPosition);
            $anonymizedLocalPart = $id.'.'.$localPart[0].str_repeat('*', \strlen($localPart) - 1);
            $anonymizedEmail = $anonymizedLocalPart.substr($email, $atPosition, \strlen($email));
        } else {
            $anonymizedEmail = null;
        }

        $user->setEmail($anonymizedEmail);
        $user->setEnabled(false);
    }
}
