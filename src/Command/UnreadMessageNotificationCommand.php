<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;
use App\Repository\ThreadRepository;
use App\Repository\UserRepository;
use App\Services\MailManager;
use App\Services\MatchingManagerHelper;
use App\Services\MessagingHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UnreadMessageNotificationCommand extends Command
{
    private const HOURS_INTERVAL = 2;
    private const MAX_NOTIF = 3;

    private $em;
    private $userRepo;
    private $mailManager;
    private $matchingManagerHelper;
    private $messagingHelper;
    private $threadRepo;
    private $threadChannelType;
    private $notificationField;

    public function __construct(
        EntityManagerInterface $em,
        UserRepository $userRepo,
        MailManager $mailManager,
        MatchingManagerHelper $matchingManagerHelper,
        MessagingHelper $messagingHelper,
        ThreadRepository $threadRepo,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->userRepo = $userRepo;
        $this->mailManager = $mailManager;
        $this->matchingManagerHelper = $matchingManagerHelper;
        $this->messagingHelper = $messagingHelper;
        $this->threadRepo = $threadRepo;

        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('cron:notification:unread_messages')
            ->setDescription('Sent email notifications if unread messages')
            ->addArgument('threadChannelType', InputArgument::REQUIRED, 'Which threadChannelType?')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->threadChannelType = $input->getArgument('threadChannelType');
        $this->notificationField = 'new'.ucfirst($this->threadChannelType).'Message';

        $batchSize = 1000;
        $batchCount = 0;
        $minimumId = 0;

        do {
            [$batchCount, $minimumId] = $this->runOneBatch($io, $batchSize, $minimumId);
        } while ($batchCount === $batchSize);

        return 0;
    }

    private function runOneBatch(SymfonyStyle $io, int $batchSize, int $minimumId): array
    {
        $batch = $this->userRepo->createQueryBuilder('u')
            ->distinct()
            ->innerJoin('u.userNotification', 'notif')
            ->andWhere('u.id > :minId')
            ->andWhere("notif.{$this->notificationField} = :newMessage")
            ->andWhere("(notif.newMessageSentAt is null or
                        CURRENT_TIMESTAMP() >= DATE_ADD(notif.newMessageSentAt, CASE WHEN notif.nbNewMessageNotifSent > 1 THEN 2 ELSE 1 END,'DAY'))")
            ->andWhere('notif.nbNewMessageNotifSent < :maxNotif')
            ->setMaxResults($batchSize)
            ->orderBy('u.id')
            ->setParameter('minId', $minimumId)
            ->setParameter('maxNotif', self::MAX_NOTIF)
            ->setParameter('newMessage', true)
            ->getQuery()->getResult()
        ;

        $batchCount = \count($batch);

        if ($batchCount > 0) {
            $minimumId = $batch[$batchCount - 1]->getId();

            /** @var User $user */
            foreach ($batch as $user) {
                $threads = $this->messagingHelper->getSubscribedThread(
                    $user,
                    $this->threadChannelType
                );

                if (\count($threads) > 0) {
                    if ($this->threadRepo->hasUnread($user, $threads, self::HOURS_INTERVAL)) {
                        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
                            $io->comment("\t<comment>".$user->getEmail().'</comment>');
                        }

                        $this->mailManager->sendNotification(
                            $user,
                            'new_message',
                            $this->matchingManagerHelper->getMatchingManagerOfUser($user)
                        );

                        $user->getUserNotification()->increaseNbNewMessageNotifSent();
                        $this->em->persist($user);
                    }
                }

                $user = null;
                unset($user);
                $threads = null;
                unset($threads);
            }

            $batch = null;
            unset($batch);

            $this->em->flush();
            $this->em->clear();
            gc_collect_cycles();
        }

        return [$batchCount, $minimumId];
    }
}
