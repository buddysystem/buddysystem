<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\StatsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateWeekStatsCommand extends Command
{
    private $statsService;

    public function __construct(StatsService $statsService, ?string $name = null)
    {
        $this->statsService = $statsService;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('cron:stats:update')
            ->setDescription('Update stats of the week. This cron will be executed every sunday evening')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->statsService->generateAllWeekStats();

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->text('Stats updated');
        }

        return 0;
    }
}
