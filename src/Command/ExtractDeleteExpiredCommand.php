<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\FileWriterHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ExtractDeleteExpiredCommand extends Command
{
    private $fileWriterHelper;

    public function __construct(FileWriterHelper $fileWriterHelper, ?string $name = null)
    {
        $this->fileWriterHelper = $fileWriterHelper;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('extract:delete_expired')
            ->setDescription('Delete expired extract files')
            ->addOption('date', null, InputOption::VALUE_OPTIONAL, 'Date filter')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $finder = new Finder();
        $filesystem = new Filesystem();

        $dateOption = $input->getOption('date');
        $date = \is_string($dateOption)
            ? $dateOption
            : 'since 1 week ago'
        ;

        $filesystem->remove(
            $finder->files()->in($this->fileWriterHelper->getExtractDir())->date($date)
        );

        return 0;
    }
}
