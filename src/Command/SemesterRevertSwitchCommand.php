<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Buddy;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Entity\UserHistory;
use App\Enum\BuddyStatusEnum;
use App\Enum\SemesterPeriodEnum;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\InstituteRepository;
use App\Repository\SemesterRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SemesterRevertSwitchCommand extends Command
{
    private $associationRepo;
    private $instituteRepo;
    private $userRepo;
    private $semesterRepo;
    private $buddyRepo;
    private $em;

    public function __construct(
        EntityManagerInterface $em,
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        UserRepository $userRepo,
        SemesterRepository $semesterRepo,
        BuddyRepository $buddyRepo,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->associationRepo = $associationRepo;
        $this->instituteRepo = $instituteRepo;
        $this->userRepo = $userRepo;
        $this->semesterRepo = $semesterRepo;
        $this->buddyRepo = $buddyRepo;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('semester:switch:revert')
            ->setDescription('Revert semester of matching manager to the previous semester')
            ->addOption('id', null, InputOption::VALUE_REQUIRED, 'Id of the matching manager')
            ->addOption('type', null, InputOption::VALUE_REQUIRED, 'type of the matching manager (association or institute)')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $id = $input->getOption('id');
        $type = $input->getOption('type');

        if (!\in_array($type, [MatchingManagerInterface::TYPE_INSTITUTE, MatchingManagerInterface::TYPE_ASSOCIATION])) {
            throw new \Exception('type error.');
        }

        if (MatchingManagerInterface::TYPE_ASSOCIATION === $type) {
            $matchingManager = $this->associationRepo->findOneBy(['id' => $id]);
        } else {
            $matchingManager = $this->instituteRepo->findOneBy(['id' => $id]);
        }

        if ($matchingManager) {
            $semester = $matchingManager->getSemester();

            if ($semester) {
                if (SemesterPeriodEnum::PERIOD_SECOND_SEMESTER === $semester->getPeriod()) {
                    $period = [SemesterPeriodEnum::PERIOD_FIRST_SEMESTER];
                    $year = $semester->getYear();
                } else {
                    $period = [SemesterPeriodEnum::PERIOD_SECOND_SEMESTER, SemesterPeriodEnum::PERIOD_ALL_YEAR];
                    $year = ($semester->getStartYear() - 1).' - '.$semester->getStartYear();
                }

                $users = $this->userRepo->getUsersToUnarchive($matchingManager, $period, $year);
                $matchingManager->setSemester($this->semesterRepo->findOneBy(['period' => $period[0], 'year' => $year]));

                $io->createProgressBar();
                $io->progressStart(\count($users));

                /** @var User $user */
                foreach ($users as $user) {
                    $user->setArchived(false);
                    $user->setIsAskNextSemester(false);

                    /** @var UserHistory $history */
                    $history = $user->getHistories()->last();

                    $user->setNbReport($history->getNbReport());
                    $user->setNbBuddiesRemoved($history->getNbBuddiesRemoved());
                    $user->setNbWarned($history->getNbWarned());
                    $user->setNbBuddiesRefused($history->getNbBuddiesRefused());
                    $user->setNbBuddies($history->getNbBuddies());
                    $user->setSemester($history->getSemester());

                    $user->getHistories()->removeElement($history);
                    $io->progressAdvance();
                }

                $io->progressFinish();
                $this->em->flush();

                /** @var User $user */
                foreach ($users as $user) {
                    /** @var Buddy $buddy */
                    foreach ($this->buddyRepo->getMyBuddiesArchived($user) as $buddy) {
                        $mentor = $buddy->getMentor();
                        $mentee = $buddy->getMentee();

                        if (!$mentor->isArchived() && !$mentee->isArchived()) {
                            $buddy->setArchived(false);

                            if (BuddyStatusEnum::STATUS_CONFIRMED === $buddy->getStatus()) {
                                $mentor->setIsMatched(true);
                                $mentee->setIsMatched(true);
                            }
                        }
                    }
                }

                $this->em->flush();
            } else {
                $io->text('Matching manager semester is null');
            }
        } else {
            $io->text('Matching manager not found');
        }

        return 0;
    }
}
