<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Buddy;
use App\Repository\BuddyRepository;
use App\Services\MailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BuddiesRemoveCommand extends Command
{
    private $em;
    private $buddyRepo;
    private $mailManager;

    public function __construct(
        EntityManagerInterface $em,
        BuddyRepository $buddyRepo,
        MailManager $mailManager,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->buddyRepo = $buddyRepo;
        $this->mailManager = $mailManager;
        parent::__construct($name);
    }

    public function configure(): void
    {
        $this
            ->setName('cron:buddies:remove')
            ->setDescription('Notify and remove buddies {manager.unmatchDelay} days after the matching if the match has not been accepted neither refused')
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $buddies = $this->buddyRepo->getBuddiesToRemove();

        if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
            $io->createProgressBar();
            $io->progressStart(\count($buddies));
        }

        /** @var Buddy $buddy */
        foreach ($buddies as $buddy) {
            $mentor = $buddy->getMentor();

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERY_VERBOSE) {
                $io->comment("\t<comment>".$mentor->getEmail().'</comment>');
            }

            $this->mailManager->sendWarningToLocal(
                $mentor,
                'remove',
                $buddy->getMatchingManager()
            );

            $mentor->incrementNbBuddiesRemoved();
            $this->em->persist($mentor);
            $this->em->remove($buddy);

            if ($io->getVerbosity() >= OutputInterface::VERBOSITY_VERBOSE) {
                $io->progressAdvance();
            }
        }

        $this->em->flush();

        return 0;
    }
}
