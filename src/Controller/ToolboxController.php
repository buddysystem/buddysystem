<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Association;
use App\Entity\Document;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Enum\DocumentRecipientEnum;
use App\Enum\DocumentVoterEnum;
use App\Enum\UserVoterEnum;
use App\Form\DocumentType;
use App\Repository\DocumentRepository;
use App\Services\MatchingManagerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/toolbox")
 */
class ToolboxController extends BaseController
{
    /**
     * @Route(name="toolbox")
     */
    public function index(
        Request $request,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        DocumentRepository $documentRepo
    ): Response {
        $params = [];
        $recipient = null;
        $user = $this->getUser();

        if ($this->isGranted(UserVoterEnum::IS_MANAGER, $user)) {
            $document = new Document();

            $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager($user);
            $matchingManagers = $matchingManager
                ? [$matchingManager]
                : []
            ;

            if ($matchingManager instanceof Institute) {
                $recipient = DocumentRecipientEnum::RECIPIENT_INSTITUTE_MANAGER;
                $document->setInstitute($matchingManager);
                $matchingManagers += $matchingManager->getAncestors()->toArray();
            } elseif ($matchingManager instanceof Association) {
                $recipient = DocumentRecipientEnum::RECIPIENT_ASSOCIATION_MANAGER;
                $document->setAssociation($matchingManager);
            }

            $params['addedDocuments'] = $documentRepo->getDocuments(
                $matchingManagers,
                null,
                false
            );

            $form = $this->createForm(DocumentType::class, $document);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $om = $this->getDoctrine()->getManager();
                $om->persist($document);
                $om->flush();

                $this->addFlash('success', $translator->trans('toolbox.document.flash.created'));

                return new RedirectResponse($this->generateUrl('toolbox'));
            }

            $params['form'] = $form->createView();
        } else {
            $institutes = $matchingManagerHelper->getAllMatchingManagerOfUser(
                $user,
                false,
                false,
                MatchingManagerInterface::TYPE_INSTITUTE
            );

            $associations = $matchingManagerHelper->getAllMatchingManagerOfUser(
                $user,
                false,
                false,
                MatchingManagerInterface::TYPE_ASSOCIATION
            );

            if ($user->isLocal()) {
                $recipient = DocumentRecipientEnum::RECIPIENT_MENTOR;
            } else {
                $recipient = DocumentRecipientEnum::RECIPIENT_MENTEE;
            }

            if (!empty($associations)) {
                $params['associationDocuments'] = $documentRepo->getDocuments($associations, $recipient);
            }

            if (!empty($institutes)) {
                $params['instituteDocuments'] = $documentRepo->getDocuments($institutes, $recipient);
            }
        }

        if ($recipient) {
            $params['generalDocuments'] = $documentRepo->getDocuments([], $recipient);
        }

        return $this->render('Toolbox/index.html.twig', $params);
    }

    /**
     * @Route("/document/{id}/edit", name="toolbox_document_edit", methods={"GET", "POST"}, options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function edit(TranslatorInterface $translator, Request $request, Document $document): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoterEnum::EDIT, $document);
        $form = $this->createForm(DocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($document);
            $om->flush();

            $this->addFlash('success', $translator->trans('toolbox.document.flash.edited'));

            return new JsonResponse(null, Response::HTTP_OK);
        }

        return $this->render('Toolbox/edit.html.twig', [
            'form' => $form->createView(),
            'document' => $document,
        ]);
    }

    /**
     * @Route("/document/{id}/delete", name="toolbox_document_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function delete(TranslatorInterface $translator, Request $request, Document $document): Response
    {
        $this->denyAccessUnlessGranted(DocumentVoterEnum::EDIT, $document);
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($document);
            $om->flush();

            $this->addFlash('success', $translator->trans('toolbox.document.flash.deleted'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'document' => $document,
            'type' => 'Document',
            'name' => $document->getName(),
            'id' => $document->getId(),
        ]);
    }
}
