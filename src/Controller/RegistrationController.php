<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Enum\RegistrationEmailDomainUserTypeEnum;
use App\Enum\UserProfileTypeEnum;
use App\Form\RegistrationType;
use App\Repository\InstituteRepository;
use App\Repository\UserRepository;
use App\Security\Authentication\EmailVerifier;
use App\Services\AllowedDomainHelper;
use App\Services\LoggerHelper;
use App\Services\MailManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

/**
 * @Route("/register")
 */
class RegistrationController extends BaseController
{
    private $emailVerifier;
    private $instituteRepo;
    private $allowedDomainHelper;
    private $mailManager;
    private $loggerHelper;
    private $translator;
    private $passwordHasher;

    public function __construct(
        EmailVerifier $emailVerifier,
        InstituteRepository $instituteRepo,
        AllowedDomainHelper $allowedDomainHelper,
        MailManager $mailManager,
        LoggerHelper $loggerHelper,
        TranslatorInterface $translator,
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->emailVerifier = $emailVerifier;
        $this->instituteRepo = $instituteRepo;
        $this->allowedDomainHelper = $allowedDomainHelper;
        $this->mailManager = $mailManager;
        $this->loggerHelper = $loggerHelper;
        $this->translator = $translator;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @Route(name="registration", options={ "expose" : true })
     */
    public function register(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_USER);
    }

    /**
     * @Route("/admin", name="registration_admin")
     */
    public function registerAdmin(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_ADMIN);
    }

    /**
     * @Route("/association-manager", name="registration_association-manager")
     */
    public function registerAssociationManager(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_ASSOCIATION);
    }

    /**
     * @Route("/institute-manager", name="registration_institute-manager")
     */
    public function registerInstituteManager(Request $request): Response
    {
        return $this->showRegistration($request, UserProfileTypeEnum::PROFILE_INSTITUTE);
    }

    /**
     * @Route("/confirm", name="register_confirm")
     */
    public function confirm(
        Request $request,
        TranslatorInterface $translator,
        UserRepository $userRepo,
        TokenStorageInterface $tokenStorage,
        Session $session
    ): Response {
        $user = null;

        if ($request->query->has('email') && $email = $request->get('email')) {
            $user = $userRepo->findByEmail($email);
        }

        if ($user) {
            try {
                $this->emailVerifier->handleEmailConfirmation($request, $user);
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $tokenStorage->setToken($token);
                $session->set('_security_main', serialize($token));
            } catch (VerifyEmailExceptionInterface $exception) {
                $this->addFlash('error', $exception->getReason());

                return $this->redirectToRoute('homepage');
            }
        } else {
            $this->addFlash('error', $translator->trans('registration.flash.email_not_found'));

            return $this->redirectToRoute('homepage');
        }

        $this->addFlash('success', $translator->trans('registration.flash.email_confirmed'));

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/complete_iframe", name="registration-complete_iframe")
     */
    public function registrationCompleteIframe(UrlGeneratorInterface $router): Response
    {
        return $this->render('Registration/registration_complete_iframe.html.twig', [
            'baseUrl' => $router->generate('homepage', [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);
    }

    private function showRegistration(Request $request, string $mode): Response
    {
        $user = new User();
        $user->setLocaleLanguage($request->getLocale());

        $matchingManager = $this->instituteRepo->findOneById($request->query->get('manager')) ?? null;
        $isIframe = $request->query->has('iframe');

        $form = $this->createForm(
            RegistrationType::class,
            $user,
            [
                'mode' => $mode,
                'matchingManager' => $matchingManager,
            ]
        );
        $form->handleRequest($request);

        $allowedDomains = [];
        $showDomainWarning = false;

        if ($form->isSubmitted()) {
            if (null === $user->isLocal()) {
                $userType = RegistrationEmailDomainUserTypeEnum::BOTH;
            } elseif ($user->isLocal()) {
                $userType = RegistrationEmailDomainUserTypeEnum::MENTORS;
            } else {
                $userType = RegistrationEmailDomainUserTypeEnum::MENTEES;
            }

            if ($user->getInstitute() && $user->getEmail()) {
                $allowedDomains = implode('<br>', $this->allowedDomainHelper->getAllowedDomains($user->getInstitute(), $userType));
                $showDomainWarning = !$this->allowedDomainHelper->isEmailAllowed($user->getEmail(), $user->getInstitute(), $userType);
            }

            if ($form->isValid()) {
                if (UserProfileTypeEnum::PROFILE_USER === $mode) {
                    $user->setApproved($this->allowedDomainHelper->isEmailAllowed($user->getEmail(), $user->getInstitute(), $userType));
                }

                if ($matchingManager) {
                    $matchingManager->increaseRegistration($isIframe);
                }

                $user->setPassword($this->passwordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                ));

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->emailVerifier->sendEmailConfirmation(
                    'register_confirm',
                    $user,
                    $this->mailManager->getEmailConfirmation($user->getEmail())
                );

                $this->loggerHelper->logRegistration('['.$user->getEmail().'] confirmed');

                if ($isIframe) {
                    $route = 'registration-complete_iframe';
                } else {
                    $route = 'homepage';
                    $this->addFlash('success', $this->translator->trans('registration.check_email'));
                }

                $this->addFlash('success', $this->translator->trans('registration.flash.user_created'));

                return new RedirectResponse($this->generateUrl($route, $request->query->all()));
            }

            $this->loggerHelper->logRegistration('['.$user->getEmail().'] '.' errors');
            $this->addFlash('error', $this->translator->trans('registration.flash.errors'));
        }

        return $this->render('Registration/registration.html.twig', [
            'form' => $form->createView(),
            'mode' => $mode,
            'no_index' => UserProfileTypeEnum::PROFILE_USER !== $mode,
            'allowedDomains' => $allowedDomains,
            'showDomainWarning' => $showDomainWarning,
        ]);
    }
}
