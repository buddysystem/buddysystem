<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\UserListResponse;
use App\Entity\MatchingManagerInterface;
use App\Entity\Report;
use App\Entity\User;
use App\Entity\UserApprovedHistory;
use App\Enum\BuddyStatusEnum;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\UserType;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\CityRepository;
use App\Repository\InstituteRepository;
use App\Repository\SemesterRepository;
use App\Repository\StudyRepository;
use App\Repository\UserApprovedHistoryRepository;
use App\Repository\UserRepository;
use App\Services\GrantedService;
use App\Services\MailManager;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Asset\Packages;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

/**
 * @Route("/admin/users")
 */
class UserController extends BaseController
{
    /**
     * @Route(name="user_index")
     */
    public function index(
        InstituteRepository $instituteRepo,
        SemesterRepository $semesterRepo,
        UserRepository $userRepo,
        AssociationRepository $associationRepo,
        CityRepository $cityRepo,
        StudyRepository $studyRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $manager = $matchingManagerHelper->getMatchingManagerOfManager();

        $params = [
            'institutes' => $instituteRepo->getByMatchingManager($manager),
            'nationalities' => $userRepo->getNationalitiesByMatchingManager($manager),
            'years' => $semesterRepo->getDistinctYears($manager),
            'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE, $manager),
            'studies' => $studyRepo->getUsedByInstitutes($manager),
            'hasUsersToApprove' => $userRepo->getNbUsers(['approved' => 0], $manager) > 0,
            'languages' => Languages::getLanguageCodes(),
        ];

        if (empty($params['studies'])) {
            $params['studies'] = $studyRepo->getUsedByInstitutes();
        }

        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params += [
                'associations' => $associationRepo->findAll(),
                'roles' => UserRoleEnum::$notSuperAdmin,
            ];
        } else {
            $params['roles'] = array_merge([UserRoleEnum::ROLE_USER], UserRoleEnum::$managers);
        }

        return $this->render('User/index.html.twig', $params);
    }

    /**
     * @Route("/list_users", name="user_list", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function getListUsers(
        Request $request,
        UserRepository $userRepo,
        DataTableResponseFactory $responseFactory,
        MatchingManagerHelper $matchingManagerHelper,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $manager = $matchingManagerHelper->getMatchingManagerOfManager();
        $search = $dataTableColumns->getSearch();

        if ($request->query->has('user_to_approve') && $request->query->get('user_to_approve')) {
            $search['approved'] = 0;
            $dataTableColumns->setSearch($search);
        }

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $userRepo->getNbUsers($search, $manager),
            'data' => $responseFactory->getResponse(
                UserListResponse::class,
                $userRepo->getListInfo($dataTableColumns, $manager),
                [
                    'showActions' => true,
                ]
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", options={ "expose" : true })
     *
     * @return RedirectResponse|Response
     */
    public function edit(
        Request $request,
        GrantedService $grantedService,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        User $user
    ) {
        $userLogged = $this->getUser();
        if ($userLogged === $user) {
            return $this->redirectToRoute('profile_edit');
        }
        $this->denyAccessUnlessGranted(UserVoterEnum::EDIT_OTHER, $user);

        $options = ['roles' => []];

        switch ($grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_ADMIN:
                $role = UserRoleEnum::ROLE_ADMIN;
                $options['mode'] = UserProfileTypeEnum::PROFILE_ADMIN;

                if ($this->isGranted(UserRoleEnum::ROLE_SUPER_ADMIN)) {
                    array_push($options['roles'], UserRoleEnum::ROLE_SUPER_ADMIN);
                }
                break;
            case UserRoleEnum::ROLE_INSTITUTE_MANAGER:
                $role = UserRoleEnum::ROLE_INSTITUTE_MANAGER;
                $options['mode'] = UserProfileTypeEnum::PROFILE_INSTITUTE;
                break;
            case UserRoleEnum::ROLE_BUDDYCOORDINATOR:
                $role = UserRoleEnum::ROLE_BUDDYCOORDINATOR;
                $options['mode'] = UserProfileTypeEnum::PROFILE_ASSOCIATION;
                break;
            default:
                $role = UserRoleEnum::ROLE_USER;
                $options['mode'] = UserProfileTypeEnum::PROFILE_USER;

                if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
                    array_push($options['roles'], UserRoleEnum::ROLE_ADMIN);
                }
                break;
        }

        $editForm = $this->createForm(UserType::class, $user, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $user->addRole($role);

            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', $translator->trans('profile.flash.updated'));

                return $this->redirectToRoute('user_index');
            }

            $this->addFlash('error', $translator->trans('profile.flash.errors'));
        }

        $showOverrideMaxRefusedLimitationButton = ($user->isLocal() && $user->hasMaxBuddiesRefused());

        return $this->render('User/edit_other.html.twig', [
            'user' => $user,
            'role' => $role,
            'matchingManagers' => $matchingManagerHelper->getAllMatchingManagerOfUser($user),
            'edit_form' => $editForm->createView(),
            'showOverrideMaxRefusedLimitationButton' => $showOverrideMaxRefusedLimitationButton,
        ]);
    }

    /**
     * @Route("/{id}/reports", name="user_report", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function showReport(EntityManagerInterface $em, User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoterEnum::SHOW, $user);

        $em->getFilters()->disable('softdeleteable');

        $reports = $user->getReports();
        $data = [];

        /** @var Report $report */
        foreach ($reports as $report) {
            $buddy = $report->getBuddy();
            $data[] = [
                'id' => $report->getId(),
                'comment' => $report->getComment(),
                'createdAt' => $report->getCreatedAt(),
                'reportedBy' => $user === $buddy->getMentor() ? $buddy->getMentee() : $buddy->getMentor(),
                'buddyDeleted' => $report->isDeleteBuddy(),
            ];
        }

        $em->getFilters()->enable('softdeleteable');

        return $this->render('Report/list_by_user.html.twig', [
            'user' => $user,
            'reports' => $data,
        ]);
    }

    /**
     * @Route("/{id}/buddies", name="user_buddies", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function showBuddiesList(BuddyRepository $buddyRepo, User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoterEnum::SHOW, $user);

        return $this->render('Buddy/list.html.twig', [
            'user' => $user,
            'buddies' => $buddyRepo->getBuddiesByUser(
                $user,
                $user->isLocal(),
                false,
                [
                    BuddyStatusEnum::STATUS_PENDING,
                    BuddyStatusEnum::STATUS_CONFIRMED,
                ]
            ),
        ]);
    }

    /**
     * @Route("/{id}/buddies-refused", name="user_buddies_refused", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function showBuddiesRefusedList(BuddyRepository $buddyRepo, User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoterEnum::SHOW, $user);

        return $this->render('Buddy/list.html.twig', [
            'user' => $user,
            'buddies' => $buddyRepo->getBuddiesByUser(
                $user,
                $user->isLocal(),
                true,
                [BuddyStatusEnum::STATUS_REFUSED]
            ),
        ]);
    }

    /**
     * @Route("/user-suggest", name="user_suggest", options={ "expose" : true })
     */
    public function userSuggest(
        UserRepository $userRepo,
        UploaderHelper $uploaderHelper,
        Packages $assetsManager,
        Request $request,
        MatchingManagerHelper $matchingManagerHelper
    ): JsonResponse {
        $user = $this->getUser();
        $query = $request->get('search', null);
        $users = $userRepo->getUsersSearchAutocomplete(
            $query,
            8,
            null,
            $user,
            $matchingManagerHelper->getMatchingManagerOfManager()
        );
        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'email' => $user->getEmail(),
                'fullname' => $user->getFullName(),
                'image' => $uploaderHelper->asset($user, 'pictureFile') ?? $assetsManager->getUrl('images/pictos/no_photo.png'),
            ];
        }

        return new JsonResponse($data, 200, [
            'Cache-Control' => 'no-cache',
        ]);
    }

    /**
     * @Route("/to-approve", name="user_to-approve")
     */
    public function userToApprove(
        SemesterRepository $semesterRepo,
        CityRepository $cityRepo,
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $manager = $matchingManagerHelper->getMatchingManagerOfManager();

        $roles = [UserRoleEnum::ROLE_USER];

        if ($this->isGranted(UserRoleEnum::ROLE_INSTITUTE_MANAGER)) {
            array_push($roles, UserRoleEnum::ROLE_INSTITUTE_MANAGER);
        }

        if ($this->isGranted(UserRoleEnum::ROLE_BUDDYCOORDINATOR)) {
            array_push($roles, UserRoleEnum::ROLE_BUDDYCOORDINATOR);
        }

        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            array_push($roles, UserRoleEnum::ROLE_ADMIN);
        }

        return $this->render('User/to_approve.html.twig', [
            'years' => $semesterRepo->getDistinctYears($manager),
            'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE, $manager),
            'languages' => Languages::getLanguageCodes(),
            'associations' => $associationRepo->findAll(),
            'institutes' => $instituteRepo->getByMatchingManager($manager),
            'roles' => $roles,
        ]);
    }

    /**
     * @Route("/{id}/approve", name="approve_user", options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function approveUser(
        Request $request,
        TranslatorInterface $translator,
        MailManager $mailManager,
        User $userToApprove
    ): Response {
        $this->denyAccessUnlessGranted(UserVoterEnum::APPROVE, $userToApprove);
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userToApprove->setApproved(true);

            $userApprovedHistory = new UserApprovedHistory();
            $userApprovedHistory->setUserApproved($userToApprove);
            $userApprovedHistory->setUserValidator($this->getUser());

            $om = $this->getDoctrine()->getManager();
            $om->persist($userApprovedHistory);
            $om->persist($userToApprove);
            $om->flush();

            $mailManager->sendUserApprovedEmail($userToApprove);

            $this->addFlash('success', $translator->trans('user.flash.approved', ['%email%' => $userToApprove->getEmail()]));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('User/confirm_approval.html.twig', [
            'form' => $form->createView(),
            'name' => $userToApprove->getEmail(),
            'id' => $userToApprove->getId(),
        ]);
    }

    /**
     * @Route("/approved-history", name="user_approved-history")
     *
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function userApprovedHistory(UserApprovedHistoryRepository $userApprovedHistoryRepo): Response
    {
        $approvedHistories = $userApprovedHistoryRepo->findAll();

        return $this->render('User/approved_history.html.twig', [
            'approvedHistories' => $approvedHistories,
        ]);
    }

    /**
     * @Route("/{id}/override_max_refused", name="user_override-max-refused")
     */
    public function overrideMaxBuddiesRefused(TranslatorInterface $translator, User $user): Response
    {
        $this->denyAccessUnlessGranted(UserVoterEnum::EDIT_OTHER, $user);

        $user->setOverrideMaxRefusedLimitation(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $this->addFlash('success', $translator->trans('profile.flash.updated'));

        return $this->redirectToRoute('user_edit', ['id' => $user->getId()]);
    }
}
