<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MatchingManagerInterface;
use App\Entity\Thread;
use App\Enum\ThreadVoterEnum;
use App\Enum\UserVoterEnum;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use App\Services\JWTFactory;
use App\Services\MatchingManagerHelper;
use App\Services\MessagingHelper;
use App\Services\MessagingProvider;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\WebLink\Link;

/**
 * @Route("/messaging")
 */
class MessagingController extends BaseController
{
    /**
     * @Route(name="messaging_homepage")
     */
    public function homepage(
        Request $request,
        MatchingManagerHelper $matchingManagerHelper,
        MessagingProvider $messagingProvider,
        UserRepository $userRepo
    ): Response {
        $user = $this->getUser();

        if ($this->isGranted(UserVoterEnum::IS_MANAGER, $user)) {
            $newThread = [];

            if ($request->query->has('recipient')) {
                $recipient = $userRepo->findOneBy(['id' => $request->query->get('recipient')]);
                if ($recipient) {
                    $newThread['recipient'] = [
                        'fullname' => $recipient->getFullName(),
                        'email' => $recipient->getEmail(),
                    ];
                }
            }
        } else {
            $newThread = ($institute = $user->getInstitute())
                ? $matchingManagerHelper->getMatchingManagerTypeWithManager($institute)
                : []
            ;
        }

        return $this->render('Messaging/homepage.html.twig', [
            'user' => [
                'id' => trim((string) $user->getId()),
                'iri' => $messagingProvider->getUserIRI($user),
                'isManager' => $this->isGranted(UserVoterEnum::IS_MANAGER, $user),
            ],
            'newThread' => $newThread,
        ]);
    }

    /**
     * @Route("/api/threads_groups", methods={"GET"}, name="messaging_api_get_threads_groups", options={ "expose" : true })
     */
    public function apiGetThreadsGroups(
        Request $request,
        MessagingProvider $messagingProvider,
        SerializerInterface $serializer,
        JWTFactory $JWTFactory,
        string $mercureSubscribeUrl
    ): Response {
        $user = $this->getUser();
        $this->addLink($request, new Link('mercure', $mercureSubscribeUrl));

        $response = new JsonResponse(
            $serializer->serialize($messagingProvider->getAllowedThreads($user), 'json'),
            Response::HTTP_OK,
            [],
            true
        );

        $response->headers->setCookie($JWTFactory->getCookie($messagingProvider->getAllTopicsIRI($user)));

        return $response;
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/messages",
     *     methods={"GET"},
     *     name="messaging_api_get_threads_groups_messages",
     *     options={ "expose" : true }
     * )
     */
    public function apiGetThreadGroupMessages(
        Request $request,
        MessagingProvider $messagingProvider,
        MessageRepository $messageRepo,
        SerializerInterface $serializer,
        string $id
    ): Response {
        $threads = $messagingProvider->getThreadsByThreadGroupId($id, $this->getUser());

        if (empty($threads)) {
            throw new NotFoundHttpException(sprintf('ThreadGroup not found.'));
        }

        foreach ($threads as $thread) {
            $this->denyAccessUnlessGranted(ThreadVoterEnum::SEE, $thread);
        }

        $offset = $request->query->getInt('page', 1);

        $data = [];
        $messages = $messageRepo->getByThreads($threads, $offset - 1);

        foreach ($messages as $message) {
            $data[] = $messagingProvider->getMessageDTO($message);
        }

        return new JsonResponse(
            $serializer->serialize($data, 'json'),
            Response::HTTP_OK,
            ['Link' => $id.'; rel="mercure"'],
            true
        );
    }

    /**
     * @Route(
     *     "/api/threads/{id}/messages",
     *     methods={"POST"},
     *     name="messaging_api_post_thread_messages",
     *     options={ "expose" : true }
     * )
     */
    public function apiPostThreadMessage(
        Request $request,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        Thread $thread
    ): Response {
        $data = json_decode((string) $request->getContent(), true);
        if (!$data || !isset($data['content'])) {
            throw new \InvalidArgumentException('Missing content.');
        }

        $this->denyAccessUnlessGranted(ThreadVoterEnum::REPLY, $thread);

        $message = $messagingHelper->createMessage($thread, $data['content'], $this->getUser());

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new JsonResponse(
            $serializer->serialize($messagingProvider->getMessageDTO($message), 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/mark_as_read",
     *     methods={"POST"},
     *     name="messaging_api_put_threads_groups_mark_as_read",
     *     options={ "expose" : true }
     * )
     */
    public function apiThreadGroupMarkAsRead(
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        string $id
    ): Response {
        $user = $this->getUser();
        $threads = $messagingProvider->getThreadsByThreadGroupId($id, $user);

        if (empty($threads)) {
            throw new NotFoundHttpException(sprintf('ThreadGroup not found.'));
        }

        foreach ($threads as $thread) {
            $messagingHelper->markAsRead($thread, $user);
        }

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new Response();
    }

    /**
     * @Route(
     *     "/api/threads_groups/{id}/toggle_favorite",
     *     methods={"POST"},
     *     name="messaging_api_put_threads_groups_toggle_favorite",
     *     options={ "expose" : true }
     * )
     */
    public function apiThreadGroupToggleFavorite(MessagingHelper $messagingHelper, string $id): Response
    {
        $messagingHelper->toggleFavorite($id, $this->getUser());

        return new Response();
    }

    /**
     * @Route(
     *     "/api/threads",
     *     methods={"POST"},
     *     name="messaging_api_post_thread",
     *     options={ "expose" : true }
     * )
     */
    public function apiNewThread(
        Request $request,
        SerializerInterface $serializer,
        MessagingProvider $messagingProvider,
        MessagingHelper $messagingHelper,
        MatchingManagerHelper $matchingManagerHelper,
        UserRepository $userRepo
    ): Response {
        $user = $this->getUser();

        $data = json_decode((string) $request->getContent(), true);
        if (!$data || !isset($data['content']) || !isset($data['recipient'])) {
            throw new \InvalidArgumentException('Missing content.');
        }

        $recipient = $data['recipient'];

        if ($this->isGranted(UserVoterEnum::IS_MANAGER, $user)) {
            $userRecipient = $userRepo->findOneBy(['email' => $recipient]);

            $this->denyAccessUnlessGranted(UserVoterEnum::SEND_MESSAGE, $userRecipient);

            $thread = $messagingHelper->getThreadBySubjectAndParticipants(
                MessagingHelper::DIRECT_MESSAGE,
                [$user, $userRecipient]
            );
        } else {
            if (MatchingManagerInterface::TYPE_ASSOCIATION === $recipient) {
                $subject = MessagingHelper::CHANNEL_ASSOCIATION_USER;
            } elseif (MatchingManagerInterface::TYPE_INSTITUTE === $recipient) {
                $subject = MessagingHelper::CHANNEL_INSTITUTE_USER;
            } else {
                throw $this->createAccessDeniedException();
            }

            $matchingManagers = $matchingManagerHelper->getAllMatchingManagerOfUser(
                $user,
                true,
                false,
                $recipient
            );

            if (empty($matchingManagers)) {
                throw $this->createNotFoundException();
            }

            $thread = $messagingHelper->getThreadUserManagers($subject, $matchingManagers, $user);
        }

        $message = $messagingHelper->createMessage($thread, $data['content'], $user);

        $om = $this->getDoctrine()->getManager();
        $om->flush();

        return new JsonResponse(
            $serializer->serialize($messagingProvider->getMessageDTO($message), 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }
}
