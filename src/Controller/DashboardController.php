<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Association;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Report;
use App\Enum\BuddyStatusEnum;
use App\Enum\BuddyVoterEnum;
use App\Enum\ReportVoterEnum;
use App\Enum\StatTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\RefusalBuddyType;
use App\Form\ReportType;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\InstituteRepository;
use App\Repository\UserRepository;
use App\Services\MailManager;
use App\Services\MatchingManagerHelper;
use App\Services\SemesterHelper;
use App\Services\StatsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/dashboard")
 */
class DashboardController extends BaseController
{
    /**
     * @Route(name="dashboard")
     */
    public function dashboard(
        UserRepository $userRepo,
        BuddyRepository $buddyRepo,
        SemesterHelper $semesterHelper,
        MatchingManagerHelper $matchingManagerHelper,
        InstituteRepository $instituteRepo
    ): Response {
        $user = $this->getUser();
        $params = ['user' => $user];

        if ($this->isGranted(UserVoterEnum::IS_MANAGER, $user)) {
            $params['isManager'] = true;

            if ($matchingManager = $matchingManagerHelper->getMatchingManagerOfManager()) {
                $params['manager'] = $matchingManager;

                if ($matchingManager instanceof Institute) {
                    $params['managerType'] = MatchingManagerInterface::TYPE_INSTITUTE;
                    $params['dateSwitchSemester'] = $semesterHelper->getDateNextSwitch($matchingManager);
                } elseif ($matchingManager instanceof Association) {
                    $params['managerType'] = MatchingManagerInterface::TYPE_ASSOCIATION;
                    $params['institutes'] = $instituteRepo->getByAssociationOrderByChildren($matchingManager);
                }

                $params['nbMenteesNotMatched'] = $userRepo->getNbUsers(
                    [
                        'local' => 0,
                        'nbBuddies' => 0,
                        'archived' => 0,
                    ],
                    $matchingManager,
                    UserRoleEnum::$managerAndAdmin,
                    true,
                    true
                );
            } else {
                $params['institutes'] = $instituteRepo->findAll('i.id');
            }
        } else {
            $params['confirmedBuddies'] = $buddyRepo->getMyBuddiesValidated($user);
            $params['archivedBuddies'] = $buddyRepo->getMyBuddiesArchived($user);
            $params['manager'] = $matchingManagerHelper->getMatchingManagerOfUser($user);
            $params['isManager'] = false;

            if ($user->isLocal()) {
                $params['waitingBuddies'] = $buddyRepo->getMyBuddiesWaitingValidation($user);
            }
        }

        return $this->render('Dashboard/index.html.twig', $params);
    }

    /**
     * @Route(
     *     "/stats", name="dashboard_stats",
     *     methods={"GET"},
     *     options={"expose" : true},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function dashboardStats(
        Request $request,
        StatsService $statsService,
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        SemesterHelper $semesterHelper
    ): JsonResponse {
        $isAdmin = $this->isGranted(UserRoleEnum::ROLE_ADMIN);

        if (!$isAdmin
            && (!$request->query->has('matchingManagerId') || !$request->query->has('matchingManagerType'))
        ) {
            throw new \InvalidArgumentException('Missing argument.');
        }

        $matchingManagerId = $request->query->get('matchingManagerId');
        $matchingManagerType = $request->query->get('matchingManagerType');

        switch ($matchingManagerType) {
            case MatchingManagerInterface::TYPE_ASSOCIATION:
                $matchingManager = $associationRepo->findOneBy(['id' => $matchingManagerId]);
                break;
            case MatchingManagerInterface::TYPE_INSTITUTE:
                $matchingManager = $instituteRepo->findOneBy(['id' => $matchingManagerId]);
                break;
            default:
                $matchingManager = null;
                break;
        }

        if (!$isAdmin && !$matchingManager) {
            throw new NotFoundHttpException('Organisation not found.');
        }

        $statsType = !$matchingManager || !$matchingManager->isFullYearOnly()
            ? (string) $request->query->get('statsType')
            : StatTypeEnum::TYPE_YEAR;
        $isYearStats = StatTypeEnum::TYPE_YEAR === $statsType;

        $params = array_merge(
            [
                'manager' => $matchingManager,
                'isYearStats' => $isYearStats,
            ],
            $statsService->getStatsValues($matchingManager, $statsType),
            $statsService->getLastWeekStats($matchingManager, $isYearStats)
        );

        if ($matchingManager instanceof Institute) {
            $params['dateSwitchSemester'] = $semesterHelper->getDateNextSwitch($matchingManager);
        }

        return new JSonResponse($this->renderView('Dashboard/_manager_view_stats.html.twig', $params));
    }

    /**
     * @Route("/buddies/{id}/validate", name="buddy_matching_validate")
     */
    public function validationMatching(MailManager $mailManager, Buddy $buddy): RedirectResponse
    {
        $this->denyAccessUnlessGranted(BuddyVoterEnum::CONFIRM, $buddy);
        $buddy->setStatus(BuddyStatusEnum::STATUS_CONFIRMED);

        $om = $this->getDoctrine()->getManager();
        $om->persist($buddy);
        $om->flush();

        $mailManager->notifyNewBuddy($buddy, $buddy->getMentee());

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/buddies/{id}/reason_refusal", name="buddy_reason_refusal", methods={"GET", "POST"}, options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function reasonRefusalBuddy(TranslatorInterface $translator, Request $request, Buddy $buddy): Response
    {
        $this->denyAccessUnlessGranted(BuddyVoterEnum::CONFIRM, $buddy);
        $form = $this->createForm(RefusalBuddyType::class, $buddy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $buddy->setStatus(BuddyStatusEnum::STATUS_REFUSED);
            $om = $this->getDoctrine()->getManager();
            $om->persist($buddy);
            $om->flush();

            $this->addFlash('success', $translator->trans('buddy.flash.refused'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Dashboard/refusal.html.twig', [
            'form' => $form->createView(),
            'buddy' => $buddy,
        ]);
    }

    /**
     * @Route("/buddies/{id}/report/type/{reportType}", name="buddy_report", methods={"GET", "POST"}, options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function reportBuddy(
        TranslatorInterface $translator,
        Request $request,
        Buddy $buddy,
        string $reportType
    ): Response {
        $report = new Report();
        $buddy->addReport($report);

        $this->denyAccessUnlessGranted(ReportVoterEnum::CREATE, $report);

        if ('no_reply' === $reportType) {
            $report->setComment($translator->trans('dashboard.buddy.no_reply.comment'));
            $showComment = false;
        } else {
            $showComment = true;
        }

        if ($this->getUser()->isLocal()) {
            $buddy->getMentee()->addReport($report);
        } else {
            $buddy->getMentor()->addReport($report);
        }

        $form = $this->createForm(ReportType::class, $report, ['show_comment' => $showComment]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($report);

            if ($report->isDeleteBuddy()) {
                $om->remove($buddy);
            }
            $om->flush();

            $this->addFlash('success', $translator->trans('dashboard.buddy.report.flash'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Report/report.html.twig', [
            'form' => $form->createView(),
            'buddy' => $buddy,
            'reportType' => $reportType,
        ]);
    }

    /**
     * @Route("/answer_next_semester/{response}", name="answer_next_semester", requirements={"response" : "0|1"})
     */
    public function answerNextSemester(TranslatorInterface $translator, int $response): RedirectResponse
    {
        $user = $this->getUser();

        if ($user->isAskNextSemester()) {
            if ($response) {
                $user->setArchived(false);
                $user->setArrival(null);
                $user->setSemester($user->getInstitute()->getSemester());

                $this->addFlash('info', $translator->trans('dashboard.next_semester.flash.check_profile'));
            }

            $user->setIsAskNextSemester(false);

            $om = $this->getDoctrine()->getManager();
            $om->persist($user);
            $om->flush();
        }

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route(
     *     "/users/{id}/unarchived_banner",
     *     name="unarchived_banner",
     *     methods={"GET"},
     *     options={ "expose" : true },
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function unarchivedBanner(): Response
    {
        $user = $this->getUser();

        if (!$user->isAskNextSemester()) {
            $om = $this->getDoctrine()->getManager();
            $user->setIsAskNextSemester(true);
            $om->persist($user);
            $om->flush();
        }

        return $this->render('Dashboard/archived_banner.html.twig', [
            'isModal' => true,
        ]);
    }
}
