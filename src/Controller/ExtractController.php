<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Extract;
use App\Extract\ExtractModel;
use App\Form\ExtractType;
use App\Message\ExtractMessage;
use App\Repository\ExtractRepository;
use App\Services\FileWriterHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/extract")
 */
class ExtractController extends BaseController
{
    /**
     * @Route(name="extract_index")
     */
    public function index(
        Request $request,
        TranslatorInterface $translator,
        ExtractRepository $extractRepo,
        int $maxSimultaneousExtractsByUser
    ): Response {
        $user = $this->getUser();

        if (\count($extractRepo->findBy(['user' => $user, 'generated' => false])) >= $maxSimultaneousExtractsByUser) {
            return $this->render('Extract/limit_reached.html.twig', [
                'limit' => $maxSimultaneousExtractsByUser,
            ]);
        }

        $extractModel = new ExtractModel();
        $extractModel->setUser($user);
        $form = $this->createForm(ExtractType::class, $extractModel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $extractEntity = new Extract();
            $extractEntity->setFilename($extractModel->getFilename());
            $user->addExtract($extractEntity);

            $om = $this->getDoctrine()->getManager();
            $om->persist($extractEntity);
            $om->flush();

            $this->dispatchMessage(new ExtractMessage($extractModel));
            $this->addFlash('info', $translator->trans('extract.generating.flash'));

            return $this->redirectToRoute('extract_index');
        }

        return $this->render('Extract/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/download/{filename}", name="extract_download", methods={"GET"})
     */
    public function downloadExtract(
        EntityManagerInterface $em,
        FileWriterHelper $fileWriterHelper,
        ExtractRepository $extractRepo,
        string $filename
    ): Response {
        $extract = $extractRepo->findOneBy(['filename' => $filename, 'user' => $this->getUser(), 'generated' => true]);

        if (!$extract) {
            throw $this->createAccessDeniedException();
        }

        $filepath = $fileWriterHelper->getExtractDir().$filename;

        if (!file_exists($filepath)) {
            throw $this->createNotFoundException();
        }

        $extract->incrementNbDownloads();
        $em->flush();

        $response = new BinaryFileResponse($filepath);
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();

        if ($mimeTypeGuesser->isGuesserSupported()) {
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($filepath));
        } else {
            $response->headers->set('Content-Type', 'mime/type');
        }

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;
    }
}
