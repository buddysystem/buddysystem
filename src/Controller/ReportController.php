<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Report;
use App\Enum\ReportVoterEnum;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/report")
 */
class ReportController extends BaseController
{
    /**
     * @Route("/{id}/delete", name="report_delete",  methods={"GET", "POST"}, options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function deleteReport(Request $request, TranslatorInterface $translator, Report $report): Response
    {
        $this->denyAccessUnlessGranted(ReportVoterEnum::DELETE, $report);
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($report);
            $om->flush();

            $this->addFlash('success', $translator->trans('report.flash.deleted'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Report',
            'name' => $report->getComment(),
            'id' => $report->getId(),
        ]);
    }
}
