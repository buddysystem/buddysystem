<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\InstituteListResponse;
use App\DataTable\Response\InstituteRequestListResponse;
use App\Entity\Institute;
use App\Entity\InstituteRequest;
use App\Entity\MatchingManagerInterface;
use App\Form\InstituteRequestType;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\InstituteRepository;
use App\Repository\InstituteRequestRepository;
use App\Services\LocaleManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/institutes_finder")
 */
class InstituteFinderController extends BaseController
{
    /**
     * @Route(
     *     name="institutes_finder",
     *     methods={"GET"},
     *     options={"expose" : true},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function instituteFinder(
        LocaleManager $localeManager,
        CountryRepository $countryRepo,
        CityRepository $cityRepo,
        InstituteRepository $instituteRepo
    ): Response {
        return $this->render('Institute/finder.html.twig', [
            'countries' => $countryRepo->getWithMatchingManager(
                $localeManager->getCurrentLocale(),
                MatchingManagerInterface::TYPE_INSTITUTE
            ),
            'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE),
            'institutes' => $instituteRepo->findBy(['activated' => true], ['name' => 'asc']),
        ]);
    }

    /**
     * @Route(
     *     "/list_institutes",
     *     name="institute_finder_list",
     *     options={ "expose" : true },
     *     methods={"GET"},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function getListInstitutes(
        InstituteRepository $instituteRepo,
        DataTableResponseFactory $responseFactory,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $dataTableColumns->setSearch(array_merge($dataTableColumns->getSearch(), ['activated' => 1]));
        $nbInstitutes = $instituteRepo->getNbInstitutes($dataTableColumns->getSearch());
        $institutes = $instituteRepo->getListInfo($dataTableColumns);

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $nbInstitutes,
            'data' => $responseFactory->getResponse(InstituteListResponse::class, $institutes),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route(
     *     "/new_request",
     *     name="new_institute_request",
     *     options={ "expose" : true },
     *     methods={"GET", "POST"},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function newInstituteRequest(Request $request, TranslatorInterface $translator): Response
    {
        $instituteRequest = new InstituteRequest();
        $form = $this->createForm(InstituteRequestType::class, $instituteRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($instituteRequest);
            $om->flush();

            $this->addFlash('success', $translator->trans('institute_finder.not_found.flash'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Institute/finder_not_found.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/institutes_requested",
     *     name="institutes_requested",
     *     methods={"GET"}
     * )
     */
    public function instituteRequests(
        CountryRepository $countryRepo,
        LocaleManager $localeManager,
        CityRepository $cityRepo
    ): Response {
        return $this->render('Institute/list_requested.html.twig', [
            'countries' => $countryRepo->getWithMatchingManager(
                $localeManager->getCurrentLocale(),
                MatchingManagerInterface::TYPE_INSTITUTE
            ),
            'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE),
        ]);
    }

    /**
     * @Route(
     *     "/list_institutes_requested",
     *     name="institutes_requested_list",
     *     options={ "expose" : true },
     *     methods={"GET"},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function getListInstitutesRequested(
        InstituteRequestRepository $instituteRequestRepo,
        DataTableResponseFactory $responseFactory,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $nbRequests = $instituteRequestRepo->getNbRequests($dataTableColumns->getSearch());
        $requests = $instituteRequestRepo->getListInfo($dataTableColumns);

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $nbRequests,
            'data' => $responseFactory->getResponse(InstituteRequestListResponse::class, $requests),
        ];

        return new JSonResponse($response);
    }
}
