<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\UserListResponse;
use App\Entity\Association;
use App\Entity\Buddy;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\User;
use App\Enum\BuddyStatusEnum;
use App\Enum\BuddyVoterEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Form\BuddyType;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\CityRepository;
use App\Repository\InstituteRepository;
use App\Repository\MotivationRepository;
use App\Repository\StudyRepository;
use App\Repository\UserRepository;
use App\Services\MailManager;
use App\Services\MatchingHelper;
use App\Services\MatchingManagerHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/matching")
 */
class MatchingController extends BaseController
{
    /**
     * STEP 1 : List of international students
     *
     * @Route(name="matching_step1", options={"expose" : true})
     */
    public function step1(
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        MotivationRepository $motivationRepo,
        CityRepository $cityRepo,
        StudyRepository $studyRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params = [
                'associations' => $associationRepo->findAll(),
                'languages' => Languages::getLanguageCodes(),
                'institutes' => $instituteRepo->getByMatchingManager(),
                'motivations' => $motivationRepo->findAll(),
                'studies' => $studyRepo->getUsedByInstitutes(),
                'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE),
            ];
        } else {
            $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();
            $params = [
                'languages' => Languages::getLanguageCodes(),
                'institutes' => $instituteRepo->getByMatchingManager($matchingManager, false, true),
                'motivations' => $motivationRepo->getAllSelected($matchingManager, false),
                'studies' => $studyRepo->getUsedByInstitutes($matchingManager),
                'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE, $matchingManager),
            ];
        }

        if (empty($params['studies'])) {
            $params['studies'] = $studyRepo->getUsedByInstitutes();
        }

        return $this->render('Matching/step1.html.twig', $params);
    }

    /**
     * @Route("/list_mentees", name="mentees_list", options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function getListStep1(
        UserRepository $userRepo,
        DataTableResponseFactory $responseFactory,
        MatchingManagerHelper $matchingManagerHelper,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();

        $dataTableColumns->setSearch(array_merge($dataTableColumns->getSearch(), ['local' => 0]));
        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $userRepo->getNbUsersNotMatched($dataTableColumns->getSearch(), $matchingManager),
            'data' => $responseFactory->getResponse(
                UserListResponse::class,
                $userRepo->getListStudentsNotMatched($dataTableColumns, $matchingManager)
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * STEP 2 : List of local students
     *
     * @Route("/mentee/{id}", name="matching_step2", options={"expose" : true})
     */
    public function step2(
        InstituteRepository $instituteRepo,
        MotivationRepository $motivationRepo,
        BuddyRepository $buddyRepo,
        CityRepository $cityRepo,
        StudyRepository $studyRepo,
        MatchingManagerHelper $matchingManagerHelper,
        TranslatorInterface $translator,
        User $international,
        EntityManagerInterface $entityManager
    ): Response {
        if ($international->isLocal()) {
            throw $this->createAccessDeniedException();
        }

        $realNbBuddies = $buddyRepo->getRealNbBuddiesByUser($international);
        if ($realNbBuddies !== $international->getNbBuddies()) {
            $international->setNbBuddies($realNbBuddies);
            $entityManager->flush();
        }

        $this->denyAccessUnlessGranted(
            UserVoterEnum::MATCH,
            $international,
            $realNbBuddies >= $international->getNbBuddiesWanted()
                ? $translator->trans('matching.error.max_buddies_reached', ['%name%' => $international->getFullName()])
                : $translator->trans('error.403')
        );

        $params = ['international' => $international];

        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params += [
                'languages' => Languages::getLanguageCodes(),
                'institutes' => $instituteRepo->getByMatchingManager(),
                'motivations' => $motivationRepo->findAll(),
                'studies' => $studyRepo->getUsedByInstitutes(),
                'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE),
            ];
        } else {
            $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();
            $params += [
                'languages' => Languages::getLanguageCodes(),
                'institutes' => $instituteRepo->getByMatchingManager($matchingManager, false, true),
                'motivations' => $motivationRepo->getAllSelected($matchingManager, true),
                'studies' => $studyRepo->getUsedByInstitutes($matchingManager),
                'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE, $matchingManager),
            ];
        }

        if (empty($params['studies'])) {
            $params['studies'] = $studyRepo->getUsedByInstitutes();
        }

        return $this->render('Matching/step2.html.twig', $params);
    }

    /**
     * @Route("/list_mentors/{id}", name="mentors_list", options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function getListStep2(
        UserRepository $userRepo,
        DataTableResponseFactory $responseFactory,
        MatchingHelper $matchingHelper,
        MatchingManagerHelper $matchingManagerHelper,
        User $international,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        if ($international->isLocal()) {
            throw $this->createAccessDeniedException();
        }

        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager() ?? $matchingManagerHelper->getMatchingManagerOfUser($international);

        $dataTableColumns->setSearch(array_merge($dataTableColumns->getSearch(), ['local' => 1]));
        $locals = $userRepo->getListStudentsNotMatched(
            $dataTableColumns,
            $matchingManager,
            $international
        );

        $sort = $dataTableColumns->getSort();
        $orderDir = \array_key_exists('field', $sort) && 'score' === $sort['field']
            ? $sort['dir']
            : null
        ;

        [$locals, $nbTotal] = $matchingHelper->matching(
            $international,
            $locals,
            $orderDir,
            $dataTableColumns->getFirstResult(),
            $dataTableColumns->getMaxResults()
        );

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $nbTotal,
            'data' => $responseFactory->getResponse(
                UserListResponse::class,
                $locals
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * STEP 3 : Compare
     *
     * @Route("/mentee/{internationalId}/mentor/{localId}", name="matching_step3", options={"expose" : true})
     *
     * @ParamConverter("international", options={"id" : "internationalId"})
     * @ParamConverter("local", options={"id" : "localId"})
     */
    public function step3(
        MatchingHelper $matchingHelper,
        MatchingManagerHelper $matchingManagerHelper,
        BuddyRepository $buddyRepo,
        UserRepository $userRepo,
        RouterInterface $router,
        TranslatorInterface $translator,
        User $international,
        User $local,
        DataTableModel $dataTableColumns,
        EntityManagerInterface $entityManager
    ): Response {
        if ($international->isLocal() || !$local->isLocal()) {
            throw $this->createAccessDeniedException();
        }

        $realNbBuddiesLocal = $buddyRepo->getRealNbBuddiesByUser($local);
        if ($realNbBuddiesLocal !== $local->getNbBuddies()) {
            $local->setNbBuddies($realNbBuddiesLocal);
            $entityManager->flush();
        }

        $this->denyAccessUnlessGranted(
            UserVoterEnum::MATCH,
            $international,
            $buddyRepo->getRealNbBuddiesByUser($international) >= $international->getNbBuddiesWanted()
                ? $translator->trans('matching.error.max_buddies_reached', ['%name%' => $international->getFullName()])
                : $translator->trans('error.403')
        );
        $this->denyAccessUnlessGranted(
            UserVoterEnum::MATCH,
            $local,
            $realNbBuddiesLocal >= $local->getNbBuddiesWanted()
                ? $translator->trans('matching.error.max_buddies_reached', ['%name%' => $local->getFullName()])
                : $translator->trans('error.403')
        );

        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager() ?? $matchingManagerHelper->getMatchingManagerOfUser($international);

        $dataTableColumns->setSearch(array_merge($dataTableColumns->getSearch(), ['local' => 1]));
        $locals = $userRepo->getListStudentsNotMatched(
            $dataTableColumns,
            $matchingManager,
            $international
        );

        [$locals, $nbTotal] = $matchingHelper->matching(
            $international,
            $locals,
            'desc'
        );

        if (array_key_last($locals) !== $local->getId()) {
            while (key($locals) !== $local->getId()) {
                next($locals);
            }
            next($locals);

            $nextMentor = $router->generate(
                'matching_step3',
                [
                    'internationalId' => $international->getId(),
                    'localId' => key($locals),
                ]
            );
        } else {
            $nextMentor = false;
        }

        if ($matchingHelper->getAgeDiff($international->getAge(), $local->getAge()) > $matchingHelper::WARNING_AGE_DIFF) {
            $this->addFlash(
                'warning',
                $translator->trans(
                    'matching.flash.age_diff',
                    ['%age_diff%' => $matchingHelper::WARNING_AGE_DIFF]
                )
            );
        }

        return $this->render('Matching/matching.html.twig', array_merge(
            [
                'international' => $international,
                'local' => $local,
                'toMatch' => true,
                'matchingManager' => $international->getInstitute(),
                'nextMentor' => $nextMentor,
            ],
            $matchingHelper->compareBuddies($international, $local)
        ));
    }

    /**
     * @Route("/mentee/{internationalId}/mentor/{localId}/proposal", name="propose_matching", options={"expose" : true})
     * @ParamConverter("international", options={"id" : "internationalId"})
     * @ParamConverter("local", options={"id" : "localId"})
     */
    public function matchingProposal(
        Request $request,
        MailManager $mailManager,
        MatchingManagerHelper $matchingManagerHelper,
        TranslatorInterface $translator,
        User $international,
        User $local
    ): Response {
        if ($international->isLocal() || !$local->isLocal()) {
            throw $this->createAccessDeniedException();
        }

        $this->denyAccessUnlessGranted(UserVoterEnum::MATCH, $international);
        $this->denyAccessUnlessGranted(UserVoterEnum::MATCH, $local);

        $buddy = new Buddy($local, $international);
        $this->denyAccessUnlessGranted(BuddyVoterEnum::CREATE, $buddy);

        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager() ?? $matchingManagerHelper->getMatchingManagerOfUser($international);

        if ($matchingManager instanceof Institute) {
            $buddy->setInstitute($matchingManager);
        } else {
            if ($matchingManager instanceof Association) {
                $buddy->setAssociation($matchingManager);
            }

            $buddy->setInstitute($international->getInstitute());
        }

        $form = $this->createForm(BuddyType::class, $buddy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($buddy);
            $om->flush();

            if ($buddy->getInstitute()->isMentorValidationRequired()) {
                $mailManager->notifyNewBuddyToValidate($buddy, $buddy->getMentor());
            } else {
                $buddy->setStatus(BuddyStatusEnum::STATUS_CONFIRMED);
                $om->flush();

                $mailManager->notifyNewBuddy($buddy, $buddy->getMentor());
                $mailManager->notifyNewBuddy($buddy, $buddy->getMentee());
            }

            $this->addFlash('success', $translator->trans('matching.flash.done'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Matching/approve_matching.html.twig', [
            'form' => $form->createView(),
            'international' => $international,
            'local' => $local,
        ]);
    }
}
