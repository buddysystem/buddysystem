<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Enum\UserProfileTypeEnum;
use App\Enum\UserRoleEnum;
use App\Enum\UserVoterEnum;
use App\Exporter\ProfileExporter;
use App\Form\UserEditPasswordType;
use App\Form\UserType;
use App\Services\GrantedService;
use App\Services\MailManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/profile")
 */
class ProfileController extends BaseController
{
    /**
     * @Route("/edit", name="profile_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, GrantedService $grantedService, TranslatorInterface $translator): Response
    {
        $user = $this->getUser();

        switch ($grantedService->getRole($user)) {
            case UserRoleEnum::ROLE_SUPER_ADMIN:
            case UserRoleEnum::ROLE_ADMIN:
                $mode = UserProfileTypeEnum::PROFILE_ADMIN;
                break;
            case UserRoleEnum::ROLE_BUDDYCOORDINATOR:
                $mode = UserProfileTypeEnum::PROFILE_ASSOCIATION;
                break;
            case UserRoleEnum::ROLE_INSTITUTE_MANAGER:
                $mode = UserProfileTypeEnum::PROFILE_INSTITUTE;
                break;
            default:
                $mode = UserProfileTypeEnum::PROFILE_USER;
                break;
        }

        $editForm = $this->createForm(UserType::class, $user, ['mode' => $mode]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                $this->addFlash('success', $translator->trans('profile.flash.updated'));

                return new RedirectResponse($this->generateUrl('profile_edit'));
            }

            $this->addFlash('error', $translator->trans('profile.flash.errors'));
        }

        return $this->render('User/edit.html.twig', [
            'user' => $user,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/edit-password", name="user_edit_password", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function editPassword(
        UserPasswordHasherInterface $passwordHasher,
        TranslatorInterface $translator,
        Request $request
    ): Response {
        $user = $this->getUser();
        $form = $this->createForm(UserEditPasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordHasher->hashPassword(
                $user,
                $form->get('plainPassword')->getData()
            ));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', $translator->trans('profile.flash.updated'));

            return new JSONResponse('saved', Response::HTTP_OK);
        }

        return $this->render('User/edit_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="user_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function delete(
        TranslatorInterface $translator,
        Request $request,
        TokenStorageInterface $tokenStorage,
        MailManager $mailManager,
        Session $session,
        User $user
    ): Response {
        $this->denyAccessUnlessGranted(UserVoterEnum::DELETE, $user);
        $form = $this->createFormBuilder()->getForm();

        $callback = $this->isGranted(UserVoterEnum::IS_MANAGER, $this->getUser())
            ? $this->generateUrl('user_index')
            : $this->generateUrl('homepage')
        ;

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var EntityManagerInterface $om */
            $om = $this->getDoctrine()->getManager();
            // temporarily disable softdeleteable filter to force delete linked soft-deleted entities
            $om->getFilters()->disable('softdeleteable');
            $om->remove($user);
            $om->getFilters()->enable('softdeleteable');
            $om->flush();

            $userLogged = $this->getUser();
            if ($user === $userLogged) {
                $tokenStorage->setToken(null);
                $session->invalidate();
            } else {
                $mailManager->accountDeletedByManager($user, $userLogged);
            }

            $this->addFlash('success', $translator->trans('profile.flash.deleted'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'User',
            'name' => $user->getEmail(),
            'id' => $user->getId(),
            'callback' => $callback,
        ]);
    }

    /**
     * @Route("/consent", name="user_consent", methods={"GET", "POST"})
     */
    public function consent(Request $request, TranslatorInterface $translator): Response
    {
        $consentForm = $this->createFormBuilder()->getForm();

        $consentForm->handleRequest($request);
        if ($consentForm->isSubmitted() && $consentForm->isValid()) {
            $this->getUser()->acceptPrivacy();

            $om = $this->getDoctrine()->getManager();
            $om->flush();

            $this->addFlash('success', $translator->trans('profile.flash.updated'));

            return $this->redirectToRoute('profile_edit');
        }

        return $this->render('User/consent.html.twig', [
            'consentForm' => $consentForm->createView(),
        ]);
    }

    /**
     * @Route(
     *     "/consent_charter/{page}",
     *     name="user_consent-charter",
     *     methods={"GET", "POST"},
     *     requirements={"page" : "\d+"}
     * )
     */
    public function consentCharter(Request $request, TranslatorInterface $translator, int $page = 1): Response
    {
        $consentForm = $this->createFormBuilder()->getForm();
        $consentForm->handleRequest($request);

        if ($consentForm->isSubmitted() && $consentForm->isValid()) {
            if (4 === $page) {
                $this->getUser()->setCharterAccepted(true);

                $om = $this->getDoctrine()->getManager();
                $om->flush();

                $this->addFlash('success', $translator->trans('profile.flash.updated'));

                return $this->redirectToRoute('profile_edit');
            }

            return $this->redirectToRoute('user_consent-charter', ['page' => ++$page]);
        }

        return $this->render('User/consent_charter.html.twig', [
            'consentForm' => $consentForm->createView(),
            'page' => $page,
        ]);
    }

    /**
     * @Route("/not-approved", name="user_not-approved", methods={"GET"})
     */
    public function notApproved(): Response
    {
        return $this->render('User/not_approved.html.twig');
    }

    /**
     * @Route("/{id}/export", name="user_export", methods={"GET"})
     */
    public function export(ProfileExporter $profileExporter): Response
    {
        return $profileExporter->export();
    }
}
