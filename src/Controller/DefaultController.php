<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends BaseController
{
    /**
     * @Route(name="homepage")
     */
    public function index(): Response
    {
        return $this->render('Home/index.html.twig');
    }

    /**
     * @Route("/institutions", name="institutions")
     */
    public function institutions(): Response
    {
        return $this->render('Home/institutions.html.twig');
    }

    /**
     * @Route("/our-partners", name="partners")
     */
    public function partners(): Response
    {
        return $this->render('Home/partners.html.twig');
    }

    /**
     * @Route("/the-project", name="project")
     */
    public function project(): Response
    {
        return $this->render('Home/project.html.twig');
    }

    /**
     * @Route("/privacy", name="privacy")
     */
    public function privacy(): Response
    {
        return $this->render('Home/privacy.html.twig');
    }

    /**
     * @Route("/terms-of-use", name="termsOfUse")
     */
    public function termsOfUse(): Response
    {
        return $this->render('Home/terms_of_use.html.twig');
    }

    /**
     * @Route("/cookie-policy", name="cookiePolicy", options={"expose" : true})
     */
    public function cookiePolicy(): Response
    {
        return $this->render('Home/cookie_policy.html.twig');
    }

    /**
     * @Route("/charter", name="charter")
     */
    public function charter(): Response
    {
        return $this->render('Home/charter.html.twig');
    }

    /**
     * @Route("/contact-us", name="contactUs")
     */
    public function contactUs(): Response
    {
        return $this->render('Home/contact_us.html.twig');
    }
}
