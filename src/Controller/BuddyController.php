<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\BuddyListResponse;
use App\Entity\Buddy;
use App\Enum\BuddyVoterEnum;
use App\Enum\UserRoleEnum;
use App\Repository\AssociationRepository;
use App\Repository\BuddyRepository;
use App\Repository\InstituteRepository;
use App\Repository\SemesterRepository;
use App\Services\MatchingHelper;
use App\Services\MatchingManagerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/buddies")
 */
class BuddyController extends BaseController
{
    /**
     * @Route(name="buddy_index")
     */
    public function index(
        MatchingManagerHelper $matchingManagerHelper,
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        SemesterRepository $semesterRepo
    ): Response {
        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();
        $params = ['years' => $semesterRepo->getDistinctYears($matchingManager)];

        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params['associations'] = $associationRepo->findAll();
            $params['institutes'] = $instituteRepo->findAll();
        } else {
            $params['institutes'] = $instituteRepo->getByMatchingManager($matchingManager);
        }

        return $this->render('Buddy/index.html.twig', $params);
    }

    /**
     * @Route("/list_buddies", name="buddy_list", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function getListBuddies(
        BuddyRepository $buddyRepo,
        DataTableResponseFactory $responseFactory,
        MatchingManagerHelper $matchingManagerHelper,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $buddyRepo->getNbBuddies($dataTableColumns->getSearch(), $matchingManager),
            'data' => $responseFactory->getResponse(
                BuddyListResponse::class,
                $buddyRepo->getListInfo($dataTableColumns, $matchingManager)
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route("/{id}/show", name="buddy_show")
     */
    public function show(MatchingHelper $matchingHelper, Buddy $buddy): Response
    {
        $this->denyAccessUnlessGranted(BuddyVoterEnum::SHOW, $buddy);

        $international = $buddy->getMentee();
        $local = $buddy->getMentor();

        $params = array_merge(
            [
                'international' => $international,
                'local' => $local,
                'toMatch' => false,
                'matchingManager' => $international->getInstitute(),
            ],
            $matchingHelper->compareBuddies($international, $local)
        );

        return $this->render('Matching/matching.html.twig', $params);
    }

    /**
     * @Route("/{id}/delete", name="buddy_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function delete(TranslatorInterface $translator, Request $request, Buddy $buddy): Response
    {
        $this->denyAccessUnlessGranted(BuddyVoterEnum::DELETE, $buddy);
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($buddy);
            $om->flush();

            $this->addFlash('success', $translator->trans('buddy.flash.deleted'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Buddy',
            'name' => $buddy->getMentor()->getFullName().' <=> '.$buddy->getMentee()->getFullName(),
            'id' => $buddy->getId(),
        ]);
    }

    /**
     * @Route("/{id}/reports", name="buddy_reports", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function showReport(Buddy $buddy): Response
    {
        $this->denyAccessUnlessGranted(BuddyVoterEnum::SHOW, $buddy);

        return $this->render('Report/list_by_buddy.html.twig', [
            'buddy' => $buddy,
            'mentor' => $buddy->getMentor(),
            'mentee' => $buddy->getMentee(),
        ]);
    }
}
