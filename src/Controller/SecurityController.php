<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\EmailFormType;
use App\Repository\UserRepository;
use App\Services\MailManager;
use Hslavich\OneloginSamlBundle\Security\Http\Authenticator\Token\SamlTokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SecurityController extends BaseController
{
    /**
     * @Route("/login", name="login", options={ "expose" : true })
     */
    public function login(Request $request, TranslatorInterface $translator): Response
    {
        if ($request->isXmlHttpRequest()) {
            return $this->render('Security/login_modal.html.twig');
        }

        if ($request->query->has('logged_out')) {
            $this->addFlash('error', $translator->trans('login.flash.logged_out'));
        }

        return $this->render('Security/login.html.twig');
    }

    /**
     * @Route("/login_fail", name="login_fail")
     */
    public function loginFalse(TranslatorInterface $translator): Response
    {
        $this->addFlash('error', $translator->trans('login.flash.error'));

        return $this->render('Security/login.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(TokenStorageInterface $tokenStorage, string $samlIdpHost): Response
    {
        if ($tokenStorage->getToken() instanceof SamlTokenInterface) {
            return $this->redirect("{$samlIdpHost}/profile/Logout");
        }

        return $this->redirectToRoute('saml_logout');
    }

    /**
     * @Route("/login_link", name="login_link")
     */
    public function requestLoginLink(
        Request $request,
        LoginLinkHandlerInterface $loginLinkHandler,
        UserRepository $userRepository,
        MailManager $mailManager
    ): Response {
        $form = $this->createForm(EmailFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $userRepository->findOneBy(['email' => $email]);

            if ($user) {
                $loginLinkDetails = $loginLinkHandler->createLoginLink($user);

                $mailManager->sendLoginLinkEmail($email, $loginLinkDetails);
            }

            return $this->redirectToRoute('check_email');
        }

        return $this->render('Security/login_link.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/check-email", name="check_email")
     */
    public function checkEmail(): Response
    {
        return $this->render('Security/check_email.html.twig');
    }
}
