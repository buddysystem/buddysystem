<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\AssociationListResponse;
use App\Entity\Association;
use App\Entity\MatchingManagerInterface;
use App\Enum\AssociationVoterEnum;
use App\Enum\UserRoleEnum;
use App\Form\AssociationType;
use App\Repository\AssociationRepository;
use App\Repository\CityRepository;
use App\Services\MatchingManagerHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/associations")
 */
class AssociationController extends BaseController
{
    /**
     * @Route(name="association_index")
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function index(CityRepository $cityRepo): Response
    {
        return $this->render('Association/index.html.twig', [
            'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_ASSOCIATION),
        ]);
    }

    /**
     * @Route(
     *     "/list_associations",
     *     name="association_list",
     *     options={ "expose" : true },
     *     methods={"GET"},
     *     condition="request.isXmlHttpRequest()"
     * )
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function getListAssociations(
        AssociationRepository $associationRepo,
        DataTableResponseFactory $responseFactory,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $associationRepo->getNbAssociations($dataTableColumns->getSearch()),
            'data' => $responseFactory->getResponse(
                AssociationListResponse::class,
                $associationRepo->getListInfo($dataTableColumns)
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route("/new", name="association_new", methods={"GET", "POST"})
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     *
     * @return RedirectResponse|Response
     */
    public function new(TranslatorInterface $translator, Request $request): Response
    {
        $association = new Association();
        $form = $this->createForm(AssociationType::class, $association);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $om = $this->getDoctrine()->getManager();
                $om->persist($association);
                $om->flush();

                $this->addFlash('success', $translator->trans('association.flash.created', [], 'admin'));

                return $this->redirectToRoute('association_index');
            }
            $this->addFlash('error', $translator->trans('association.flash.errors'));
        }

        return $this->render('Association/new.html.twig', [
            'association' => $association,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="association_edit", methods={"GET", "POST"})
     */
    public function edit(
        Request $request,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        Association $association
    ): Response {
        $this->denyAccessUnlessGranted(AssociationVoterEnum::EDIT, $association);

        $editForm = $this->createForm(AssociationType::class, $association);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            if ($editForm->isValid()) {
                $om = $this->getDoctrine()->getManager();
                $om->persist($association);
                $om->flush();

                $this->addFlash('success', $translator->trans('association.flash.updated'));

                return ($this->isGranted(UserRoleEnum::ROLE_ADMIN))
                    ? $this->redirectToRoute('association_index')
                    : $this->redirectToRoute('association_edit', ['id' => $association->getId()])
                ;
            }
            $this->addFlash('error', $translator->trans('association.flash.errors'));
        }

        return $this->render('Association/edit.html.twig', [
            'association' => $association,
            'form' => $editForm->createView(),
            'upperLevelsManaged' => $matchingManagerHelper->getUpperLevelsCriteriasManaged($association),
        ]);
    }
}
