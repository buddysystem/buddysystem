<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Hobby;
use App\Entity\MatchingManagerInterface;
use App\Enum\UserRoleEnum;
use App\Form\HobbyType;
use App\Repository\HobbyRepository;
use App\Services\LocaleManager;
use App\Services\MatchingManagerHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/hobbies")
 */
class HobbyController extends BaseController
{
    private string $defaultLocale;

    public function __construct(LocaleManager $localeManager)
    {
        $this->defaultLocale = $localeManager->getDefaultLocale();
    }

    /**
     * @Route(name="hobby_index")
     */
    public function index(HobbyRepository $hobbyRepo, MatchingManagerHelper $matchingManagerHelper): Response
    {
        $params = ['hobbies' => $hobbyRepo->findAll()];
        if ($manager = $matchingManagerHelper->getMatchingManagerOfManager()) {
            $params['hobbiesManager'] = $hobbyRepo->getIdsByMatchingManager($manager);
        }

        return $this->render('Hobby/index.html.twig', $params);
    }

    /**
     * @Route("/{id}/toggle", name="toggle_hobby_manager", options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function toggleMatchingManagerHobby(
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        Hobby $hobby
    ): JsonResponse {
        /** @var MatchingManagerInterface $matchingManager */
        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();

        if ($matchingManager) {
            if ($matchingManager->getHobbies()->contains($hobby)) {
                $matchingManager->removeHobby($hobby);
                $key = 'hobby.flash.removed';
            } else {
                $key = 'hobby.flash.added';
                $matchingManager->addHobby($hobby);
            }

            $om = $this->getDoctrine()->getManager();
            $om->persist($matchingManager);
            $om->flush();
        }

        return new JsonResponse([
            'message' => $translator->trans($key, ['%name%' => $hobby->getName()]),
            'labelMessage' => 'success',
        ]);
    }

    /**
     * @Route("/new", name="hobby_new", methods={"GET", "POST"})
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $hobby = new Hobby();
        $hobby->setTranslatableLocale($this->defaultLocale);

        $form = $this->createForm(HobbyType::class, $hobby);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var MatchingManagerInterface $manager */
            foreach ($matchingManagerHelper->getAllMatchingManager(true) as $manager) {
                $manager->addHobby($hobby);
            }
            $om = $this->getDoctrine()->getManager();
            $om->persist($hobby);
            $om->flush();

            $this->addFlash('success', $translator->trans('hobby.flash.created', [], 'admin'));

            return $this->redirectToRoute('hobby_index');
        }

        return $this->render('Hobby/new.html.twig', [
            'hobby' => $hobby,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="hobby_edit", methods={"GET", "POST"})
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function edit(Request $request, TranslatorInterface $translator, Hobby $hobby): Response
    {
        $om = $this->getDoctrine()->getManager();

        $hobby->setTranslatableLocale($this->defaultLocale);
        $om->refresh($hobby);

        $editForm = $this->createForm(HobbyType::class, $hobby);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $om->persist($hobby);
            $om->flush();

            $this->addFlash('success', $translator->trans('hobby.flash.edited', [], 'admin'));

            return $this->redirectToRoute('hobby_index');
        }

        return $this->render('Hobby/edit.html.twig', [
            'hobby' => $hobby,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="hobby_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function delete(TranslatorInterface $translator, Request $request, Hobby $hobby): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($hobby);
            $om->flush();

            $this->addFlash('success', $translator->trans('hobby.flash.deleted', [], 'admin'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Hobby',
            'name' => $hobby->getName(),
            'id' => $hobby->getId(),
        ]);
    }
}
