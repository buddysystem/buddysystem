<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\StatListResponse;
use App\Enum\UserRoleEnum;
use App\Repository\InstituteRepository;
use App\Repository\StatRepository;
use App\Services\MatchingManagerHelper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/stats")
 */
class StatController extends BaseController
{
    /**
     * @Route(name="stats_index")
     */
    public function index(
        StatRepository $statRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params = [
                'years' => $statRepo->getDistinctYears(),
            ];
        } else {
            $manager = $matchingManagerHelper->getMatchingManagerOfManager();
            $params = ['years' => $statRepo->getDistinctYears($manager)];
        }

        $params['institutes'] = $instituteRepo->findAll();

        return $this->render('Stat/index.html.twig', $params);
    }

    /**
     * @Route("/list_stats", name="stats_list", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function getListStats(
        StatRepository $statRepo,
        DataTableResponseFactory $responseFactory,
        MatchingManagerHelper $matchingManagerHelper,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $manager = $matchingManagerHelper->getMatchingManagerOfManager();

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $statRepo->getNbStats($dataTableColumns->getSearch(), $manager),
            'data' => $responseFactory->getResponse(
                StatListResponse::class,
                $statRepo->getListInfo($dataTableColumns, $manager)
            ),
        ];

        return new JSonResponse($response);
    }
}
