<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Study;
use App\Form\StudyType;
use App\Repository\StudyRepository;
use App\Services\LocaleManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/studies")
 */
class StudyController extends BaseController
{
    private $defaultLocale;

    public function __construct(LocaleManager $localeManager)
    {
        $this->defaultLocale = $localeManager->getDefaultLocale();
    }

    /**
     * @Route(name="study_index")
     */
    public function index(StudyRepository $studyRepo): Response
    {
        return $this->render('Study/index.html.twig', [
            'studies' => $studyRepo->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="study_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TranslatorInterface $translator): Response
    {
        $study = new Study();
        $study->setTranslatableLocale($this->defaultLocale);

        $form = $this->createForm(StudyType::class, $study);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($study);
            $om->flush();

            $this->addFlash('success', $translator->trans('study.flash.created', [], 'admin'));

            return $this->redirectToRoute('study_index');
        }

        return $this->render('Study/new.html.twig', [
            'study' => $study,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="study_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TranslatorInterface $translator, Study $study): Response
    {
        $om = $this->getDoctrine()->getManager();

        $study->setTranslatableLocale($this->defaultLocale);
        $om->refresh($study);

        $editForm = $this->createForm(StudyType::class, $study);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $om->persist($study);
            $om->flush();

            $this->addFlash('success', $translator->trans('study.flash.edited', [], 'admin'));

            return $this->redirectToRoute('study_index');
        }

        return $this->render('Study/edit.html.twig', [
            'study' => $study,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="study_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function delete(TranslatorInterface $translator, Request $request, Study $study): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($study);
            $om->flush();

            $this->addFlash('success', $translator->trans('study.flash.deleted', [], 'admin'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Study',
            'name' => $study->getName(),
            'id' => $study->getId(),
        ]);
    }
}
