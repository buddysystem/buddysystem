<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\MatchingManagerInterface;
use App\Entity\Motivation;
use App\Enum\UserRoleEnum;
use App\Form\MotivationType;
use App\Repository\MotivationRepository;
use App\Services\LocaleManager;
use App\Services\MatchingManagerHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/motivations")
 */
class MotivationController extends BaseController
{
    private $defaultLocale;

    public function __construct(LocaleManager $localeManager)
    {
        $this->defaultLocale = $localeManager->getDefaultLocale();
    }

    /**
     * @Route(name="motivation_index")
     */
    public function index(MotivationRepository $motivationRepo, MatchingManagerHelper $matchingManagerHelper): Response
    {
        $params = ['motivations' => $motivationRepo->findAll()];
        if ($manager = $matchingManagerHelper->getMatchingManagerOfManager()) {
            $params['motivationsManager'] = $motivationRepo->getIdsByMatchingManager($manager);
        }

        return $this->render('Motivation/index.html.twig', $params);
    }

    /**
     * @Route("/new", name="motivation_new", methods={"GET", "POST"})
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function new(
        Request $request,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $motivation = new Motivation();
        $motivation->setTranslatableLocale($this->defaultLocale);

        $form = $this->createForm(MotivationType::class, $motivation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var MatchingManagerInterface $manager */
            foreach ($matchingManagerHelper->getAllMatchingManager(true) as $manager) {
                $manager->addMotivation($motivation);
            }

            $om = $this->getDoctrine()->getManager();
            $om->persist($motivation);
            $om->flush();

            $this->addFlash('success', $translator->trans('motivation.flash.created', [], 'admin'));

            return $this->redirectToRoute('motivation_index');
        }

        return $this->render('Motivation/new.html.twig', [
            'motivation' => $motivation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/toggle", name="toggle_motivation_manager", options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function toggleMatchingManagerMotivation(
        TranslatorInterface $translator,
        Motivation $motivation,
        MatchingManagerHelper $matchingManagerHelper
    ): JsonResponse {
        $om = $this->getDoctrine()->getManager();

        $manager = $matchingManagerHelper->getMatchingManagerOfManager();

        if ($manager) {
            if ($manager->getMotivations()->contains($motivation)) {
                $manager->removeMotivation($motivation);
                $key = 'motivation.flash.removed';
            } else {
                $key = 'motivation.flash.added';
                $manager->addMotivation($motivation);
            }
            $om->persist($manager);
            $om->flush();
        }

        return new JsonResponse([
            'message' => $translator->trans($key, ['%name%' => $motivation->getNameMentee()]),
            'labelMessage' => 'success',
        ]);
    }

    /**
     * @Route("/{id}/edit", name="motivation_edit", methods={"GET", "POST"})
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function edit(Request $request, TranslatorInterface $translator, Motivation $motivation): Response
    {
        $om = $this->getDoctrine()->getManager();

        $motivation->setTranslatableLocale($this->defaultLocale);
        $om->refresh($motivation);

        $editForm = $this->createForm(MotivationType::class, $motivation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $om->persist($motivation);
            $om->flush();

            $this->addFlash('success', $translator->trans('motivation.flash.edited', [], 'admin'));

            return $this->redirectToRoute('motivation_index');
        }

        return $this->render('Motivation/edit.html.twig', [
            'motivation' => $motivation,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="motivation_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     * @IsGranted(UserRoleEnum::ROLE_ADMIN)
     */
    public function delete(TranslatorInterface $translator, Request $request, Motivation $motivation): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($motivation);
            $om->flush();

            $this->addFlash('success', $translator->trans('motivation.flash.deleted', [], 'admin'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Motivation',
            'name' => $motivation->getNameMentee(),
            'id' => $motivation->getId(),
        ]);
    }
}
