<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\AssociationInstituteLinkResponse;
use App\DataTable\Response\InstituteListResponse;
use App\DataTable\Response\InstituteRightResponse;
use App\Entity\Association;
use App\Entity\AssociationInstitute;
use App\Entity\Institute;
use App\Entity\MatchingManagerInterface;
use App\Entity\Study;
use App\Enum\AssociationInstituteVoterEnum;
use App\Enum\InstituteVoterEnum;
use App\Enum\UserRoleEnum;
use App\Form\AssociationInstituteRightType;
use App\Form\InstitutePreferenceType;
use App\Form\InstituteType;
use App\Repository\AssociationInstituteRepository;
use App\Repository\AssociationRepository;
use App\Repository\CityRepository;
use App\Repository\InstituteRepository;
use App\Repository\StudyRepository;
use App\Services\DuplicateInstituteHelper;
use App\Services\MatchingManagerHelper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/institutes")
 */
class InstituteController extends BaseController
{
    /**
     * @Route(name="institute_index")
     */
    public function index(
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        CityRepository $cityRepo,
        StudyRepository $studyRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $matchingManager = $matchingManagerHelper->getMatchingManagerOfManager();
        $params = ($this->isGranted(UserRoleEnum::ROLE_ADMIN))
            ? [
                'associations' => $associationRepo->findAll(),
                'cities' => $cityRepo->findBy([], ['name' => 'asc']),
                'institutes' => $instituteRepo->findBy([], ['name' => 'asc']),
                'studies' => $studyRepo->getUsedByInstitutes(),
            ]
            : [
                'cities' => $cityRepo->getUsedByManager(MatchingManagerInterface::TYPE_INSTITUTE, $matchingManager),
                'institutes' => $instituteRepo->getByMatchingManager($matchingManager),
                'studies' => $studyRepo->getUsedByInstitutes($matchingManager),
            ]
        ;

        if (empty($params['studies'])) {
            $params['studies'] = $studyRepo->getUsedByInstitutes();
        }

        return $this->render('Institute/index.html.twig', $params);
    }

    /**
     * @Route(
     *     "/list_institutes",
     *     name="institute_list",
     *     options={ "expose" : true },
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function getListInstitutes(
        Request $request,
        InstituteRepository $instituteRepo,
        DataTableResponseFactory $responseFactory,
        MatchingManagerHelper $matchingManagerHelper,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $matchingManager = $instituteRepo->findOneById($request->query->get('manager')) ?? $matchingManagerHelper->getMatchingManagerOfManager();

        $nbInstitutes = $instituteRepo->getNbInstitutes($dataTableColumns->getSearch(), $matchingManager);
        $institutes = $instituteRepo->getListInfo($dataTableColumns, $matchingManager);

        $params = ['matchingManager' => $matchingManager];
        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $nbInstitutes,
        ];

        $response['data'] = ($request->query->has('delegate_rights') && $request->query->get('delegate_rights'))
            ? $responseFactory->getResponse(InstituteRightResponse::class, $institutes, $params)
            : $responseFactory->getResponse(InstituteListResponse::class, $institutes, $params)
        ;

        return new JSonResponse($response);
    }

    /**
     * @Route("/new", name="institute_new", methods={"GET", "POST"})
     *
     * @return RedirectResponse|Response
     */
    public function new(
        TranslatorInterface $translator,
        Request $request,
        MatchingManagerHelper $matchingManagerHelper,
        DuplicateInstituteHelper $duplicateInstituteHelper
    ) {
        $institute = new Institute();
        $this->denyAccessUnlessGranted(InstituteVoterEnum::CREATE, $institute);

        $manager = $matchingManagerHelper->getMatchingManagerOfManager();
        if ($manager instanceof Institute) {
            $institute->setRoot($manager);
            if ($manager->getLevel() === Institute::MAX_LEVEL - 1) {
                $manager->addChild($institute);
            }
        }

        $form = $this->createForm(InstituteType::class, $institute, ['manager' => $manager]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($form->has('parents')) {
                    $parents = $form->get('parents')->getData();
                } else {
                    $parents = [];
                }
                $duplicateInstituteHelper->manageDuplicates($institute, $parents);

                $om = $this->getDoctrine()->getManager();
                $om->persist($institute);
                $om->flush();

                $this->addFlash('success', $translator->trans('institute.flash.created'));

                return $this->redirectToRoute('institute_index');
            }
            $this->addFlash('error', $translator->trans('institute.flash.errors'));
        }

        return $this->render('Institute/new.html.twig', [
            'institute' => $institute,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="institute_edit", methods={"GET", "POST"})
     *
     * @return RedirectResponse|Response
     */
    public function edit(
        Request $request,
        TranslatorInterface $translator,
        Institute $institute,
        MatchingManagerHelper $matchingManagerHelper,
        DuplicateInstituteHelper $duplicateInstituteHelper
    ) {
        $this->denyAccessUnlessGranted(InstituteVoterEnum::EDIT, $institute);
        $form = $this->createForm(
            InstituteType::class,
            $institute,
            ['manager' => $matchingManagerHelper->getMatchingManagerOfManager()]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if ($form->has('parents')) {
                    $parents = $form->get('parents')->getData();
                } else {
                    $parents = [];
                }
                $duplicateInstituteHelper->manageDuplicates($institute, $parents);

                $om = $this->getDoctrine()->getManager();
                $om->flush();

                $this->addFlash('success', $translator->trans('institute.flash.updated'));

                return $this->redirectToRoute('institute_index');
            }
            $this->addFlash('error', $translator->trans('institute.flash.errors'));
        }

        return $this->render('Institute/edit.html.twig', [
            'institute' => $institute,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit_preferences", name="institute_edit-preferences", methods={"GET", "POST"})
     *
     * @return RedirectResponse|Response
     */
    public function editPreferences(
        Request $request,
        TranslatorInterface $translator,
        MatchingManagerHelper $matchingManagerHelper,
        Institute $institute
    ): Response {
        $this->denyAccessUnlessGranted(InstituteVoterEnum::EDIT_PREFERENCES, $institute);

        $form = $this->createForm(InstitutePreferenceType::class, $institute);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $om = $this->getDoctrine()->getManager();
                $om->persist($institute);
                $om->flush();

                $this->addFlash('success', $translator->trans('institute.flash.updated'));

                return $this->redirectToRoute('institute_index');
            }
            $this->addFlash('error', $translator->trans('institute.flash.errors'));
        }

        return $this->render('Institute/editPreferences.html.twig', [
            'institute' => $institute,
            'form' => $form->createView(),
            'lowerLevelManager' => $matchingManagerHelper->getLowerLevelCriteriasManager($institute),
            'upperLevelsManaged' => $matchingManagerHelper->getUpperLevelsCriteriasManaged($institute),
        ]);
    }

    /**
     * @Route("/link_association", name="institute_association_link", methods={"GET"}, options={"expose" : true})
     *
     * @IsGranted(UserRoleEnum::ROLE_BUDDYCOORDINATOR)
     */
    public function linkAssociation(
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $params = [];
        if ($this->isGranted(UserRoleEnum::ROLE_ADMIN)) {
            $params['associations'] = $associationRepo->findBy([], ['name' => 'asc']);
        }

        $params['institutes'] = $instituteRepo->getByMatchingManager(
            $matchingManagerHelper->getMatchingManagerOfManager(),
            true
        );

        return $this->render('Institute/link_association.html.twig', $params);
    }

    /**
     * @Route("/{id}/link_study", name="institute_study_link", methods={"GET"}, options={"expose" : true})
     */
    public function linkStudy(StudyRepository $studyRepo, Institute $institute): Response
    {
        $this->denyAccessUnlessGranted(InstituteVoterEnum::EDIT, $institute);

        return $this->render('Institute/link_study.html.twig', [
            'institute' => $institute,
            'studies' => $studyRepo->findAll(),
            'studiesManager' => $studyRepo->getIdsByInstitute($institute),
        ]);
    }

    /**
     * @Route(
     *     "/list_institutes/link_associations",
     *     name="institute_list_associations_link",
     *     options={ "expose" : true },
     *     condition="request.isXmlHttpRequest()"
     * )
     * @IsGranted(UserRoleEnum::ROLE_BUDDYCOORDINATOR)
     */
    public function getListInstitutesAssociationsLink(
        Request $request,
        InstituteRepository $instituteRepo,
        CityRepository $cityRepo,
        DataTableResponseFactory $responseFactory,
        AssociationRepository $associationRepo,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $association = $request->query->has('manager')
            ? $associationRepo->findOneById($request->query->get('manager'))
            : $this->getUser()->getAssociation()
        ;

        if (!$association) {
            throw new NotFoundHttpException(sprintf('Association not found.'));
        }

        $dataTableColumns->setSearch(array_merge($dataTableColumns->getSearch(), ['city' => $cityRepo->getIdsByMatchingManager($association)]));

        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $instituteRepo->getNbInstitutes($dataTableColumns->getSearch()),
            'data' => $responseFactory->getResponse(
                AssociationInstituteLinkResponse::class,
                $instituteRepo->getListInfo($dataTableColumns),
                ['manager' => $association]
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route(
     *     "/{id}/toggle_association_link",
     *     name="institute_association_toggle_link",
     *     options={"expose" : true},
     *     condition="request.isXmlHttpRequest()"
     * )
     * @IsGranted(UserRoleEnum::ROLE_BUDDYCOORDINATOR)
     */
    public function toggleAssociationLink(
        Request $request,
        AssociationRepository $associationRepo,
        AssociationInstituteRepository $associationInstituteRepo,
        TranslatorInterface $translator,
        Institute $institute
    ): JsonResponse {
        $association = $request->query->has('manager')
            ? $associationRepo->findOneById($request->query->get('manager'))
            : $this->getUser()->getAssociation()
        ;

        if (empty(array_intersect($association->getCities()->toArray(), $institute->getCitiesWithParents()))) {
            throw $this->createAccessDeniedException();
        }

        $om = $this->getDoctrine()->getManager();

        $associationInstitute = $associationInstituteRepo->findOneBy(
            [
                'institute' => $institute, 'association' => $association,
            ]
        );

        if ($associationInstitute) {
            $om->remove($associationInstitute);
            $key = 'institute.flash.unlink';
        } else {
            $key = 'institute.flash.link';
            $associationInstitute = new AssociationInstitute();
            $associationInstitute->setAssociation($association);
            $associationInstitute->setInstitute($institute);
            $institute->addAssociationInstitute($associationInstitute);
            $om->persist($associationInstitute);
        }
        $om->flush();

        return new JsonResponse([
            'message' => $translator->trans($key, ['%name%' => $institute->getName()]),
            'labelMessage' => 'success',
        ]);
    }

    /**
     * @Route(
     *     "/{id}/studies/{idStudy}/toggle_study_link",
     *     name="institute_study_toggle_link",
     *     options={"expose" : true},
     *     condition="request.isXmlHttpRequest()"
     * )
     * @ParamConverter("study", options={"id" : "idStudy"})
     */
    public function toggleStudyLink(TranslatorInterface $translator, Institute $institute, Study $study): JsonResponse
    {
        $this->denyAccessUnlessGranted(InstituteVoterEnum::EDIT, $institute);
        $om = $this->getDoctrine()->getManager();

        if ($institute->getStudies()->contains($study)) {
            $institute->removeStudy($study);
            $key = 'study.flash.unlink';
        } else {
            $institute->addStudy($study);
            $key = 'study.flash.link';
        }

        $om->persist($institute);
        $om->flush();

        return new JsonResponse([
            'message' => $translator->trans($key, ['%name%' => $study->getName()]),
            'labelMessage' => 'success',
        ]);
    }

    /**
     * @Route("/delegate_rights", name="institute_delegate_rights", methods={"GET"}, options={"expose" : true})
     *
     * @IsGranted(UserRoleEnum::ROLE_INSTITUTE_MANAGER)
     */
    public function delegateRights(
        AssociationRepository $associationRepo,
        InstituteRepository $instituteRepo,
        MatchingManagerHelper $matchingManagerHelper
    ): Response {
        $params = [
            'institutes' => $instituteRepo->getByMatchingManager(
                $matchingManagerHelper->getMatchingManagerOfManager(),
                true
            ),
            'associations' => $associationRepo->findAll(),
        ];

        return $this->render('Institute/delegate_rights.html.twig', $params);
    }

    /**
     * @Route(
     *     "/{id}/associations/{idAssociation}/change_rights",
     *     name="institute_change_rights",
     *     methods={"POST"},
     *     options={"expose" : true}
     * )
     *
     * @ParamConverter("association", options={"id" : "idAssociation"})
     *
     * @IsGranted(UserRoleEnum::ROLE_INSTITUTE_MANAGER)
     */
    public function changeRights(
        Request $request,
        AssociationInstituteRepository $associationInstituteRepo,
        Institute $institute,
        Association $association,
        TranslatorInterface $translator
    ): Response {
        $associationInstitute = $associationInstituteRepo->findOneBy(['association' => $association, 'institute' => $institute]);

        if (!$associationInstitute || !$this->isGranted(AssociationInstituteVoterEnum::CHANGE_RIGHTS, $associationInstitute)) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(AssociationInstituteRightType::class, $associationInstitute);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($associationInstitute);
            $om->flush();

            $this->addFlash('success', $translator->trans('institute.rights.flash.updated'));
        } else {
            $this->addFlash('success', $translator->trans('error.403'));
        }

        return new JSONResponse(null, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/{id}/delete",
     *     name="institute_delete",
     *     methods={"GET", "POST"},
     *     options={"expose" : true},
     *     condition="request.isXmlHttpRequest()"
     * )
     */
    public function delete(Request $request, TranslatorInterface $translator, Institute $institute): Response
    {
        $this->denyAccessUnlessGranted(InstituteVoterEnum::DELETE, $institute);
        $form = $this->createFormBuilder()->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($institute);
            $om->flush();

            $this->addFlash('success', $translator->trans('institute.flash.deleted'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'Institute',
            'name' => $institute->getName(),
            'id' => $institute->getId(),
        ]);
    }
}
