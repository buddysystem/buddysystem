<?php

declare(strict_types=1);

namespace App\Controller;

use App\DataTable\DataTableModel;
use App\DataTable\DataTableResponseFactory;
use App\DataTable\Response\CityResponse;
use App\Entity\City;
use App\Form\CityType;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin/cities")
 */
class CityController extends BaseController
{
    /**
     * @Route(name="city_index")
     */
    public function index(CountryRepository $countryRepo, CityRepository $cityRepo): Response
    {
        return $this->render('City/index.html.twig', [
            'cities' => $cityRepo->findAll(),
            'countries' => $countryRepo->findAll(),
        ]);
    }

    /**
     * @Route("/list_cities", name="city_list", options={ "expose" : true }, condition="request.isXmlHttpRequest()")
     */
    public function getListCities(
        CityRepository $cityRepo,
        DataTableResponseFactory $responseFactory,
        DataTableModel $dataTableColumns
    ): JsonResponse {
        $response = [
            'draw' => $dataTableColumns->getDraw(),
            'recordsFiltered' => $cityRepo->getNbCities($dataTableColumns->getSearch()),
            'data' => $responseFactory->getResponse(
                CityResponse::class,
                $cityRepo->getListInfo($dataTableColumns)
            ),
        ];

        return new JSonResponse($response);
    }

    /**
     * @Route("/new", name="city_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $city = new City();
        $form = $this->createForm(CityType::class, $city, ['locale' => $request->getLocale()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($city);
            $om->flush();

            return $this->redirectToRoute('city_index');
        }

        return $this->render('City/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="city_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, City $city): Response
    {
        $editForm = $this->createForm(CityType::class, $city, ['locale' => $request->getLocale()]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->persist($city);
            $om->flush();

            return $this->redirectToRoute('city_index');
        }

        return $this->render('City/edit.html.twig', [
            'city' => $city,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="city_delete", methods={"GET", "POST"}, options={"expose" : true}, condition="request.isXmlHttpRequest()")
     */
    public function delete(TranslatorInterface $translator, Request $request, City $city): Response
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $om = $this->getDoctrine()->getManager();
            $om->remove($city);
            $om->flush();

            $this->addFlash('success', $translator->trans('city.flash.deleted', [], 'admin'));

            return new JSONResponse(null, Response::HTTP_OK);
        }

        return $this->render('Default/delete.html.twig', [
            'form' => $form->createView(),
            'type' => 'City',
            'name' => $city->getName(),
            'id' => $city->getId(),
        ]);
    }
}
