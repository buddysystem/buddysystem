<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationInstitutePermEnum extends AbstractEnumType
{
    public const PERM_VIEW = 'canView';
    public const PERM_MATCH = 'canMatch';
    public const PERM_PREFERENCES = 'canEditPreferences';

    protected static $choices = [
        self::PERM_VIEW => 'institute.rights.view',
        self::PERM_MATCH => 'institute.rights.match',
        self::PERM_PREFERENCES => 'institute.rights.preferences',
    ];
}
