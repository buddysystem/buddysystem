<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class MessagingRecipientTypeEnum extends AbstractEnumType
{
    public const ALL_USERS = 'ALL';
    public const ONLY_MENTORS = 'MENTORS';
    public const ONLY_MENTEES = 'MENTEES';
    public const ONLY_MANAGERS = 'MANAGERS';

    public static $recipientTypeSelector = [
        self::ALL_USERS => self::ALL_USERS,
        self::ONLY_MENTORS => self::ONLY_MENTORS,
        self::ONLY_MENTEES => self::ONLY_MENTEES,
    ];

    protected static $choices = [
        self::ALL_USERS => self::ALL_USERS,
        self::ONLY_MENTORS => self::ONLY_MENTORS,
        self::ONLY_MENTEES => self::ONLY_MENTEES,
        self::ONLY_MANAGERS => self::ONLY_MANAGERS,
    ];
}
