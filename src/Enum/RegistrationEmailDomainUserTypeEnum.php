<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class RegistrationEmailDomainUserTypeEnum extends AbstractEnumType
{
    public const BOTH = 'both';
    public const MENTORS = 'mentors';
    public const MENTEES = 'mentees';

    protected static $choices = [
        self::BOTH => 'registration.domain.user.'.self::BOTH,
        self::MENTORS => 'registration.domain.user.'.self::MENTORS,
        self::MENTEES => 'registration.domain.user.'.self::MENTEES,
    ];
}
