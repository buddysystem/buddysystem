<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ExtractTypeEnum extends AbstractEnumType
{
    public const TYPE_USERS = 'users';
    public const TYPE_BUDDIES = 'buddies';

    protected static $choices = [
        self::TYPE_USERS => 'extract.type.'.self::TYPE_USERS,
        self::TYPE_BUDDIES => 'extract.type.'.self::TYPE_BUDDIES,
    ];
}
