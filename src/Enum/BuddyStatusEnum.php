<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class BuddyStatusEnum extends AbstractEnumType
{
    public const STATUS_PENDING = 'pending';
    public const STATUS_CONFIRMED = 'confirmed';
    public const STATUS_REFUSED = 'refused';

    protected static $choices = [
        self::STATUS_PENDING => 'buddy.confirmed.none',
        self::STATUS_CONFIRMED => 'buddy.confirmed.ok',
        self::STATUS_REFUSED => 'buddy.confirmed.ko',
    ];
}
