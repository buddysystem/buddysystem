<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserVoterEnum extends AbstractEnumType
{
    public const EDIT_OTHER = 'edit_other';
    public const EDIT_ALL = 'edit_all';
    public const EDIT_LOCATION = 'edit_location';
    public const EDIT_SENSITIVE_DATA = 'edit_sensitive_data';
    public const DELETE = 'delete';
    public const MATCH = 'match';
    public const SHOW = 'show';
    public const BE_MATCHED = 'be_matched';
    public const IS_MANAGER = 'is_manager';
    public const HAS_MATCHING_RIGHT = 'has_matching_right';
    public const APPROVE = 'approve';
    public const SEND_MESSAGE = 'send_message';

    protected static $choices = [
        self::SHOW => self::SHOW,
        self::EDIT_OTHER => self::EDIT_OTHER,
        self::EDIT_ALL => self::EDIT_ALL,
        self::EDIT_SENSITIVE_DATA => self::EDIT_SENSITIVE_DATA,
        self::DELETE => self::DELETE,
        self::MATCH => self::MATCH,
        self::BE_MATCHED => self::BE_MATCHED,
        self::IS_MANAGER => self::IS_MANAGER,
        self::HAS_MATCHING_RIGHT => self::HAS_MATCHING_RIGHT,
        self::EDIT_LOCATION => self::EDIT_LOCATION,
        self::APPROVE => self::APPROVE,
        self::SEND_MESSAGE => self::SEND_MESSAGE,
    ];
}
