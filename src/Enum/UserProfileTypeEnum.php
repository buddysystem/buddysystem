<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserProfileTypeEnum extends AbstractEnumType
{
    public const PROFILE_USER = 'user';
    public const PROFILE_ASSOCIATION = 'association';
    public const PROFILE_INSTITUTE = 'institute';
    public const PROFILE_ADMIN = 'admin';

    protected static $choices = [
        self::PROFILE_USER => self::PROFILE_USER,
        self::PROFILE_ASSOCIATION => self::PROFILE_ASSOCIATION,
        self::PROFILE_INSTITUTE => self::PROFILE_INSTITUTE,
        self::PROFILE_ADMIN => self::PROFILE_ADMIN,
    ];
}
