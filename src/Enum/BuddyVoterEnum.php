<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class BuddyVoterEnum extends AbstractEnumType
{
    public const CREATE = 'create';
    public const SHOW = 'show';
    public const CONFIRM = 'confirm';
    public const DELETE = 'delete';

    protected static $choices = [
        self::CREATE => self::CREATE,
        self::SHOW => self::SHOW,
        self::CONFIRM => self::CREATE,
        self::DELETE => self::DELETE,
    ];
}
