<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class StatTypeEnum extends AbstractEnumType
{
    public const TYPE_WEEK = 'week';
    public const TYPE_SEMESTER = 'semester';
    public const TYPE_YEAR = 'year';

    protected static $choices = [
        self::TYPE_WEEK => 'stat.type.'.self::TYPE_WEEK,
        self::TYPE_SEMESTER => 'stat.type.'.self::TYPE_SEMESTER,
        self::TYPE_YEAR => 'stat.type.'.self::TYPE_YEAR,
    ];
}
