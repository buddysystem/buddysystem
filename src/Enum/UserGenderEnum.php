<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserGenderEnum extends AbstractEnumType
{
    public const GENDER_MALE = 'm';
    public const GENDER_FEMALE = 'f';
    public const GENDER_OTHER = 'o';

    protected static $choices = [
        self::GENDER_MALE => 'user.gender.male',
        self::GENDER_FEMALE => 'user.gender.female',
        self::GENDER_OTHER => 'user.gender.other',
    ];
}
