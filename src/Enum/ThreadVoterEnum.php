<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ThreadVoterEnum extends AbstractEnumType
{
    public const SEE = 'see';
    public const REPLY = 'reply';
    public const UNCHANGED_RECIPIENTS = 'unchanged_recipients';

    protected static $choices = [
        self::SEE => self::SEE,
        self::REPLY => self::REPLY,
        self::UNCHANGED_RECIPIENTS => self::UNCHANGED_RECIPIENTS,
    ];
}
