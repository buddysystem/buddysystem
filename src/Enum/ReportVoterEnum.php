<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ReportVoterEnum extends AbstractEnumType
{
    public const CREATE = 'create';
    public const DELETE = 'delete';

    protected static $choices = [
        self::CREATE => self::CREATE,
        self::DELETE => self::DELETE,
    ];
}
