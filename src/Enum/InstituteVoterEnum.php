<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class InstituteVoterEnum extends AbstractEnumType
{
    public const CREATE = 'create';
    public const EDIT = 'edit';
    public const EDIT_PREFERENCES = 'edit_preferences';
    public const DELETE = 'delete';

    protected static $choices = [
        self::CREATE => self::CREATE,
        self::EDIT => self::EDIT,
        self::EDIT_PREFERENCES => self::EDIT_PREFERENCES,
        self::DELETE => self::DELETE,
    ];
}
