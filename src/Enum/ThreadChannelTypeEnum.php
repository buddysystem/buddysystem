<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class ThreadChannelTypeEnum extends AbstractEnumType
{
    public const GLOBAL_CHANNEL = 'global';
    public const INDIVIDUAL_CHANNEL = 'individual';
    public const GROUP_CHANNEL = 'group';

    protected static $choices = [
        self::GLOBAL_CHANNEL => self::GLOBAL_CHANNEL,
        self::INDIVIDUAL_CHANNEL => self::INDIVIDUAL_CHANNEL,
        self::GROUP_CHANNEL => self::GROUP_CHANNEL,
    ];
}
