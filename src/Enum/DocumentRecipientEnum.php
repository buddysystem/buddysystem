<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class DocumentRecipientEnum extends AbstractEnumType
{
    public const RECIPIENT_MENTOR = 'mentor';
    public const RECIPIENT_MENTEE = 'mentee';
    public const RECIPIENT_ASSOCIATION_MANAGER = 'association_manager';
    public const RECIPIENT_INSTITUTE_MANAGER = 'institute_manager';

    public static array $managerChoices = [
        self::RECIPIENT_MENTOR => 'toolbox.document.recipient.mentor',
        self::RECIPIENT_MENTEE => 'toolbox.document.recipient.mentee',
    ];

    public static array $instituteManagerOnlyChoices = [
        self::RECIPIENT_INSTITUTE_MANAGER => 'toolbox.document.recipient.institute_manager',
    ];

    public static array $associationManagerOnlyChoices = [
        self::RECIPIENT_ASSOCIATION_MANAGER => 'toolbox.document.recipient.association_manager',
    ];

    protected static $choices = [
        self::RECIPIENT_MENTOR => 'toolbox.document.recipient.mentor',
        self::RECIPIENT_MENTEE => 'toolbox.document.recipient.mentee',
        self::RECIPIENT_ASSOCIATION_MANAGER => 'toolbox.document.recipient.association_manager',
        self::RECIPIENT_INSTITUTE_MANAGER => 'toolbox.document.recipient.institute_manager',
    ];
}
