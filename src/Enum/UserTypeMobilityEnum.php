<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserTypeMobilityEnum extends AbstractEnumType
{
    public const TYPE_ERASMUS = 'erasmus';
    public const TYPE_OUT_OF_EUROPE = 'out_of_europe';
    public const TYPE_FREE_MOVER = 'free_mover';
    public const TYPE_DIGITAL = 'digital_mobility';

    protected static $choices = [
        self::TYPE_ERASMUS => 'user.type_of_mobility.'.self::TYPE_ERASMUS,
        self::TYPE_OUT_OF_EUROPE => 'user.type_of_mobility.'.self::TYPE_OUT_OF_EUROPE,
        self::TYPE_FREE_MOVER => 'user.type_of_mobility.'.self::TYPE_FREE_MOVER,
        self::TYPE_DIGITAL => 'user.type_of_mobility.'.self::TYPE_DIGITAL,
    ];
}
