<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserRoleEnum extends AbstractEnumType
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_BUDDYCOORDINATOR = 'ROLE_BUDDYCOORDINATOR';
    public const ROLE_INSTITUTE_MANAGER = 'ROLE_INSTITUTE_MANAGER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    public static $notSuperAdmin = [
        self::ROLE_USER => self::ROLE_USER,
        self::ROLE_BUDDYCOORDINATOR => self::ROLE_BUDDYCOORDINATOR,
        self::ROLE_INSTITUTE_MANAGER => self::ROLE_INSTITUTE_MANAGER,
        self::ROLE_ADMIN => self::ROLE_ADMIN,
    ];

    public static $managers = [
        self::ROLE_INSTITUTE_MANAGER => self::ROLE_INSTITUTE_MANAGER,
        self::ROLE_BUDDYCOORDINATOR => self::ROLE_BUDDYCOORDINATOR,
    ];

    public static $admins = [
        self::ROLE_ADMIN => self::ROLE_ADMIN,
        self::ROLE_SUPER_ADMIN => self::ROLE_SUPER_ADMIN,
    ];

    public static $managerAndAdmin = [
        self::ROLE_INSTITUTE_MANAGER => self::ROLE_INSTITUTE_MANAGER,
        self::ROLE_BUDDYCOORDINATOR => self::ROLE_BUDDYCOORDINATOR,
        self::ROLE_ADMIN => self::ROLE_ADMIN,
        self::ROLE_SUPER_ADMIN => self::ROLE_SUPER_ADMIN,
    ];

    protected static $choices = [
        self::ROLE_USER => self::ROLE_USER,
        self::ROLE_BUDDYCOORDINATOR => self::ROLE_BUDDYCOORDINATOR,
        self::ROLE_INSTITUTE_MANAGER => self::ROLE_INSTITUTE_MANAGER,
        self::ROLE_ADMIN => self::ROLE_ADMIN,
        self::ROLE_SUPER_ADMIN => self::ROLE_SUPER_ADMIN,
    ];
}
