<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class SemesterPeriodEnum extends AbstractEnumType
{
    public const PERIOD_FIRST_SEMESTER = 'first_semester';
    public const PERIOD_SECOND_SEMESTER = 'second_semester';
    public const PERIOD_ALL_YEAR = 'all_year';

    protected static $choices = [
        self::PERIOD_FIRST_SEMESTER => 'semester.period.'.self::PERIOD_FIRST_SEMESTER,
        self::PERIOD_SECOND_SEMESTER => 'semester.period.'.self::PERIOD_SECOND_SEMESTER,
        self::PERIOD_ALL_YEAR => 'semester.period.'.self::PERIOD_ALL_YEAR,
    ];

    protected static $choicesToGenerate = [
        self::PERIOD_FIRST_SEMESTER,
        self::PERIOD_ALL_YEAR,
        self::PERIOD_SECOND_SEMESTER,
    ];

    public static function getChoicesToGenerate(): array
    {
        return static::$choicesToGenerate;
    }
}
