<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class BuddyRefuseEnum extends AbstractEnumType
{
    public const REASON_REFUSAL_TIME = 'time';
    public const REASON_REFUSAL_BUDDY = 'buddy';
    public const REASON_REFUSAL_OTHER = 'other';

    protected static $choices = [
        self::REASON_REFUSAL_TIME => 'buddy.refused.'.self::REASON_REFUSAL_TIME,
        self::REASON_REFUSAL_BUDDY => 'buddy.refused.'.self::REASON_REFUSAL_BUDDY,
        self::REASON_REFUSAL_OTHER => 'buddy.refused.'.self::REASON_REFUSAL_OTHER,
    ];
}
