<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserLevelStudyEnum extends AbstractEnumType
{
    public const LEVEL_BACHELOR = 'bachelor';
    public const LEVEL_MASTER = 'master';
    public const LEVEL_DOCTORATE = 'doctorate';
    public const LEVEL_CAP = 'CAP';
    public const LEVEL_BEP = 'BEP';
    public const LEVEL_BACPRO = 'BAC Professionel';
    public const LEVEL_BTS = 'BTS';
    public const LEVEL_LICENCEPRO = 'Licence professionelle';
    public const LEVEL_ALUMNI = 'alumni';
    public const LEVEL_OTHER = 'other';

    protected static $choices = [
        self::LEVEL_BACHELOR => 'user.level_of_study.'.self::LEVEL_BACHELOR,
        self::LEVEL_MASTER => 'user.level_of_study.'.self::LEVEL_MASTER,
        self::LEVEL_DOCTORATE => 'user.level_of_study.'.self::LEVEL_DOCTORATE,
        self::LEVEL_CAP => 'user.level_of_study.'.self::LEVEL_CAP,
        self::LEVEL_BEP => 'user.level_of_study.'.self::LEVEL_BEP,
        self::LEVEL_BACPRO => 'user.level_of_study.'.self::LEVEL_BACPRO,
        self::LEVEL_BTS => 'user.level_of_study.'.self::LEVEL_BTS,
        self::LEVEL_LICENCEPRO => 'user.level_of_study.'.self::LEVEL_LICENCEPRO,
        self::LEVEL_ALUMNI => 'user.level_of_study.'.self::LEVEL_ALUMNI,
        self::LEVEL_OTHER => 'user.level_of_study.'.self::LEVEL_OTHER,
    ];
}
