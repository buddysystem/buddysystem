<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class AssociationInstituteVoterEnum extends AbstractEnumType
{
    public const CHANGE_RIGHTS = 'change_rights';

    protected static $choices = [
        self::CHANGE_RIGHTS => self::CHANGE_RIGHTS,
    ];
}
