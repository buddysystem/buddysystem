<?php

declare(strict_types=1);

namespace App\Enum;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

class UserRegistrationTypeEnum extends AbstractEnumType
{
    public const REGISTRATION_CLASSIC = 'classic';
    public const REGISTRATION_MSE = 'mse';

    protected static $choices = [
        self::REGISTRATION_CLASSIC => self::REGISTRATION_CLASSIC,
        self::REGISTRATION_MSE => self::REGISTRATION_MSE,
    ];
}
