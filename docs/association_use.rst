Buddy Coordinator use
=====================
First the BC has to read and accept a charter. There are 4 pages
 
* The first page is a “preamble”.
   He/she can read it and click on “next” to go to the next page.
 
* The second page is called “acceptance of the user charter”.
   The BC can click on “next” or “previous” if he/she wants to go to the previous page.
 
* The third page is called “managers responsibility”.
   The BC can click on “next” or previous.
* The fourth page is called “responsibility of the platform”.
   The BC can click on “previous”, “I disagree, delete profile” if he/she disagrees with the charter of the platform or “I agree” if he/she agrees with the charter. If he/she agrees, the message “profile updated” is displayed.
 
Dashboard
---------
*  The student association can see statistics regarding its users. But this page does not allow the organization to modify or complete its profile. The page aims to give information about the structure. 
* Elements belonging to the profile of the organization
    - Buddy Coordinator profile picture
         
    - Name of the organization

    - Button “There are some students to match” : when we click on this button, we are redirected to a page where we see a list of international students. We can select an international student. Once we have selected this international student, a new page appears with a list of local students,  who can be matched with the international student selected before (like a ranking of compatibility). Thus, we can select a local student. A matching appears, with two possibilities: to click on MATCH (the two students will be matched) or to click on OTHER PROPOSITIONS to return to the previous page. 

    - Logo organization (optional)
    - Description of the organization(optional): a little text describes the organization
    - Social networks links : when we click on the logo of a social network/website, we are automatically redirected on the social network/website.

*  Insights : figures representing enrollments and interactions on the platform
    - We can find some relevant information about the current semester, the start date of the next semester and the number of users in the section. 

    - Button “Show all the Week Stats” redirects the BC on a page to know statistics about registrations of users and matching. Once on the page, we have different search bars: “Created at, nb users, nb mentees, nb mentees not matched, nb mentors not matched, nb new users, nb new mentees, nb new mentors, nb match confirmed, nb match refused”. We can fill them by adding figures or select the arrow (down/up) of each search bar to class informations in ascendant or descendant order. We can choose the number of information we want on the page. (5, 10, 25, 50, 100)  “Show… entries”. If we select “5”, we will have 5 results on the page. 

*  Users
    - This part shows 2 graphics. 
    - The first one is about mentees. Those who are matched are in red, those who are not matched are in yellow. 
    - The second graphic shows the same statistics, with the same colors but for the mentors. 

*  Registration 
    - When we click on the information button, the message > “Users may have registered for a different semester than the current one”. This part allows the BC to see a graphic with the users registrations each week of the year. 
        - In red, we see the number of new users. 
        - In yellow, we see the number of new mentors.
        - In pink, we see the number of new mentees. 

*  Buddies 
    - This part shows a graphic, with in red the buddies refused, in green buddies confirmed and in yellow buddies who are waiting for their registration/matching. 

*  Institutes
    - This part lets the BC know which university users come from. There is a search bar on which we can select an interval of display. There are 10 universities represented on a diagram, in descending order. 
        - The “1-10” interval shows the 10 first universities where there are the most students, etc.
        - In the “motivations” part, every motivation is associated to a color so we can compare the share of each of them. 
        - In the “nationality” part nationalities are ranked in descending order of number of users. 

My Profile 
----------
*  How does it work?
    - This part shows 4 steps to make the registration and the matching of the user easier. 
        1. Complete your profile as much as you can to increase chances to find a better buddy 
        2. We warn you when we find your buddy 
        3. Contact your buddy via email or WhatsApp
        4. Set up a meeting together and get to know each other! 

*  Account
    - Email 
        This shows the pre-filled information of the user : his or her email address. This information is mandatory, as shown by the small star next to it.
    - Phone
        This shows the pre-filled information of the user : his or her phone number. This information is not mandatory. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save changes.

    - Button “edit password”
        When clicking on it, a pop-up page appears, asking you to write the new password the user would like to use from now on, in two different cases. It also asks to put the previous password. Then the user can either confirm or cancel the action. This action is different from the “forgot your password?” button. 
    - Button “export profile”
        When clicking on it, a pop-up page appears, so the user can choose how he or she would like to download his or her profile data and information on a document, outside of the platform. This does not delete the profile nor the data and information, it just provides a copy of it. 
    - Button “delete profile” 
        When you click on the button “Delete profile” , the message “Do you really want to delete the User..?” is displayed. Then, you have two possibilities:  Delete (to delete the profile) or Cancel (to cancel the action). Once the profile is deleted, with all the personal information and data, the user can no longer access it. 

*  Profile
    - Profile picture 
        This shows the user’s profile picture in a circle shape. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.
    - Button “Search”
        This button allows the user to search and choose a picture from the user’s computer.
    - Firstname
        This shows the pre-filled information of the user : his or her first name.If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save changes.
    - Lastname
        This shows the pre-filled information of the user : his or her last name.If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes. 
    - Date of birth 
        This shows the pre-filled information of the user : his or her date of birth. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save changes.
    - Select Button “Month/Day/Year” : If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save changes.
    - Gender 
        This shows the pre-filled information of the user : his or her gender. If the user wants to correct the information, it is possible by selecting button (Not specified, male, female, other). He or she then has to click on the “edit” button at the bottom of the page to save changes.  
        

*  Situation
    - Spoken language(s)
        This shows the pre-filled information of the user, here the languages spoken. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.
    - Add Button
        The user can add another language by writing it. A language can be deleted by clicking on the X next to it. 
    - Nationality
        This shows the pre-filled information of the user, here the languages spoken. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.
    - Select Button
        The user can select the nationality among a list of countries. It is not possible to indicate more than one. 

*  Notifications: Here the user can choose the desired notifications.
    - Choice slider button “I agree to receive news and updates about the BuddySystem, functionalities and events”
        The user can choose to receive this by sliding the white circle to the right. The blue color when doing so confirms that the notifications are activated for that part. If the user doesn’t want to receive them, the action can be reversed by sliding the white circle to the left. The grey color when doing so confirms that the notifications are not activated.
    - Choice slider button “Receive an email when: New message received”
        The user can choose to receive this by sliding the white circle to the right. The blue color when doing so confirms that the notifications are activated for that part. If the user doesn’t want to receive them, the action can be reversed by sliding the white circle to the left. The grey color when doing so confirms that the notifications are not activated.

    - Button “Edit”
        When clicking on this button, a small black information box appears to let the user know that the information was saved, even if nothing changed. The user remains on the same page. 

Messages  
--------
In the “messages” part you can communicate in different discussion channels. 

*  The “student channel” 
    This channel gathers local and international students matched by your university. In this channel you can choose the faculty of the students you want to send a message to and you can choose to send it to the local students by clicking “mentors” or to the international students by clicking “mentees”, or to both mentors and mentees. 
*  The “institute managers channel”
    This channel gathers every manager in your university. You can send a message to any manager in every faculty.
*  The individual channels  
    You can send a message to only one student. To do it, you can directly type the name of the student you want to send a message to, or you can click on “management”, then “users”, find the one you want and go to “actions”, “message”. 
*  The group channels 
    One student can send a message to you and other managers of his/her faculty. 

In the messaging system, the chat channels will be displayed in order: most recent first. If you want to pin a conversation to appear at the top of the list, simply click on the star to the right of each conversation.  

Management 
----------
This regroups the core activities of the Buddy System

*  Matching
    Once you clicked on “Matching”, a page with all the international students of the section appears. You can select an international student on the list. Then, a list of local students appears to do the matching. A percentage of compatibility is displayed between the two students. Once you select a local student, a matching page appears. 

- On the matching page, we have pieces of information about the international and local students. Some pieces of information refer to the interactions of the BC on the platform : number of buddies, nb of refused match, nb report, nb of warnings, nb of removed matches, last connection.

- Other pieces of information refer to the profile of the user: city, university, date of arrival, age, spoken languages, languages wanted, hobbies, expectations

Still on this page, you also have 2 buttons:

- a button “Match”
     used to match the 2 students, the BC receive a “matching done” message
- a button “Other propositions”
     used to return to the previous page with the list of local students to match.
- a button “Next mentor” 
     used to show a new page of matching with another student. 


*  Users
    When you click on users, a list of users appears. A search bar helps the BC finding its users, by entering a name, an e-mail address, a semester, an institute. If you focus on a line of the list and choose an user, you will have two possibilities:
- to delete the profile of the user: the message “do you really want to delete the User..” will be displayed.
- to edit the profile of the student: in this case, you will be redirected on an another page called “Edit user (+name of the user)”. 
    
This page shows the pieces of information filled by the user. There are different parts but the BC cannot modify personal details given by the user.

Account:
    You cannot modify the e-mail address or the phone number.
    If the profile is archived or not, you can select Yes/No on the section “Is Archived?”
    You also have a button to delete the profile of the user. 

Profile:
    Here the BC cannot modify anything. He/she just sees information about the user like his/her profile picture, firstname, last name, date of birth, gender.

Situation:
    The BC cannot modify some pieces of information: the spoken language, the nationality, the city, the institute, the semester, the date of arrival, the date of departure.
    But he/she can select if the user is a local student or an international student.
    He/she can select the institute of the student and at which semester the student stays. 
 
Preferences:
    It is still the profile of the user and the BC cannot modify anything.
The user gives:
    - the language(s) he wants for his/her buddies, 
    - the number of buddies he/she wants
    - his/her hobbies
    - his/her expectations about this Buddy experience
    - he chooses to activate or deactivate the Button “I want someone with the same gender as me” 

Notifications: 
    The BC cannot modify anything. The user can activate/desactivate the button “I agree to receive news and updates about the Buddy System, functionalities and events”. There is the same button “activate/deactivate” for the user if the wants to receive an email when a new message: “Your buddy(ies) may be removed as a result of your change." 
Then, the BC can edit the profile with Button “Edit”. The data he filled will be saved. 


*  Buddies 
    Here the BC sees a Buddies list, a list of matching with in each line the name of the international student and the local student, their e-mail addresses, and the confirmation of their match. OK, KO,  or waiting; 
“OK” means the match is confirmed, “KO” means it is not confirmed by a student or the two students, “waiting” means one of the student or the two students did not reply.

The button “Show” will allow the BC to see the matching page.
The button “Delete” will allow the BC to delete a match, the message “Do you really want to delete the Buddy (the name of each student) ?”. Then, he/she can click on “Delete” to confirm the delete or “Return” to come back to the previous page. 


*  Association
    - Informations:
    The BC can modify and complete informations. He/She has to put his/her name, and his/her city. These pieces of information are compulsory. He/she can also add another city, if his/her section includes several universities from neighbouring cities. Other elements are optional. He/she can give the link of his/her website and describe his/her section. He/she can show his logo by selecting a picture from his/her documents. He/she can share the links of his Facebook, Twitter and Instagram profiles.
    
    - Matching preferences:
    He/she can select the current semester. For each semester, he/she can select the start date (he/she selects a month) and the end date (he can select a month). The number of week before pre-opening cannot be modified by the BC, it’s automatic. An information message says that “the next semester is automatically opened 4 weeks before the end of the current semester”.  

    - Weighting of matching criteria:
    The BC can select the importance of criteria among the following elements: the city, the institute, the studies, the languages, the availability, the age, the hobbies, the expectations. Thanks to a slider, the BC can give to an element a number between 0 and 10. 10 (mandatory) means the element is mandatory for the matching, 0 (enabled) means the element is not important for the matching.

    - Automatic emails configurations:
    The BC can set automatic deadlines thanks to a slider. First, he/she can choose to contact the user after he/she has been matched. If the BC writes “7” in the box “number of days before sending a warning”, the user will be contacted 7 days after his/her match, to remind him he has been matched. The range of values is from 1 day to 15 days.
    Second, he can choose to remove a match, if an user didn’t reply to the match. For example, if the BC wants the users accept or refuse the match 14 days max after it occured, he/she will write “14” in the box “number of days before removing the match”. The range of values is from 2 days to 30 days. 


*  Institutes
    The BC can click on the button “manage institutes linked”. A new page named “ linkable institutes” will appear, with the possibility to watch the different institutes of the association and the number of students per institute. 

    Still on this page, he/she can click on the button “create a new institute”, if there is an institute missing. He/she will be redirected on a page “create institute”, and he/she will have to fill information (name, city, website, description, logo). 

    On the list of institutes, he/she can select an institute and activate or deactivate, if he/she wants to link an institute to the association. 

Previous page: institutes list
    The BC has a list of institutes that belongs to the section. Thus, he/she can see the number of users by institute. He/she can select an institute and has 3 options: to “edit”, to “manage children” or to “manage studies”

1) The BC can select an institute and click on “Edit”, the page “Edit institutes” appears.

In his/her matching preferences, he/she can activate the button “studies required” if he/she wants that students study in a common field, or he/she can deactivate the button, if it is not necessary. On his/her registration preferences, he/she can give an email address allowed for mentors. For example, if the BC only accepts students whose address ends by @hotmail.fr, he/she can add an e-mail domain. Then, to save these elements, he/she can do it by clicking on “Edit”.  

2) If he/she clicks on “manage children”, he/she will be able to add academic campus ( if the institutes have several campus)

3) If he/she clicks on “manage studies”, a list of subjects appears with a button activate/deactivate, to inform if the subject exists or does not exist in the university. 


*  Hobbies
    In this part, the BC has a list of hobbies (animals, dancing, music...). By activating or not the blue button, he/she  decides to suggest or not some hobbies for the user.This action will have an impact on the registrations of users. If he/she decides to deactivate “music” as hobby, the user will not be able to select “music” as hobby, when he/she will create his/her profile. 

*  Motivations
    Here, the BC has a list of motivations (meet new people, have information on a country, have someone to discover the city). By activating or not the blue button, he/she decides to suggest or not some motivations for the user. It will have an impact for the user when he/she will choose his/her motivations. If the motivation “meet new people” is deactivated by the BC, the user will not be able to select this option. 
