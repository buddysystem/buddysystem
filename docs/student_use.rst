Student use 
===========

Home page
---------

This is the first content you see when you arrive on the Buddy System platform, displaying a top bar with 6 menus (Buddy logo, Project, Our partners, Log in, Sign up, Languages choice), a main page introducing the project, the buddy system and being a Buddy while inviting the user to discover more about these aspects, and a bottom bar with 2 menus and 7 submenus ( About, Home, The project, Our partners, Contact us, Legal information, Privacy Policy, Terms and conditions, Cookie Policy). There are also different information zones, and click buttons, described below.

*  Menus tabs bar - top page
    Buddy logo
        This is the Buddy System project and platform logo, when clicking on it, it brings the user on the Home page when not logged in.
    Project
        When clicking on it, it brings the user to a page presenting the project.
    Our partners
        When clicking on it, it brings the user to a page presenting the project partners and their logos.
    Log in
        When clicking on it, it brings the user to a pop-up menu, to log in and access personalised information and features, for registered members. 
    Sign up
        When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
    Languages choice
        When clicking on it, it brings the user to a short menu to choose the platform language. 

*  Button “Let’s Go!”
    When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
*  Button “Learn More”
    When clicking on it, it brings the user to a page presenting the project.
*  Zone Why should I get a Buddy? -> more information
    When clicking on it, it brings the user to a page presenting the project.
*  Zone Why should I be a Buddy? -> more information
    When clicking on it, it brings the user to a page presenting the project.
*  Button “Sign Up”
    When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
*  Button “Learn More About the Project”
    When clicking on it, it brings the user to a page presenting the project.
 

*  Menus tab bar - bottom
    About
        Home
            When clicking on it, it brings the user to the Home page.
        The project
            When clicking on it, it brings the user to a page presenting the project.
        Our partners
            When clicking on it, it brings the user to a page presenting the project partners and their logos.
        Contact us
            When clicking on it, it opens a pop-up page linked to the user personal email device, where he/she is invited to write an email direct to the technical team (buddysystem@ixesn.fr).
    Legal information
        Privacy Policy
            This policy is designed to tell you about the platform practices regarding the collection, use and disclosure of information that any user may provide via this platform or the mobile application.
        Terms and conditions
            This page details what can be done and what can not be done on the Buddy System platform, by the user or by the technical team.
        Cookie Policy
            This page describes what cookies are and how the Website Operator uses them on the buddysystem.eu website and any of its products or services.

Buddy logo - Dashboard
----------------------

On the dashboard, mentees (international student) can see a summary of their buddies with whom they might have matched. 

*  Profile picture and name
    Here you can see if the profile is complete and if terms of use have been signed.

*  Zone Organization/Institute
    - Link to Organization’s website, Facebook and Twitter
        Here you might find the logo of the organization in charge of you. Then you can have access thanks to links to the organization’s website, Facebook and Twitter.
    - Link to “Contact my coordinator” -> Messages section
        You can also contact your coordinator (someone from your organization or from your institute) thanks to a link that leads to the messages section.

*  Zone My Buddies
    - Buddies confirmed/ awaiting 
        - Link to “ mentor’s mail address” -> Mailbox: Here mentees can see with whom they have been matched (if they have). They can see information about their mentors (profile picture, date of birth, institute, spoken language(s) and expectations).
        - Link to “Contact” -> Messages section: They can also contact them thanks to a link that leads to their private mailbox or to the messages section of the BuddySystem.

My profile
----------

*  How does it work?
    On the left part, there is a section called: “How does it work”, followed by 4 steps to follow to finish properly your application.

1. Complete your profile as much as you can to increase chances to find a better buddy 
2. We warn you when we find your buddy 
3. Contact your buddy via email or WhatsApp
4. Set up a meeting together and get to know each other!

*  Account
    In this “Account” section, the mail (compulsory), the phone number and a password (compulsory) have to be filled in by mentees.
        - Email 
        - Phone
        - Password
    - Button “export profile”
        When you click on the the button “Export profile”, an error page appears, with the message -“attempted to call an undefined method named “getSemester” of class “App/Entity/User”
    - Button “delete profile”
        When you click on the button “Delete profile” , the message “Do you really want to delete the User “username”?” is displayed. Then, you have two possibilities:  either you “Delete” (deleting the profile) or you “Cancel” (cancelling the action).

*  Profile
    This part allows mentees to complete their profiles. The only non compulsory information is the profile picture. Otherwise, you have to give your first name and last name, your date of birth and your gender.

- Profile picture: Button “Search”
- Firstname
- Lastname
- Date of birth: Select Button “Month/Day/Year” 
- Gender: Select Button (Not specified, male, female, other)

*  Situation
    Here mentees are asked to give many information such as language(s) they speak, their nationality (they have to check if they are a local or an international student), the country and the city they are from, their hometown institute. They also have to fill in for which semester they are abroad and their dates of availability and departure. All these information will permit the coordinator to find them the best local student to match with.

- Spoken language(s): Add Button
- Nationality: Select Button
- Choose between Button “Local Student” or “International Student” -> Button “i” information    
- Country: Select Button
- City: Select Button
- Institute: Select Button
- Semester: Select Button
- Date of availability: Select Button “Month/Day/Year” 
- Date of departure: Select Button “Month/Day/Year” 

*  Preferences
    This section gives information to the BC about what kind of mentors mentees would like to have. Mentees have to say which language(s) they want for their buddy (ies), the number of buddies they want, hobbies they do or practice and expectations they have about this Buddy experience. Mentees also choose to activate or deactivate the Button “I want someone with the same gender as me”.

- Languages wanted for my buddies: Add Button
- Number of buddies wanted: Add Button
- Hobbies: Add Button
- Expectations: Select Button
- I want someone with the same gender as me (don’t check if you don’t care): Button “Check”
- Comments
    There is a part for any comments mentees would like to give to their future mentors. As all of these information are “preferences”, the BC must do his or her maximum to find the perfect mentee(s) but it is not compulsory.


*  Notifications
    Mentees can activate/deactivate the Button “I agree to receive news and updates about the Buddy System, functionalities and events”. Then, they have the same button, this time to “activate/deactivate” email notifications for any received message (the section “Message your buddy(ies)” may be removed as a result of your change). 

- I agree to news and updates about the Buddy System, functionalities and events: Button “Check”
- Receive an email when News message received: Button “Check”
- Button “Edit”
    Mentees can edit their profile with the Button “Edit” and make the changes saved.

Messages
--------

*  Inbox
    This part is called “Inbox” with all the conversations. Among these conversations, you have some chats with your mentor(s) and the mentees channel.

- Refresh icon: to know if you have received new messages
- Life buoy icon -> Contact my coordinator: leads to a conversation with your coordinator (organization or institute). 
- Send message icon: To send messages, you have a section down the window chat where you can “Write your message” and send it thanks to the blue icon.














