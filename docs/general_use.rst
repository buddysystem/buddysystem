External and general use
========================

Home page
---------
This is the first content you see when you arrive on the Buddy System platform, displaying a top bar with 6 menus (Buddy logo, Project, Our partners, Log in, Sign up, Languages choice), a main page introducing the project, the buddy system and being a Buddy while inviting the user to discover more about these aspects, and a bottom bar with 2 menus and 7 submenus ( About, Home, The project, Our partners, Contact us, Legal information, Privacy Policy, Terms and conditions, Cookie Policy). There are also different information zones, and click buttons, described below.

* Menus tabs bar - top page

  * Buddy logo
        This is the Buddy System project and platform logo, when clicking on it, it brings the user on the Home page when not logged in.
  * Project
        When clicking on it, it brings the user to a page presenting the project.
  * Our partners
        When clicking on it, it brings the user to a page presenting the project partners and their logos.
  * Log in
        When clicking on it, it brings the user to a pop-up menu, to log in and access personalised information and features, for registered members.
  * Sign up
        When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
  * Languages choice
        When clicking on it, it brings the user to a short menu to choose the platform language.


* Button “Let’s Go!”
        When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.


* Button “Learn More”
        When clicking on it, it brings the user to a page presenting the project.


* Zone Why should I get a Buddy?
        When clicking on it, it brings the user to a page presenting the project.


* Zone Why should I be a Buddy?
        When clicking on it, it brings the user to a page presenting the project.


* Button “Sign Up”
        When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.


* Button “Learn More About the Project”
        When clicking on it, it brings the user to a page presenting the project.


* Footer

  * About

    * Home
        When clicking on it, it brings the user to the Home page.

    * The project
        When clicking on it, it brings the user to a page presenting the project.

    * Our partners
        When clicking on it, it brings the user to a page presenting the project partners and their logos.

    * Contact us
        When clicking on it, it opens a pop-up page linked to the user personal email device, where he/she is invited to write an email direct to the technical team (buddysystem@ixesn.fr).


  * Legal information

    * Privacy Policy
        This policy is designed to tell you about the platform practices regarding the collection, use and disclosure of information that any user may provide via this platform or the mobile application.

    * Terms and conditions
        This page details what can be done and what can not be done on the Buddy System platform, by the user or by the technical team.

    * Cookie Policy
        This page describes what cookies are and how the Website Operator uses them on the buddysystem.eu website and any of its products or services.


The project
-----------
This page presents the project, its objectives and where it comes from, as well as its partners.

* “The results are available here.”
        The project conducted a research about buddy programmes and how they were implemented in different countries, along with their benefits. Clicking on it will allow the user to read the research.
* Map
        This map shows where the Buddy System first started spreading. It is not dynamic yet, but it will show in real time every city where the Buddy System is implemented.
* Button “Sign Up”
        When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
* Button “Learn More About Our Partners”
        When clicking on it, it brings the user to a page presenting the project partners and their logos.


Our partners
------------
This page explains the European scale of the project, and the partners behind it: the first line of logos shows the main partners, and the second line shows partners associated to the project, helping with the communication and dissemination.

* Button “Sign Up”
       When clicking on it, it brings the user to a pop-up menu, to register by creating a member profile on the platform.
* Button “Learn More About The Project”
       When clicking on it, it brings the user to a page presenting the project.


Log In
------
When clicking on it, it brings the user to a pop-up menu, to log in and access personalised information and features, for registered members.
email area
The user is asked to identify him/herself by writing the email address used when registering.
password area
The user is asked to identify him/herself by writing the password used when registering.

* Button “Remember me”
       The “Remember Me” autologin cookie remembers your password and automatically logs you in the Buddy System website so that the user won’t have to re-enter his/her email and password. You should only use this feature if your computer is used exclusively by yourself.
* “Forgotten password” link
       When clicking, the user lands on a page where he/she is asked to write his/her registering email address, so he/she can receive a link to reset a new password.
* Button “Log In”
       When both the email and password are completed, the user clicks to be redirected to his/her dashboard, as an identified member of the platform.
* Button “Sign Up”
       If the user doesn’t have an registered account yet, he/she can click on “Sign Up”.

Sign Up
-------
On this page, a new user can register, to become a member of the platform, and access personalised content.

* “email” area
       The user is asked to write an email address to be used when logging on the platform. On this address, the user will receive an email to confirm the registration, by clicking on a link.
* “password” area
       The user is asked to choose a password, used later when logging in to confirm the identity of a user. It is meant to be secret and personal, while an email address can be known by other people.
* “repeat password” area
       The user is asked to write again the password, to confirm its choice.
* “local student” - “international student” choice button
       The user must states whether he/she is a local student willing to help an international student coming on a mobility, or an international student on a mobility interested in getting help from a local student.
* i button
       The information button helps understanding the difference between international and local.
* “Firstname” area
       The user is asked to indicate his/her first name.
* “Lastname” area
       The user is asked to indicate his/her last name.
* “Spoken language(s)” choice selector
       The user is asked to indicate the languages he/she speaks, to then be matched with someone with the closest possible profile.
* “Your city for the next semester” choice selector
       The user is asked to indicate the city he/she will be living in during the upcoming/current semester, to then be matched with someone available in the same city.
* “University Semester” choice selector
       The user is asked to indicate the university or school he/she will be studying in (or currently is) for the semester, to be matched with someone from the same institution. Depending on the institution, the user might be asked to detail the level of detail available (the campus, and the faculty, for example.)
* “I agree that the data filled in this form will be used by the BuddySystem to find a person with a profile as similar as possible, in accordance with the Privacy Policy” agreement tick box
       The user is asked to acknowledge and accept that the information he/she gives will be used for matching purposes and kept on the platform. The user can consult the Privacy Policy to understand how and why, and to know the user rights regarding the data.
* “I fully agree with the general rules and conditions” agreement tick box
       The user is asked to acknowledge and accept the rules and conditions of use of the Buddy System platform. The user can consult them by clicking on the link.
* Button “Sign Up”
       The user is asked to confirm his/her registration by clicking on the button Sign Up. This action will automatically detect if a field is not completed or if there is an issue.
* Button “Cancel”
       The user can cancel his/her registration if he/she no longer wants to become a member of the platform. By clicking on the Cancel button, the data filled will be deleted.

Languages choice
----------------
When clicking on it, it brings the user to a short menu to choose the platform language.

Contact us
----------
When clicking on it, it opens a pop-up page linked to the user personal email device, where he/she is invited to write an email direct to the technical team (buddysystem@ixesn.fr).

Privacy Policy
--------------
When clicking, the user is brought to the privacy policy, designed to tell the user about the platform practices regarding the collection, use and disclosure of information that any user may provide via this platform or the mobile application.

Terms and conditions
--------------------
This page details what can be done and what can not be done on the Buddy System platform, by the user or by the technical team.

* “About ESN France” link
    Since ESN France is the entity conducting both the project and the platform management, the user is invited to discover more about it by bringing the user to its website by clicking on the link.
* “Buddy System experience” link
    When clicking on it, it brings the user to a page presenting the project.
* “Buddy System Erasmus+ project” link
    When clicking on it, it brings the user to a page presenting the project partners and their logos.

Cookie Policy
-------------
This page describes what cookies are and how the Website Operator uses them on the buddysystem.eu website and any of its products or services.

* “internetcookies.org” link
    The user can discover more about cookies and how to manage them by clicking on this website link.
