Welcome to BuddySystem's documentation!
=======================================

Table of Contents
-----------------
.. toctree::

   general_use
   student_use
   association_use
   institute_use
   admin_use
