Administrator use
=================

Dashboard
----------------------
This page gives an overview of the platform overall activity.

* Profile picture and name
        Here, the user’s name and profile picture is displayed.

* Insights
        This part is meant to give a quick overview of the platform activity regarding the buddies number, the entities registered on it, by displaying several types of information.

  * Number of users
        This is the amount of users registered on the platform currently. It shows also the growth since July, 30th, when the statistics first started.

  * Number of associations
        This is the amount of entities registered on the platform currently, both students organisation and higher education institutions.

  * Association - choice selector
       The user can select which entity he/she wants to see the specific statistics from.

  * Button “Show all week stats”
       This will open another page, where the user can see the detailed statistics per day per entity. This data can be refined by column (Day, Mentor count, Mentee count, Mentor not matched, Mentee not matched, New users, New Mentee, New Mentor, Confirmed matches, Refused matches, Waiting matches, Semester of registration, Year, Managing Entity).
       The user can also download the data selected in several formats (CSV, Excel, PDF).
  * Button "Institutes requested"
       This will open another page, where the user can find the list of institutes requested by users to be available on the platform, that are not yet registered. This data can be refined by column (Name, Country, City, Date of Request)
       The user can also download the data selected in several formats (CSV, Excel, PDF).

* Users
        This part shows 2 graphics.
        The first one is about mentees. Those who are matched are in red, those who are not matched are in yellow.
        The second graphic shows the same statistics, with the same colors but for the mentors.

* Registration
        This part allows the user to see a graphic with the users registrations each week of the year.

  * Information “ Users may have registered on a different semester”
      This warns the user that this data is to be understood as a raw information, it doesn’t distinguish users that registered to be matched on a future semester.

    * Number of new users
        In red, we see the number of new users.

    * Number of new mentors
        In yellow, we see the number of new mentors.

    * Number of new mentees
        In pink, we see the number of new mentees.

* Buddies
    This part shows a graphic, with in red the buddies refused, in green buddies confirmed and in yellow buddies who are waiting for their registration/matching

    * Confirmed
        If afterward, a Buddy rejects the match, the pair goes in the refused count

    * Waiting
        These are the users not matched yet.

    * Refused
        These are users whose buddy rejected the proposal. They can be matched again.

* Institutes
        This graphics displays the number of users registered in the different entities present on the platform, comparing with the others.

  * Choice selector button
      Here the user can choose which group of entities he/she would like to display the data from. There are displayed by groups of ten, the ten first being the entities welcoming the most numbers of users, the second group of ten welcoming the second most numbers of users,etc.

* Motivations
        This graphic displays the different motivations chosen by the mentors and mentees, by comparison of importance.

* Nationalities
        This graphic displays the different nationalities represented on the platform

  * Choice selector button
      Here the user can choose which group of nationalities number he/she would like to display the data from. There are displayed by groups of ten, the ten first being the nationalities the most represented, the second group of ten welcoming the second most represented nationalities.

My profile
----------

* How does it work?
    This part shows 4 steps to make the registration and the matching of the user easier.

  * 1. Complete your profile as much as you can to increase chances to find a better buddy
  * 2. We warn you when we find your buddy
  * 3. Contact your buddy via the messaging system
  * 4. Set up a meeting together and get to know each other!

* Account

  * Email
      This shows the pre-filled information of the user, here his or her email. This information is mandatory, as shows the small star next to it.

  * Phone
      This shows the pre-filled information of the user, here his or her phone number. This information is not mandatory. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

    * Button “edit password”
        When clicking on it, a pop-up page appears, asking you to write the new password the user would like to use from now on, in two different cases. It also asks to put the previous password. Then the user can either confirm or cancel the action. This action is different from the “forgot your password?” button.

    * Button “export profile”
        When clicking on it, a pop-up page appears, so the user can choose how he or she would like to download his or her profile data and information on a document, outside of the platform. This does not delete the profile nor the data and information, it just provides a copy of it.

    * Button “delete profile”
        When you click on the button “Delete profile” , the message “Do you really want to delete the User..?” is displayed. Then, you have two possibilities:  Delete (to delete the profile) or Cancel (to cancel the action). Once the profile is deleted, with all the personal information and data, the user can no longer access it.

* Profile

  * Profile picture
      This shows the user’s profile picture in a circle shape.If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

    * Button “search”
        This button allows the user to search and choose a picture from the user’s computer.

  * Firstname
      This shows the pre-filled information of the user, here his or her first name.If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

  * Lastname
      This shows the pre-filled information of the user, here his or her last name.If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

  * Date of birth
      This shows the pre-filled information of the user, here his or her date of birth. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

    * Select Button “Month/Day/Year”
        If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

  * Gender

    * Select Button (Not specified, male, female, other)
        This shows the pre-filled information of the user, here his or her gender. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

* Situation

  * Spoken language(s)
      This shows the pre-filled information of the user, here the languages spoken. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

  * Button "Add"
      The user can add another language by writing it. A language can be deleted by clicking on the X next to it.

  * Nationality
      This shows the pre-filled information of the user, here the languages spoken. If the user wants to correct the information, it is possible. He or she then has to click on the “edit” button at the bottom of the page to save the changes.

  * Button select"
      The user can select the nationality among a list of countries. It is not possible to indicate more than one.

* Notifications
    Here the user can choose the desired notifications.

    * Choice slider button “I agree to receive news and updates about the BuddySystem, functionalities and events”
        The user can choose to receive this by sliding the white circle to the right. The blue color when doing so confirms that the notifications are activated for that part. If the user doesn’t want to receive them, the action can be reversed by sliding the white circle to the left. The grey color when doing so confirms that the notifications are not activated.

    * Choice slider button “Receive an email when: New message received”
        The user can choose to receive this by sliding the white circle to the right. The blue color when doing so confirms that the notifications are activated for that part. If the user doesn’t want to receive them, the action can be reversed by sliding the white circle to the left. The grey color when doing so confirms that the notifications are not activated.

* Button “Edit”
        When clicking on this button, a small black information box appears to let the user know that the information was saved, even if nothing changed. The user remains on the same page.

Messages
--------

This part is called “Inbox” with all the conversations. Among these conversations, you have a channel already created, with all the Buddy coordinators, and if you have created a conversation with an individual user, you will see it listed too.

* "Refresh" icon
    When clicking on this button, it updates the page and the messages received, as the process is not dynamic yet.

* "Write" icon
    When clicking on this button, a new part of the inbox opens, and the user can write the name of the user he or she wants to contact individually. An admin user can reach any user on the plaform.

* "Send" icon
    To send messages, you have a section down the window chat where you can write your message and send it by clicking on the blue icon.

Management
----------

This regroups the core activities of the Buddy System.
 
* Matching
   * Number selector “entries” (5, 10, 25, 50, 100)
        This selector allows you to choose the length or number of entries you want the page to show, either 5 lines, 10, etc. 

   * Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the matching activity, and in different document formats, CSV, Excel or PDF. 

   * Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns.


   * Columns (first name, last name, date of arrival, created at, semester, manager, city, institute, spoken languages, age, gender, languages wanted, expectations, nb of buddies, nb of buddies wanted.

   * Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.

   * Button Page number
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.

   * Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
  
  > When clicking on a user
   If you click on a user’s name, you see a list of potential users he/she could be matched with, ranked in matching compatibility percentages.


   * Number selector “entries” (5, 10, 25, 50, 100)
        This selector allows you to choose the length or number of entries you want the page to show, either 5 lines, 10, etc. 

   * Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the matching activity, and in different document formats, CSV, Excel or PDF. 

   * Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns.

   * Columns (% of compatibility, first name, last name, date of arrival, created at, semester, city, institute, studies, spoken languages, age, gender, languages wanted, expectations, nb of buddies, nb of buddies wanted, nb of refused matches, nb of warnings, nb of removed matches, nb of reports).

   * Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.

   * Button “Page number”
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.

   * Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.

  > When clicking on a match suggestion
        You see the matching recap of a user, and the information that match and does not match. The green information is the matching one, and the red, the non-matching. It is possible to check the criteria compatibility result by clicking (or going over) the percentages. 
There are three options:

- Button “Match!”
    When clicking this button, you approve this particular match.

- Button “Other propositions”
    When clicking this button, you come back to the potential matches list.

- Button “Next mentor”
    When clicking this button, you see the next best match option.

* Users
   * List of users waiting for approval
        This page shows you the users that need to be approved to be able to fully use the platform, such as students, Buddy Coordinators, and Managers. Buddy Coordinators, Managers and Administrators can take action on this list. Students to be approved are students who did not have a pre-validated email registration, and that potentially can not use the platform because they don’t belong to a specific HEI. Buddy Coordinators can approve other BCs, Managers can approve other Manager, and both profiles can approve each other’s if both the association and the HEI are linked on the platform. Administrators can approve any kind of profile.

   * List users approved
        This page shows you the users that have been approved by either Administrators, Buddy Coordinators or Managers. 

   * Search bar
        You can search a specific user to see when he/she was approved and by whom. 

   * Actions buttons

     * Edit
        By clicking this button, you are able to edit a user’s profile information.

     * Match
        By clicking this button, you are able to see the possible profiles a user can be matched with, and choose who to match this user with. This button is only available for students profiles. 

     * Approve
        By clicking this button, you are approving the user registration on the platform, and he/she is able to access his/her related functionalities depending on the user profile (student, Buddy Coordinator, Manager).

     * Delete
        By clicking this button, you are deleting a user’s profile from the platform, and refusing his/her access to its functionalities.

*  When on the of List users waiting for approval
    - Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the matching activity, and in different document formats, CSV, Excel or PDF. 
    - Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns: first name, last name, date of arrival, created at, semester, manager, city, institute, spoken languages, age, gender, languages wanted, expectations, nb of buddies, nb of buddies wanted 
    - Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
    - Button Page number
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
    - Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
    - Actions buttons
        - Edit
            By clicking this button, you are able to edit a user’s profile information.
        - Match
            By clicking this button, you are able to see the possible profiles a user can be matched with, and choose who to match this user with. This button is only available for students profiles. 
        - Approve
            By clicking this button, you are approving the user registration on the platform, and he/she is able to access his/her related functionalities depending on the user profile (student, Buddy Coordinator, Manager).
        - Delete
            By clicking this button, you are deleting a user’s profile from the platform, and refusing his/her access to its functionalities. 

*  When on the Users page:
    - Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the matching activity, and in different document formats, CSV, Excel or PDF. 
    - Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns: first name, last name, date of arrival, created at, semester, manager, city, institute, spoken languages, age, gender, languages wanted, expectations, nb of buddies, nb of buddies wanted 
    - Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
    -  Button Page number
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
    - Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
    - Actions buttons
        - Edit
            By clicking this button, you are able to edit a user’s profile information.
        - Match
            By clicking this button, you are able to see the possible profiles a user can be matched with, and choose who to match this user with. This button is only available for students profiles. 
        - Delete
            By clicking this button, you are deleting a user’s profile from the platform, and refusing his/her access to its functionalities. 


*  Buddies
    -  Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the matching activity, and in different document formats, CSV, Excel or PDF. 
    - Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns: first name, last name, date of arrival, created at, semester, manager, city, institute, spoken languages, age, gender, languages wanted, expectations, nb of buddies, nb of buddies wanted 
    - Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
    - Button Page number
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
    - Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
    - Actions buttons
        - Show
            When clicking this button, you see the matching recap of a user, and the information that matched and did not match.
        - Delete
            When clicking this button, a pop page appears to ask if you really want to delete the match, or if you wish to cancel your action.

*  Association
    - Create a new association
        When clicking on this button, you are brought to an association creation form.
    - Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
        You can download data from the platform, to analyse the associations registered, and in different document formats, CSV, Excel or PDF. 
    - Selector button “Columns visibility” (mentioned below)
        This button allows you to choose the type of information you want the page to show you, within specific columns: name, city, website, logo, facebook, twitter, instagram, users count, activated
    - Button “Previous”
        When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
    - Button Page number
        You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
    - Button “Next”
        When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
    - Actions buttons
        - Edit
            When clicking on this button, you are brought to the association’s profile, and you are able to edit its information.

*  Institutes
    - Create a new institute
        When clicking on this button, you are brought to an institute creation form.
    - Manage institutes linked
        On this page, you can manage the links between an association (or several) and a Higher Education Institution.
            - Selection bar
                You have to choose an association to link to a HEI. Once you do, you can see all the HEIs available on the platform in the geographical area around the association.
            - Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
                You can download data from the platform, to analyse the associations, and in different document formats, CSV, Excel or PDF. 
            - Selector button “Columns visibility” (mentioned below)
                This button allows you to choose the type of information you want the page to show you, within specific columns: name, website, logo, users count
            - Button “Previous”
                When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
            - Button Page number
                You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
            - Button “Next”
                When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
            - Slider “available”
                This slider allows you to choose if an association can be linked to a specific HEI or not.
    - Manage rights
        - Selection bar
            You have to choose a HEI 
        - Data format downloading choice buttons “CSV”, “EXCEL”, “PDF”
            You can download data from the platform, to analyse the HEIs, and in different document formats, CSV, Excel or PDF. 
        - Selector button “Columns visibility” (mentioned below)
            This button allows you to choose the type of information you want the page to show you, within specific columns: name, website, logo, users count, association’s linked
        - Button “Previous”
            When clicking this button, you are brought to the previous list page. If you are on page 1, you can not click this button.
        - Button Page number
            You can select the page number you want the platform to show. The number of pages changes according to your selection of entries number.
        - Button “Next”
            When clicking this button, you are brought to the next list page. If you are on the last page, you can not click this button.
        - Button rights 

    - Actions buttons
        - Edit
        - Manage children
        - Manage studies
        - Delete

*  Studies
    - Create a new study
        When clicking on this button, you are able to add a new study topic to the list. 
    - Search bar
    - Actions buttons
        - Edit
        - Delete

*  Hobbies
    - Create a new hobby
        When clicking on this button, you are able to add a new hobby to the list. 
    - Search bar
    - Actions buttons
        - Edit
        - Delete

*  Motivation
    - Create a new motivation
        When clicking on this button, you are able to add a new motivation to the list. Be careful to adapt this new motivation to a mentee profile and a mentor profile.
    - Search bar
    - Actions buttons
        - Edit
        - Delete

Translate website 

*  Languages choice
    When clicking on it, it brings the user to a short menu to choose the platform language. 
*  Contact us
    When clicking on it, it opens a pop-up page linked to the user personal email device, where he/she is invited to write an email direct to the technical team (buddysystem@ixesn.fr).
*  Privacy Policy
    When clicking, the user is brought to the privacy policy, designed to tell the user about the platform practices regarding the collection, use and disclosure of information that any user may provide via this platform or the mobile application.
*  Terms and conditions
    This page details what can be done and what can not be done on the Buddy System platform, by the user or by the technical team.
        - “About ESN France” link
            Since ESN France is the entity conducting both the project and the platform management, the user is invited to discover more about it by bringing the user to its website by clicking on the link.
        - “Buddy System experience” link
            When clicking on it, it brings the user to a page presenting the project.
        - “Buddy System Erasmus+ project” link
            When clicking on it, it brings the user to a page presenting the project partners and their logos.
*  Cookie Policy
    This page describes what cookies are and how the Website Operator uses them on the buddysystem.eu website and any of its products or services.
        - “internetcookies.org” link
            The user can discover more about cookies and how to manage them by clicking on this website link. 













