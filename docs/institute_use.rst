Internal and HEI BC use
=================

First the institute manager has to read and accept a charter. There are 4 pages 
 
* The first page is a “preamble”.
   This passage is a summary of the following charter. They can read it and click on “next” to go to the next page.
 
* The second page is called “acceptance of the user charter”.
   The acceptance of the user charter explains that the institute manager has to agree and accept to respect the terms and conditions of navigating on the platform. The institute manager can click on “next” or “previous” if he/she wants to go to the previous page
 
* The third page is called “managers responsibility”.
   The managers responsibility is about the rules and responsabilities that the users, whether it is the institute manager, the buddies or the buddy coordinator have to abide by. It also explains the communication between the users and the technical team and some advices for the users about the activites outside the platform. After reading it, the institute manager can click on “next” or previous.
* The fourth page is called “responsibility of the platform”.
   The last page is about the responsability of the platform, the security and the integrity of it. Finally, the institute manager can click on “previous”, “I disagree, delete profile” if he/she disagrees the charter of the platform or “I agree” if he/she agrees the charter. If he/she agrees, the message “profile updated” is displayed. 


Dashboard
---------
This page gives an overview of the platform overall activity.The institute manager can see statistics regarding its users. But this page does not allow them to modify or complete their profile. The page aims to give information about the structure. 



* You find some elements that belong to the profile of the institute manager:
    - Institute manager profile picture
         
    - Name of the institute manager

    - Button “There are some users to match” : When you click on this button, you are redirected on a page where you see a list of international students. Like the BC, the institute manager can select students to match them.

    - Logo organization (optional)
    - Description of the organization(optional): a little text describes the organization
    - Social networks links (optional): when we click on the logo of a social network/website, you are automatically redirected on the social network/website

*  Insights : figures representing enrollments and interactions on the platform
    - Here you have some relevant information about the current semester, the start date of the next semester and the number users in the section. 
 
    - Button “Show all the Week Stats” which redirects the institute manager on a page to know statistics about registrations of users and matching. Once you are on the page, you have different search bars: “Created at, nb users, nb mentees, nb mentees not matched, nb mentors not matched, nb new users, nb new mentees, nb new mentors, nb match confirmed, nb match refused”. We can fill them by adding figures or select the arrow (down/up) of each search bar to class informations in ascendant or descendant order.  We can choose the number of information we want on the page. (5, 10, 25, 50, 100, 500)  “Show… entries”. If you select “5”, you will have 5 results on the page. 




*  Users
    - This part shows 2 graphics. 
    - The first one is about mentees. Those who are matched are in red, those who are not matched are in yellow. 
    - The second graphic shows the same statistics, with the same colors but for the mentors.  


*  Registration 
    - When you click on the information button, the message > “Users may have registered for a different semester than the current one”. This part allows the institute manager to see a graphic with the users registrations each week of the year. 
        - In red, you see the number of new users. 
        - In yellow,you see the number of new mentors.
        - In pink, you see the number of new mentees.


*  Buddies
    - This part shows a graphic, with in red the buddies refused, in green buddies confirmed and in yellow buddies who are waiting for their registration/matching. 
    

*  Institutes
    - This part lets the institute manager know which universities users are coming from. 
    
    
*  Motivations
    - Each category of motivation is associated to a color, you can compare the importance of all motivations. 

*  Nationalities
    - A ranking is created to know in which country the BS is used
 
    
My Profile 
----------

*  How does it work?
    - This part shows 4 steps to make the registration and the matching of the user easier. 
        1. Complete your profile as much as you can to increase chances to find a better buddy 
        2. We warn you when we find your buddy 
        3. Contact your buddy via email or WhatsApp
        4. Set up a meeting together and get to know each other! 

*  Account
    - This part shows the email of the account. You can also choose to put a phone number.
    - You can edit, your password, export and/or delete your profile. 

*  Profile
    - This part allows the institute manager to complete his/her profile. The only not mandatory information is the profile picture.Otherwise, you can also give your first name, your last name, your date of birth and your gender. 

        
*  Situation
    - Here, you have to give information about your spoken language(s), country, city and your institute (and the level you're on)


*  Notifications: 
    - You can choose to receive notifications or not. The message “I agree to receive news and updates about the Buddy System, functionalities and events” is displayed. You can activate/deactivate the button. If you active it, you will receive news and updates, if you don’t, you will not receive anything.
    - There is the same manipulation for mails about new messages received from : a global channel, a group channel and an individual channel. If you want to be warned about them, you have to activate the button “New message received”. If you don’t want to, you have to deactivate it.
    - Once you have completed your profile, you can click on the button “Edit” to edit it.
    

Messages  
--------

*   There is a part called “Inbox” with all the conversations of the institute manager. Among these conversations, you have some chats with users. Contacts appear in a conversation when they send the institute manager a message. You also have a mentors channel, where  you can chat with mentors.

    - There is a button “to update” new messages, to know if you receive new messages
    - There is a button “to search” . The institute manager searches an user from his/her section, and can write him/her a message. 


Management 
----------
*  Matching
    Once you click on “Matching”, a page with all the international students of the section appears. Other pieces of informations refer to the profile of the Buddy Coordinator:(city, university, date of arrival, age, spoken languages, languages wanted, hobbies, expectations). You can select an international student and match him/her with a local student.

*  Users
    When you click on "Users", a page with all of the users appears. You can : 
    - send a message to the user. 
    - delete the profile of the user. The message “do you really want to delete the User..” will be displayed.
    - match him/her with another student
    - edit the profile of the student. In this case, you will be redirected on an another page called “Edit user (+name of the user)”  This page shows the pieces of information filled by the user. There are different parts but the institute manager cannot modify personal details given by the user.

Account:
    You cannot modify the e-mail address or the phone number. If the profile is archived or not, you can select Yes/No on the section “Is Archived?”
    You have also a button to delete the profile of the user. 
 
Profile:
    Here the institute manager cannot modify anything. He/she just sees informations about the user like his/her profile picture, firstname, last name, date of birth, gender.

Situation:
    The institute manager cannot modify some pieces of information: the spoken language, the nationality, the city, the institute, the semester, the date of arrival, the date of departure. 
    But he/she can select if the user is a local student or an international student.
    He/she can select the institute of the student and at which semester the student stays.
 
 
Preferences:
    It is still the profile of the user and the institute manager cannot modify anything.
The user gives:
    - the language(s) he wants for his/her buddies, 
    - the number of buddies he/she wants
    - his/her hobbies
    - his/her expectations about this Buddy experience
    - he chooses to activate or deactivate the Button “I want someone with the same gender as me” 

Notifications: 
    The institute manager cannot modify anything. He/she just can click on “Edit” to save the profile of the user.
    The user can choose to be contact by email if there are some notifications. 


*  Buddies 
    Here the institute manager sees a Buddies list, a list of matching with in each line the name of the international student and the local student, their e-mail addresses, and the confirmation of their match. OK, KO,  or waiting; 
    “OK” means the match is confirmed, “KO” means it is not confirmed by a student or the two students, “waiting” means one of the student or the two students did not reply.

    - The button “Show” will allow the institute manager to see the matching page. He/she will see the compatibility between the two students. But, he/she will not be able to match them. He can only return to the previous page, by clicking on “back to the list”.
    - The button “Delete” will allow the institute manager to delete a match, the message “Do you really want to delete the Buddy (the name of each student) ?”. Then, he/she can click on “Delete” to confirm the delete or “Return” to come back to the previous page.

*  Institutes
    - The institute manager has a list of institutes that belongs to the establisment. Thus, he/she can see the number of users by institute. He/she can select an institute and has 4 options: to “edit”, to “manage studies”, modify "institute preferences" or to "delete"
        - Faculties: If he/she clicks on “manage studies”, a studies list appears, with all the field of studies on the platform.
        - He/she can also click on the button “create a new institute”, if there is an institute missing. He/she will be redirected on a page “create institute”, and he/she will have to fill information (name, city, website, description, logo).
        - Button: manage rights 

    - When the institute manager click on “Edit”, the page “Edit institutes” appears. He/she will be able to modify information of the institute, matching preferences and registration preferences. He/she can do the same operations with the new institute created. 
        - For a faculty:In his/her matching preferences, he/she can activate the button “studies required” if he/she wants that students study in a common field, or he/she can deactivate the button, if it is not necessary. On his/her registration preferences, he can give an email address allowed for mentors. For example, if the institute manager only accepts students whose address ends by @hotmail.fr, he/she add a domain name. Then, to save these elements, he/she can do it by clicking on “create”.  
        - For the campus: When the institute manager selects an university and clicks on “Edit”, a new page appears. The institute manager can select the importance of criterias among the following elements: the city, the institute, the studies, the languages, the availability, the age, the hobbies, he expectations. Thanks to a slider, the institute manager can give to an element a number between 0 and 10. 10 (mandatory) means the element is mandatory for the matching, 0 (enabled) means the element is not important for the matching.

    - Automatic email configurations
        - The institute manager can set automatic deadlines thanks to a slider. First, he/she can choose to contact the user after he/she has been matched. If the institute manager writes “7” in the box “number of days before sending a warning”, the user will be contacted 7 days after his/her match, to remind him he has been matched. The range of values is from 1 day to 15 days.
        -  Second, he/she can choose to remove a match, if an user didn’t reply to the match. For example, if the institute manager wants the users accept or refuse the match 14 days max after it occured, he/she will write “14” in the box “number of days before removing the match”. The range of values is from 2 days to 30 days.


*  Hobbies
    In this part, the institute manager has a list of hobbies (animals, dancing, music...). By activating or not the blue button, he/she decides to suggest or not some hobbies for the user.This action will have an impact on the registrations of users. If he/she decides to deactivate “music” as hobby, the user will not be able to select “music” as hobby, when he/she will create his/her profile. 

*  Motivations
    Here, the institute manager has a list of motivations (meet new people, have information on a country, have someone to discover the city). By activating or not the blue button, he/she decides to suggest or not some motivations for the user. It will have an impact for the user when he/she will choose his/her motivations. If the motivation “meet new people” is deactivated by the institute manager, the user will not be able to select this option. 

