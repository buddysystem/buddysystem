$(() => {
    const table = $('#week-stat');

    let columns = [
        { data: 'createdAt' },
        { data: 'nbUsers' },
        { data: 'nbMentees' },
        { data: 'nbMentors' },
        { data: 'nbMenteesNotMatched' },
        { data: 'nbMentorsNotMatched' },
        { data: 'nbNewUsers' },
        { data: 'nbNewMentees' },
        { data: 'nbNewMentors' },
        { data: 'nbMatchConfirmed' },
        { data: 'nbMatchRefused' },
        { data: 'nbMatchWaiting' },
        { data: 'semester' },
        { data: 'year' },
        { data: 'type' }
    ];

    if (1 === table.data('bc')) {
        columns = $.merge(columns, [
            { data: 'institute', orderable: false }
        ]);
    }

    tools.initDatatable(table, {
        order: [[0, 'desc']],
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('stats_list'),
        columns
    }, 'BuddySystem - Stats List');

    table.on('draw.dt', () => {
        loader.stop();
    });
});
