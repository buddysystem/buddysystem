class UserReport {
    static init() {
        window._urq = window._urq || [];
        _urq.push(['initSite', process.env.USER_REPORT_KEY]);
        (function () {
            const ur = document.createElement('script');
            ur.type = 'text/javascript';
            ur.async = true;
            ur.src = ('https:' === document.location.protocol ?
                'https://cdn.userreport.com/userreport.js' :
                'http://cdn.userreport.com/userreport.js'
            );
            const s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ur, s);
        }());
    }
}

export default UserReport;
