(function ($) {
    $.fn.saveForm = function (options) {
        const defaults = {
            toFade: null,
            revert: false
        };

        const params = $.extend(defaults, options);

        return this.each(function () {
            const $this = $(this);
            const $submit = $this.find('[type="submit"], button, a');
            const $inputs = $this.find('input, textarea');

            if (!params.revert) {
                $inputs.prop('disabled', true);
                $submit.addClass('loading');
                $submit.prop('disabled', true);

                if (null != params.toFade) {
                    params.toFade.css('opacity', '.4');
                }
            } else {
                $inputs.prop('disabled', false);
                $submit.removeClass('loading');
                $submit.prop('disabled', false);

                if (null != params.toFade) {
                    params.toFade.removeAttr('style');
                }
            }
        });
    };
}(jQuery));
