const Routing = require('../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.js');
const routerConfig = require('../../../public/js/fos_js_routes.json');

Routing.setRoutingData(routerConfig);

Routing.generateImpl = Routing.generate;
Routing.generate = function (url, params) {
    let paramsExt = {};
    if (params) {
        paramsExt = params;
    }
    if (!paramsExt._locale) {
        paramsExt._locale = Translator.locale;
    }
    return Routing.generateImpl(url, paramsExt);
};

export default Routing;
