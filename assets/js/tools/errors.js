window.errors = [];

function addError(error) {
    if ('object' !== typeof (error)) {
        return;
    }

    errors.push(error);
}

window.onerror = function (message, url, line, column, error) {
    addError({
        type: 'javascript',
        message,
        location: `${url}:${line}:${column}`
    });
};

$(document).ajaxError((event, xhr, settings, error) => {
    addError({
        type: 'xhr',
        url: settings.url,
        method: settings.type,
        message: error,
        statusCode: xhr.status,
        response: xhr.responseJSON ? xhr.responseText : null
    });
});
