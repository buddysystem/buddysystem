import Const from './Const';

class Flashbag {
    constructor() {
        this.$flashbags = $('#flashbags');
        this.id = 1;
    }

    _getTemplate(text, type, button) {
        let icon;
        switch (type) {
        case 'info':
            icon = 'info-circle';
            break;
        case 'error':
            icon = 'times-circle';
            break;
        case 'warning':
            icon = 'exclamation-circle';
            break;
        case 'success':
            icon = 'check-circle';
            break;
        default:
            icon = 'info-circle';
            break;
        }

        button = ('undefined' !== typeof button || null === button) ? button : '';

        const flashbagType = $(`<div id="flashbag-${this.id}" class="flashbag ${type}"></div>`);

        $(flashbagType).append(
            $(`<p><i class="fas fa-${icon}"></i> ${text}</p>` +
                `<div class="buttons">${button}</div>` +
                '<a class="dismiss"><i class="fa fa-times"></i></a>')
        );

        if (button) {
            $(flashbagType).addClass('flashbag__button');
        }

        return {
            html: flashbagType,
            id: `#flashbag-${this.id}`
        };
    }

    show(text, type, button, timing) {
        const flashbagTemplate = this._getTemplate(text, type, button);
        this.$flashbags.append(flashbagTemplate.html);
        $(flashbagTemplate.id).slideDown(100);

        $('.dismiss').on('click', function () {
            $(this).parent().slideUp(100);
        });

        if ('undefined' === typeof timing) {
            timing = Const.flashbagTimeout;
        }

        setTimeout(() => {
            $(flashbagTemplate.id).slideUp(100);
        }, timing);

        this.id += 1;

        $(window).trigger('new_element', $(flashbagTemplate.id));

        return $(flashbagTemplate.id);
    }
}

export default new Flashbag();
