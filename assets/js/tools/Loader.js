/**
 * @type {{start, stop}}
 */
class Loader {
    constructor() {
        this.$loader = $('.loader');
        this.$body = $('body');
        this.loaderHTML = '<div class="loader"><span></span></div>';
    }

    start() {
        if (0 === this.$loader.length) {
            this.$body.prepend(this.loaderHTML);
            this.$loader = $('.loader');
        }

        this.$loader.show();
    }

    stop() {
        this.$loader.hide();
    }
}

export default new Loader();
