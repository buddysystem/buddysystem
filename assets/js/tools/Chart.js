import Chartist from 'chartist/dist/chartist.min';
import ChartistTooltip from 'chartist-plugin-tooltips-updated';
import 'chartist/dist/scss/chartist.scss';
import 'chartist-plugin-tooltips-updated/dist/chartist-plugin-tooltip.scss';

class Chart {
    static pieChart(elem, data, options) {
        Chartist.Pie(document.querySelector(elem), data, $.extend({
            labelInterpolationFnc(value) {
                return (0 < value) ? value : null;
            }
        }, options));
    }

    static stackedBarChart(elem, data, options) {
        Chartist.Bar(document.querySelector(elem), data, $.extend({
            stackBars: true,
            axisY: {
                labelInterpolationFnc(value) {
                    return value;
                },
                onlyInteger: true
            }
        }, options));
    }

    static simpleLineChart(elem, data, options) {
        Chartist.Line(document.querySelector(elem), data, $.extend({
            fullWidth: true,
            chartPadding: {
                right: 40
            },
            axisY: {
                labelInterpolationFnc(value) {
                    return value;
                },
                onlyInteger: true
            },
            axisX: {
                labelInterpolationFnc(value, index) {
                    return 0 === index % 4 ? value : null;
                }
            },
            plugins: [
                ChartistTooltip()
            ]
        }, options));
    }

    static overlappingBarChart(elem, data, options) {
        Chartist.Bar(document.querySelector(elem), data, $.extend({
            seriesBarDistance: 20,
            axisY: {
                onlyInteger: true
            },
            stretch: true
        }, options), [
            ['screen and (max-width: 640px)', {
                seriesBarDistance: 10
            }]
        ]);
    }

    static donutChart(elem, data, options) {
        Chartist.Pie(document.querySelector(elem), data, $.extend({
            donut: true,
            donutWidth: 60,
            donutSolid: true,
            startAngle: 270,
            showLabel: true
        }, options));
    }
}

export default Chart;
