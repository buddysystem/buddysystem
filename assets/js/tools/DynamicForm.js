class DynamicForm {
    init(options) {
        this.params = $.extend({
            countrySelector: null,
            citySelector: null,
            instituteSelector: null,
            associationSelector: null,
            semesterSelector: null,
            studySelector: null,
            emailField: null,
            motivationSelector: null,
            hobbySelector: null,
            isLocalSelector: null,
            typeOfMobilitySelector: null,
            levelOfStudySelector: null,
            levelOfStudyOther: null,
            nbBuddiesWantedField: null,
            departure: null,
            fullYearOnly: null,
            bonusOption: null,
            extractType: null,
            domainWarning: '.domain_warning',
            dynamicFormGroup: '.dynamic-form-group'
        }, options);

        this.initCountrySelector();
        this.initCitySelector();
        this.initInstituteSelector();
        this.initEmailField();
        this.initLevelOfStudy();
        this.initIsLocal();
        this.initFullYearOnly();
        this.initExtractType();
    }

    initCountrySelector() {
        const $countrySelector = $(this.params.countrySelector);
        if ($countrySelector.length) {
            $countrySelector.off('change');
            $countrySelector.on('change', () => {
                loader.start();
                const dynamicForm = this;
                const $form = $countrySelector.closest('form');
                const data = dynamicForm._getData($form);

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.citySelector, html);
                        dynamicForm._replaceWith(`#${dynamicForm.params.instituteSelector}`, html, dynamicForm.params.dynamicFormGroup);
                        dynamicForm._replaceWith(dynamicForm.params.associationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.semesterSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.studySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.hobbySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.motivationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.nbBuddiesWantedField, html);
                        dynamicForm._replaceWith(dynamicForm.params.bonusOption, html);

                        dynamicForm.initCitySelector();
                        dynamicForm.initInstituteSelector();
                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });
            });
        }
    }

    initCitySelector() {
        const $citySelector = $(this.params.citySelector);
        if ($citySelector.length) {
            $citySelector.off('change');
            $citySelector.on('change', () => {
                loader.start();
                const dynamicForm = this;
                const $form = $citySelector.closest('form');
                const data = dynamicForm._getData($form);

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.citySelector, html);
                        dynamicForm._replaceWith(`#${dynamicForm.params.instituteSelector}`, html, dynamicForm.params.dynamicFormGroup);
                        dynamicForm._replaceWith(dynamicForm.params.associationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.semesterSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.studySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.hobbySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.motivationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.nbBuddiesWantedField, html);
                        dynamicForm._replaceWith(dynamicForm.params.bonusOption, html);

                        dynamicForm.initCitySelector();
                        dynamicForm.initInstituteSelector();
                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });
            });
        }
    }

    initInstituteSelector() {
        const $institutesSelectors = $(`[id^='${this.params.instituteSelector}']`);
        if ($institutesSelectors.length) {
            $institutesSelectors.off('change');
            $institutesSelectors.on('change', (element) => {
                loader.start();
                const dynamicForm = this;
                const $form = $institutesSelectors.closest('form');
                const data = dynamicForm._getData($form);
                data[$(`#${dynamicForm.params.instituteSelector}`).attr('name')] = $(element.target).val();

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.semesterSelector, html);
                        dynamicForm._replaceWith(`#${dynamicForm.params.instituteSelector}`, html, dynamicForm.params.dynamicFormGroup);
                        dynamicForm._replaceWith(dynamicForm.params.studySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.domainWarning, html, '');
                        dynamicForm._replaceWith(dynamicForm.params.motivationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.hobbySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.nbBuddiesWantedField, html);
                        dynamicForm._replaceWith(dynamicForm.params.bonusOption, html);

                        dynamicForm.initInstituteSelector();
                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });
            });
        }
    }

    initEmailField() {
        const $emailField = $(this.params.emailField);
        if ($emailField.length && $(this.params.domainWarning).length) {
            let emailTimeout;

            $emailField.off('input');
            $emailField.on('input', () => {
                clearTimeout(emailTimeout);
                emailTimeout = setTimeout(() => {
                    const dynamicForm = this;
                    const $form = $emailField.closest('form');
                    const data = dynamicForm._getData($form);

                    $.ajax({
                        url: $form.attr('action'),
                        type: $form.attr('method'),
                        data,
                        success(html) {
                            dynamicForm._replaceWith(dynamicForm.params.domainWarning, html, '');
                        }
                    });
                }, 1000);
            });
        }
    }

    initLevelOfStudy() {
        const $levelOfStudySelector = $(this.params.levelOfStudySelector);
        if ($levelOfStudySelector.length) {
            const $levelOfStudyOtherField = $(this.params.levelOfStudyOther);

            $levelOfStudySelector.on('change', () => {
                if ('other' === $levelOfStudySelector.val()) {
                    $levelOfStudyOtherField.closest('.form-group').removeClass('d-none');
                } else {
                    $levelOfStudyOtherField.closest('.form-group').addClass('d-none');
                }
            });
        }
    }

    initFullYearOnly() {
        const $fullYearOnlySelector = $(this.params.fullYearOnly);
        if ($fullYearOnlySelector.length) {
            $fullYearOnlySelector.off('change');
            $fullYearOnlySelector.on('change', () => {
                loader.start();
                const dynamicForm = this;
                const $form = $fullYearOnlySelector.closest('form');
                const data = dynamicForm._getData($form);

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.fullYearOnly, html, dynamicForm.params.dynamicFormGroup);
                        dynamicForm.initFullYearOnly();
                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });
            });
        }
    }

    initExtractType() {
        const $extractType = $(this.params.extractType);
        if ($extractType.length) {
            $extractType.off('change');
            $extractType.on('change', () => {
                loader.start();
                const dynamicForm = this;
                const $form = $extractType.closest('form');
                const data = dynamicForm._getData($form);

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.extractType, html, dynamicForm.params.dynamicFormGroup);
                        dynamicForm.initExtractType();
                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });
            });
        }
    }

    initIsLocal() {
        const $isLocalSelector = $(this.params.isLocalSelector);
        if ($isLocalSelector.length) {
            $isLocalSelector.off('change');
            $isLocalSelector.on('change', () => {
                loader.start();
                const dynamicForm = this;
                const $form = $isLocalSelector.closest('form');
                const data = dynamicForm._getData($form);

                $.ajax({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data,
                    success(html) {
                        dynamicForm._replaceWith(dynamicForm.params.typeOfMobilitySelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.motivationSelector, html);
                        dynamicForm._replaceWith(dynamicForm.params.bonusOption, html);
                        dynamicForm._replaceWith(dynamicForm.params.domainWarning, html, '');

                        tools.initSelect2();
                        tools.initTooltip();
                        loader.stop();
                    }
                });

                const $departure = $(this.params.departure);
                if ('1' === $isLocalSelector.find(':checked').val()) {
                    $departure.closest('.form-group').addClass('d-none');
                } else {
                    $departure.closest('.form-group').removeClass('d-none');
                }
            });
        }
    }

    _getData($form) {
        const data = {};

        $form.find('select, input, textarea').each((i, elem) => {
            const $elem = $(elem);
            const name = $elem.attr('name');
            const type = $elem.attr('type');

            if (name !== undefined && name.indexOf($form.attr('name')) > -1 && name.indexOf('_token') < 0) {
                if ('hidden' !== type) {
                    if ('checkbox' === type) {
                        data[name] = $elem.is(':checked') ? 1 : 0;
                    } else if ('radio' === type) {
                        if ($elem.is(':checked')) {
                            data[name] = $(elem).val();
                        }
                    } else {
                        data[name] = $(elem).val();
                    }
                }
            }
        });

        return data;
    }

    _replaceWith(elem, html, parentElem = '.form-group') {
        let $elem = $(elem);
        let $value = $(html).find(elem);

        if (0 < parentElem.length) {
            $elem = $elem.closest(parentElem);
            $value = $value.closest(parentElem);
        }

        $value.find('.is-invalid').removeClass('is-invalid');
        $value.find('.form-error').hide();

        $elem.replaceWith($value);
    }
}

export default new DynamicForm();
