import 'bootstrap-slider/dist/bootstrap-slider.min';

class RangeSlider {
    static initSliders() {
        let params = {
            tooltip: 'always',
            focus: true
        };

        $('.range-slider').slider(params);

        params = $.merge(params, {
            ticks: [0, 10],
            ticks_positions: [0, 100]
        });

        $('.range-slider__max-value').slider($.extend({
            formatter(value) {
                if (1 === value) {
                    return Translator.trans('organization.criteria_weight.disabled');
                }

                return value;
            }
        }, params));

        $('.range-slider__matching-weight').slider($.extend({
            formatter(value) {
                switch (value) {
                case 0:
                    return Translator.trans('organization.criteria_weight.disabled');
                case 10:
                    return Translator.trans('organization.criteria_weight.mandatory');
                default:
                    return value;
                }
            }
        }, params));
    }
}

export default RangeSlider;
