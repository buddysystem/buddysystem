import 'select2/dist/js/select2.full.min';
import bsCustomFileInput from 'bs-custom-file-input';
import pdfMake from 'pdfmake/build/pdfmake.min';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import JSZip from 'jszip/dist/jszip.min';
import 'moment/min/locales';
import 'datatables.net/js/jquery.dataTables.min';
import 'datatables.net-plugins/sorting/datetime-moment';
import 'datatables.net-plugins/dataRender/ellipsis';
import 'datatables.net-buttons/js/dataTables.buttons.min';
import 'datatables.net-buttons/js/buttons.html5.min';
import 'datatables.net-buttons/js/buttons.flash.min';
import 'datatables.net-buttons/js/buttons.print.min';
import 'datatables.net-buttons/js/buttons.colVis.min';
import 'symfony-collection/jquery.collection';
import './Form';
import 'select2/dist/css/select2.min.css';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
window.JSZip = JSZip;

class tools {
    static initTooltip() {
        $('[data-toggle="tooltip"]').tooltip({ container: 'body' });
    }

    static fillPopup(title, content, options, cancelCallback) {
        if ('undefined' === typeof options || !options) {
            options = {};
        }

        const $popup = ('undefined' !== typeof (options.popup) && '' !== options.popup) ? options.popup : $('#popup');
        const $title = $popup.find('.modal-title');
        const $body = $popup.find('.modal-body');

        $title.html(title);
        $body.html(content);

        if ('undefined' !== typeof (options.class) && '' !== options.class) {
            $popup.addClass(options.class);

            $popup.on('hidden.bs.modal.customClass', () => {
                $popup.removeClass(options.class);
                $popup.off('hidden.bs.modal.customClass');
            });
        }

        if (cancelCallback !== undefined) {
            $popup.on('hidden.bs.modal', () => {
                cancelCallback();
            });
        }

        $popup.modal(options);
        $(window).trigger('new_element', $popup);
    }

    static initSubmitForm(elem, route, params, callback, onSubmit) {
        elem.submit(function (e) {
            e.preventDefault();

            if (this.checkValidity) {
                const formData = $(this).serialize();
                $(this).saveForm({ toFade: $(this) });

                if (onSubmit === 'function') {
                    onSubmit();
                } else {
                    loader.start();
                }

                $.ajax({
                    type: 'POST',
                    url: Routing.generate(route, params),
                    data: formData,
                    success(data) {
                        callback(data);
                    },
                    error() {
                        Flashbag.show('Error. Please try again.', 'error');
                    },
                    complete() {
                        if (onSubmit === 'function') {
                            loader.stop();
                        }
                    }
                });
            } else {
                $(this).find('button[type=submit]').trigger('click');
            }
        });
    }

    static initPopupOnClick(elem, route, params, title, options, onShow) {
        elem.off('click');
        elem.on('click', function (e) {
            e.preventDefault();

            if (params.id === undefined) {
                let id = $(this).data('id');
                if (id === undefined) {
                    id = $(this).closest('tr').data('id');
                }

                if (id !== undefined) {
                    params.id = id;
                }
            }

            loader.start();
            $.ajax({
                url: Routing.generate(route, params),
                success(data) {
                    tools.fillPopup(title, data, options, undefined);
                    tools.initTooltip();

                    if (onShow === undefined) {
                        loader.stop();
                    } else {
                        onShow();
                    }
                },
                error() {
                    loader.stop();
                }
            });
        });
    }

    static initAjaxCheckboxes(elem, route, options, callback) {
        elem.off('click');
        elem.on('click', function () {
            const checkbox = $(this);
            const params = {};

            if ('undefined' !== typeof options) {
                $.each(options, (index, value) => {
                    params[index] = checkbox.closest(value).data('id');
                });
            }

            if (!Object.prototype.hasOwnProperty.call(params, 'id')) {
                params.id = $(this).data('id');
                if (params.id === undefined) {
                    params.id = $(this).closest('tr').data('id');
                }
            }

            if (Object.prototype.hasOwnProperty.call(params, 'id')) {
                loader.start();
                $.ajax({
                    url: Routing.generate(route, params),
                    type: 'POST',
                    success(data) {
                        if ('error' === data.labelMessage) {
                            checkbox.prop('checked', checkbox.is(':checked') ? '' : 'checked');
                        }

                        if (callback !== undefined) {
                            callback(data);
                        } else {
                            loader.stop();
                            Flashbag.show(data.message, data.labelMessage);
                        }
                    },
                    fail() {
                        loader.stop();
                        Flashbag.show('Error. Please try again.', 'error');
                    }
                });
            }
        });
    }

    static initDatatable(elem, options, title) {
        const params = {
            scrollX: true,
            scrollY: 400,
            scrollCollapse: true,
            paging: true,
            bInfo: false,
            responsive: true,
            bSortCellsTop: true,
            dom: 'Blrtip',
            lengthMenu: [[5, 10, 25, 50, 100], [5, 10, 25, 50, 100]],
            iDisplayLength: 10,
            stateSave: true,
            buttons: [
                {
                    extend: 'colvis',
                    className: 'btn btn-sm btn-outline-dark mt-2 mt-sm-0',
                    init(api, node) {
                        $(node).removeClass('dt-button');
                    }
                },
                {
                    name: 'clear_filters',
                    text: Translator.trans('list.button.clear_filters'),
                    attr: {
                        class: 'btn btn-sm btn-outline-dark mt-2 mt-sm-0'
                    },
                    action(e, dt) {
                        dt.state.clear();
                        window.location.reload();
                    }
                },
                {
                    extend: 'csvHtml5',
                    footer: false,
                    exportOptions: {
                        columns: 'th:not(.no-export)'
                    },
                    title,
                    attr: {
                        class: 'btn btn-sm btn-outline-dark mt-2 mt-sm-0 ml-5'
                    }
                },
                {
                    extend: 'excelHtml5',
                    footer: false,
                    exportOptions: {
                        columns: 'th:not(.no-export)'
                    },
                    title,
                    attr: {
                        class: 'btn btn-sm btn-outline-dark mt-2 mt-sm-0'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    footer: false,
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: 'th:not(.no-export)'
                    },
                    title,
                    attr: {
                        class: 'btn btn-sm btn-outline-dark mt-2 mt-sm-0'
                    }
                }
            ],
            language: {
                buttons: {
                    colvis: Translator.trans('list.button.visibility')
                }
            }
        };

        $.fn.dataTable.moment('MM/DD/YYYY');
        $.fn.dataTable.moment('DD/MM/YYYY');

        const datatable = elem.DataTable($.extend(params, options));
        const $filters = $(elem.closest('.dataTables_scroll').find('.dataTables_scrollHead #column-filters th'));

        if ($filters.length) {
            const state = datatable.state.loaded();
            if (state) {
                $filters.each(function (i) {
                    const colSearch = state.columns[i].search;

                    if (colSearch.search) {
                        $(this).find('input, select').val(colSearch.search);
                    }
                });
            }

            $filters.each(function (i) {
                $('input, select', this).on('keyup change', function () {
                    if (datatable.column(i).search() !== $(this).val()) {
                        datatable
                            .column(i)
                            .search($(this).val())
                            .draw();
                    }
                });
            });
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab', () => {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

        elem.on('processing.dt', (e, settings, processing) => {
            if (processing) {
                loader.start();
            }
        });

        tools.initSelect2();

        return datatable;
    }

    static initSelect2(elem) {
        if ('undefined' === typeof elem) {
            elem = $('.select2selector');
        }

        elem.select2({
            dropdownAutoWidth: true,
            theme: 'classic'
        });
    }

    static initCollectionType(elem, options) {
        elem.collection($.extend({
            min: 1,
            init_with_n_elements: 1,
            allow_up: false,
            allow_down: false,
            custom_add_location: true
        }, options));
    }

    static initCollapseAccordion() {
        $('.collapse.show').each(function () {
            $(this).prev('.accordion_button').find('.fa').addClass('fa-angle-up')
                .removeClass('fa-angle-down');
        });

        $('.collapse').on('show.bs.collapse', function () {
            $(this).prev('.accordion_button').find('.fa').removeClass('fa-angle-down')
                .addClass('fa-angle-up');
        }).on('hide.bs.collapse', function () {
            $(this).prev('.accordion_button').find('.fa').removeClass('fa-angle-up')
                .addClass('fa-angle-down');
        });
    }

    static initFileInput() {
        bsCustomFileInput.init();
    }

    static filePreview(input, divPreview) {
        if (input.files && input.files[0]) {
            const reader = new FileReader();
            reader.onload = function (e) {
                divPreview.removeClass('empty-file-preview');
                divPreview.html(`<embed src="${e.target.result}" class="file-preview"/>`);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
}

export default tools;
