class InstituteFinder {
    __initNotFoundForm() {
        tools.initPopupOnClick(
            $('#institute-finder_not-found'),
            'new_institute_request',
            {},
            Translator.trans('institute_finder.not_found.title'),
            {},
            () => {
                loader.stop();

                const $citySelectors = $('#institute_request_city');
                const $cityOther = $('#institute_request_cityOther');

                $('#institute_request_country').on('change', function () {
                    const countryId = $(this).find('option:selected').val();
                    $citySelectors.find(`option[data_country_id!='${countryId}']`).hide();
                    $citySelectors.find(`option[data_country_id='${countryId}']`).show();
                    $citySelectors.find('option[value=""]').show();
                });

                $citySelectors.on('change', () => {
                    if ('' === $citySelectors.find('option:selected').val()) {
                        $cityOther.show();
                    } else {
                        $cityOther.hide();
                    }
                });

                tools.initSubmitForm(
                    $('form[name=\'institute_request\']'),
                    'new_institute_request',
                    {},
                    () => { window.location.reload(); }
                );
            }
        );
    }

    init() {
        tools.initPopupOnClick(
            $('.institute-finder__link'),
            'institutes_finder',
            {},
            Translator.trans('institute.list.title'),
            {},
            () => {
                const table = $('#institute-finder__table');
                const columns = [
                    { data: 'name' },
                    { data: 'country', orderable: false },
                    { data: 'city', orderable: false },
                    { data: 'logo', orderable: false },
                    { data: 'treeview', orderable: false }
                ];

                tools.initDatatable(table, {
                    processing: loader.start(),
                    serverSide: true,
                    ajax: Routing.generate('institute_finder_list'),
                    columns,
                    buttons: []
                });

                table.on('draw.dt', () => {
                    loader.stop();
                });

                this.__initNotFoundForm();
            }
        );
    }
}

export default new InstituteFinder();
