$(() => {
    const table = $('#cities');

    const columns = [
        { data: 'name' },
        { data: 'country' },
        { data: 'actions', orderable: false }
    ];

    tools.initDatatable(table, {
        order: [[0, 'asc']],
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('city_list'),
        columns
    }, 'BuddySystem - Cities List');

    table.on('draw.dt', () => {
        tools.initPopupOnClick(
            $('.city_delete'),
            'city_delete',
            {},
            Translator.trans('city.popup.delete', {}, 'admin')
        );
        loader.stop();
    });
});
