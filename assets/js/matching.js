$(() => {
    function _getColumns(dataTable) {
        let columns = [];

        if ('matching_locals' === dataTable.attr('id')) {
            columns.push({ data: 'score' });
        }

        columns = $.merge(columns, [
            { data: 'firstname' },
            { data: 'lastname' },
            { data: 'arrival' },
            { data: 'createdAt' },
            { data: 'semester' }
        ]);

        if (1 === dataTable.data('admin')) {
            columns.push({ data: 'association', orderable: false });
        }

        columns = $.merge(columns, [
            { data: 'city' },
            { data: 'institute' },
            { data: 'studies', orderable: false },
            { data: 'levelOfStudy', orderable: false }
        ]);

        if ('matching_internationals' === dataTable.attr('id')) {
            columns.push({ data: 'typeOfMobility' });
        }

        columns = $.merge(columns, [
            { data: 'languages', orderable: false },
            { data: 'age', orderable: false },
            { data: 'sex' },
            { data: 'sexWanted' },
            { data: 'languagesWanted', orderable: false },
            { data: 'motivation' },
            { data: 'nbBuddies' },
            { data: 'nbBuddiesWanted' }
        ]);

        if ('matching_locals' === dataTable.attr('id')) {
            columns = $.merge(columns, [
                { data: 'nbBuddiesRefused' },
                { data: 'nbWarned' },
                { data: 'nbBuddiesRemoved' },
                { data: 'nbReport' }
            ]);
        }

        return columns;
    }

    function step1InitClick() {
        $('#matching_internationals tbody tr').on('click', function (e) {
            if (e.target.tagName === 'A') return;

            const id = $(this).data('id');

            if (id !== undefined) {
                window.location = Routing.generate('matching_step2', { id });
            }
        });
    }

    function step1() {
        const dataTable = $('#matching_internationals');

        if (dataTable.length) {
            tools.initDatatable(dataTable, {
                order: [[3, 'asc']],
                processing: loader.start(),
                serverSide: true,
                ajax: Routing.generate('mentees_list'),
                columns: _getColumns(dataTable)
            }, 'BuddySystem - Matching Mentees List');

            dataTable.on('draw.dt', () => {
                step1InitClick();
                tools.initCollapseAccordion();
                loader.stop();
            });
        }
    }

    function step2InitClick() {
        $('#matching_locals tbody tr').on('click', function (e) {
            if (e.target.tagName === 'A') return;

            const internationalId = $(this).closest('.table').data('id');
            const localId = $(this).data('id');

            if (internationalId !== undefined && localId !== undefined) {
                window.location = Routing.generate('matching_step3', {
                    internationalId,
                    localId
                });
            }
        });
    }

    function step2() {
        const dataTable = $('#matching_locals');
        const menteeTable = $('#selected_international');

        if (dataTable.length) {
            tools.initDatatable(dataTable, {
                order: [[0, 'desc']],
                processing: loader.start(),
                serverSide: true,
                ajax: Routing.generate('mentors_list', { id: dataTable.data('id') }),
                columns: _getColumns(dataTable)
            }, 'BuddySystem - Matching Mentors List');

            dataTable.on('draw.dt', () => {
                step2InitClick();
                tools.initCollapseAccordion();
                loader.stop();
            });
        }

        if (menteeTable.length) {
            tools.initDatatable(menteeTable, {
                paging: false, bSort: false, bFilter: false, dom: 'ltipr'
            });
        }
    }

    step1();
    step2();
});
