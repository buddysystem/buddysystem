import DynamicForm from './tools/DynamicForm';

$(() => {
    DynamicForm.init({
        extractType: '#extract_type',
    });

    tools.initTooltip();
    tools.initSelect2();
});
