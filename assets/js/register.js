import DynamicForm from './tools/DynamicForm';
import InstituteFinder from './tools/InstituteFinder';

$(() => {
    DynamicForm.init({
        countrySelector: '#registration_country',
        citySelector: '#registration_city',
        instituteSelector: 'registration_institute',
        associationSelector: '#registration_association',
        semesterSelector: '#registration_semester',
        studySelector: '#registration_studies',
        emailField: '#registration_email',
        motivationSelector: '#registration_motivation',
        isLocalSelector: '#registration_isLocal',
        typeOfMobilitySelector: '#registration_typeOfMobility',
        levelOfStudySelector: '#registration_levelOfStudy',
        levelOfStudyOther: '#registration_levelOfStudyOther',
        nbBuddiesWantedField: '#registration_nbBuddiesWanted',
        bonusOption: '#registration_bonusOption'
    });

    tools.initTooltip();
    tools.initSelect2();
    InstituteFinder.init();
});
