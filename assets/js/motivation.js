$(() => {
    tools.initDatatable($('#motivations'), { paging: false, dom: 'lftipr' });
    tools.initAjaxCheckboxes($('.toggle_checkbox'), 'toggle_motivation_manager');
    tools.initPopupOnClick(
        $('.motivation_delete'),
        'motivation_delete',
        {},
        Translator.trans('motivation.popup.delete', {}, 'admin')
    );
});
