import '../sass/components/_cookie_consent.scss'
import 'cookieconsent';

$(() => {
    $('#login_button').on('click', () => {
        loader.start();
        $.ajax({
            url: Routing.generate('login'),
            success(data) {
                tools.fillPopup(Translator.trans('login.title'), data);
            },
            complete() {
                loader.stop();
            }
        });
    });

    window.cookieconsent.initialise({
        palette: {
            popup: {
                background: '#ad004f',
                text: 'white'
            }
        },
        content: {
            message: Translator.trans('cookie_policy.banner.message'),
            dismiss: Translator.trans('cookie_policy.banner.dismiss'),
            link: Translator.trans('cookie_policy.banner.link'),
            href: Routing.generate('cookiePolicy')
        }
    });
});
