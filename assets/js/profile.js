import DynamicForm from './tools/DynamicForm';
import InstituteFinder from './tools/InstituteFinder';

$(() => {
    const $warning = $('#warning');
    const sex = '#user_sex';
    const institute = '#user_institute';
    const sexWanted = '#user_sexWanted';
    const semester = '#user_semester';
    const isLocal = '#user_isLocal';

    function _manageWarning() {
        if ($(`${isLocal} :checked`).val() !== String($(isLocal).data('value')) ||
            $(`${sex} :selected`).val() !== String($(sex).data('value')) ||
            $(`${institute} :selected`).val() !== String($(institute).data('value')) ||
            $(sexWanted).prop('checked') !== $(sexWanted).data('value') ||
            $(`${semester} :selected`).val() !== String($(semester).data('value'))) {
            $warning.html(Translator.trans('profile.warning.remove_buddies'));
        } else {
            $warning.html('');
        }
    }

    tools.initFileInput();
    InstituteFinder.init();
    tools.initPopupOnClick($('#user_delete'), 'user_delete', {}, Translator.trans('user.popup.delete'));
    tools.initPopupOnClick(
        $('#user_edit_password'),
        'user_edit_password',
        {},
        Translator.trans('user.popup.edit_password')
    );

    if (0 < $warning.data('nb_buddies')) {
        $(`${sex}, ${institute}, ${sexWanted}, ${semester}, ${isLocal}`).on('change', () => {
            _manageWarning();
        });
    }

    DynamicForm.init({
        countrySelector: '#user_country',
        citySelector: '#user_city',
        instituteSelector: 'user_institute',
        associationSelector: '#user_association',
        semesterSelector: semester,
        studySelector: '#user_studies',
        motivationSelector: '#user_motivation',
        hobbySelector: '#user_hobbies',
        isLocalSelector: isLocal,
        typeOfMobilitySelector: '#user_typeOfMobility',
        levelOfStudySelector: '#user_levelOfStudy',
        levelOfStudyOther: '#user_levelOfStudyOther',
        nbBuddiesWantedField: '#user_nbBuddiesWanted',
        departure: '#user_departure',
        emailField: '#user_email',
        bonusOption: '#user_bonusOption'
    });
});
