import $ from 'jquery';
import tools from './tools/tools';
import Flashbag from './tools/Flashbag';
import loader from './tools/Loader';
import * as Const from './tools/Const';
import UserReport from './tools/UserReport';
import Routing from './tools/Routing';
import './tools/errors';
import 'bootstrap-v4-rtl';
import 'popper.js/dist/umd/popper';
import '../sass/main.scss';

global.$ = $;
global.jQuery = $;
global.Const = Const;
global.Flashbag = Flashbag;
global.tools = tools;
global.UserReport = UserReport;
global.Routing = Routing;
global.loader = loader;

$(() => {
    tools.initTooltip();
    tools.initSelect2();

    $('#languageswitcher li a').on('click', () => {
        loader.start();
    });
});
