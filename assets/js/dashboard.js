$(() => {
    tools.initPopupOnClick(
        $('.refusal_button'),
        'buddy_reason_refusal',
        {},
        Translator.trans('dashboard.matching.refusal.title')
    );
    tools.initPopupOnClick(
        $('.report-buddy_button'),
        'buddy_report',
        {'reportType': 'report'},
        Translator.trans('dashboard.buddy.report.title')
    );
    tools.initPopupOnClick(
        $('.no-reply-buddy_button'),
        'buddy_report',
        {'reportType': 'no_reply'},
        Translator.trans('dashboard.buddy.no_reply.title')
    );
    tools.initPopupOnClick(
        $('.account-archived'),
        'unarchived_banner',
        {},
        Translator.trans('dashboard.profile.archived')
    );

    $('#popup').on('shown.bs.modal', () => {
        const $reportForm = $('form[name=\'report\']');
        const $refusalForm = $('form[name=\'refusal_buddy\']');

        if ($reportForm.length) {
            tools.initSubmitForm($reportForm, 'buddy_report', { id: $reportForm.data('id'), reportType: $reportForm.data('type') }, () => {
                window.location.reload();
            });
        }

        if ($refusalForm.length) {
            tools.initSubmitForm($refusalForm, 'buddy_reason_refusal', { id: $refusalForm.data('id') }, () => {
                window.location.reload();
            });

            const $refusalReason = $refusalForm.find('#refusal_buddy_reasonRefusal');
            const $refusalReasonOther = $refusalForm.find('#refusal_buddy_reasonRefusalOther');
            $refusalReason.on('change', () => {
                if ($refusalReason.find(":selected").val() === 'other') {
                    $refusalReasonOther.parent('div').removeClass('d-none');
                } else {
                    $refusalReasonOther.parent('div').addClass('d-none');
                }
            });
        }
    });
});
