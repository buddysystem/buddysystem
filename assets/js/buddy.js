$(() => {
    const table = $('#buddies');
    let columns = [{ data: 'createdAt' }];
    let searchCols = [null];

    if (1 === table.data('admin')) {
        columns.push(
            { data: 'association', orderable: false },
            { data: 'institute', orderable: false }
        );
        searchCols.push(null);
        searchCols.push(null);
    }

    columns = $.merge(columns, [
        { data: 'mentor', orderable: false },
        { data: 'mentorEmail', orderable: false },
        { data: 'mentorInstitute', orderable: false },
        { data: 'mentee', orderable: false },
        { data: 'menteeEmail', orderable: false },
        { data: 'menteeInstitute', orderable: false },
        { data: 'semester', orderable: false },
        { data: 'year', orderable: false },
        { data: 'languagePairing' },
        { data: 'status' },
        { data: 'reasonRefusal' },
        { data: 'lastMessageAt', orderable: false },
        { data: 'warnedAt' },
        { data: 'responseAt' },
        { data: 'archived' },
        { data: 'actions', orderable: false }
    ]);

    searchCols = $.merge(searchCols, [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        { search: '0' },
        null
    ]);

    tools.initDatatable(table, {
        order: [[0, 'desc']],
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('buddy_list'),
        columns,
        searchCols
    }, 'BuddySystem - Buddies List');

    table.on('draw.dt', () => {
        tools.initPopupOnClick(
            $('.buddy_delete'),
            'buddy_delete',
            {},
            Translator.trans('buddy.popup.delete')
        );
        tools.initPopupOnClick(
            $('.buddy_reports'),
            'buddy_reports',
            {},
            Translator.trans('buddy.popup.report.list.title')
        );
        loader.stop();
    });

    $('#popup').on('shown.bs.modal', () => {
        const $reportList = $('#list_report_buddy');

        if ($reportList.length) {
            tools.initPopupOnClick($('.report_delete'), 'report_delete', {}, Translator.trans('report.popup.delete'));
            tools.initDatatable($reportList, { paging: false, bFilter: false });
        }
    });
});
