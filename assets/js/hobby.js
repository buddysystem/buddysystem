$(() => {
    tools.initDatatable($('#hobbies'), { paging: false, dom: 'lftipr' });
    tools.initAjaxCheckboxes($('.toggle_checkbox'), 'toggle_hobby_manager');
    tools.initPopupOnClick($('.hobby_delete'), 'hobby_delete', {}, Translator.trans('hobby.popup.delete', {}, 'admin'));
});
