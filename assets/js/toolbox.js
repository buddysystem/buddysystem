$(() => {
    tools.initSelect2();
    tools.initFileInput();
    $('#document_documentFile_file').on('change', function () {
        tools.filePreview(this, $('#document-preview'));
    });

    tools.initPopupOnClick(
        $('.edit_button'),
        'toolbox_document_edit',
        {},
        Translator.trans('actions.edit')
    );

    tools.initPopupOnClick(
        $('.delete_button'),
        'toolbox_document_delete',
        {},
        Translator.trans('actions.delete')
    );

    $('#popup').on('shown.bs.modal', () => {
        tools.initSelect2();

        const $editForm = $('#document_edit');

        if ($editForm.length) {
            tools.initSubmitForm($editForm, 'toolbox_document_edit', { id: $editForm.data('id') }, () => {
                window.location.reload();
            });
        }
    });
});
