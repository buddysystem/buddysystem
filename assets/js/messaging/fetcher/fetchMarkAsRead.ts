import Routing from '../../tools/Routing';
import fetcher from './fetcher';

export default (id: string): Promise<null> => {
    const markAsReadUrl = Routing.generate('messaging_api_put_threads_groups_mark_as_read', { id });

    return fetcher(markAsReadUrl, 'POST');
};
