import Routing from '../../tools/Routing';
import fetcher from './fetcher';
import User from '../type/User';

export default (search: string): Promise<User[]> => {
    const searchUrl = Routing.generate('user_suggest', { search });

    return fetcher(searchUrl, 'GET');
};
