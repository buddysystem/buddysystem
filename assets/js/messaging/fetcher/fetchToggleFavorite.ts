import Routing from '../../tools/Routing';
import fetcher from './fetcher';

export default (id: string): Promise<null> => {
    const toggleFavoriteUrl = Routing.generate('messaging_api_put_threads_groups_toggle_favorite', { id });

    return fetcher(toggleFavoriteUrl, 'POST');
};
