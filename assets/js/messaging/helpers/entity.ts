import { find } from 'lodash';

export const findById = <T>(list: T[], id: string): T => find(list, { '@id': id });
export const getJoinedIds = <T>(list: T[]): string => list.map((e) => e['@id']).join(',');
