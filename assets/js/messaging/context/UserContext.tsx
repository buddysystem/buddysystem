import React, { ReactNode, useContext, useState } from 'react';

const UserContext = React.createContext({});

type Context = {
    id: string;
    isManager: boolean;
    iri: string;
    newThread: string[];
    recipientClear: boolean;
    setRecipientClear(boolean): void;
};

type Props = {
    id: string;
    isManager: boolean;
    iri: string;
    newThread: string[];
    children?: ReactNode;
};

export const UserProvider = ({id, isManager, iri, newThread, children }: Props) => {
    const [recipientClear, setRecipientClear] = useState<boolean>(false);

    const value: Context = {
        id,
        isManager,
        iri,
        newThread,
        recipientClear,
        setRecipientClear
    };

    return (<UserContext.Provider value={value}>{children}</UserContext.Provider>);
};
export const useUserContext = (): Context => useContext(UserContext);
