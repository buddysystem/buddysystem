import { useState, useEffect } from 'react';
import fetcher from '../fetcher/fetchMessages';
import Thread from '../type/Thread';
import Message from '../type/Message';

const useMessagesQuery = (thread: Thread, callback) => {
    const [data, setData] = useState<Message[]>([]);
    const [page, setPage] = useState<number>(1);
    const [hasNext, setHasNext] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<Error>();

    useEffect(
        () => setPage(1),
        [thread['@id']]
    );

    useEffect(
        () => {
            if (1 === page) setData([]);

            setLoading(true);

            const abortController = new AbortController();
            fetcher(thread['@id'], page, abortController)
                .then((response) => {
                    if (0 === response.length) {
                        setHasNext(false);
                    } else if (1 === page) {
                        setData(response);
                    } else {
                        setData(response.concat(data));
                    }

                    callback();
                })
                .catch((e) => setError(e))
                .finally(() => setLoading(false));

            return () => abortController?.abort();
        },
        [thread['@id'], page]
    );

    return {
        data, loading, error, page, hasNext, loadNext: () => setPage(page + 1), setData
    };
};

export default useMessagesQuery;
