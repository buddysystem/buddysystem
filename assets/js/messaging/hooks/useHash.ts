import { useCallback, useState } from 'react';

export const enum Hash {
    NEW_THREAD = 'new-thread',
}

const getHash = (): string => window.location.hash.substr(1);

const useHash = () => {
    const [state, setState] = useState<string>(getHash());

    window.onhashchange = useCallback(
        () => { setState(getHash()); },
        [window]
    );

    const setHash = (hash: string): void => {
        setState(hash);
        window.location.hash = hash;
    };

    return { hash: state, setHash };
};

export default useHash;
