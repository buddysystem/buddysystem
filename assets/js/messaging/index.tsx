import 'regenerator-runtime/runtime';
import 'core-js/stable';
import React from 'react';
import ReactDOM from 'react-dom';
import { ThreadProvider } from './context/ThreadContext';
import { MercureProvider } from './context/MercureContext';
import { InboxProvider } from './context/InboxContext';
import { UnreadProvider } from './context/UnreadContext';
import { UserProvider } from './context/UserContext';
import Inbox from './components/Inbox/Inbox';
import Thread from './components/Thread/Thread';

import '../../sass/pages/inbox.scss';

type Props = {
    user: string;
    newThread: string;
};

const App = ({ user, newThread }: Props) => (
    <UserProvider {...JSON.parse(user)} newThread={...JSON.parse(newThread) || []}>
        <ThreadProvider>
            <MercureProvider>
                <InboxProvider>
                    <UnreadProvider>
                        <Inbox />
                        <Thread />
                    </UnreadProvider>
                </InboxProvider>
            </MercureProvider>
        </ThreadProvider>
    </UserProvider>
);

const root = document.getElementById('messaging-app');

ReactDOM.render(<App user={root.dataset.user} newThread={root.dataset.new_thread} />, root);
