import React from 'react';
import moment from 'moment';
import Translator from '../../../tools/Translator';
import Message from '../../type/Message';
import useTooltip, { Placement } from '../../hooks/useTooltip';
import Avatar from '../Common/Avatar';
import MessageTags from './MessageTags';

type Props = {
    message: Message;
}

const setInnerHtml = (msg: string) => ({ __html: msg });

const MessageReceived = ({ message }: Props) => {
    const { ref } = useTooltip(Placement.RIGHT);

    return (
        <div className="message message--incoming">
            <div className="message__img">
                <Avatar image={message.author.avatar} title={`${message.author.firstname} ${message.author.lastname}`}/>
            </div>
            <div className="message__received">
                <div ref={ref} title={moment(message.datetime).locale(Translator.locale).format('LTS')}>
                    <p dangerouslySetInnerHTML={setInnerHtml(message.body)} />
                </div>
            </div>
            <MessageTags message={message}/>
        </div>
    );
};

export default MessageReceived;
