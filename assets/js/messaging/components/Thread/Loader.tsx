import React from 'react';

const Loader = () => <div className="small-loader" style={{ display: 'block', opacity: 0.5 }} />;

export default Loader;
