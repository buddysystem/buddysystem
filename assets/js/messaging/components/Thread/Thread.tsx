import React from 'react';
import { useInboxContext, Mode } from '../../context/InboxContext';
import NewThread from './NewThread';
import CurrentThread from './CurrentThread';

const Thread = () => {
    const { mode, current } = useInboxContext();

    if (mode === Mode.CREATE) {
        return <NewThread />;
    }

    if (mode === Mode.VIEW && current) {
        return <CurrentThread thread={current} />;
    }

    return null;
};

export default Thread;
