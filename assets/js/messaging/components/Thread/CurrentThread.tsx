import React, { useCallback, useState } from 'react';
import { Formik, FormikHelpers } from 'formik';
import Translator from '../../../tools/Translator';
import ThreadMessages from './ThreadMessages';
import fetcher from '../../fetcher/fetchPostMessage';
import Thread from '../../type/Thread';
import useTooltip, { Placement } from '../../hooks/useTooltip';
import CurrentForm from './Form/CurrentForm';

type Props = {
    thread: Thread;
};

type Values = {
    content: string;
    threadId: string;
};

const getThreadName = (thread: Thread) => {
    if (thread.profileUrl) {
        return <p className="thread__user pt-2" onClick={() => window.location.href = thread.profileUrl}>{thread.name}</p>;
    }
    return <p className="thread__user pt-2">{thread.name}</p>;
};

const CurrentThread = ({ thread }: Props) => {
    const [error, setError] = useState<Error>();
    const handleSubmit = useCallback(
        ({ content, threadId }: Values, { setSubmitting, setFieldValue, setErrors }: FormikHelpers<Values>) => {
            if (content && threadId) {
                fetcher(threadId, content)
                    .then(() => setFieldValue('content', ''))
                    .then(() => setErrors({}))
                    .catch((e) => setError(e))
                    .finally(() => setSubmitting(false));
            }
        },
        [thread['@id']]
    );

    const { ref } = useTooltip(Placement.BOTTOM);

    return (
        <div className="current-thread text-left">
            <div className="thread thread--new">
                <div className="thread__header" data-cy="thread-header">
                    {getThreadName(thread)}
                    <span className="thread__type" title={thread.channelTypeTooltip} ref={ref}>
                        { Translator.trans(`inbox.channel.type.${thread.channelType}`) }
                    </span>
                </div>

                { <ThreadMessages thread={thread} /> }

                { thread.canReply && 0 < thread.topics.length &&
                    <div className="thread__form message-form">
                        <Formik
                            initialValues={{ content: '', threadId: '' }}
                            onSubmit={handleSubmit}
                        >
                            {() => <CurrentForm thread={thread} error={error} />}
                        </Formik>
                    </div>
                }
            </div>
        </div>
    );
};

export default CurrentThread;
