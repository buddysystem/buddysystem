import React, { useCallback, useEffect } from 'react';
import { debounce, noop, without } from 'lodash';
import moment from 'moment';
import Translator from '../../../tools/Translator';
import { MARK_AS_READ_DELAY } from '../../constant';
import { useInboxContext, ThreadType } from '../../context/InboxContext';
import { useUnreadContext } from '../../context/UnreadContext';
import Thread from '../../type/Thread';
import Message, {MessageTypeEnum} from '../../type/Message';
import useMessagesQuery from '../../hooks/useMessagesQuery';
import { useMercureContext } from '../../context/MercureContext';
import fetcher from '../../fetcher/fetchMarkAsRead';
import Loader from './Loader';
import MessageComponent from './Message';
import useScroll from '../../hooks/useScroll';
import {useThreadContext} from "../../context/ThreadContext";

type Props = {
    thread: Thread;
};

const displayMessages = (messages: Message[]) => {
    const nodes = [];
    let currentDay = '';

    messages.forEach((message) => {
        const day = moment(message.datetime).locale(Translator.locale).format('LL');

        if (currentDay !== day) {
            nodes.push(<div key={day} className="date-message">{ day }</div>);
            currentDay = day;
        }
        nodes.push(<MessageComponent key={message['@id']} message={message} />);
    });

    return nodes;
};

const ThreadMessages = ({ thread }: Props) => {
    const { isCurrent, current } = useInboxContext();
    const { list } = useThreadContext();
    const { unread, setUnread } = useUnreadContext();
    const { ref, scrollToBottom, scrollToTop } = useScroll();

    // Previous messages
    const {
        loading, data, page, hasNext, loadNext, setData
    } = useMessagesQuery(thread, () => {
        if (1 === page) scrollToBottom();
    });
    const handleScroll = (event) => {
        if (!loading && hasNext && 0 === event.target.scrollTop) {
            loadNext();
            scrollToTop(10);
        }
    };

    // New messages
    const { refresh } = useThreadContext();
    const { mercure, subscribeToThread } = useMercureContext();
    useEffect(
        () => {
            return subscribeToThread(thread, (message) => {
                refresh().catch(noop);

                if (MessageTypeEnum.REFRESH_TOPICS !== message['@type'] && message['thread'] === current['@id']) {
                    setData((m) => [...m, message]);
                }
                scrollToBottom();
            })();
        },
        [mercure, thread]
    );

    // Mark as read MARK_AS_READ_DELAY millisecond after last received message
    const markAsReadCallback = useCallback(
        () => {
            if (!isCurrent(thread)) return undefined;
            const debounced = debounce(() => {
                fetcher(thread['@id']).catch(noop);
                setUnread(without(unread, thread['@id']));
            }, MARK_AS_READ_DELAY);

            debounced();

            return debounced.cancel;
        },
        [thread, unread.join(',')]
    );
    useEffect(markAsReadCallback, [thread, data.length, list]);

    return (
        <div className={"thread__container mt-4 " + (!thread.canReply ? 'thread__container--no-recipient-form' : thread['@id'] === ThreadType.MATCHING_MANAGER ? 'thread__container--recipient-form' : '')} onScroll={handleScroll}>
            <div ref={ref}>
                { loading && <Loader /> }
                { displayMessages(data) }
            </div>
        </div>
    );
};

export default ThreadMessages;
