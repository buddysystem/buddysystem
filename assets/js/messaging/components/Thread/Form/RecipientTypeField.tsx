import React from 'react';
import { Field } from 'formik';
import Translator from '../../../../tools/Translator';

type Props = {
    types: string[];
    recipient?: string;
    onChange:(string) => void;
};

const RecipientTypeField = ({ types, recipient, onChange }: Props) => {
    if (1 === types.length) {
        return (
            <div className="mt-2">
                <p className="thread__header--single-option">{ types[0] }</p>
                <Field type="hidden" name="recipient" value={types[0]} />
            </div>
        );
    }

    return (
        <div className="btn-group btn-group-toggle">
            {types.map((type) => (
                <label key={type} className={"btn btn-outline-secondary" + (type === recipient ? ' active' : '')}>
                    <Field type="radio"
                           name="recipient"
                           value={type}
                           onChange={() => onChange(type)}
                    />
                    { Translator.trans(`inbox.new_thread.manager_user.type.${type}`) }
                </label>
            ))}
        </div>
    );
};

export default RecipientTypeField;
