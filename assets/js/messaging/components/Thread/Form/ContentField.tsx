import React from 'react';
import { Field } from 'formik';
import Translator from '../../../../tools/Translator';

type Props = {
    disabled: boolean;
    disabledSubmit: boolean;
    onEnter: () => void;
};

const ContentField = ({ disabled, disabledSubmit, onEnter }: Props) => (
    <Field
        as="textarea"
        name="content"
        placeholder={Translator.trans('inbox.reply.body')}
        className="message-form__input col-md-11"
        disabled={disabled}
        onKeyDown={(e: KeyboardEvent) => {
            if (!disabledSubmit && 'Enter' === e.key) {
                onEnter();
            }
        }}
    />
);

export default ContentField;
