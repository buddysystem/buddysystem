import React, { useEffect } from 'react';
import {
    Field, Form, useFormikContext
} from 'formik';
import RecipientManagerField from './RecipientManagerField';
import ContentField from './ContentField';
import SubmitField from './SubmitField';
import Thread from '../../../type/Thread';
import {ThreadType} from "../../../context/InboxContext";

type Props = {
    thread: Thread;
    error: Error;
};

type Values = {
    content: string;
    threadId: string;
};

const CurrentForm = ({ thread, error }: Props) => {
    const {
        isSubmitting, values, setFieldValue, submitForm
    } = useFormikContext<Values>();

    useEffect(
        () => {
            if (thread['@id'] !== ThreadType.MATCHING_MANAGER) {
                setFieldValue('threadId', thread.topics[0].threadId);
            }
        },
        [thread['@id']]
    );

    return (
        <Form>
            {thread['@id'] === ThreadType.MATCHING_MANAGER && (
                <div className="row form-group col-md-12 m-1">
                    <Field component={RecipientManagerField} thread={thread} name="threadId"/>
                </div>
            )}
            <div className="row col-12">
                <ContentField disabled={isSubmitting} disabledSubmit={isSubmitting || !values.content} onEnter={() => submitForm()}/>
                <SubmitField disabled={isSubmitting || !values.content} error={error}/>
            </div>
        </Form>
    );
};

export default CurrentForm;
