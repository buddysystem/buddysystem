import React, { useEffect, useState } from 'react';
import { FieldProps } from 'formik';
import Translator from '../../../../tools/Translator';
import Thread, { RecipientTypeEnum } from '../../../type/Thread';

const computeMatchingManagers = (thread: Thread) => {
    const matchingManagers = {};

    thread.topics
        .filter((topic) => null !== topic.matchingManager)
        .forEach((topic) => {
            const { id, label, type } = topic.matchingManager;

            matchingManagers[`${type}_${id}`] = label;
        });

    return matchingManagers;
};

const computeTypes = (thread: Thread, matchingManager: string) => {
    const [type, id] = matchingManager.split('_');
    const types = {};

    thread.topics
        .filter((topic): boolean => (
            null !== topic.matchingManager &&
            type === topic.matchingManager.type &&
            id === topic.matchingManager.id
        ))
        .forEach((topic): void => {
            types[topic.matchingManager.recipientType] = topic.matchingManager.recipientType;
        });

    return types;
};

const getThreadId = (thread: Thread, matchingManager: string, recipientType: string): string => {
    const [type, id] = matchingManager.split('_');
    const topics = thread.topics.filter((topic): boolean => (
        topic.matchingManager &&
        topic.matchingManager.recipientType === recipientType &&
        topic.matchingManager.id === id &&
        topic.matchingManager.type === type
    ));

    if (!topics.length) {
        return null;
    }

    return topics[0].threadId;
};

type Props = FieldProps<string> & {
    thread: Thread;
};

const RecipientManagerField = ({ thread, field, form }: Props) => {
    const matchingManagers = computeMatchingManagers(thread);
    const matchingManagersKeys = Object.keys(matchingManagers);
    if (0 === matchingManagersKeys.length) return null;
    const [matchingManager, setMatchingManager] = useState<string>(matchingManagersKeys[0]);

    const types = computeTypes(thread, matchingManager);
    const typesKeys = Object.keys(types);
    if (0 === typesKeys.length) return null;
    const [recipientType, setRecipientType] = useState<number>(RecipientTypeEnum.ALL);

    useEffect(
        () => {
            form.setFieldValue(field.name, getThreadId(thread, matchingManager, recipientType));
        },
        [matchingManager, recipientType]
    );

    return (
        <>
            <select
                name="thread_matchingManager"
                value={matchingManager}
                onChange={(e) => setMatchingManager(e.target.value)}
                className="form-control col-md-7 select2"
            >
                {Object.entries(matchingManagers).map(([k, v]) => (
                    <option key={k} value={k}>
                        { v }
                    </option>
                ))}
            </select>

            <select
                name="thread_type"
                value={recipientType}
                onChange={(e) => setRecipientType(e.target.value)}
                className="form-control col-md-3 ml-md-3 mt-1 mt-md-0"
            >
                {Object.entries(types).map(([v]) => (
                    <option key={v} value={v}>
                        { Translator.trans(`inbox.channel.matching_manager.type.${v.toLowerCase()}`) }
                    </option>
                ))}
            </select>
        </>
    );
};

export default RecipientManagerField;
