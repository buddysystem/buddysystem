import React from 'react';
import Translator from '../../../tools/Translator';
import useTooltip, { Placement } from '../../hooks/useTooltip';

type Props = {
    image: string;
    title?: string;
    className?: string;
}

const Avatar = ({ image, title, className }: Props) => {
    const { ref } = useTooltip(Placement.RIGHT);

    return (
        <div className={className ? className : "buddy-photo-rounded"}>
            <img src={ image || '/images/pictos/no_photo.png' }
                alt={ Translator.trans('profile.picture') }
                title={ title }
                ref={ref}
            />
        </div>
    );
};

export default Avatar;
