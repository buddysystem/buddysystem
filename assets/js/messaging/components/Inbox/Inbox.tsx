import React, { useEffect } from 'react';
import Translator from '../../../tools/Translator';
import InboxItem from './InboxItem';
import { useThreadContext } from '../../context/ThreadContext';
import Icon from '../Common/Icon';
import { useUserContext } from '../../context/UserContext';
import { useInboxContext, Mode } from '../../context/InboxContext';
import { useMercureContext } from '../../context/MercureContext';

const Inbox = () => {
    const { list, refresh } = useThreadContext();
    const { setMode } = useInboxContext();
    const { iri, isManager, setRecipientClear } = useUserContext();
    const { mercure, subscribeToThread } = useMercureContext();

    const handleClick = () => {
        setMode(Mode.CREATE);
        setRecipientClear(true);
    };

    useEffect(
        () => subscribeToThread(null, refresh)(),
        [iri, mercure]
    );

    return (
        <div className="inbox__left-wrapper text-left">
            <div className="inbox__left-head">
                <div className="d-none d-lg-block">
                    <h4>{ Translator.trans('inbox.title') }</h4>
                </div>
                <div className="row mr-1">
                    <button type="button" id="new-thread" onClick={handleClick}>
                        <Icon name={ isManager ? 'edit' : 'life-ring'} />
                    </button>
                </div>
            </div>
            <ul id="threads-list" className="thread-selector">
                { list.map((thread) => <InboxItem key={thread['@id']} thread={thread} />) }
            </ul>
        </div>
    );
};

export default Inbox;
