import React from 'react';
import { noop } from 'lodash';
import Thread from "../../type/Thread";
import fetcher from "../../fetcher/fetchToggleFavorite";
import {useThreadContext} from "../../context/ThreadContext";

type Props = {
    thread: Thread;
};

const InboxItemFavorite = ({ thread }: Props) => {
    const { refresh } = useThreadContext();

    const toggleFavorite = async () => {
        await fetcher(thread['@id']).catch(noop);
        await refresh();
    }

    return (
        <div className="thread-selector__favorite" >
            <i className={"fa-star " + (thread.favorite ? ' fas' : 'far')} onClick={toggleFavorite} />
        </div>
    );
}

export default InboxItemFavorite;
