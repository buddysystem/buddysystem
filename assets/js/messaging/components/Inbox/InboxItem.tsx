import React, { useEffect } from 'react';
import moment from 'moment';
import Translator from '../../../tools/Translator';
import { useMercureContext } from '../../context/MercureContext';
import { useInboxContext } from '../../context/InboxContext';
import Avatar from '../Common/Avatar';
import Icon from '../Common/Icon';
import Thread from '../../type/Thread';
import { useUnreadContext } from '../../context/UnreadContext';
import InboxItemFavorite from './InboxItemFavorite';

const getIcon = (thread: Thread) => {
    if (thread.avatar) {
        return <Avatar image={thread.avatar} title={thread.name} />;
    }

    return <Icon name={thread.icon || 'question-circle'} />;
};

type Props = {
    key: string;
    thread: Thread;
};

const InboxItem = ({ thread }: Props) => {
    const { mercure, subscribeToThread } = useMercureContext();
    const { isCurrent, setCurrent } = useInboxContext();
    const { unread, setUnread } = useUnreadContext();

    useEffect(
        subscribeToThread(thread, () => {
            if (isCurrent(thread) || -1 !== unread.indexOf(thread['@id'])) {
                return;
            }
            setUnread([...unread, thread['@id']]);
        }),
        [mercure, thread]
    );

    const handleClick = () => {
        setCurrent(thread);
    };

    return (
        <li className="thread-selector__item" aria-hidden="true">
            <div title={thread.name} className={`thread-selector__item-info ${isCurrent(thread) ? 'active' : ''}`} >
                <div
                    className={`thread-selector__link ${-1 !== unread.indexOf(thread['@id']) ? 'unread' : ''}`}
                    onClick={handleClick}
                    data-cy={`inbox-item-${thread['@id']}`}
                >
                    <div className="thread-selector__icon d-none d-md-block">
                        { getIcon(thread) }
                    </div>

                    <div className="thread-selector__user text-truncate">
                        <span className="thread-selector__name">{ thread.name }</span>
                        <span className="thread__type">
                            { Translator.trans(`inbox.channel.type.${thread.channelType}`) }
                        </span>
                        <span className="thread-selector__date d-none d-md-block">
                            {
                                thread.lastMessageAt &&
                                moment(thread.lastMessageAt).locale(Translator.locale).format('LL')
                            }
                        </span>
                    </div>
                </div>

                <InboxItemFavorite thread={thread}/>
            </div>
        </li>
    );
};

export default InboxItem;
