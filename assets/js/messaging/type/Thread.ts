import Entity from './Entity';

export const enum RecipientTypeEnum {
    MENTORS = 'MENTORS',
    MENTEES = 'MENTEES',
    ALL = 'ALL'
}

export const enum MatchingManagerTypeEnum {
    INSTITUTE = 'institute',
    ASSOCIATION = 'association'
}

type TopicMatchingManager = {
    id: string;
    type: MatchingManagerTypeEnum;
    recipientType: RecipientTypeEnum;
    label: string;
};

type Topic = Entity & {
    threadId: string;
    matchingManager: TopicMatchingManager;
};

type Thread = Entity & {
    name: string;
    channelType: string;
    channelTypeTooltip: string;
    icon: string;
    avatar: string;
    lastMessageAt: number;
    topics: Topic[];
    canReply: boolean;
    read: boolean;
    favorite: boolean;
    profileUrl?: string;
};

export default Thread;
