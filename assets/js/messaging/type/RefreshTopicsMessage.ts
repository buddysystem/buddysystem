import { MessageTypeEnum } from './Message';

type RefreshTopicsMessage = {
    type: MessageTypeEnum.REFRESH_TOPICS;
}

export default RefreshTopicsMessage;
