$(() => {
    const table = $('#users');

    let columns = [
        { data: 'firstname' },
        { data: 'lastname' },
        { data: 'email' },
        { data: 'semester' },
        { data: 'year' }
    ];

    let searchCols = [
        null,
        null,
        null,
        null,
        null
    ];

    if (1 === table.data('admin')) {
        columns = $.merge(columns, [
            { data: 'association', orderable: false }
        ]);

        searchCols.push(null);
    }

    columns = $.merge(columns, [
        { data: 'roles' },
        { data: 'nationality' },
        { data: 'city' },
        { data: 'institute' },
        { data: 'studies', orderable: false },
        { data: 'levelOfStudy', orderable: false },
        { data: 'typeOfMobility' },
        { data: 'languages', orderable: false },
        { data: 'local' },
        { data: 'arrival' },
        { data: 'createdAt' },
        { data: 'nbBuddies' },
        { data: 'nbBuddiesWanted' },
        { data: 'hasMaxBuddies', orderable: false },
        { data: 'nbBuddiesRefused' },
        { data: 'nbRefusedByBuddies' },
        { data: 'sex' },
        { data: 'sexWanted' },
        { data: 'bonusOption' },
        { data: 'archived' },
        { data: 'actions', orderable: false }
    ]);

    searchCols = $.merge(searchCols, [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        { search: '0' },
        null
    ]);

    tools.initDatatable(table, {
        order: [[2, 'asc']],
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('user_list'),
        columns,
        searchCols
    }, 'BuddySystem - Users List');

    table.on('draw.dt', () => {
        tools.initPopupOnClick($('.user_delete'), 'user_delete', {}, Translator.trans('user.popup.delete'));
        tools.initPopupOnClick($('.user_report'), 'user_report', {}, Translator.trans('buddy.popup.report.list.title'));
        tools.initCollapseAccordion();
        loader.stop();
    });

    $('#popup').on('shown.bs.modal', () => {
        const $reportList = $('#list_report_user');

        if ($reportList.length) {
            tools.initPopupOnClick($('.report_delete'), 'report_delete', {}, Translator.trans('report.popup.delete'));
            tools.initDatatable($reportList, { paging: false, bFilter: false });
        }
    });
});
