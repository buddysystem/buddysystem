import RangeSlider from './tools/RangeSlider';
import DynamicForm from './tools/DynamicForm';

$(() => {
    const $nameField = $('#matching_manager_name');
    const instituteParentsField = '#matching_manager_parents';
    const fullYearOnly = 'input[id$="fullYearOnly"]';
    let $instituteParentsCollection = $(instituteParentsField);
    const $cityCollection = $('#matching_manager_cities');
    const $countrySelector = $('#matching_manager_country');
    const $collections = $('.collection-widget');
    const $lowerLevelManagement = $('#institute_preference_lowerLevelManagement');

    DynamicForm.init({
        fullYearOnly: fullYearOnly
    });

    tools.initCollapseAccordion();
    tools.initFileInput();
    RangeSlider.initSliders();

    function _initParentsCollection() {
        $instituteParentsCollection = $(instituteParentsField);
        tools.initCollectionType($instituteParentsCollection, { min: 0, init_with_n_elements: 0 });
    }

    function _updateInstituteParents() {
        loader.start();
        const $form = $instituteParentsCollection.closest('form');
        const data = {};

        data[$nameField.attr('name')] = $nameField.val();
        $cityCollection.find('select[id^=\'matching_manager_cities_\']').each((k, e) => {
            data[$(e).attr('name')] = $(e).val();
        });

        $instituteParentsCollection.find('select[id^=\'matching_manager_parents_\']').each((k, e) => {
            data[$(e).attr('name')] = $(e).val();
        });

        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            data,
            success(html) {
                $instituteParentsCollection.closest('.form-group').replaceWith(
                    $(html).find(instituteParentsField).closest('.form-group')
                );

                _initParentsCollection();

                loader.stop();
            }
        });
    }

    function _applyCountryFilter() {
        if ($countrySelector.length) {
            const country = $countrySelector.find('option:selected').text();
            const $citySelectors = $cityCollection.find('select[id^=\'matching_manager_cities_\']');
            const firstOption = $citySelectors.children(`optgroup[label='${country}']`).find('option:first').val();

            $citySelectors.children(`optgroup[label!='${country}']`).hide();
            $citySelectors.children(`optgroup[label='${country}']`).show();

            $citySelectors.each(function () {
                if ($(this).find('option:selected').parent().attr('label') !== country) {
                    $(this).val(firstOption);
                }
            });
        }

        if ($instituteParentsCollection.length) {
            _updateInstituteParents();
        }
    }

    if ($instituteParentsCollection.length) {
        _initParentsCollection();

        tools.initCollectionType($cityCollection, {
            after_add(collection, element) {
                _applyCountryFilter();

                $(element).on('change', () => {
                    _updateInstituteParents();
                });
            },
            after_remove() { _updateInstituteParents(); }
        });
    } else if ($cityCollection.length) {
        tools.initCollectionType($cityCollection);
    }

    if ($lowerLevelManagement.length) {
        const $sliders = $('[class^="range-slider"]');
        const $selects = $('select');
        const $switch = $('.matching_preference_switch');
        const $lowerLevelManagementManager = $('#lowerLevelManagement_manager');
        const $lowerLevelManagementUpperManaged = $('#lowerLevelManagement_upperManaged');

        if ($lowerLevelManagement.is(':checked')) {
            $sliders.slider('disable');
            $selects.attr('readonly', true);
            $switch.attr('readonly', true);
            $switch.on('click', () => {return false;});
        }

        $lowerLevelManagement.on('change', () => {
            $sliders.slider('toggle');
            $selects.attr('readonly', $selects.attr('readonly') !== 'readonly');

            if ($switch.attr('readonly') === 'readonly') {
                $switch.attr('readonly', false);
                $switch.off('click');
            } else {
                $switch.attr('readonly', true);
                $switch.on('click', () => {return false;});
            }

            if ($lowerLevelManagementManager.length) {
                if ($lowerLevelManagementManager.hasClass( 'd-none' )) {
                    $lowerLevelManagementManager.removeClass('d-none');
                } else {
                    $lowerLevelManagementManager.addClass('d-none');
                }
            }

            if ($lowerLevelManagementUpperManaged.length) {
                if ($lowerLevelManagementUpperManaged.hasClass('d-none')) {
                    $lowerLevelManagementUpperManaged.removeClass('d-none');
                } else {
                    $lowerLevelManagementUpperManaged.addClass('d-none');
                }
            }
        });
    }

    if ($collections.length) {
        tools.initCollectionType($collections);
    }

    if ($countrySelector.length) {
        $countrySelector.on('change', () => {
            _applyCountryFilter();
        });

        if (!$instituteParentsCollection.length) {
            _applyCountryFilter();
        }
    }
});
