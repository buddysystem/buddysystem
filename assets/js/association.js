$(() => {
    const table = $('#associations');

    const columns = [
        { data: 'name' },
        { data: 'city', orderable: false },
        { data: 'website' },
        { data: 'logo', orderable: false },
        { data: 'facebook' },
        { data: 'twitter' },
        { data: 'instagram' },
        { data: 'nbUsers' },
        { data: 'activated' },
        { data: 'actions', orderable: false }
    ];

    tools.initDatatable(table, {
        order: [[0, 'asc']],
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('association_list'),
        columns
    }, 'BuddySystem - Associations List');

    table.on('draw.dt', () => {
        loader.stop();
    });
});
