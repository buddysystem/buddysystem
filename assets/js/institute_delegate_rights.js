$(() => {
    const $table = $('#institutes');

    function initOnDraw() {
        $table.on('draw.dt', () => {
            const $rightSelectors = $('.rights_selector');
            $rightSelectors.off('change');
            $rightSelectors.on('change',function () {
                loader.start();
                const $that = $(this);

                $.ajax({
                    type: 'POST',
                    url: Routing.generate(
                        'institute_change_rights',
                        { id: $that.closest('tr').data('id'), idAssociation: $that.data('id') }
                    ),
                    data: $that.serialize(),
                    complete() {
                        window.location.reload();
                    }
                });
            });

            tools.initTooltip();
            loader.stop();
        });
    }

    function initDatatable() {
        let columns = [
            { data: 'name' },
            { data: 'parents', orderable: false }
        ];

        columns = $.merge(columns, [
            { data: 'association', orderable: false },
            { data: 'rights', orderable: false }
        ]);

        tools.initDatatable($table, {
            processing: loader.start(),
            serverSide: true,
            ajax: Routing.generate('institute_list', { delegate_rights: true }),
            columns
        }, 'BuddySystem - Institutes Rights List');
    }

    initDatatable();
    initOnDraw();
});
