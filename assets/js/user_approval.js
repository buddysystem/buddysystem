$(() => {
    const table = $('#users-to-approve');

    let columns = [
        { data: 'firstname' },
        { data: 'lastname' },
        { data: 'email' },
        { data: 'languages', orderable: false },
        { data: 'city' },
        { data: 'institute' },
        { data: 'semester' },
        { data: 'year' },
        { data: 'createdAt' },
        { data: 'roles' }
    ];

    if (1 === table.data('admin')) {
        columns = $.merge(columns, [
            { data: 'association', orderable: false }
        ]);
    }

    columns = $.merge(columns, [
        { data: 'actions', orderable: false }
    ]);

    tools.initDatatable(table, {
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('user_list', { user_to_approve: true }),
        columns
    }, 'BuddySystem - Users To Approve List');

    table.on('draw.dt', () => {
        tools.initPopupOnClick($('.user_delete'), 'user_delete', {}, Translator.trans('user.popup.delete'));
        tools.initPopupOnClick($('.user_approve'), 'approve_user', {}, Translator.trans('user.popup.approve'));
        tools.initCollapseAccordion();
        loader.stop();
    });
});
