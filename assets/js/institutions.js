import '../sass/pages/institutions.scss';
import '@accessible360/accessible-slick/slick/slick.min';
import '@accessible360/accessible-slick/slick/slick.min.css';
import '@accessible360/accessible-slick/slick/slick-theme.min.css';


$(() => {
   $('.slider').slick({
       slidesToShow: 3,
       autoplay: true,
       autoplaySpeed: 0,
       speed: 3000,
       cssEase:'linear',
       infinite: true,
       arrows: false,
       responsive: [{
           breakpoint: 1024,
           settings: {
               slidesToShow: 2,
           }
       }, {
           breakpoint: 480,
           settings: {
               slidesToShow: 1
           }
       }]
   })
});