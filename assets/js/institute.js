$(() => {
    const table = $('#institutes');
    const isAdmin = 1 === table.data('admin');

    let columns = [
        { data: 'name' },
        { data: 'city', orderable: false },
        { data: 'website' },
        { data: 'logo', orderable: false }
    ];

    if (isAdmin) {
        columns = $.merge(columns, [
            { data: 'association', orderable: false }
        ]);
    }

    columns = $.merge(columns, [
        { data: 'studies', orderable: false },
        { data: 'nbUsers', orderable: false },
        { data: 'level' },
        { data: 'treeview', orderable: false },
        { data: 'matchingPreferences', orderable: false }
    ]);

    if (!isAdmin) {
        columns.push({ data: 'rights' });
    }

    columns = $.merge(columns, [
        { data: 'activated' },
        { data: 'actions', orderable: false }
    ]);

    tools.initDatatable(table, {
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('institute_list'),
        columns
    }, 'BuddySystem - Institutes List');

    table.on('draw.dt', () => {
        tools.initPopupOnClick(
            $('.institute_delete'),
            'institute_delete',
            {},
            Translator.trans('institute.popup.delete')
        );

        tools.initCollapseAccordion();

        loader.stop();
    });
});
