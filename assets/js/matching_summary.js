$(() => {
    tools.initPopupOnClick(
        $('.list-buddies_link'),
        'user_buddies',
        {},
        Translator.trans('buddy.list.title')
    );
    tools.initPopupOnClick(
        $('.list-buddies-refused_link'),
        'user_buddies_refused',
        {},
        Translator.trans('buddy.refused.list.title')
    );
    tools.initPopupOnClick(
        $('.list-reports_link'),
        'user_report',
        {},
        Translator.trans('buddy.popup.report.list.title')
    );

    const $matchingButton = $('#matching-proposal_button');

    tools.initPopupOnClick(
        $matchingButton,
        'propose_matching',
        {'internationalId': $matchingButton.data('international'), 'localId': $matchingButton.data('local')},
        Translator.trans('matching.match.approval.title')
    );

    $('#popup').on('shown.bs.modal', () => {
        const buddiesList = '#list_buddies_user';
        const $buddiesList = $(buddiesList);
        const $reportList = $('#list_report_user');
        const $approvalMatchingForm = $('form[name=\'approve_matching\']');

        if ($buddiesList.length) {
            tools.initDatatable($buddiesList, { paging: false, bFilter: false });

            $(`${buddiesList} tbody tr`).on('click', function () {
                const userId = $(this).data('id');

                if (userId !== undefined) {
                    window.open(Routing.generate('user_edit', { id: userId }));
                }
            });
        }

        if ($reportList.length) {
            tools.initPopupOnClick($('.report_delete'), 'report_delete', {}, Translator.trans('report.popup.delete'));
            tools.initDatatable($reportList, { paging: false, bFilter: false });
        }

        if ($approvalMatchingForm.length) {
            tools.initSubmitForm($approvalMatchingForm, 'propose_matching', {'internationalId': $approvalMatchingForm.data('international'), 'localId': $approvalMatchingForm.data('local')}, () => {
                location.href = Routing.generate('matching_step1');
            });
        }
    });
});
