import Chart from './tools/Chart';
import '../sass/pages/dashboard_insight.scss';

$(() => {
    const $managerSelector = $('#manager-selector');
    const $statsContent = $('#stats-content');
    const institutesSelector = '#chart-institutes__selector';
    const institutesChart = '.ct-chart-institutes';
    const userChart = '.ct-chart-users';
    const registrationChart = '.ct-chart-registration';
    const matchChart = '.ct-chart-matches';
    const motivationChart = '.ct-chart-motivations';
    const nationalitiesSelector = '#chart-nationalities__selector';
    const nationalitiesChart = '.ct-chart-nationalities';
    let statsType = 'semester';
    let matchingManagerId = $statsContent.data('id');
    let matchingManagerType = $statsContent.data('type');

    function _initInstitutesChart(institutes) {
        let dataInstitutes = institutes;

        if ($(institutesChart).length) {
            if ($(institutesSelector).length) {
                const $legends = $('.chart-institutes .chart-legend li');
                const loopIndex = parseInt($(institutesSelector)
                    .val());

                $legends.filter(`[data-institute-loop="${loopIndex}"]`)
                    .removeClass('d-none');
                $legends.filter(`[data-institute-loop!="${loopIndex}"]`)
                    .addClass('d-none');
                dataInstitutes = dataInstitutes.slice(loopIndex * 10, loopIndex * 10 + 9);
            }

            Chart.donutChart(institutesChart, { series: dataInstitutes });
        }
    }

    function _initNationalitiesChart(nationalities) {
        let dataNationalities = nationalities;

        if ($(nationalitiesChart).length) {
            if ($(nationalitiesSelector).length) {
                const $legends = $('.chart-nationalities .chart-legend li');
                const loopIndex = parseInt($(nationalitiesSelector).val());

                $legends.filter(`[data-nationalities-loop="${loopIndex}"]`)
                    .removeClass('d-none');
                $legends.filter(`[data-nationalities-loop!="${loopIndex}"]`)
                    .addClass('d-none');
                dataNationalities = dataNationalities.slice(loopIndex * 10, loopIndex * 10 + 9);
            }

            Chart.donutChart(nationalitiesChart, { series: dataNationalities });
        }
    }

    function _initCharts() {
        const $insight = $('.insight');
        const registration = $insight.data('registration');
        const institutes = $insight.data('institutes');
        const nationalities = $insight.data('nationalities');
        const $statsTypeSelector = $('#stats-type');

        if ($(userChart).length) {
            const dataUsers = [];
            $('.chart-users .chart-legend li').each(function () {
                dataUsers.push([$(this).data('mentees'), $(this).data('mentors')]);
            });
            Chart.stackedBarChart(userChart, {
                labels: [
                    Translator.trans('dashboard.insights.users.chart.mentees'),
                    Translator.trans('dashboard.insights.users.chart.mentors')
                ],
                series: dataUsers
            });
        }

        if ($(registrationChart).length) {
            Chart.simpleLineChart(registrationChart, registration);
        }

        if ($(matchChart).length) {
            const dataMatches = [];
            $('.chart-matches .chart-legend li').each(function () {
                dataMatches.push($(this).data('value'));
            });
            Chart.pieChart(matchChart, { series: dataMatches });
        }

        if ($(motivationChart).length) {
            const dataMotivations = [];
            $('.chart-motivations .chart-legend li').each(function () {
                dataMotivations.push($(this).data('value'));
            });
            Chart.donutChart(motivationChart, { series: dataMotivations });
        }

        if ($(nationalitiesSelector).length) {
            $(document).on('change', nationalitiesSelector, () => {
                _initNationalitiesChart();
            });
        }
        _initNationalitiesChart(nationalities);

        if ($(institutesSelector).length) {
            $(document).on('change', institutesSelector, () => {
                _initInstitutesChart();
            });
        }
        _initInstitutesChart(institutes);

        $statsTypeSelector.on('change', function () {
            statsType = $(this).find('input[name="type"]:checked').val();
            _loadData();
        });
    }

    function _loadData() {
        loader.start();

        $.ajax({
            url: Routing.generate(
                'dashboard_stats',
                {
                    matchingManagerId,
                    matchingManagerType,
                    statsType
                }
            ),
            success(data) {
                $statsContent.html(data);
                _initCharts();
                tools.initTooltip();
            },
            error() {
                Flashbag.show('Error. Please try again.', 'error');
            },
            complete() {
                loader.stop();
            }
        });
    }

    if ($managerSelector.length) {
        $managerSelector.on('change', function () {
            const selectedOption = $(this).find('option:selected');

            matchingManagerId = selectedOption.val();
            matchingManagerType = selectedOption.data('type');
            _loadData();
        });
    }

    _loadData();
});
