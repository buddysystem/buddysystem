$(() => {
    tools.initDatatable($('#studies'), { paging: false, dom: 'lftipr' });
    tools.initAjaxCheckboxes(
        $('.toggle_checkbox'),
        'institute_study_toggle_link',
        {
            id: 'table',
            idStudy: 'tr'
        }
    );
    tools.initPopupOnClick($('.study_delete'), 'study_delete', {}, Translator.trans('study.popup.delete', {}, 'admin'));
});
