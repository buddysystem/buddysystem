$(() => {
    const table = $('#institutes-requested__table');

    const columns = [
        { data: 'name' },
        { data: 'country' },
        { data: 'city' },
        { data: 'createdAt' }
    ];

    tools.initDatatable(table, {
        processing: loader.start(),
        serverSide: true,
        ajax: Routing.generate('institutes_requested_list'),
        columns
    }, 'BuddySystem - Institutes Requested List');

    table.on('draw.dt', () => {
        loader.stop();
    });
});
