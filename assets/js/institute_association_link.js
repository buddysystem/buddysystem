$(() => {
    const $table = $('#institutes');
    const $managerSelector = $('#manager-selector');
    let datatable = null;

    function initDatatable(manager) {
        const columns = [
            { data: 'name' },
            { data: 'parents', orderable: false },
            { data: 'nbUsers', orderable: false },
            { data: 'isAvailable', orderable: false }
        ];

        const url = (null == manager ?
            Routing.generate('institute_list_associations_link') :
            Routing.generate('institute_list_associations_link', { manager })
        );
        datatable = tools.initDatatable($table, {
            order: [[0, 'asc']],
            processing: loader.start(),
            serverSide: true,
            ajax: url,
            columns
        }, 'BuddySystem - Institutes List');
    }

    function initOnDraw(manager) {
        $table.on('draw.dt', () => {
            const params = (null == manager ?
                {} :
                { manager: 'table' }
            );
            tools.initAjaxCheckboxes(
                $('.toggle_checkbox'),
                'institute_association_toggle_link',
                params,
                (data) => {
                    const paramsAjax = (null == manager ?
                        {} :
                        { manager }
                    );
                    datatable.ajax.url(Routing.generate('institute_list_associations_link', paramsAjax)).load();
                    initOnDraw(manager);
                    Flashbag.show(data.message, data.labelMessage);
                }
            );
            loader.stop();
        });
    }

    if (!$managerSelector.length) {
        initDatatable();
        initOnDraw();
    } else {
        $managerSelector.on('change', () => {
            const manager = $managerSelector.val();
            $table.data('id', manager);

            if (datatable) {
                datatable.ajax.url(Routing.generate('institute_list_associations_link', { manager })).load();
            } else {
                initDatatable(manager);
            }

            initOnDraw(manager);
        });
    }
});
