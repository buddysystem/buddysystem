<?php

namespace Deployer;

use Symfony\Component\Dotenv\Dotenv;

require 'vendor/autoload.php';
require 'recipe/symfony4.php';
require 'recipe/slack.php';
require 'recipe/yarn.php';
require 'recipe/cachetool.php';

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env.local');

set('application', 'buddysystem');
set('repository', 'https://gitlab.com/buddysystem/buddysystem.git');

set('slack_webhook', getenv('SLACK_WEBHOOK'));
set('slack_text', 'deploying `{{branch}}` to *{{target}}*');

set('git_tty', false);
set('bin/php', '/usr/bin/php7.4');

set('http_user', 'www-data');
set('keep_releases', 3);

set('cachetool', '/var/run/php/php-fpm.sock');

add('shared_dirs', ['public/maintenance', 'public/uploads', 'public/media/cache', 'public/extract']);

set('clear_paths', [
    '/doc',
    '/dump',
    '/features',
    '/tests',
    '.circleci',
    '.docker',
    '.editorconfig',
    '.env.test',
    '.eslintrc',
    '.git',
    '.gitignore',
    '.gitlab-ci.yml',
    '.php-cs-fixer.dist.php',
    'acme.json',
    'behat.yml.dist',
    'config.toml',
    'cypress.json',
    'deploy.php',
    'deploy_config.dist',
    'docker-compose.yml',
    'docker-compose.arm.yml.dist',
    'docker-compose.override.yml.dist',
    'docker-compose.ci.yml',
    'Makefile',
    'Makefile.cypress',
    'phpunit.xml.dist',
    'psalm.xml',
    'README.md',
]);

set('bin/composer', function () {
    run('cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}');
    $composer = '{{release_path}}/composer.phar';

    return '{{bin/php}} '.$composer;
});

host('prod', 'staging', 'demo')
    ->configFile('deploy_config')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->multiplexing(false)
    ->forwardAgent(true)
;

host('prod')
    ->stage('prod')
    ->set('host', 'prod')
    ->set('symfony_env', 'prod')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/buddysystem')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader')
;

host('staging')
    ->stage('staging')
    ->set('host', 'staging')
    ->set('symfony_env', 'dev')
    ->set('branch', 'staging')
    ->set('deploy_path', '/var/www/staging')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader')
;

host('demo')
    ->stage('demo')
    ->set('host', 'demo')
    ->set('symfony_env', 'prod')
    ->set('branch', 'master')
    ->set('deploy_path', '/var/www/demo')
    ->set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader')
;

desc('Dump .env files for production');
task('deploy:dump-env', function () {
    run('cd {{release_path}} && {{bin/composer}} dump-env {{symfony_env}}');
});

desc('Build CSS/JS files');
task('deploy:build_local_assets', function () {
    run('cd {{release_path}} && {{bin/php}} bin/console fos:js-routing:dump --format=json --target={{release_path}}/public/js/fos_js_routes.json');
    run('cd {{release_path}} && {{bin/yarn}} run {{symfony_env}}');
});

desc('Import new translations to DB');
task('translations:import', function () {
    run('{{bin/php}} {{bin/console}} translation:download');
    run('{{bin/php}} {{bin/console}} bazinga:js-translation:dump public/js/');
});

desc('Sync metadata storage for doctrine migration');
task('database:sync-meta', function () {
    run(sprintf('{{bin/php}} {{bin/console}} doctrine:migrations:sync-metadata-storage {{console_options}}'));
});

desc('Build sphinx documentation');
task('sphinx:build', function () {
    run('cd {{release_path}}/docs && make html');
});

desc('Generate stats on demo');
task('data:generate-stats', function () {
    run('{{bin/php}} {{bin/console}} cron:stats:update');
})->onHosts('demo');

desc('Restart supervisor');
task('supervisor:restart', function () {
    run('sudo /usr/bin/supervisorctl restart messenger-consume-{{host}}:*');
})->onHosts('prod', 'staging');

desc('Kill symfony messenger');
task('sf-messenger:kill', function () {
    run('{{bin/php}} {{bin/console}} messenger:stop-workers');
});

before('deploy', 'slack:notify');
after('success', 'slack:notify:success');
after('deploy:failed', 'slack:notify:failure');
after('deploy:update_code', 'deploy:clear_paths');
after('deploy:shared', 'yarn:install');
after('deploy:vendors', 'deploy:dump-env');
after('deploy:vendors', 'deploy:build_local_assets');
before('deploy:symlink', 'sphinx:build');
before('database:migrate', 'database:sync-meta');
before('deploy:symlink', 'database:migrate');
after('database:migrate', 'translations:import');
after('database:migrate', 'data:generate-stats');
after('database:migrate', 'supervisor:restart');
after('supervisor:restart', 'sf-messenger:kill');
after('deploy:symlink', 'cachetool:clear:opcache');
after('deploy:failed', 'deploy:unlock');
